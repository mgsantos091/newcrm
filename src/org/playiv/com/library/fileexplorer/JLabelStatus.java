package org.playiv.com.library.fileexplorer;

import javax.swing.JLabel;

public class JLabelStatus extends JLabel {
   
   String fullMessage;

   public void setTextO(String text){
      fullMessage = text;
      //System.out.println("Overridden setText");
      super.setText(text);
   }
   
   public String getTextO(){
      //System.out.println("Overridden getText");     
      return fullMessage;
   }
   
   public void refreshText(int x){
	   
	   //This is not required if just resizing the label when resize the window.
	   // ... wil be put in automagically
	   
      //System.out.println("Starting Refresh   " + x);
      super.setText(this.fullMessage);
      //if (this.getWidth() > (x-15)) {
      if ((fullMessage.length()*6.6) > (x-20)) {
      //reduce width ot message 
         //int charsAllowed = (panelWidth - 10)/6 ;
         for (int y=fullMessage.length(); y>0; y--){
            String temp = this.fullMessage.substring(0,y) + "..." ;        
            //System.out.println("trying   " + temp + this.getWidth());
            //if (this.getWidth() < (x-15)) {
            if ((temp.length()*6.6) < (x-15) ) {

               this.setText(temp); 
               y=0;
            }
         }
      }// if 
   }//public void refresh

}
