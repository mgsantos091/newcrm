package org.playiv.com.library.fileexplorer.pref;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.playiv.com.library.fileexplorer.filesort.DirectoryView;

public class View extends JPanel {

	public final static String SHOW_TOOLBAR = new String("showToolbar");
	public final static String SHOW_HIDDEN  = new String("showHidden");
	public final static String SORT_MODE    = new String("sortMode");
	public final static boolean SHOW_TOOLBAR_DEFAULT = true;
	public final static boolean SHOW_HIDDEN_DEFAULT = false;
	public final static int SORT_MODE_DEFAULT = 0;
   private Preferences prefsView;
   
   protected ResourceBundle resbundle;
   
   //View Menu Items
   protected JCheckBox showToolbarMenuItem;
    
   //Menu Items
   protected JCheckBox showHiddenMenuItem;
   protected JRadioButton[] m_sort;
   
   ActionListener setShowToolbarAction = new ActionListener () {
		public void actionPerformed(ActionEvent e) {
         prefsView.putBoolean(SHOW_TOOLBAR, showToolbarMenuItem.isSelected());
		}
	};
   
   ActionListener setShowHiddenAction = new ActionListener () {
		public void actionPerformed(ActionEvent e) {
         prefsView.putBoolean(SHOW_HIDDEN, showHiddenMenuItem.isSelected());
		}
	};
   
	ActionListener setSortAction = new ActionListener () {
		public void actionPerformed(ActionEvent e) {
			for (int i=0; i<DirectoryView.SORT_OPTIONS; i++) {
				if (m_sort[i].isSelected() == true) {
					prefsView.putInt(SORT_MODE, i);
				}
			}
		}
	};
	
   
	public View(){
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
      // The ResourceBundle below contains all of the strings used in this
      // application.  ResourceBundles are useful for localizing applications.
      // New localities can be added by adding additional properties files.
      resbundle = ResourceBundle.getBundle ("FileExplorer", new Locale("pt","BR"));
      
      prefsView = Preferences.userRoot().node("net/amaras/fileexplorer/view"); 
      
		JPanel defLoc = new JPanel();
		defLoc.setLayout(new BoxLayout(defLoc, BoxLayout.Y_AXIS));
		

      showToolbarMenuItem = new JCheckBox(resbundle.getString("showToolbar"));
      showToolbarMenuItem.setSelected(prefsView.getBoolean(SHOW_TOOLBAR, SHOW_TOOLBAR_DEFAULT));
      showToolbarMenuItem.addActionListener(setShowToolbarAction);
		defLoc.add(showToolbarMenuItem);
      
      showHiddenMenuItem = new JCheckBox(resbundle.getString("showHidden"));
      showHiddenMenuItem.setSelected(prefsView.getBoolean(SHOW_HIDDEN, SHOW_HIDDEN_DEFAULT));
      showHiddenMenuItem.addActionListener(setShowHiddenAction);
		defLoc.add(showHiddenMenuItem);

      //TODO add radio list to select sort option
		int textHeight     = 23;
	      
		
		ButtonGroup sortGroup = new ButtonGroup();
	      m_sort = new JRadioButton[DirectoryView.SORT_OPTIONS];
	      int mode = prefsView.getInt(SORT_MODE, SORT_MODE_DEFAULT);
	      
	      for (int i=0; i<m_sort.length; i++) {
	         m_sort[i] = new JRadioButton(DirectoryView.text[i]);
	         m_sort[i].setSelected(i == mode);	         
	         m_sort[i].addActionListener(setSortAction);
	         sortGroup.add(m_sort[i]);
	         defLoc.add(m_sort[i]);
	      }
		
		
		
		
		
		
		this.add(defLoc);
      
	}
	
   //Prefs Tab Title/Name
	public String getName() {
		return new String("Visualizar");
	}
   
   
}