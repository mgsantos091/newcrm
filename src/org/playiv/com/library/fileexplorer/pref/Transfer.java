package org.playiv.com.library.fileexplorer.pref;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class Transfer extends JPanel {

	//FireFox
	//Show the downloads window when downloading
	//close window when downloads complete
	public final static String OPEN_TRANSFER   = new String("openTransfer");
	public final static String CLOSE_TRANSFER  = new String("closeTransfer");
	public final static boolean OPEN_TRANSFER_DEFAULT = true;
	public final static boolean CLOSE_TRANSFER_DEFAULT = false;
	
   private Preferences prefsTransfer;
   protected ResourceBundle resbundle;
   
   //Menu Items
   protected JCheckBox openTransferMenuItem; 
   protected JCheckBox closeTransferMenuItem;
   
   ActionListener setOpenTransferAction = new ActionListener () {
		public void actionPerformed(ActionEvent e) {
			prefsTransfer.putBoolean(OPEN_TRANSFER, openTransferMenuItem.isSelected());
		}
	};
   
   ActionListener setCloseTransferAction = new ActionListener () {
		public void actionPerformed(ActionEvent e) {
			prefsTransfer.putBoolean(CLOSE_TRANSFER, closeTransferMenuItem.isSelected());
		}
	};
	
   
	public Transfer(){
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
      // The ResourceBundle below contains all of the strings used in this
      // application.  ResourceBundles are useful for localizing applications.
      // New localities can be added by adding additional properties files.
	  resbundle = ResourceBundle.getBundle ("FileExplorer", new Locale("pt","BR"));
      
      prefsTransfer = Preferences.userRoot().node("net/amaras/fileexplorer/transfer"); 
      
		JPanel defLoc = new JPanel();
		defLoc.setLayout(new BoxLayout(defLoc, BoxLayout.Y_AXIS));
		

		openTransferMenuItem = new JCheckBox(resbundle.getString("openTransfer"));
		openTransferMenuItem.setSelected(prefsTransfer.getBoolean(OPEN_TRANSFER, OPEN_TRANSFER_DEFAULT));
		openTransferMenuItem.addActionListener(setOpenTransferAction);
		defLoc.add(openTransferMenuItem);
      
		closeTransferMenuItem = new JCheckBox(resbundle.getString("closeTransfer"));
		closeTransferMenuItem.setSelected(prefsTransfer.getBoolean(CLOSE_TRANSFER, CLOSE_TRANSFER_DEFAULT));
		closeTransferMenuItem.addActionListener(setCloseTransferAction);
		defLoc.add(closeTransferMenuItem);

      //TODO add radio list to select sort option
		int textHeight     = 23;
		
		
		this.add(defLoc);
      
	}
	
   //Prefs Tab Title/Name
	public String getName() {
		return new String("Transferir");
	}
   
   
}