package org.playiv.com.library.fileexplorer.pref;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Application extends JPanel {

   public final static String APP_LOCATION = new String("defaultLocation");
   public final static String APP_LOCATION_DEFAULT = new String(); 
   
   private JTextField labelLoc;
   private Preferences prefsApp;


   ActionListener browseAction = new ActionListener () {
      public void actionPerformed(ActionEvent e) {

         String location = prefsApp.get(APP_LOCATION, APP_LOCATION_DEFAULT);
         final JFileChooser fc = new JFileChooser(location);
         fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

         int returnVal = fc.showOpenDialog(Application.this);

         if (returnVal == JFileChooser.APPROVE_OPTION) {
            //Handle open button action.
            try {
               File file = fc.getSelectedFile();
               System.out.println("Setting Default Home to : " + file.getCanonicalPath() );
               prefsApp.put(APP_LOCATION, file.getCanonicalPath() );
               labelLoc.setText(file.getCanonicalPath());
            } catch (java.io.IOException except) {
               System.err.println("Error " + except);
            }
         } else {
            System.out.println("Open command cancelled by user." );
         }
      }
   };
   
   
   ActionListener setHomeAction = new ActionListener () {
      public void actionPerformed(ActionEvent e) {
         System.out.println("Pressed SetHome");
         String newAppLoc = getPresetDefault();
         labelLoc.setText(newAppLoc);
         prefsApp.put(APP_LOCATION, newAppLoc );
      }
   };
   
   
   ActionListener setNewLocAction = new ActionListener () {
      public void actionPerformed(ActionEvent e) {
         System.out.println("Setting new Default Loc");
         File newLocFile = new File(labelLoc.getText());
         if (newLocFile.exists() && newLocFile.isDirectory() ) {
            prefsApp.put(APP_LOCATION, labelLoc.getText() );
         }
      }
   };
   
   private String getPresetDefault() {
	   String appdefault = new String();
	      if (System.getProperty("os.name").contains("Mac")) {
	    	  appdefault = "/Applications";
	      } else if (System.getProperty("os.name").contains("Win")) {
	    	  appdefault = "C:\\Program\\ Files";
	      } else {
	    	  appdefault = System.getProperty("user.home");
	      }
	      return appdefault;
   }
   
   public Application(){
      super();
    //TODO find correct way for getting button height. this way will not scale with fonts
      int textHeight     = 27;
      
      this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
      
      JPanel defLoc = new JPanel();
      defLoc.setLayout(new BoxLayout(defLoc, BoxLayout.X_AXIS));
      labelLoc = new JTextField();
      
      labelLoc.addActionListener(setNewLocAction);
      
      prefsApp = Preferences.userRoot().node("net/amaras/fileexplorer/application"); 
      
      JLabel message = new JLabel("The default path to for \"Open With\" \n");
      
      //BoxLayout sets all vertical components to have the same centre.
      //There fore message has to be same width as container to position correctly
      message.setMinimumSize  (new Dimension(600, textHeight));
      message.setPreferredSize(new Dimension(600, textHeight));
      message.setMaximumSize  (new Dimension(600, textHeight));
      this.add(message);
      
      labelLoc.setText( prefsApp.get(APP_LOCATION, getPresetDefault()));// 
      
      labelLoc.setMinimumSize  (new Dimension(150, textHeight));
      labelLoc.setPreferredSize(new Dimension(150, textHeight));
      labelLoc.setMaximumSize  (new Dimension(300, textHeight));
      
      defLoc.add(labelLoc);
      
      JButton browseButton = new JButton(new String("Browse"));
      browseButton.addActionListener(browseAction);
      defLoc.add(browseButton);
      
      JButton setHomeButton = new JButton(new String("Default"));
      setHomeButton.addActionListener(setHomeAction);
      defLoc.add(setHomeButton);
      
      this.add(defLoc);
      
   }
   
   //Prefs Tab Title/Name
   public String getName() {
      return new String("Applications");
   }
   
   
}