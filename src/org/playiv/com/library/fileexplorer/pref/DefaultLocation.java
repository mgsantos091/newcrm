package org.playiv.com.library.fileexplorer.pref;

public class DefaultLocation {
	
	private static DefaultLocation defaultLocation;
	
	private String defaultStartLocation = new String(System.getProperty("user.home"));
	
	private DefaultLocation( ) {
		
	}
	
	public static DefaultLocation getInstance( ) {
		if(defaultLocation==null) defaultLocation = new DefaultLocation();
		return defaultLocation;
	}
	
	// Trecho de c�digo do site Mkyong.com
	// How to detect OS in Java � System.getProperty(�os.name�)
	// http://www.mkyong.com/java/how-to-detect-os-in-java-systemgetpropertyosname/

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
	}

	public static boolean isSolaris() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("sunos") >= 0);
	}

	public String getDefaultStartLocation() {
		return defaultStartLocation;
	}

	public void setDefaultStartLocation(String defaultStartLocation) {
		this.defaultStartLocation = defaultStartLocation;
	}
	
}