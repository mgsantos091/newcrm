package org.playiv.com.library.fileexplorer;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class TransferListItem extends JPanel {

   private String msg;
   public JLabel msgLabel;
   
   public TransferListItem(){
      super();
      helperConstructor();
   }
   
   public TransferListItem(String msg){
      super();
      this.msg = msg;
      helperConstructor();
   }
   
   public void setMessage(String msg) {
      this.msg = msg;
      msgLabel.setText(msg);
   }
   
   private void helperConstructor(){
      if (this.msg == null) {
         msgLabel = new JLabel("Testing 123");
      } else {
         msgLabel = new JLabel(msg);
      }
      
      this.add(msgLabel);
   }
   
}