package org.playiv.com.library.fileexplorer;


//import javax.swing.ImageIcon;
import javax.swing.JToolBar;

import java.awt.event.*;

import javax.swing.JButton;

public class FileExplorerJToolBar extends JToolBar {

	private static final long serialVersionUID = -6832879980419587018L;

	FileExplorer fileexplorerParent;
	
	FileExplorerJToolBar(FileExplorer fileexplorerParent){
		super();
		
		this.fileexplorerParent = fileexplorerParent;

		JButton copiarColarBtn = new JButton(new String("Copiar/Colar"));
		
//		JButton cutButton   = new JButton(new String("Recortar"));
//	    JButton copyButton  = new JButton(new String("Copiar"));
//	    JButton pasteButton = new JButton(new String("Colar"));

//	    cutButton.setIcon(new ImageIcon(FileExplorerJToolBar.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo/recorta_32x32.png")));
//	    copyButton.setIcon(new ImageIcon(FileExplorerJToolBar.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo/copia_32x32.png")));
//	    pasteButton.setIcon(new ImageIcon(FileExplorerJToolBar.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo/cola_32x32.png")));

//	    cutButton.addActionListener(cutToolAction);
//	    copyButton.addActionListener(copyToolAction);
//	    pasteButton.addActionListener(pasteToolAction);

		copiarColarBtn.addActionListener(copyPasteAction);
		
//	    this.add(cutButton);
//	    this.add(copyButton);
//	    this.add(pasteButton);
		
		this.add(copiarColarBtn);
		
	}
	
//	ActionListener cutToolAction = new ActionListener () {
//	      public void actionPerformed(ActionEvent e) {
//	  		
//	         fileexplorerParent.getActiveBrowserWindow().cutAction();
//	      }
//	   };
//	    
//	   ActionListener copyToolAction = new ActionListener () {
//	      public void actionPerformed(ActionEvent e) {
//	    	  fileexplorerParent.getActiveBrowserWindow().copyAction();
//	      }
//	   };
//	      
//	   ActionListener pasteToolAction = new ActionListener () {
//	      public void actionPerformed(ActionEvent e) {
//	    	  fileexplorerParent.getActiveBrowserWindow().pasteAction();
//	      }
//	   };
	
	ActionListener copyPasteAction = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if(fileexplorerParent.getActiveBrowserWindow()==fileexplorerParent.getHomeBrowserWindow()) {
				fileexplorerParent.getHomeBrowserWindow().copyAction();
				fileexplorerParent.getServerBrowserWindow().pasteAction();
				fileexplorerParent.selectServerBrowserWindow();
			} else {
				fileexplorerParent.getServerBrowserWindow().copyAction();
				fileexplorerParent.getHomeBrowserWindow().pasteAction();
				fileexplorerParent.selectHomeBrowserWindow();
			}
		}
	};
	
}
