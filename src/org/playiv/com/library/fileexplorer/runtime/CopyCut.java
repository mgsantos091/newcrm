package org.playiv.com.library.fileexplorer.runtime;

import org.playiv.com.library.fileexplorer.TransferListItem;

public class CopyCut {

	 public static final int COPY           = 1;
    public static final int CUT            = 2;
    public static final int DMG            = 4;
	
    public static int QUE            = 0;
    public static int ACTIVE         = 1;
    public static int COMPLETE       = 2;
    public static int CANCELED       = 3;
    public static int ERROR          = 4;
   
    public static String [] statusString = {"Lista de espera", "Ativo", "Completado", "Cancelado", "Erro"};
    
    
	private int    mode;
	private String source;
	private String destination;
    private int    status;
    private TransferListItem transferlistitem;
	
   public CopyCut (int mode, String source, String destination){
		this.mode        = mode;
		this.source      = source;
		this.destination = destination;
        this.status      = QUE;
        transferlistitem = new TransferListItem("Fila   :" + msgEnd());
	}

	
	public int getMode() {
		return mode;
	}
	
	public String getSource(){
		return source;
	}
	
	public String getDestination(){
		return destination;
	}
   
   public int getStatus(){
		return status;
	}
   
   public TransferListItem getTransferlistitem(){
      return transferlistitem;
   }
   
   public void setQued(){
      status = QUE;
      transferlistitem.setMessage("Fila   :"+ msgEnd());
   }
   
   public void setActive(){
      status = ACTIVE;
      transferlistitem.setMessage("Ativo   :"+ msgEnd());
   }
   
   public void setComplete(){
      status = COMPLETE;
      transferlistitem.setMessage("Completado :"+ msgEnd());
   }
   
   public void setError(){
	  status = ERROR;
	  transferlistitem.setMessage("Erro :"+ msgEnd());
   }
   
   public void setCanceled(){
      status = CANCELED;
      transferlistitem.setMessage("Cancelado :"+ msgEnd());
   }
   
   private String msgEnd() {
      return mode +" : "+ source +" : "+ destination ;
   }
}
