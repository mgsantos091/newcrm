package org.playiv.com.library.fileexplorer.runtime;

//import String;

import java.util.Vector;
//For getting Prefs
import java.util.prefs.Preferences;

import org.playiv.com.library.fileexplorer.BrowserPane;
import org.playiv.com.library.fileexplorer.TransferList;

public class PasteQue {

	private static int debugOn        = 1;
	protected Runtime currentRuntime;
	private   Vector joblist;
	private   Vector completedlist;
 private   TransferList transferlist;
	
 private BrowserPane brwPane;
 private String location;
 
	public void setLocation(String location) {
	this.location = location;
}

	public void setBrwPane(BrowserPane brwPane) {
	this.brwPane = brwPane;
}

	public PasteQue() {
		joblist       = new Vector();
		completedlist = new Vector();
	};
 
 public PasteQue(TransferList transferlist) {
		this.transferlist = transferlist;
    joblist       = new Vector();
		completedlist = new Vector();
	};
 
 public synchronized CopyCut getJob(){
    while(joblist.size() == 0){
       try {
          wait();
       } catch (java.lang.InterruptedException e){
          System.err.println(e);
       }
    }
    CopyCut currentJob = (CopyCut) joblist.firstElement();
    return  currentJob;     
    
 }
 
 public boolean joblistComplete() {
    //Todo for the transfer list window will probably only have 1 list with
    //Status indicating qued active complete
    //This method will have to be updated to ignore complete list items.
    if (joblist.size() == 0) {
       
       Preferences prefsTransfer  = Preferences.userRoot().node("net/amaras/fileexplorer/transfer");
       // Get property from pref menu     
       /*boolean closeTransfer = prefsTransfer.getBoolean(fileexplorer.pref.Transfer.CLOSE_TRANSFER, 
                            fileexplorer.pref.Transfer.CLOSE_TRANSFER_DEFAULT);*/
       transferlist.setVisible(false);
       if(this.brwPane!=null)
      	 this.brwPane.reloadBodyPanel();
       /*if (closeTransfer == true) {
          transferlist.setVisible(false);
       }*/
       return true;
    } else {
       return false;
    }
 }
 
 public void completedJob(CopyCut completedJob) {
	   
    completedlist.add(completedJob);
	   joblist.removeElement(completedJob); //Removes first Element
    if (transferlist != null) {
       String completemsg = CopyCut.statusString[completedJob.getStatus()] +
       					  " :"+ completedJob.getMode() + ": " +
                            completedJob.getSource() + " : " + 
                            completedJob.getDestination() ;
       
       //transferlist.add(completemsg);
       completedJob.getTransferlistitem().msgLabel.setText(completemsg);
    }
    //Trigger Closing the Transfer Window if required
    joblistComplete();
    
    /*if(brwPane!=null)
  	  if(location!=null)
  		  brwPane.buildBodyPanel(location);*/
    
	  System.out.println("Complete Job - joblist Size:" + joblist.size());
 }
 
	
 public synchronized void add(int mode, String file, String dest){
		CopyCut newitem = new CopyCut(mode, file, dest);
    joblist.add(newitem);

    if (transferlist != null) {
       transferlist.add(newitem.getTransferlistitem().msgLabel);
    }
    
    //Notify thread that there is work to be done
    notify();
	};

 public void transferListSetVisible(boolean show){
    transferlist.setVisible(show);
 }

 
 public void transferListToggle(){
    transferlist.toggleVisible();
 }
}
