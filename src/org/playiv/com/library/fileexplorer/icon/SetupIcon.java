package org.playiv.com.library.fileexplorer.icon;

public class SetupIcon {
	   javax.swing.ImageIcon iconImage;
	   
	   public SetupIcon() {
	   }
	   
	   public SetupIcon(String path, String description) {
	      setImageIcon(path, description);
	   }
	   
	   //http://java.sun.com/docs/books/tutorial/uiswing/components/icon.html
	   /** Returns an ImageIcon, or null if the path was invalid. */
	   public void setImageIcon(String path, String description) {
	      //~ try {
	         java.net.URL imgURL = getClass().getResource(path);
	         //~ java.net.URL imgURL = new java.net.URL(path); 
	         //~ java.net.URL imgURL = this.getRunDirectory();
	         System.out.println(description + ": " + imgURL + getClass());
	      
	         if (imgURL != null) {
	            iconImage = new javax.swing.ImageIcon(imgURL, description);
	         } else {
	            System.err.println("Couldn't find file: " + path);
	            iconImage = null;
	         }
	      //~ } catch (java.net.MalformedURLException e){
	         //~ System.err.println(e);
	         //~ iconImage = null;
	      //~ }
	      
	   }
	   
	   public javax.swing.ImageIcon getImageIcon(){
	      return iconImage;
	   }
	   
	}