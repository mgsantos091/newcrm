package org.playiv.com.library.fileexplorer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;

import org.playiv.com.library.fileexplorer.pref.Application;
import org.playiv.com.library.fileexplorer.pref.Colours;
import org.playiv.com.library.fileexplorer.pref.Location;
import org.playiv.com.library.fileexplorer.pref.Transfer;
import org.playiv.com.library.fileexplorer.pref.View;

public class PrefPane extends JFrame {
    protected JButton okButton;
    protected JLabel prefsText;

    public PrefPane()
    {
		super();

//        this.getContentPane().setLayout(new BorderLayout(10, 10));
//        prefsText = new JLabel ("FileExplorer Preferences...");
//        JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
//        textPanel.add(prefsText);
//        this.getContentPane().add (textPanel, BorderLayout.NORTH);
//		
//        okButton = new JButton("OK");
//        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
//        buttonPanel.add (okButton);
//        okButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent newEvent) {
//				setVisible(false);
//			}	
//		});
//        this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
//        
        
		JTabbedPane topLevelMenu   = new JTabbedPane(JTabbedPane.TOP); 
        
        Location     locationTab    = new Location();
        View         viewTab        = new View();
        Colours      colorTab       = new Colours();
        Application  appTab         = new Application();
        Transfer     transferTab    = new Transfer();
        
		topLevelMenu.add(locationTab);
		topLevelMenu.add(viewTab);
		topLevelMenu.add(appTab);
//		topLevelMenu.add(colorTab);
		topLevelMenu.add(transferTab);
        
        
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.getContentPane().add(topLevelMenu);
		setSize(390, 300);
		setLocation(20, 40);
    }
}