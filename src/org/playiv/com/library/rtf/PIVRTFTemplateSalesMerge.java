package org.playiv.com.library.rtf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.swing.text.MaskFormatter;

import net.sourceforge.rtf.RTFTemplate;
import net.sourceforge.rtf.template.IContext;
import net.sourceforge.rtf.usecases.AbstractRTFUseCase;

import org.hibernate.Session;
import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.internationalization.java.BrazilianPortugueseOnlyResourceFactory;
import org.openswing.swing.internationalization.java.ResourcesFactory;
import org.openswing.swing.util.client.ClientSettings;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.Util;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVSalesDetailModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;

public class PIVRTFTemplateSalesMerge extends AbstractRTFUseCase {

	private static PIVGlobalSettings global_settings = PIVGlobalSettings.getInstance();

	private static PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();

	private PIVSalesMainModel venda_pai;
	private List<PIVSalesDetailModel> venda_filho;

	private PIVUserSession sessao_usuario = PIVUserSession.getInstance();

	public PIVRTFTemplateSalesMerge(PIVSalesMainModel venda_pai) {
		this(global_settings.getDEFAULT_RTF_OUTPUT_PATH() + venda_pai.getId());
	}

	private PIVRTFTemplateSalesMerge(String outDirectory) {
		super(outDirectory);
	}

	protected void putDefaultFormat(RTFTemplate rtfTemplate) {
		/*
		 * rtfTemplate.setDefaultFormat(Date.class,
		 * DateFormat.getDateInstance());
		 */
	}

	protected void putContext(IContext context) {

		// context.put("project", new Project("Jakarta Velocity"));

		// List developers2 = new ArrayList();
		// Developer developer = new Developer("Will Glass-Husain",
		// "wglass@apache.org");
		// developer.addRole("Java Developer");
		// developer.addRole("Release Manager");
		// developers2.add(developer);
		// developer = new Developer("Geir Magnusson Jr.", "geirm@apache.org");
		// developer.addRole("Java Developer");
		// developer.addRole("Release Manager");
		// developers2.add(developer);
		// developer = new Developer("Daniel L. Rall",
		// "dlr@finemaltcoding.comg");
		// developer.addRole("Java Developer");
		// developer.addRole("Release Manager");
		// developers2.add(developer);
		// context.put("developers", developers2);
		//
		// List l = new ArrayList();
		// for(PIVSalesDetailModel itemPedido : this.venda_filho) {
		// l.add(itemPedido.getProduto());
		// }
		// Produto produto = new Produto ();
		// produto.setNome("Test 1");
		// l.add(produto);
		// produto = new Produto();
		// produto.setNome("Test 2");
		// l.add(produto);
		// context.put("produtos", l);

		// List dependencies = new ArrayList();
		// Dependency dependency = new Dependency("commons-collection", "jar",
		// "1.0",
		// "http://jakarta.apache.org/commons/collection/");
		// dependencies.add(dependency);
		// dependency = new Dependency("logkit", "jar", "1.0.1",
		// "http://logkit");
		// dependencies.add(dependency);
		// dependency = new Dependency("oro", "jar", "2.0.8",
		// "http://jakarta.apache.org/oro/");
		// dependencies.add(dependency);
		// context.put("dependencies", dependencies);

		// busca os dados da empresa

		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = PIVDao.getInstance().getSession();

		PIVLocalConfModel empresa = (PIVLocalConfModel) session.createQuery(baseSQL).uniqueResult();

		session.close();

		context.put("EMPRESA", empresa);

		if (empresa.getNomeimagem() != null && !empresa.getNomeimagem().equals("")) {
			FileInputStream is = null;
			try {
				is = new FileInputStream(gerenciarDiretorio.getLogosPath() + empresa.getNomeimagem());
				context.put("EMPRESA_LOGO", is);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				// try {
				// is.close();
				// } catch (IOException e) {
				// e.printStackTrace();
				// }
			}
		}

		String nomeEmpresa = empresa.getNomefantasia() != null && !empresa.getNomefantasia().equals("")
				? empresa.getNomefantasia() : empresa.getRazaosocial();
		context.put("EMPRESA_NOME", nomeEmpresa);

		if (empresa.getEndereco() != null) {
			String logradouro = Util.checkNullOrEmpty(empresa.getEndereco().getLogradouro()) ? ""
					: empresa.getEndereco().getLogradouro();
			String cep = Util.checkNullOrEmpty(empresa.getEndereco().getCep()) ? "" : empresa.getEndereco().getCep();
			String cidade = Util.checkNullOrEmpty(empresa.getEndereco().getCidade()) ? ""
					: empresa.getEndereco().getCidade();
			String estado = Util.checkNullOrEmpty(empresa.getEndereco().getEstado()) ? ""
					: empresa.getEndereco().getEstado();
			String bairro = Util.checkNullOrEmpty(empresa.getEndereco().getBairro()) ? ""
					: empresa.getEndereco().getBairro();
			StringBuffer endereco = new StringBuffer();
			if (!logradouro.equals("")) {
				endereco.append(logradouro);
				if (empresa.getEndnumero() != null)
					endereco.append(", Nro. " + empresa.getEndnumero());
				if (!bairro.equals(""))
					endereco.append(", " + bairro);
				if (!cep.equals(""))
					endereco.append(", " + cep);
			}
			context.put("EMPRESA_ENDERECO", endereco);

			StringBuffer cidade_estado = new StringBuffer();
			if (!cidade.equals("")) {
				cidade_estado.append(cidade);
				if (!estado.equals("")) {
					estado = ClientSettings.getInstance().getDomain("ESTADO").getDomainPair(estado).getDescription();
					cidade_estado.append(" - " + estado);
				}
			}
			context.put("EMPRESA_CIDADE_ESTADO", cidade_estado);
		}

		String telefone = empresa.getTelefone();
		if (!Util.checkNullOrEmpty(telefone)) {
			String sTelMask = PIVMaskResources.getMask(PIVMaskResources.mask.TEL);
			MaskFormatter mfTelMask;
			try {
				mfTelMask = new MaskFormatter(sTelMask);
				mfTelMask.setValueContainsLiteralCharacters(false);
				context.put("EMPRESA_TELEFONE", mfTelMask.valueToString(telefone));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		String cnpj = empresa.getCnpj();
		if (!Util.checkNullOrEmpty(cnpj)) {
			String sCnpjMask = PIVMaskResources.getMask(PIVMaskResources.mask.CNPJ);
			MaskFormatter mfCnpjMask;
			try {
				mfCnpjMask = new MaskFormatter(sCnpjMask);
				mfCnpjMask.setValueContainsLiteralCharacters(false);
				context.put("EMPRESA_CNPJ", mfCnpjMask.valueToString(cnpj));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if (this.venda_pai.isVendaefetivada())
			context.put("ORCAMENTO_OU_PEDIDO", "PEDIDO");
		else
			context.put("ORCAMENTO_OU_PEDIDO", "OR�AMENTO");

		String pedido_num = String.format("%05d", this.venda_pai.getId());
		context.put("NUMERO", pedido_num);

		/*
		 * $PEDIDOFILHO_ID_HEADER $PEDIDOFILHO_NOME_HEADER
		 * $PEDIDOFILHO_QTD_HEADER $PEDIDOFILHO_VALOR_HEADER
		 * $PEDIDOFILHO_VALTOTAL_HEADER
		 */

		// C�DIGO PRODUTO DESCRI��O PRODUTOS / SERVI�OS QTD VALOR
		// PRODUTO/SERVI�O VALOR TOTAL

		context.put("PEDIDOFILHO_ID_HEADER", "C�DIGO PRODUTO");
		context.put("PEDIDOFILHO_NOME_HEADER", "DESCRI��O PRODUTOS / SERVI�OS");
		context.put("PEDIDOFILHO_QTD_HEADER", "QTD");
		context.put("PEDIDOFILHO_VALOR_HEADER", "VALOR PRODUTO/SERVI�O");
		context.put("PEDIDOFILHO_VALTOTAL_HEADER", "VALOR TOTAL");

		context.put("PEDIDOPAI", this.venda_pai);

		context.put("PEDIDOFILHO", this.venda_filho);

		String nomeVendedor = sessao_usuario.getVendedorSessao().getNome();
		context.put("VENDEDOR", nomeVendedor);

		PIVAddressModel endereco_empresa = empresa.getEndereco();

		String estado = ClientSettings.getInstance().getDomain("ESTADO").getDomainPair(endereco_empresa.getEstado())
				.getDescription();
		context.put("ESTADO", estado);

		String cidade = endereco_empresa.getCidade();
		context.put("CIDADE", cidade);

//		Format dateFormatter = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("pt", "BR"));
		Format dateFormatter = new SimpleDateFormat("dd/mm/yyyy", new Locale("pt", "BR"));
		String data_registro = dateFormatter.format(this.venda_pai.getDataregistro());
		context.put("EMISSAO", data_registro);

		Format horaFormatter = new SimpleDateFormat("HH:mm");
		String hora_registro = horaFormatter.format(new Date());
		context.put("HORAREGISTRO", hora_registro); 

	}

	public void setVenda_pai(PIVSalesMainModel venda_pai) {
		this.venda_pai = venda_pai;
	}

	public void setVenda_filho(ArrayList<PIVSalesDetailModel> venda_filho) {
		this.venda_filho = venda_filho;
	}

	public void gerarDocumento() throws Exception {

//		String baseDirectory = global_settings.getDEFAULT_RTF_TEMPLATE_PATH();
//		String rtfSource = global_settings.getDEFAULT_RTF_RESOURCE();
		
//		ClassLoader classLoader = getClass().getClassLoader();
//		File file = new File(global_settings.getDEFAULT_RTF_RESOURCE().getFile());	
		String filePath = PIVGlobalSettings.getInstance().getDEFAULT_RTF_TEMPLATE_PATH() + "\\proposta_comercial.rtf";

		/* String rtfSource = baseDirectory + "\\proposta_comercial.rtf"; */
//		String rtfSource = baseDirectory + global_settings.getDEFAULT_RTF_RESOURCE();

		/* saveTransformedDocument(true); */

		run(filePath);

		/*
		 * String xmlFieldsAvailable =
		 * global_settings.getDEFAULT_RTF_OUTPUT_PATH() +
		 * "\\proposta_comercial_" + this.venda_pai.getId() + ".fields.xml";
		 */

		/* saveXmlFields(xmlFieldsAvailable, false); */

	}

	public static void main(String args[]) throws Exception {

		Properties props = new Properties();
		Hashtable domains = new Hashtable();

		ResourcesFactory resource = new BrazilianPortugueseOnlyResourceFactory(props, false);

		Domain estadoDomain = new Domain("ESTADO");
		estadoDomain.addDomainPair("AC", "Acre");
		estadoDomain.addDomainPair("AL", "Alagoas");
		estadoDomain.addDomainPair("AP", "Amap�");
		estadoDomain.addDomainPair("AM", "Amazonas");
		estadoDomain.addDomainPair("BA", "Bahia");
		estadoDomain.addDomainPair("CE", "Cear�");
		estadoDomain.addDomainPair("DF", "Distrito Federal");
		estadoDomain.addDomainPair("ES", "Esp�rito Santo");
		estadoDomain.addDomainPair("GO", "Goi�s");
		estadoDomain.addDomainPair("MA", "Maranh�o");
		estadoDomain.addDomainPair("MT", "Mato Grosso");
		estadoDomain.addDomainPair("MS", "Mato Grosso do Sul");
		estadoDomain.addDomainPair("MG", "Minas Gerais");
		estadoDomain.addDomainPair("PA", "Par�");
		estadoDomain.addDomainPair("PB", "Para�ba");
		estadoDomain.addDomainPair("PR", "Paran�");
		estadoDomain.addDomainPair("PE", "Pernambuco");
		estadoDomain.addDomainPair("PI", "Piau�");
		estadoDomain.addDomainPair("RJ", "Rio de Janeiro");
		estadoDomain.addDomainPair("RN", "Rio Grande do Norte");
		estadoDomain.addDomainPair("RS", "Rio Grande do Sul");
		estadoDomain.addDomainPair("RO", "Rond�nia");
		estadoDomain.addDomainPair("RR", "Roraima");
		estadoDomain.addDomainPair("SC", "Santa Catarina");
		estadoDomain.addDomainPair("SP", "S�o Paulo");
		estadoDomain.addDomainPair("SE", "Sergipe");
		estadoDomain.addDomainPair("TO", "Tocantis");

		domains.put(estadoDomain.getDomainId(), estadoDomain);

		ClientSettings clientSettings = new ClientSettings(resource, domains);

		String baseSQL = "from Venda in class org.playiv.com.mvc.model.PIVSalesMainModel where Venda.id = 2";
		Session session = PIVDao.getInstance().getSession();

		PIVSalesMainModel venda_pai = (PIVSalesMainModel) session.createQuery(baseSQL).uniqueResult();

		session.close();

		baseSQL = "from VendaFilho in class org.playiv.com.mvc.model.PIVSalesDetailModel where VendaFilho.vendaPai.id = 2";
		session = PIVDao.getInstance().getSession();

		ArrayList<PIVSalesDetailModel> venda_filho = (ArrayList<PIVSalesDetailModel>) session.createQuery(baseSQL)
				.list();

		session.close();

		gerenciarDiretorio.checkOrganizacaoPastas();

		PIVRTFTemplateSalesMerge rtfTemplateTest = new PIVRTFTemplateSalesMerge(venda_pai);
		rtfTemplateTest.setVenda_pai(venda_pai);
		rtfTemplateTest.setVenda_filho(venda_filho);
		rtfTemplateTest.gerarDocumento();

	}

	public void copyFileToServer() {

		String filePath = global_settings.getDEFAULT_RTF_OUTPUT_PATH() + venda_pai.getId() + "\\"
//				+ global_settings.getDEFAULT_RTF_RESOURCE() + ".vmRTFTemplate.out.rtf";
				+ "proposta_comercial" + ".rtf.vmRTFTemplate.out.rtf";

		/*
		 * String destino = gerenciarDiretorio.getArquivosDeClientePath( ) +
		 * "propostas\\";
		 */

		String nomePastaUsuario = this.venda_pai.getCliente().getId().toString();

		String destino = PIVDirectoryMngtFunc.getInstance().getArquivosDeClientePath() + nomePastaUsuario
				+ "\\propostas" + (PIVGlobalSettings.isWindows() ? "\\" : "//");

		File destinoFile = new File(destino);
		if (!destinoFile.isDirectory())
			destinoFile.mkdirs();

		gerenciarDiretorio.copiaArquivosPServidor(filePath, destino, "proposta" + "_"
				+ /* venda_pai.getCliente().getNomefantasia().trim() + "_" + */ venda_pai.getId() + ".rtf");

	}

}