package org.playiv.com.library.jasperreports;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;

public class PIVReportFactory {
	
	private Connection jdbcConnection = PIVDao.getInstance().getJdbcConnection();
	
	private static PIVReportFactory instance;
	
	private PIVReportFactory( ) {
		
	}
	
	public static PIVReportFactory getInstance( ) {
		if(instance==null)
			instance = new PIVReportFactory( );
		return instance;
	}
	
	public void abrirRelatorioVendasPorClientes( ) {
		abrirRelatorio("/relatorios/playiv_vendasxcliente.jrxml");
	}
	
	public void abrirRelatorioVendasPorVendedor( ) {
		abrirRelatorio("/relatorios/playiv_vendasxvendedor.jrxml");
	}
	
	public void abrirRelatorioClienterPorVendedor( ) {
		abrirRelatorio("/relatorios/playiv_vendedorxcliente.jrxml");
	}
	
	public void abrirRelatorioVendasPorProduto( ) {
		abrirRelatorio("/relatorios/playiv_vendsxaproduto.jrxml");
	}
	
	private void abrirRelatorio( String nomeArquivo ) {
		
		try {
			String relatorio = PIVReportFactory.class.getResource(nomeArquivo).getPath();
			JasperDesign jasperDesign = JRXmlLoader.load(relatorio);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			
			Map parametros = new HashMap();
			parametros.put("SUBREPORT_DIR", new File(relatorio).getParent() + File.separator );
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, jdbcConnection);			
			
			JasperViewer.viewReport(jasperPrint , false , new Locale("pt","BR"));
		} catch (JRException e) {
			PIVLogSettings.getInstance().error(e.getMessage(),e);
			e.printStackTrace();
		}
		
	}
	
}