package org.playiv.com.library.webservice;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientGeoPosModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.webservice.PIVGoogleGeocoding;

public class PIVMapFrame extends JXMapKit {

	private ArrayList<PIVWaypoint> points = new ArrayList<PIVWaypoint>();

	private static final long serialVersionUID = 1L;
	private WaypointPainter waypointPainter = new WaypointPainter();

	private PIVGlobalSettings globalSettings = PIVGlobalSettings.getInstance();

	/* private ArrayList<GeoPosition> points = new ArrayList<GeoPosition>(); */

	private GeoPosition posicao = null;

	public PIVMapFrame() {
		TileFactoryInfo tfi = new PIVGoogleTileFactoryInfo(0, 10, 17, 256, true,
				true, false);
		setTileFactory(new DefaultTileFactory(tfi));
		getMainMap().setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}

	/*
	 * public void buscaEndereco(final String rua, final Integer numero, final
	 * String cidade, final String uf) {
	 * 
	 * GeoPosition posicao = null; try { if (rua != null && cidade != null && uf
	 * != null) { System.out.println("Buscando endere�o:" + rua.trim() + ", " +
	 * (numero != null ? numero.toString() : 0) + "-" + cidade.trim() + "-" +
	 * uf); posicao = GeoUtil.getPositionForAddress(StringFunctions
	 * .clearAccents(rua).trim() + ", " + (numero != null ? numero.toString() :
	 * 0), StringFunctions.clearAccents(cidade).trim(), uf); } else { System.out
	 * .println(
	 * "N�o foi poss�vel processar o endere�o!\nRua, Cidade ou estado, inv�lidos!"
	 * ); } setAddressLocation(posicao); } catch (Exception e) {
	 * System.out.println("Erro ao processar o endere�o!\n" + e.getMessage());
	 * e.printStackTrace(); }
	 * 
	 * }
	 */

	public void buscaEndereco(String estado, String cidade, String bairro,
			String rua) {
		if (!(estado.equals("") && cidade.equals("") && bairro.equals(""))) {
			try {

				waypointPainter.setRenderer(new WaypointRenderer() {
					@Override
					public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
							Waypoint waypoint) {
						int x;
						for (PIVWaypoint pwaypoint : PIVMapFrame.this.points) {
							if (pwaypoint.getWaypoint() == waypoint) {
								g.setColor(pwaypoint.getCor());
								g.fillRect(-5, -5, 10, 10);
								g.setColor(Color.BLACK);
								g.drawRect(-5, -5, 10, 10);
								break;
							}
						}
						return true;
					}
				});

				PIVGoogleGeocoding googleGeo = new PIVGoogleGeocoding(estado, cidade,
						rua);

				posicao = googleGeo.realizaBusca();
				Waypoint waypoint = new Waypoint(posicao.getLatitude(),
						posicao.getLongitude());

				PIVWaypoint pwaypoint = new PIVWaypoint(waypoint, Color.BLUE);
				points.add(pwaypoint);
				waypointPainter.getWaypoints().add(waypoint);

				PIVLocalConfModel confLocal = globalSettings
						.getConfiguracoesLocais();
				if (confLocal != null) {
					waypoint = new Waypoint(confLocal.getLatitude(),
							confLocal.getLongitude());
					pwaypoint = new PIVWaypoint(waypoint, Color.RED);
					points.add(pwaypoint);
					waypointPainter.getWaypoints().add(waypoint);
				}
				
				setAddressLocation(posicao);

				/*
				 * points.add(new GeoPosition(posicao.getLatitude(), posicao
				 * .getLongitude()));
				 */

				getMainMap().setOverlayPainter(waypointPainter);

			} catch (Exception e) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),
						"Erro ao processar o endere�o.");
				// System.out.println("Erro ao processar o endere�o!\n"
				// + e.getMessage());
				e.printStackTrace();
				PIVLogSettings.getInstance().error(e.getMessage(), e);
			}
		} else
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"Esta faltando algum dos dados: Estado, cidade, bairro ou logradouro, favor conferir.");
	}

	public void buscaEndereco(
			ArrayList<PIVClientGeoPosModel> posicionamentos) {
		if (posicionamentos != null && posicionamentos.size() >= 1) {
			try {
				waypointPainter.setRenderer(new WaypointRenderer() {
					@Override
					public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
							Waypoint waypoint) {
						int x;
						for (PIVWaypoint pwaypoint : PIVMapFrame.this.points) {
							if (pwaypoint.getWaypoint() == waypoint) {
								g.setColor(pwaypoint.getCor());
								g.fillRect(-5, -5, 10, 10);
								g.setColor(Color.BLACK);
								g.drawRect(-5, -5, 10, 10);
								break;
							}
						}
						return true;
					}
				});
				
				GeoPosition posicao = null;
				Waypoint waypoint;
				PIVWaypoint pwaypoint;
				for (PIVClientGeoPosModel posicionamento : posicionamentos) {

					double latitude = posicionamento.getLatitude();
					double longitude = posicionamento.getLongitude();

					waypoint = new Waypoint(latitude, longitude);

					pwaypoint = new PIVWaypoint(waypoint,
							Color.BLUE);
					points.add(pwaypoint);

					waypointPainter.getWaypoints().add(waypoint);

					 posicao = new GeoPosition(latitude, longitude);  
				}
				
				PIVLocalConfModel confLocal = globalSettings
						.getConfiguracoesLocais();
				
				if (confLocal != null) {
					waypoint = new Waypoint(confLocal.getLatitude(),
							confLocal.getLongitude());
					pwaypoint = new PIVWaypoint(waypoint, Color.RED);
					points.add(pwaypoint);
					waypointPainter.getWaypoints().add(waypoint);
					posicao = new GeoPosition(confLocal.getLatitude(),
							confLocal.getLongitude());
				}
				
				getMainMap().setOverlayPainter(waypointPainter);

				setAddressLocation(posicao);

			} catch (Exception e) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),
						"Erro ao processar o endere�o.");
				// System.out.println("Erro ao processar o endere�o!\n"
				// + e.getMessage());
				e.printStackTrace();
				PIVLogSettings.getInstance().error(e.getMessage(), e);
			}
			
		}

	}

	/*
	 * @Override public void paint(Graphics g) {
	 * 
	 * g = (Graphics2D) g.create();
	 * 
	 * // convert from viewport to world bitmap Rectangle rect =
	 * getMainMap().getViewportBounds(); g.translate(-rect.x, -rect.y);
	 * 
	 * // set the line properties g.setColor(Color.RED); ((Graphics2D)
	 * g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	 * RenderingHints.VALUE_ANTIALIAS_ON); ((Graphics2D) g).setStroke(new
	 * BasicStroke(2));
	 * 
	 * // do the drawing int lastX = -1; int lastY = -1; for (GeoPosition point
	 * : points) { Point2D pt = getMainMap().getTileFactory().geoToPixel(point,
	 * getMainMap().getZoom()); if (lastX != -1 && lastY != -1) {
	 * g.drawLine(lastX, lastY, (int) pt.getX(), (int) pt.getY()); } lastX =
	 * (int) pt.getX(); lastY = (int) pt.getY(); } }
	 */

	public GeoPosition getPosicao() {
		return posicao;
	}

	public void buscaEndereco(PIVClientGeoPosModel posicionamento) {

		if (posicionamento != null) {

			try {

				waypointPainter.setRenderer(new WaypointRenderer() {
					@Override
					public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
							Waypoint waypoint) {
						int x;
						for (PIVWaypoint pwaypoint : PIVMapFrame.this.points) {
							if (pwaypoint.getWaypoint() == waypoint) {
								g.setColor(pwaypoint.getCor());
								g.fillRect(-5, -5, 10, 10);
								g.setColor(Color.BLACK);
								g.drawRect(-5, -5, 10, 10);
								return true;
							}
						}
						return false;
					}
				});
				
				Waypoint waypoint = new Waypoint(posicionamento.getLatitude(),
						posicionamento.getLongitude());

				PIVWaypoint pwaypoint = new PIVWaypoint(waypoint, Color.BLUE);
				points.add(pwaypoint);				
				waypointPainter.getWaypoints().add(waypoint);

				PIVLocalConfModel confLocal = globalSettings
						.getConfiguracoesLocais();
				
				if (confLocal != null) {
					waypoint = new Waypoint(confLocal.getLatitude(),
							confLocal.getLongitude());
					pwaypoint = new PIVWaypoint(waypoint, Color.RED);
					points.add(pwaypoint);
					waypointPainter.getWaypoints().add(waypoint);
					posicao = new GeoPosition(confLocal.getLatitude(),
							confLocal.getLongitude());
				}

				getMainMap().setOverlayPainter(waypointPainter);

				/*setAddressLocation(posicao);*/

			} catch (Exception e) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),
						"Erro ao processar o endere�o.");
				// System.out.println("Erro ao processar o endere�o!\n"
				// + e.getMessage());
				e.printStackTrace();
				PIVLogSettings.getInstance().error(e.getMessage(), e);
			}

		}

	}

	class PIVWaypoint {

		private Waypoint waypoint;
		private Color cor;

		public PIVWaypoint(Waypoint waypoint, Color cor) {
			this.waypoint = waypoint;
			this.cor = cor;
		}

		public Waypoint getWaypoint() {
			return this.waypoint;
		}

		public Color getCor() {
			return cor;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof PIVWaypoint) {
				PIVWaypoint way = (PIVWaypoint) obj;
				Waypoint waypoint = way.getWaypoint();
				if (this.getWaypoint() == waypoint)
					return true;
			}
			return false;
		}

	}

}