package org.playiv.com.library.function;

import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVClientGeoPosModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.view.PIVBusRelMainCFView;
import org.playiv.com.mvc.view.PIVMapCFView;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVMapCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	private PIVBusRelMainCFView relComView;

	public PIVMapCommandFunc(PIVBusRelMainCFView relComView) {
		this.relComView = relComView;
	}

	@Override
	public void execute() {
		System.out.println(this + " executado");
		if (this.elemento != null) {
			PIVMapCFView mapaView = null;
			try {
				PIVClientModel cliente = ((PIVBusRelElementComponent)elemento).getCliente();
				mapaView = new PIVMapCFView(cliente);
				MDIFrame.add(mapaView);
			} catch (ParseException e) {
				JOptionPane
				.showMessageDialog(
						null,
						"Ocorreu um erro ao processar o mapa, favor contatar o administrador do sistema",
						"Erro ao processar mapa",
						JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				PIVLogSettings.getInstance().error(e.getMessage(), e);
			}
		} else {
			if (this.relComView != null) {

				PIVDao pdao = PIVDao.getInstance();

				Integer idVendedor = PIVUserSession.getInstance()
						.getVendedorSessao().getId();
				Integer nivel = this.relComView.getNivelSelecionado();

				if (nivel >= 1 && nivel <= 4) {

					Session sessao = pdao.getSession();

					@SuppressWarnings("unchecked")
					ArrayList<PIVClientGeoPosModel> posicionamentos = (ArrayList<PIVClientGeoPosModel>) sessao
							.createQuery(
									"select posicionamento "
											+ "from org.playiv.com.mvc.model.PIVClientGeoPosModel as posicionamento , "
											+ "org.playiv.com.mvc.model.PIVBusRelModel as RelCom "
											+ "where posicionamento.cliente.id = RelCom.cliente.id and RelCom.vendedor.id = "
											+ idVendedor
											+ " and RelCom.idnivelcomercial = "
											+ nivel
											+ " and RelCom.ativo = true")
							.list();
					sessao.close();

					if (posicionamentos.size() > 0) {
						PIVMapCFView mapaView;
						try {
							mapaView = new PIVMapCFView(posicionamentos);
							MDIFrame.add(mapaView);
						} catch (ParseException e) {
							JOptionPane
									.showMessageDialog(
											null,
											"Ocorreu um erro ao processar o mapa, favor contate o administrador do sistema",
											"Erro ao processar mapa",
											JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
							PIVLogSettings.getInstance().error(e.getMessage(), e);
						}
					} else
						JOptionPane
								.showMessageDialog(
										null,
										"N�o foram encontradas posicionamentos armazenados, caso existam clientes cadastrados com endere�os "
												+ "v�lidos no n�vel "
												+ PIVBusRelMainCFController
														.getNivelDesc(PIVBusRelMainCFController
																.getNivelEnum(nivel))
												+ ", acesse a p�gina de cadastro e confirme o endere�o de cada cliente.");

				} else
					JOptionPane
							.showMessageDialog(
									null,
									"Voc� n�o esta em uma aba v�lida",
									"Escolha uma das abas que representem os n�veis comerciais",
									JOptionPane.ERROR_MESSAGE);

			}
			/*
			 * JOptionPane.showMessageDialog(MDIFrame.getInstance(),
			 * "Selecione um cliente");
			 */
		}
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}