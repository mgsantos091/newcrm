package org.playiv.com.library.function;

import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.swing.general.PIVCpfCnpjTextControl;


public class PIVTextControlFactory {

	private static MaskFormatter criaFormatter( String mask ) throws ParseException {

		MaskFormatter formatter;
		formatter = new MaskFormatter(mask);
		formatter.setAllowsInvalid(false);
		formatter.setValueContainsLiteralCharacters(false);
		formatter.setPlaceholderCharacter('_');
		formatter.setCommitsOnValidEdit(true);

		return formatter;

	}

	private static MaskFormatter criaFormatter( PIVMaskResources.mask maskenum ) throws ParseException {
		return criaFormatter(PIVMaskResources.getMask(maskenum));
	}

	private static FormattedTextControl criaFormattedTextControl( String mask ) throws ParseException {
		MaskFormatter formatter = criaFormatter(mask);

		FormattedTextControl formattedTextControl = new FormattedTextControl();
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(
				formatter));

		return formattedTextControl;
	}

	public static PIVCpfCnpjTextControl criaCpfCnpjTextControl( PIVMaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(maskenum);
		PIVCpfCnpjTextControl formattedTextControl = new PIVCpfCnpjTextControl();
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(
				formatter));
		return formattedTextControl;
	}

	public static JFormattedTextField criaFormattedTextField ( PIVMaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(PIVMaskResources.getMask(maskenum));
		JFormattedTextField formattedTextField = new JFormattedTextField(formatter);
		return formattedTextField;
	}
	
	public static FormattedTextControl criaFormattedTextControl( PIVMaskResources.mask maskenum ) throws ParseException {
		return criaFormattedTextControl(PIVMaskResources.getMask(maskenum));
	}

	public static FormattedTextColumn criaFormaattedTextColumn( PIVMaskResources.mask maskenum ) throws ParseException {
		MaskFormatter formatter = criaFormatter(maskenum);
		formatter.setValidCharacters("0123456789");
		FormattedTextColumn formattedTextColumn = new FormattedTextColumn();
		formattedTextColumn.setFormatterFactory(new DefaultFormatterFactory(formatter));
//		formattedTextColumn.setFormatter(formatter);
		return formattedTextColumn;
	}

	public static void mudaMascara( FormattedTextControl formattedTextControl , PIVMaskResources.mask maskenum) throws ParseException  {
		formattedTextControl.setFormatterFactory(new DefaultFormatterFactory(criaFormatter(maskenum)));
	}

}