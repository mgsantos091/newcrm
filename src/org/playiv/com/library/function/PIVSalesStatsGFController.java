package org.playiv.com.library.function;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.send.java.FilterWhereClause;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.server.QueryUtil;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVSellerGridModel;
import org.playiv.com.mvc.model.PIVSellerModel;

public class PIVSalesStatsGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;

	private PIVSalesStatsGFView grid = null;
	private Connection conn = PIVDao.getInstance().getJdbcConnection();

	private PIVSellerModel vendedor;
	
	public PIVSalesStatsGFController( PIVSellerModel vendedor ) {
		grid = new PIVSalesStatsGFView(this);
		MDIFrame.add(grid);
		this.vendedor = vendedor;
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		PreparedStatement stmt = null;
		try {

			ArrayList vals = new ArrayList();

			String sql = ""
					+ "SELECT "
					+ "	A.idvendedor idvendedor "
					+ "	, A.nome nome "
					+ "	, A.idcliente idcliente "
					+ "	, A.nomefantasia nomefantasia "
					+ "	, A.razaosocial razaosocial "
					+ "	, A.idnivelcomercial idnivelcomercial "
					+ "	, COALESCE(C.qtdligacao,0) qtdligacao "
					+ "	, COALESCE(C.qtdemail,0) qtdemail "
					+ "	, COALESCE(C.qtdobs,0) qtdobs "
					+ "	, COALESCE(D.qtdpropostas,0) qtdpropostas "
					+ "	, date_part('day' , current_timestamp - B.dataregistro) diasnestenivel "
					+ "FROM ( "
					+ "SELECT "
					+ "	X.idvendedor "
					+ "	, Z.nome "
					+ "	, X.idcliente "
					+ "	, Y.nomefantasia "
					+ "	, Y.razaosocial "
					+ "	, MAX(X.idnivelcomercial) idnivelcomercial "
					+ "FROM \"NivelComercialRelacionamento\" X INNER JOIN \"Cliente\" Y ON X.idcliente = Y.id INNER JOIN \"Vendedor\" Z ON X.idvendedor = Z.id "
					+ "WHERE X.ativo = true " + ( this.vendedor!=null?" and X.idvendedor = "+this.vendedor.getId():"" )
					+ "GROUP BY idvendedor , idcliente , Y.nomefantasia , Y.razaosocial , Z.nome "
					+ ") A	INNER JOIN \"NivelComercialRelacionamento\" B ON A.idvendedor = B.idvendedor AND A.idcliente = B.idcliente AND A.idnivelcomercial = B.idnivelcomercial "
					+ "	LEFT OUTER JOIN "
					+ "( "
					+ "SELECT "
					+ "	X.idvendedor "
					+ "	, X.idcliente "
					+ "	, (SELECT COUNT(*) FROM \"RegistroEvento\" A WHERE X.idvendedor = A.idvendedor AND X.idcliente = A.idcliente AND A.idevento = 3) qtdligacao "
					+ "	, (SELECT COUNT(*) FROM \"RegistroEvento\" A WHERE X.idvendedor = A.idvendedor AND X.idcliente = A.idcliente AND A.idevento = 4) qtdemail "
					+ "	, (SELECT COUNT(*) FROM \"RegistroEvento\" A WHERE X.idvendedor = A.idvendedor AND X.idcliente = A.idcliente AND A.idevento = 5) qtdobs "
					+ "FROM \"RegistroEvento\" X INNER JOIN \"Cliente\" Y ON X.idcliente = Y.id INNER JOIN \"Vendedor\" Z ON X.idvendedor = Z.id "
					+ "GROUP BY X.idvendedor , Z.nome , X.idcliente , Y.nomefantasia , Y.razaosocial "
					+ ") C	ON A.idvendedor = C.idvendedor AND A.idcliente = C.idcliente "
					+ "	LEFT OUTER JOIN "
					+ "( "
					+ "SELECT "
					+ "	idvendedor "
					+ "	, idcliente "
					+ "	, COUNT(*) qtdpropostas "
					+ "FROM \"VendasPai\" "
					+ "GROUP BY idvendedor , idcliente "
					+ ") D	ON A.idvendedor = D.idvendedor AND A.idcliente = D.idcliente";

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("nivelComercial", "idnivelcomercial");
			map.put("idVendedor", "idvendedor");
			map.put("nomeVendedor", "nome");
			map.put("id_cliente", "idcliente");
			map.put("razaosocial", "razaosocial");
			map.put("nomefantasia", "nomefantasia");
			map.put("diasNesteNivel", "diasnestenivel");
			map.put("qtdLigacoes", "qtdligacao");
			map.put("qtdEmails", "qtdemail");
			map.put("qtdObs", "qtdobs");
			map.put("qtdPropostas", "qtdpropostas");
			map.put("qtdPropostas", "qtdpropostas");
			map.put("filtro_dtinicio", "X.dataregistro");
			map.put("filtro_dtfim", "X.dataregistro");
			
			Iterator keys = filteredColumns.keySet().iterator();
			String attributeName = null;
			FilterWhereClause[] filterClauses = null;
			String where = "";
			while (keys.hasNext()) {
				attributeName = keys.next().toString();
				filterClauses = (FilterWhereClause[]) filteredColumns
						.get(attributeName);
				
				if (  filterClauses[0].getValue()!=null &&
			              !(filterClauses[0].getOperator().equals(Consts.IS_NOT_NULL) || filterClauses[0].getOperator().equals(Consts.IS_NULL))) {
			            if (filterClauses[0].getValue() instanceof ArrayList) {
			              // name IN (...)
			              // name NOT IN (...)
			              // (name op value1 OR name op value2 OR ...)
			              if (filterClauses[0].getOperator().equals(Consts.IN) ||
			                  filterClauses[0].getOperator().equals(Consts.NOT_IN)) {
			                // name IN (...)
			                // name NOT IN (...)
			            	  where +=
			                  map.get(attributeName) +
			                  " " + filterClauses[0].getOperator() +
			                  " (";
			                ArrayList inValues = (ArrayList)filterClauses[0].getValue();
			                for(int j=0;j<inValues.size();j++) {
			                  where += " " + inValues.get(j);
			                }
			              }
			              else {
			                  // (name op value1 OR name op value2 OR ...)
			                  int i =0; // start index
			                  boolean isExamineNull = false;
			                  where += "(";
			                  ArrayList inValues = (ArrayList)filterClauses[0].getValue();
			                  if ( filterClauses[0].getOperator().equals(Consts.EQ) &&
			                       inValues.get(0).equals(Consts.IS_NULL)) {
			                    i =1;
			                    isExamineNull = true;
			                    where += "("+ map.get(attributeName) + " IS NULL) OR (";
			                  }
			                  if ( filterClauses[0].getOperator().equals(Consts.NEQ) &&
			                       inValues.get(0).equals(Consts.IS_NULL)) {
			                    i =1;
			                    isExamineNull = true;
			                    where += "("+ map.get(attributeName) + " IS NOT NULL) OR (";
			                  }
			                  for(int j=i;j<inValues.size();j++) {
			                    where +=
			                      map.get(attributeName) +
			                      " " + filterClauses[0].getOperator() +
			                      " '" + inValues.get(j) + "' AND ";
			                  }
			            }
			            }
				}
				
				String operator = filterClauses[0].getOperator();
				String value = filterClauses[0].getValue().toString();
				where += " and " + attributeName + " " + operator + " " + value;
			}
	        
			return QueryUtil.getQuery(conn, sql, vals, map,
					PIVSellerGridModel.class, "Y", "N", new GridParams(action,
							startIndex, filteredColumns, currentSortedColumns,
							currentSortedVersusColumns, new HashMap()), true);

		} catch (Exception ex) {
			ex.printStackTrace();
			return new ErrorResponse(ex.getMessage());
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception ex1) {
			}
		}

	}

}
