/*
 *	SimpleAudioRecorder.java
 *
 *	This file is part of jsresources.org
 */

/*
 * Copyright (c) 1999 - 2003 by Matthias Pfisterer
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 |<---            this code is formatted to fit into 80 columns             --->|
 */
package org.playiv.com.library.function;

import java.io.IOException;
import java.io.File;

import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineEvent.Type;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVUserNoteDFController;

/**
 * <titleabbrev>SimpleAudioRecorder</titleabbrev> <title>Recording to an audio
 * file (simple version)</title>
 * 
 * <formalpara><title>Purpose</title> <para>Records audio data and stores it in
 * a file. The data is recorded in CD quality (44.1 kHz, 16 bit linear, stereo)
 * and stored in a <filename>.wav</filename> file.</para></formalpara>
 * 
 * <formalpara><title>Usage</title> <para> <cmdsynopsis> <command>java
 * SimpleAudioRecorder</command> <arg choice="plain"><option>-h</option></arg>
 * </cmdsynopsis> <cmdsynopsis> <command>java SimpleAudioRecorder</command> <arg
 * choice="plain"><replaceable>audiofile</replaceable></arg> </cmdsynopsis>
 * </para></formalpara>
 * 
 * <formalpara><title>Parameters</title> <variablelist> <varlistentry>
 * <term><option>-h</option></term> <listitem><para>print usage information,
 * then exit</para></listitem> </varlistentry> <varlistentry>
 * <term><option><replaceable>audiofile</replaceable></option></term>
 * <listitem><para>the file name of the audio file that should be produced from
 * the recorded data</para></listitem> </varlistentry> </variablelist>
 * </formalpara>
 * 
 * <formalpara><title>Bugs, limitations</title> <para> You cannot select audio
 * formats and the audio file type on the command line. See AudioRecorder for a
 * version that has more advanced options. Due to a bug in the Sun jdk1.3/1.4,
 * this program does not work with it. </para></formalpara>
 * 
 * <formalpara><title>Source code</title> <para> <ulink
 * url="SimpleAudioRecorder.java.html">SimpleAudioRecorder.java</ulink> </para>
 * </formalpara>
 */
public class PIVSoundMngtFunc {

	private TargetDataLine m_line;
	private AudioFileFormat.Type m_targetType;
	private AudioInputStream m_audioInputStream;
	private File m_outputFile;

	/* private PIVGerGrvVoz gerGrvVoz; */
	private Clip clip;

	private JLabel audioInfoLabel;
	
	private static PIVSoundMngtFunc instance;
	
	private boolean isRecording = false;
	
	private PIVUserNoteDFController auto;
	
	public void setAuto(PIVUserNoteDFController auto) {
		this.auto = auto;
	}

	public boolean isRecording() {
		return isRecording;
	}

	public void setAudioInfoLabel(JLabel audioInfoLabel) {
		this.audioInfoLabel = audioInfoLabel;
	}

	public static PIVSoundMngtFunc getInstance( ) {
		if(instance==null) instance = new PIVSoundMngtFunc(); return instance; 
	}
	
	private PIVSoundMngtFunc( ) {

		try {
			clip = AudioSystem.getClip();
			clip.addLineListener(new LineListener() {
				@Override
				public void update(LineEvent e) {
					if(e.getType() == Type.CLOSE && !isRecording) {
						if(auto!=null)
							auto.executaPlayPauseProx();
						return;
					}
					if(e.getType() != Type.STOP)
						return;
					if(e.getSource() instanceof Clip) {
						Clip clip = (Clip) e.getSource();
						System.out.println(e.getType()); 
						if(!clip.isRunning()) {
							if(clip.isOpen()) {
								clip.flush();
								clip.close();
								PIVSoundMngtFunc.this.audioInfoLabel.setText("Grava��o terminada...");
							}
						}
					}
				}
			});
		} catch (LineUnavailableException e1) {
			e1.printStackTrace();
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
		}
		refresh( );
	}
	
	private void refresh( ) {
		AudioFormat audioFormat = new AudioFormat(
				AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F,
				false);

		DataLine.Info info = new DataLine.Info(TargetDataLine.class,
				audioFormat);

		TargetDataLine targetDataLine = null;
		try {
			targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
			targetDataLine.open(audioFormat);
		} catch (LineUnavailableException e) {
			out("unable to get a recording line");
			e.printStackTrace();
			/*System.exit(1);*/
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			this.audioInfoLabel.setText("Aguardando a��es...");
		}

		AudioFileFormat.Type targetType = AudioFileFormat.Type.WAVE;

		m_line = targetDataLine;
		m_audioInputStream = new AudioInputStream(targetDataLine);
		m_targetType = targetType;
	}

	public void stopRecording() {
		m_line.flush();
		m_line.close();
		try {
			m_audioInputStream.close();
		} catch (IOException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
		refresh( );
		this.isRecording = false;
		this.audioInfoLabel.setText("Grava��o terminada...");
	}

	/**
	 * Main working method. You may be surprised that here, just
	 * 'AudioSystem.write()' is called. But internally, it works like this:
	 * AudioSystem.write() contains a loop that is trying to read from the
	 * passed AudioInputStream. Since we have a special AudioInputStream that
	 * gets its data from a TargetDataLine, reading from the AudioInputStream
	 * leads to reading from the TargetDataLine. The data read this way is then
	 * written to the passed File. Before writing of audio data starts, a header
	 * is written according to the desired audio file type. Reading continues
	 * untill no more data can be read from the AudioInputStream. In our case,
	 * this happens if no more data can be read from the TargetDataLine. This,
	 * in turn, happens if the TargetDataLine is stopped or closed (which
	 * implies stopping). (Also see the comment above.) Then, the file is closed
	 * and 'AudioSystem.write()' returns.
	 */
	public void run() {

	}

	private static void printUsageAndExit() {
		out("SimpleAudioRecorder: usage:");
		out("\tjava SimpleAudioRecorder -h");
		out("\tjava SimpleAudioRecorder <audiofile>");
		System.exit(0);
	}

	private static void out(String strMessage) {
		System.out.println(strMessage);
	}

	public void record(String strFilename) {

	}

/*	public static synchronized void play(final String strFilename) {

		new Thread(new Runnable() { // the wrapper thread is unnecessary, unless
					// it blocks on the Clip finishing, see
					// comments
					public void run() {
						try {
							Clip clip = AudioSystem.getClip();
							AudioInputStream inputStream = AudioSystem
									.getAudioInputStream(new File(strFilename));
							clip.open(inputStream);
							clip.start();
							while(clip.isRunning()) { }
							clip.close();
						} catch (Exception e) {
							System.err.println(e.getMessage());
							PIVLogSettings.getInstance().error(e.getMessage(), e);
							PIVSoundMngtFunc.this.audioInfoLabel.setText("ERRO...");
						}
					}
				}).start();
	}*/

	public void comecarGravacao(String pathArquivoSom) {
		
		if (m_line.isRunning()) {
			stopRecording();
		}
		
		File outputFile = new File(pathArquivoSom);
		
		this.m_outputFile = outputFile;
		
		 /*if (this.gerGrvVoz == null) {
		 JOptionPane.showMessageDialog(MDIFrame.getInstance(),
		 "N�o � poss�vel iniciar a grava��o.",
		 "Erro ao tentar realizar a grava��o", JOptionPane.ERROR_MESSAGE);
		 return; }*/
		 

		this.audioInfoLabel.setText("Gravando......");
		
		/*JOptionPane.showMessageDialog(null,
				"Aperte ok para comecar a gravar...");*/

		m_line.start();

		new Thread(new Runnable() { // the wrapper thread is unnecessary, unless
					// it blocks on the Clip finishing, see
					// comments
					public void run() {
						try {
							AudioSystem.write(m_audioInputStream, m_targetType,
									m_outputFile);
						} catch (IOException e) {
							e.printStackTrace();
							PIVLogSettings.getInstance().error(e.getMessage(), e);
						}
					}
				}).start();

		out("Recording...");

		isRecording = true;
		
		/*stopRecording();*/
		/*out("Recording stopped.");*/

	}

	public void comecarReproducao( String pathArquivoSom ) throws UnsupportedAudioFileException,
			IOException {

		if (m_line.isRunning()) {
			stopRecording();
		}
		
		File outputFile = new File(pathArquivoSom);
		
		this.m_outputFile = outputFile;

		/*
		 * if (this.gerGrvVoz == null) { JOptionPane .showMessageDialog(
		 * MDIFrame.getInstance(),
		 * "N�o � poss�vel iniciar reprodu��o de som, n�o existe grava��o.",
		 * "Erro ao tentar realizar a reprodu��o", JOptionPane.ERROR_MESSAGE);
		 * return; }
		 */

		this.audioInfoLabel.setText("Mensagem iniciada...");
		
		AudioInputStream inputStream = AudioSystem
				.getAudioInputStream(m_outputFile);
		
		if (clip != null) {
			if(clip.isActive()) {
				clip.stop();
				clip.close();
			}
		}
		
		try {
			clip.open(inputStream);
			clip.start();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			this.audioInfoLabel.setText("Aguardando a��es...");
		}

	}

	public void pausarReproducao() {

		/*
		 * if (this.gerGrvVoz == null) { JOptionPane .showMessageDialog(
		 * MDIFrame.getInstance(),
		 * "N�o � poss�vel pausar reprodu��o de som, n�o existe reprodu��o sendo executada."
		 * , "Erro", JOptionPane.ERROR_MESSAGE); return; }
		 */

		if (clip.isActive()) {
			clip.stop();
		} else
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"N�o existe nenhuma reprodu��o sendo executada neste momento.",
							"Erro", JOptionPane.ERROR_MESSAGE);
		this.audioInfoLabel.setText("Aguardando a��es...");

	}

	public void pararReproducao() {

		this.audioInfoLabel.setText("Mensagem terminada...");
		
		/*
		 * if (this.gerGrvVoz == null) { JOptionPane .showMessageDialog(
		 * MDIFrame.getInstance(),
		 * "N�o � poss�vel pausar reprodu��o de som, n�o existe reprodu��o sendo executada."
		 * , "Erro", JOptionPane.ERROR_MESSAGE); return; }
		 */

		if (clip.isOpen()) {
			clip.drain();
			clip.close();
		} else
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"N�o existe nenhuma reprodu��o sendo executada neste momento.",
							"Erro", JOptionPane.ERROR_MESSAGE);
		this.audioInfoLabel.setText("Aguardando a��es...");

	}
	
	public boolean isSoundBeingUsed( ) {
		return this.clip == null ? false : this.clip.isOpen() || this.clip.isRunning();
	}

}