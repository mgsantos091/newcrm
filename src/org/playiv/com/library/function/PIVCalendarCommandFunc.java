package org.playiv.com.library.function;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.swing.general.k5n.k5ncal.AgendaCFView;

public class PIVCalendarCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;

	@Override
	public void execute() {
		System.out.println(this + " executado");
		/*Main app = new Main ();
		org.playiv.com.swing.component.src.us.k5n.k5ncal.Main adaptee = new org.playiv.com.swing.component.src.us.k5n.k5ncal.Main();*/
//		PIVK5nCalInternalFrameAdapter.incluirCalendario();
		AgendaCFView calendarioView = new AgendaCFView();
		MDIFrame.add(calendarioView, true);
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}
