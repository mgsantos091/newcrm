package org.playiv.com.library.function;

import java.util.ArrayList;

import org.playiv.com.library.general.IPIVElementPanel;

public class PIVSalesCommandSubjectFunc {

	private ArrayList<IPIVCommandFunc> commands;
	
	private IPIVElementPanel elementoAnterior;

	public PIVSalesCommandSubjectFunc( ) {
		commands = new ArrayList<IPIVCommandFunc>();
	}
	
	public void registerCommand( IPIVCommandFunc command ) {
		commands.add(command);
		System.out.println( command + " registrado" );
	}
	
	public void removeCommand( IPIVCommandFunc command ) {
		commands.remove(command);
		System.out.println( command + " removido" );
	}
	
	public void notifyObservers( IPIVElementPanel elemento ) {
		
		if(this.elementoAnterior!=null)
			this.elementoAnterior.desfazSeleciona();
		
		if(elemento!=null)		
			elemento.seleciona();
				
		for( IPIVCommandFunc command : commands ) {
			command.setElemento(elemento);
			System.out.println( command + " notificado" );
		}
		
		this.elementoAnterior = elemento;
		
	}
	
}