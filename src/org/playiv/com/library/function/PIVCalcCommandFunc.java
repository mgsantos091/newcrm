package org.playiv.com.library.function;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.library.general.PIVGlobalSettings;

public class PIVCalcCommandFunc implements IPIVCommandFunc {

	@Override
	public void execute() {
//		System.out.println(this + " executado");
//		PIVJCalcInternalFrameAdapter adapter = new PIVJCalcInternalFrameAdapter();
//		MDIFrame.add(adapter);
//		System.out.println("Calculadora incluida");
		 try {
			 if(PIVGlobalSettings.isWindows()) {
				 Runtime.getRuntime().exec("calc");
			 }
		} catch (IOException e) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o foi poss�vel abrir a calculadora do Windows.");
		}
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		// n�o faz nada
	}

}
