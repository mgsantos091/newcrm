package org.playiv.com.library.function;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.miscellaneous.client.AlertWindow;
import org.openswing.swing.miscellaneous.client.IconifableWindow;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVEmailFunc;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.controller.PIVCubeTreeFrameController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.model.PIVCubeFileTreeNodeModel;
import org.playiv.com.swing.general.PIVCubeTreeFrame;

public class PIVDesktopFunc {

	private Desktop desktop = Desktop.getDesktop();

	private static PIVDesktopFunc acoesDesktop;

	private PIVDao pdao = PIVDao.getInstance();

	private PIVGlobalSettings settings = PIVGlobalSettings.getInstance();

	private PIVDesktopFunc() {

	}

	public static PIVDesktopFunc getInstance() {
		// S� realiza o retorno de 'acoesDesktop' se o SO for suportado pela
		// fun��o
		if (acoesDesktop == null && Desktop.isDesktopSupported())
			acoesDesktop = new PIVDesktopFunc();
		return acoesDesktop;
	}

	public void lancarBrowser(PIVClientModel cliente) {
		if (cliente != null) {
			String site = cliente.getWebsite();
			if (!site.equals("")) {
				// Pergunta ao usu�rio se deseja abrir o site do cadastro do
				// cliente
				Object[] options = { "SITE", "DIGITAR" };
				int option = JOptionPane
						.showOptionDialog(null,
								"Site do cliente encontrado, deseja acessar ?",
								"Acessar site do cliente cadastrado",
								JOptionPane.DEFAULT_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, options,
								options[0]);
				if (option == 0) {
					if (!site.startsWith("http://")
							&& !site.startsWith("https://"))
						site = "http://" + site;
					try {
						desktop.browse(new URI(site));
					} catch (IOException ioe) {
						ioe.printStackTrace();
						PIVLogSettings.getInstance().error(ioe.getMessage(),
								ioe);
					} catch (URISyntaxException e) {
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
					}
				} else {
					site = (String) JOptionPane.showInputDialog(null,
							"Qual o site a ser acessado",
							"Favor digite o link do website",
							JOptionPane.DEFAULT_OPTION);
					if (!site.equals(""))
						if (!site.startsWith("http://")
								&& !site.startsWith("https://"))
							site = "http://" + site;
					cliente.setWebsite(site);
					pdao.update(cliente);
					try {
						desktop.browse(new URI(site));
					} catch (IOException ioe) {
						ioe.printStackTrace();
						PIVLogSettings.getInstance().error(ioe.getMessage(),
								ioe);
					} catch (URISyntaxException e) {
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
					}
				}
			} else {
				int option = JOptionPane
						.showConfirmDialog(
								null,
								"Nenhum site cadastrado para o cliente requisitado encontrado, deseja informar manualmente?",
								"", JOptionPane.YES_NO_OPTION);
				if (option == JOptionPane.YES_OPTION) {
					site = (String) JOptionPane.showInputDialog(null,
							"Qual o site a ser acessado",
							"Favor digite o link do website",
							JOptionPane.DEFAULT_OPTION);
					if (!site.equals(""))
						if (!site.startsWith("http://")
								&& !site.startsWith("https://"))
							site = "http://" + site;
					try {
						desktop.browse(new URI(site));
					} catch (IOException ioe) {
						ioe.printStackTrace();
						PIVLogSettings.getInstance().error(ioe.getMessage(),
								ioe);
					} catch (URISyntaxException e) {
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
					}
				}
			}
		} else {
			int option = JOptionPane
					.showConfirmDialog(
							null,
							"Nenhum cliente selecionado, deseja informar website manualmente?",
							"", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {
				String site = (String) JOptionPane.showInputDialog(null,
						"Qual o site a ser acessado",
						"Por Favor digite o link do website",
						JOptionPane.DEFAULT_OPTION);
				if (!site.equals(""))
					if (!site.startsWith("http://")
							&& !site.startsWith("https://"))
						site = "http://" + site;
				try {
					desktop.browse(new URI(site));
				} catch (IOException ioe) {
					ioe.printStackTrace();
					PIVLogSettings.getInstance().error(ioe.getMessage(), ioe);
				} catch (URISyntaxException e) {
					e.printStackTrace();
					PIVLogSettings.getInstance().error(e.getMessage(), e);
				}
			}
		}
	}

	public void enviarEmail(ArrayList<PIVContactModel> contatos,
			PIVClientModel cliente) {

		if (PIVGlobalSettings.isWindows()) {

			/*
			 * StringBuffer contatoBuff = new StringBuffer();
			 * for(PIVContactModel contato : contatos) {
			 * contatoBuff.append(contato.getEmail()); contatoBuff.append(";");
			 * }
			 */

			PIVCubeTreeFrameController controller = new PIVCubeTreeFrameController(
					cliente);
			PIVCubeTreeFrame tree = new PIVCubeTreeFrame(controller);
//			MDIFrame.getInstance().add(tree,true);

			if (tree.getOk().booleanValue()) {

				final String assunto = tree.getAssuntoEmail();
				String conteudo = tree.getCorpoEmail();

				/* StringBuffer attachBuff = new StringBuffer(); */

				/*
				 * attachBuff.replace(attachBuff.length()-1,
				 * attachBuff.length(), "");
				 */

				// envia o e-mail

				final PIVEmailFunc email_func = PIVEmailFunc.getInstance();

				final HtmlEmail email = email_func.createHtmlEmail();

				try {
					email.setFrom(PIVUserSession.getInstance()
							.getUsuarioSessao().getUsuario_email());
				} catch (EmailException e2) {
					PIVLogSettings.getInstance().error(e2.getMessage(), e2);
					e2.printStackTrace();
				}

				email.setSubject(assunto);
				try {
//					email.setMsg(conteudo);
//					email.setHtmlMsg(tree.getCorpoEmail());
					email.setHtmlMsg(conteudo);
				} catch (EmailException e1) {
					PIVLogSettings.getInstance().error(e1.getMessage(), e1);
					e1.printStackTrace();
				}

				boolean possuiEmailValido = false;
				// adiciona os emails dos contatos selecionados
				for (PIVContactModel contato : contatos) {
					if (contato.getEmail() != null
							&& !contato.getEmail().equals("")) {
						possuiEmailValido = true;
						try {
							email.addTo(contato.getEmail());
						} catch (EmailException e) {
							PIVLogSettings.getInstance().error(e.getMessage(),
									e);
							e.printStackTrace();
						}
					}
				}

				if (possuiEmailValido != true) {
					JOptionPane
							.showMessageDialog(
									MDIFrame.getInstance(),
									"N�o foram encontrados e-mails v�lidos, por favor, verifique os cadastros dos contatos selecionados e tente novamente",
									"E-mails inv�lido",
									JOptionPane.ERROR_MESSAGE);
					return;
				}

				@SuppressWarnings("unchecked")
				Iterator<OpenSwingTreeNode> iterator = ((Iterator<OpenSwingTreeNode>) tree
						.getTree().getCheckedLeaves().iterator());
				while (iterator.hasNext()) {
					OpenSwingTreeNode node = iterator.next();
					PIVCubeFileTreeNodeModel arquivo = (PIVCubeFileTreeNodeModel) node
							.getUserObject();
					if (arquivo.isFile) {
						EmailAttachment anexo = new EmailAttachment();
						anexo.setPath(arquivo.getPathCompleto());
						anexo.setDisposition(EmailAttachment.ATTACHMENT);
						anexo.setDescription(arquivo.getNome());
						anexo.setName(arquivo.getNome());
						try {
							email.attach(anexo);
						} catch (EmailException e) {
							PIVLogSettings.getInstance().error(e.getMessage(),
									e);
							e.printStackTrace();
						}
					}
				}

				/*
				 * TreePath[] paths = tree.getTree().getSelectionPaths();
				 * for(int x = 0;x<paths.length;x++) { PIVCubeFileTreeNodeModel
				 * arquivo = (PIVCubeFileTreeNodeModel) ((OpenSwingTreeNode)
				 * paths[x].getLastPathComponent()).getUserObject();
				 * 
				 * }
				 */

				new Thread(new Runnable()
		        {
		          public void run()
		          {
		        	  try {
							email_func.sendEmail(email);
//				              AlertWindow alertaEmailEnviadoComSucesso = new AlertWindow();
//				              alertaEmailEnviadoComSucesso.setImageName("gerenciamento_relcomercial/painel_operacao/enviar_email_32x32.png");
//				              alertaEmailEnviadoComSucesso.setMainText("O e-mail [" + assunto + "] foi enviado com sucesso");
//				              alertaEmailEnviadoComSucesso.setTitle("Status envio de e-mail");
//				              alertaEmailEnviadoComSucesso.setReduceToIconOnTimeout(true);
//				              alertaEmailEnviadoComSucesso.setTimeout(5000L);
//				              alertaEmailEnviadoComSucesso.anchorWindow(null, 1);
//				              alertaEmailEnviadoComSucesso.anchorWindow(MDIFrame.getInstance().getRootPane(), IconifableWindow.BOTTOM);
//				              alertaEmailEnviadoComSucesso.showWindow();
						} catch (EmailException e) {
							JOptionPane
									.showMessageDialog(
											MDIFrame.getInstance(),
											"Houve um erro no envio do e-mail, por favor, cheque suas configura��es de e-mail e sua conex�o com a internet.\n"
													+ "Caso necess�rio, entre em contato com o administrador do sistema",
											"Erro", JOptionPane.ERROR_MESSAGE);
							PIVLogSettings.getInstance().error(e.getMessage(), e);
							e.printStackTrace();
						}
		          }
		        }).start();

			}

			/*
			 * try { Runtime.getRuntime() .exec(new String[] { "rundll32",
			 * "url.dll,FileProtocolHandler", "mailto:" + contatoBuff.toString()
			 * + "?subject=someSubject&Attach=" + attachBuff.toString() },
			 * null);
			 * 
			 * } catch (Exception e) {
			 * PIVLogSettings.getInstance().error(e.getMessage(), e);
			 * e.printStackTrace(); }
			 */

		}
		/*
		 * if (Desktop.isDesktopSupported() &&
		 * desktop.isSupported(Desktop.Action.MAIL)) { String para = "";
		 * for(PIVContactModel contato : contatos) para += contato.getEmail() +
		 * ";"; try { URI emailPara = new URI("mailto:"+para+"");
		 * desktop.mail(emailPara); } catch (IOException e) {
		 * e.printStackTrace();
		 * PIVLogSettings.getInstance().error(e.getMessage(), e); } catch
		 * (URISyntaxException esex) { esex.printStackTrace();
		 * PIVLogSettings.getInstance().error(esex.getMessage(), esex); } }
		 */
	}

	/*
	 * public void abrirPrograma( File arquivo ) { try { desktop.open(arquivo);
	 * } catch (IOException e) {
	 * PIVLogSettings.getInstance().error(e.getMessage(), e);
	 * e.printStackTrace(); } }
	 */

	public void abrirPrograma(String filePath) {
		if (filePath == null || filePath.trim().length() == 0)
			return;
		if (!Desktop.isDesktopSupported())
			return;
		Desktop dt = Desktop.getDesktop();
		try {
			dt.browse(getFileURI(filePath));
		} catch (Exception ex) {
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			ex.printStackTrace();
		}
	}

	// generate uri according to the filePath
	private URI getFileURI(String filePath) {
		URI uri = null;
		filePath = filePath.trim();
		if (filePath.indexOf("http") == 0 || filePath.indexOf("\\") == 0) {
			if (filePath.indexOf("\\") == 0)
				filePath = "file:" + filePath;
			try {
				filePath = filePath.replaceAll(" ", "%20");
				URL url = new URL(filePath);
				uri = url.toURI();
			} catch (MalformedURLException ex) {
				PIVLogSettings.getInstance().error(ex.getMessage(), ex);
				ex.printStackTrace();
			} catch (URISyntaxException ex) {
				PIVLogSettings.getInstance().error(ex.getMessage(), ex);
				ex.printStackTrace();
			}
		} else {
			File file = new File(filePath);
			uri = file.toURI();
		}
		return uri;
	}

}
