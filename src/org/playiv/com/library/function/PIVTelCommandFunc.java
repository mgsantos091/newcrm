package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVTelContactGFController;
import org.playiv.com.mvc.controller.PIVVoipRecordGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVTelCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	public PIVTelCommandFunc() {

	}

	@Override
	public void execute() {
		System.out.println(this + " executado");
		if (this.elemento != null) {
			PIVClientModel cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
//			PIVBusRelMainCFController organizaCliente = ((PIVBusRelElementComponent)this.elemento).getOrganizacliente();
			PIVVoipRecordGFController controller = new PIVVoipRecordGFController( cliente );
//			PIVTelContactGFController controller = new PIVTelContactGFController( cliente , organizaCliente );
		}
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}