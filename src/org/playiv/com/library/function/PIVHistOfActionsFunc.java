package org.playiv.com.library.function;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import org.hibernate.Session;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVEventRegisterModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;
import org.playiv.com.swing.general.PIVHistoryOfActionsTableFunc;

import net.miginfocom.swing.MigLayout;

public class PIVHistOfActionsFunc  implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	@Override
	public void execute() {
		if (this.elemento != null) {

			PIVClientModel cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
			
			InternalFrame frame = new InternalFrame();
			frame.setTitle("HIST�RICO DE A��ES");
			frame.setSize(new Dimension(400, 600));
			frame.setLayout(new MigLayout());
			
			PIVHistoryOfActionsTableFunc tbl_HistoryOfActions = new PIVHistoryOfActionsTableFunc();
			ArrayList<PIVEventRegisterModel> eventos = getEventos(cliente);
			tbl_HistoryOfActions.carregar(eventos);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.getViewport().setBackground(Color.WHITE);
			scrollPane.setViewportView(tbl_HistoryOfActions);
			frame.add(scrollPane, "center , grow ");
			MDIFrame.add(frame);
			
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}
	
	public ArrayList<PIVEventRegisterModel> getEventos(PIVClientModel cliente) {
		String sql = "from Evento in class org.playiv.com.mvc.model.PIVEventRegisterModel where Evento.vendedor.id = " + PIVUserSession.getInstance().getVendedorSessao().getId() + " and Evento.cliente.id = " + cliente.getId();
		Session session = PIVDao.getInstance().getSession();
		@SuppressWarnings("unchecked")
		ArrayList<PIVEventRegisterModel> eventos = (ArrayList<PIVEventRegisterModel>) session.createQuery(sql).list();
		session.close();
		return eventos;
	}

}
