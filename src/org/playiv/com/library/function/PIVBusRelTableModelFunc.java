package org.playiv.com.library.function;

import java.awt.Color;

import javax.swing.table.AbstractTableModel;

import org.playiv.com.library.general.PIVRelBusCell;
import org.playiv.com.swing.general.PIVBusRelDefaultTableCellRenderer;

public class PIVBusRelTableModelFunc extends AbstractTableModel {

	private final int LINHA;
	private final int COLUNA;

	private PIVRelBusCell[][] RelacionamentoComercial;

	private final PIVBusRelDefaultTableCellRenderer pivrenderer;

	private int elementos;
	
	public PIVBusRelTableModelFunc(
			PIVRelBusCell[][] RelacionamentoComercial,
			PIVBusRelDefaultTableCellRenderer pivrenderer) {
		
		this.LINHA = RelacionamentoComercial.length;
		this.COLUNA = RelacionamentoComercial.length == 0 ? 0
				: RelacionamentoComercial[RelacionamentoComercial.length-1].length;
		
		this.pivrenderer = pivrenderer;
		
		this.RelacionamentoComercial = RelacionamentoComercial;
		limpaTabela();
		
	}

	@Override
	public int getColumnCount() {
		return this.COLUNA;
	}

	@Override
	public int getRowCount() {
		return this.LINHA;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return "";
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex >= 0 && columnIndex < this.COLUNA)
			return PIVRelBusCell.class;
		else
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= 0 && rowIndex < this.LINHA)
			if (columnIndex >= 0 && columnIndex < this.COLUNA)
				return this.RelacionamentoComercial[rowIndex][columnIndex];
			else
				throw new IndexOutOfBoundsException("columnIndex out of bounds");
		else
			throw new IndexOutOfBoundsException("rowIndex out of bounds");
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	};

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public void addCelulaPintada() {
		outerloop: for (int x = this.LINHA - 1; x >= 0; x--) {
			for (int y = this.COLUNA - 1; y >= 0; y--) {
				PIVRelBusCell celula = this.RelacionamentoComercial[x][y];
				if (!celula.isUsado()) {
					celula.setUsado(true);
					this.pivrenderer.colorirCelula(x, y, Color.BLUE);
					this.elementos ++;
					break outerloop;
				}
			}
		}
	}

	public void removeCelulaPintada() {
		outerloop: for (int x = 0;x < this.LINHA; x++) {
			for (int y = 0;y < this.COLUNA; y++) {
				PIVRelBusCell celula = this.RelacionamentoComercial[x][y];
				if (celula.isUsado()) {
					celula.setUsado(false);
					this.pivrenderer.colorirCelula(x, y, Color.WHITE);
					this.elementos --;
					break outerloop;
				}
			}
		}
	}

	public void limpaTabela() {
		for (int x = this.LINHA - 1; x >= 0; x--) {
			for (int y = this.COLUNA - 1; y >= 0; y--) {
				if (this.RelacionamentoComercial[x][y] == null)
					this.RelacionamentoComercial[x][y] = new PIVRelBusCell();
				this.RelacionamentoComercial[x][y].setUsado(false);
				this.pivrenderer.colorirCelula(x, y, Color.WHITE);
			}
		}
		fireTableDataChanged();
	}

	
	
	public int getTotalElementos( ) {
		return this.elementos;
	}

}