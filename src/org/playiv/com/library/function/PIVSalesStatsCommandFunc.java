package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVSalesElementComponent;

public class PIVSalesStatsCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVSalesStatsCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		PIVSellerModel vendedorSelecionado = null;
		if(this.elemento!=null)
			vendedorSelecionado = ( ( (PIVSalesElementComponent) elemento).getVendedor() );
		new PIVSalesStatsGFController(vendedorSelecionado);
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}