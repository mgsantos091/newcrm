package org.playiv.com.library.function;

import javax.swing.JLabel;

import org.openswing.swing.util.client.ClientSettings;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;

public class PIVBusRelClientInfoFunc {

	private JLabel lblClienteSelecionadoValor;
	private JLabel lblRCValor;
	private JLabel lblResponsavelValor;
	private JLabel lblOrigemProspectValor;
	private JLabel lblEmailValor;
	private JLabel lblEnderecoValor;

	private String defaulValue = String.format(String.format("%%0%dd", 30), 0)
			.replace("0", "-");

	public PIVBusRelClientInfoFunc(JLabel lblClienteSelecionadoValor,
			JLabel lblRCValor, JLabel lblResponsavelValor,
			JLabel lblOrigemProspectValor, JLabel lblEmailValor,
			JLabel lblEnderecoValor) {
		this.lblClienteSelecionadoValor = lblClienteSelecionadoValor;
		this.lblRCValor = lblRCValor;
		this.lblResponsavelValor = lblResponsavelValor;
		this.lblOrigemProspectValor = lblOrigemProspectValor;
		this.lblEmailValor = lblEmailValor;
		this.lblEnderecoValor = lblEnderecoValor;
	}

	public void atualizaPainel(PIVClientModel cliente,
			PIVBusRelMainCFController.nivel nivelenum) {
		PIVAddressModel endereco = cliente.getEndereco();
		String enderecoCliente;
		if (endereco == null)
			enderecoCliente = "";
		else {
			String pais = endereco.getPais() == null
					|| endereco.getPais().equals("") ? "" : endereco.getPais();
			String estado = endereco.getEstado() == null
					|| endereco.getEstado().equals("") ? "" : endereco
					.getEstado() + " - ";
			String cidade = endereco.getCidade() == null
					|| endereco.getCidade().equals("") ? "" : endereco
					.getCidade() + ", ";
			String bairro = endereco.getBairro() == null
					|| endereco.getBairro().equals("") ? "" : endereco
					.getBairro() + " - ";
			String logradouro = endereco.getLogradouro() == null
					|| endereco.getLogradouro().equals("") ? "" : endereco
					.getLogradouro() + ", ";
			String numero = cliente.getEndnumero() == null
					|| cliente.getEndnumero().toString().equals("") ? ""
					: cliente.getEndnumero().toString() + ", ";
			enderecoCliente = logradouro + numero + bairro + cidade + estado
					+ pais;
		}

		Integer codFonteLead = cliente.getFonte();
		String fonteLead =  codFonteLead == null ? null : ClientSettings.getInstance().getDomain("FONTE_LEAD").getDomainPair(codFonteLead).getDescription();
		
		PIVContactModel contatoResponsavel = PIVClientFunc
				.getResponsavel(cliente);
//		PIVContactModel contato = PIVClientFunc.getContato(cliente);

		this.lblRCValor.setText(PIVBusRelMainCFController
				.getNivelDesc(nivelenum));
		this.lblResponsavelValor
				.setText(contatoResponsavel == null ? defaulValue
						: contatoResponsavel.getNome());
		this.lblOrigemProspectValor.setText(fonteLead == null ? defaulValue : fonteLead);
		this.lblEmailValor.setText(cliente.getEmail() == null ? defaulValue
				: cliente.getEmail());
		this.lblEnderecoValor.setText(enderecoCliente.equals("") ? defaulValue
				: enderecoCliente);
	}

//	public void atualizaPainel(String lblClienteSelecionadoValor,
//			String lblRCValor, String lblResponsavelValor,
//			String lblContatoValor, String lblEmailValor,
//			String lblEnderecoValor) {
//
//		this.lblClienteSelecionadoValor
//				.setText(lblClienteSelecionadoValor == null
//						|| lblClienteSelecionadoValor == "" ? defaulValue
//						: lblClienteSelecionadoValor);
//
//		this.lblRCValor
//				.setText(lblRCValor == null || lblRCValor == "" ? defaulValue
//						: lblRCValor);
//
//		this.lblResponsavelValor.setText(lblResponsavelValor == null
//				|| lblResponsavelValor == "" ? defaulValue
//				: lblResponsavelValor);
//
//		this.lblOrigemProspectValor.setText(lblContatoValor == null
//				|| lblContatoValor == "" ? defaulValue : lblContatoValor);
//
//		this.lblEmailValor
//				.setText(lblEmailValor == null || lblEmailValor == "" ? defaulValue
//						: lblEmailValor);
//
//		this.lblEnderecoValor.setText(lblEnderecoValor == null
//				|| lblEnderecoValor == "" ? defaulValue : lblEnderecoValor);
//	}

	public void setLblClienteSelecionadoValor(String lblClienteSelecionadoValor) {
		this.lblClienteSelecionadoValor
				.setText(lblClienteSelecionadoValor == null
						|| lblClienteSelecionadoValor == "" ? "Nenhum cliente selecionado"
						: lblClienteSelecionadoValor);
	}

	public void setLblRCValor(String lblRCValor) {
		this.lblRCValor
				.setText(lblRCValor == null || lblRCValor == "" ? defaulValue
						: lblRCValor);
	}

	public void setLblResponsavelValor(String lblResponsavelValor) {
		this.lblResponsavelValor.setText(lblResponsavelValor == null
				|| lblResponsavelValor == "" ? defaulValue
				: lblResponsavelValor);
	}

	public void setLblContatoValor(String lblContatoValor) {
		this.lblOrigemProspectValor.setText(lblContatoValor == null
				|| lblContatoValor == "" ? defaulValue : lblContatoValor);
	}

	public void setLblEmailValor(String lblEmailValor) {
		this.lblEmailValor
				.setText(lblEmailValor == null || lblEmailValor == "" ? defaulValue
						: lblEmailValor);
	}

	public void setLblEnderecoValor(String lblEnderecoValor) {
		this.lblEnderecoValor.setText(lblEnderecoValor == null
				|| lblEnderecoValor == "" ? defaulValue : lblEnderecoValor);
	}

	public void resetaPainel() {
		this.lblRCValor.setText(defaulValue);
		this.lblResponsavelValor.setText(defaulValue);
		this.lblOrigemProspectValor.setText(defaulValue);
		this.lblEmailValor.setText(defaulValue);
		this.lblEnderecoValor.setText(defaulValue);
	}

}