package org.playiv.com.library.function;

import javax.swing.ImageIcon;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.swing.general.PIVFileExplorerInternalFrameAdapter;
import org.playiv.com.swing.general.PIVSalesElementComponent;

public class PIVSalesCubeCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();
	
	public PIVSalesCubeCommandFunc( ) {

	}
	
	@Override
	public void execute() {

		System.out.println(this + " executado");

		if (this.elemento != null) {

			String nomePastaUsuario = ((PIVSalesElementComponent)this.elemento).getVendedor().getUsuario().getId().toString();
			String arquivosPath = PIVGlobalSettings.getInstance().getDefaultArqsFile();
			String path = arquivosPath + nomePastaUsuario
					+ (PIVGlobalSettings.isWindows() ? "\\" : "//") + "arquivos_clientes" + (PIVGlobalSettings.isWindows() ? "\\" : "//");
			
			if (gerenciarDiretorio.criaDiretorio(path)) {
				org.playiv.com.library.fileexplorer.FileExplorer adaptee = new org.playiv.com.library.fileexplorer.FileExplorer(path,new ImageIcon(PIVFileExplorerInternalFrameAdapter.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_16x16.png")));
				PIVFileExplorerInternalFrameAdapter.incluirFileExplorer(adaptee);
			}
		}
		
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}