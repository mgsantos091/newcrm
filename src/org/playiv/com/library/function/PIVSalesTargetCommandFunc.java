package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVSalesTargetGFController;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVSalesElementComponent;

public class PIVSalesTargetCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;
	
	public PIVSalesTargetCommandFunc( ) {

	}

	@Override
	public void execute() {
		System.out.println(this + " executado");
		PIVSellerModel vendedor = null;
		if(this.elemento!=null)
			vendedor = ( ( (PIVSalesElementComponent) elemento).getVendedor() );
		PIVSalesTargetGFController controller = new PIVSalesTargetGFController( vendedor );
		System.out.println(elemento + " adicionado");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}