package org.playiv.com.library.function;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVAddressModel;

public class PIVAddressFunction {
	
	private static PIVDao pdao = PIVDao.getInstance();
	
	public static PIVAddressModel getEndereco(String cep) {
		String baseSQL = "from Endereco in class org.playiv.com.mvc.model.PIVAddressModel where Endereco.cep = '"
				+ cep + "'";
		Session session = pdao.getSession(); // obtain a JDBC connection and
												// instantiate a new Session
		PIVAddressModel vo = (PIVAddressModel) session.createQuery(baseSQL)
				.uniqueResult();
		session.close();
		return vo;

	}
	
}