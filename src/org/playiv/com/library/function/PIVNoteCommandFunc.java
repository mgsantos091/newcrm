package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVUserNoteGFController;
import org.playiv.com.mvc.view.PIVUserNoteGFView;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVNoteCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;

	@Override
	public void execute() {
		System.out.println(this + " executado");
		if(this.elemento!=null) {
			PIVUserNoteGFController controller = new PIVUserNoteGFController(((PIVBusRelElementComponent)elemento).getCliente(),((PIVBusRelElementComponent)elemento).getOrganizacliente());
			PIVUserNoteGFView view = new PIVUserNoteGFView( controller , ((PIVBusRelElementComponent)elemento).getCliente() );
			controller.setGrid(view.getGrid());
			MDIFrame.add(view);
			
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Nenhum cliente selecionado, selecione um cliente.","Selecione um cliente",JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}
