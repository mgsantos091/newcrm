package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.swing.general.PIVK5nCalInternalFrameAdapter;

public class PIVSalesCalendarCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVSalesCalendarCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		PIVK5nCalInternalFrameAdapter.incluirCalendario();
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}