package org.playiv.com.library.function;

import javax.swing.JLabel;

import org.playiv.com.mvc.model.PIVSellerModel;

public class PIVSellerInfoFunc {

	private JLabel lblVendedorSelecionadoValor;

	private String defaulValue = String.format(String.format("%%0%dd", 30), 0)
			.replace("0", "-");

	public PIVSellerInfoFunc( JLabel lblClienteSelecionadoValor ) {
		this.lblVendedorSelecionadoValor = lblClienteSelecionadoValor;
	}

	public void atualizaPainel( PIVSellerModel vendedor ) {
		String vendedorSelecionadoValor = vendedor.getId() + " - " + vendedor.getNome();
		setLblClienteSelecionadoValor(vendedorSelecionadoValor);
	}

	public void setLblClienteSelecionadoValor(String lblVendedorSelecionadoValor) {
		this.lblVendedorSelecionadoValor
				.setText(lblVendedorSelecionadoValor == null
						|| lblVendedorSelecionadoValor == "" ? "Nenhum cliente selecionado"
						: lblVendedorSelecionadoValor);
	}

	public void resetaPainel() {
		setLblClienteSelecionadoValor(null);
	}

}