package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public interface IPIVCommandFunc {

	PIVBusRelElementComponent elemento = null ;

	public abstract void execute();

	public abstract void setElemento( IPIVElementPanel elemento );

}