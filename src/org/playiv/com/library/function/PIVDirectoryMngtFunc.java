package org.playiv.com.library.function;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVUserModel;

public class PIVDirectoryMngtFunc {

	private PIVGlobalSettings globalSettings = PIVGlobalSettings.getInstance();

	private PIVUserModel usuarioLogado = PIVUserSession.getInstance()
			.getUsuarioSessao();

	private String logosPath;
	private String arquivosDeSomPath;
	private String arquivosDeClientePath;

	private String path_calendario;

	private String templateFilePath;

	private static PIVDirectoryMngtFunc gerenciarDiretorios;

	private PIVDirectoryMngtFunc() {

	}

	// Fun��o para realizar as checagens das pastas utilizadas pelo PlayIV
	public void checkOrganizacaoPastas() {

		System.out
				.println("Checando diret�rio padr�o de arquivos do PlayIV...");

		if (checkDiretorioDefault()) {

			// Cria diret�rio template
			
			String templateFilePath = globalSettings.getDEFAULT_RTF_TEMPLATE_PATH();
			try {
				if (new File(templateFilePath).mkdirs())
					System.out.println("Diret�rio: " + templateFilePath + " criado");
				if(new File(templateFilePath).listFiles().length == 0) {
					URL inputUrl = globalSettings.getDEFAULT_RTF_RESOURCE();
					File dest = new File(templateFilePath + "\\proposta_comercial.rtf");
					FileUtils.copyURLToFile(inputUrl, dest);
					
//					File file = FileUtils.toFile(globalSettings.getDEFAULT_RTF_RESOURCE());
//					FileUtils.copyFileToDirectory(file, new File(templateFilePath));
				}
			} catch (Exception e) {// Catch exception if any
				System.err.println("Erro ao criar diret�rio - '" + templateFilePath
						+ "': " + e.getMessage());
			}
			
			// Cria diret�rio dos logos dos clientes
			String logoPath = globalSettings.getDefaultPlayIVPath() + "logos"
					+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
			System.out
					.println("Checando diret�rio padr�o de arquivos de logos...");
			try {
				if (new File(logoPath).mkdirs())
					System.out.println("Diret�rio: " + logoPath + " criado");
				this.logosPath = logoPath;
			} catch (Exception e) {// Catch exception if any
				System.err.println("Erro ao criar diret�rio - '" + logoPath
						+ "': " + e.getMessage());
			}

			System.out.println("Checando diret�rio de log...");
			if(checkDiretorioLog()) {

			}
			
			System.out
					.println("Checando diret�rio padr�o de arquivos do vendedor...");
			if (checkDiretorioUsuario()) {

				String path = getDefaultPathDiretorioUsuario();

				// Cria diret�rio dos arquivos de som
				String path_som = path + "som_record"
						+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
				System.out
						.println("Checando diret�rio padr�o de arquivos de som do vendedor...");
				try {
					if (new File(path_som).mkdirs())
						System.out
								.println("Diret�rio: " + path_som + " criado");
					this.arquivosDeSomPath = path_som;
				} catch (Exception e) {// Catch exception if any
					System.err.println("Erro ao criar diret�rio - '" + path_som
							+ "': " + e.getMessage());
				}

				// Cria diret�rio dos arquivos dos clientes
				String path_cli_files = path + "arquivos_clientes"
						+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
				System.out
						.println("Checando diret�rio padr�o de arquivos dos clientes do vendedor...");
				try {
					if (new File(path_cli_files).mkdirs())
						System.out.println("Diret�rio: " + path_cli_files
								+ " criado");
					this.arquivosDeClientePath = path_cli_files;

				} catch (Exception e) {// Catch exception if any
					System.err.println("Erro ao criar diret�rio - '"
							+ path_cli_files + "': " + e.getMessage());
				}
				
				// Cria diret�rio dos calend�rios dos vendedores
				String path_calendario = globalSettings.getDEFAULT_CALENDARIO_PATH();
						/*+ (PIVGlobalSettings.isWindows() ? "\\" : "//");*/
				System.out
						.println("Checando diret�rio padr�o de calend�rio do vendedor...");
				try {
					if (new File(path_calendario).mkdirs())
						System.out.println("Diret�rio: " + path_calendario
								+ " criado");
					this.path_calendario = path_calendario;

				} catch (Exception e) {// Catch exception if any
					System.err.println("Erro ao criar diret�rio - '"
							+ path_calendario + "': " + e.getMessage());
				}

			}
		}
	}

	public static PIVDirectoryMngtFunc getInstance() {
		if (gerenciarDiretorios == null)
			gerenciarDiretorios = new PIVDirectoryMngtFunc();
		return gerenciarDiretorios;
	}

	private boolean checkDiretorioDefault() {

		String arquivosPath = globalSettings.getDefaultArqsFile();

		boolean checaDir = true;
		try {
			boolean sucesso = (new File(arquivosPath)).mkdirs();
			if (sucesso) {
				System.out.println("Diret�rio: " + arquivosPath + " criado");
			}
		} catch (Exception e) {// Catch exception if any
			checaDir = false;
			System.err.println("Erro ao criar diret�rio - '" + arquivosPath
					+ "': " + e.getMessage());
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

		return checaDir;
	}

	private boolean checkDiretorioUsuario() {
		String path = getDefaultPathDiretorioUsuario();
		boolean checaDir = true;
		try {
			boolean sucesso = (new File(path)).mkdir();
			if (sucesso)
				System.out.println("Diret�rio: " + path + " criado");
		} catch (Exception e) {// Catch exception if any
			checaDir = false;
			System.err.println("Erro ao criar diret�rio - '" + path + "': "
					+ e.getMessage());
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

		return checaDir;
	}

	private boolean checkDiretorioLog() {
		String path = globalSettings.getDEFAULT_LOG_PATH();
		boolean checaDir = true;
		try {
			boolean sucesso = (new File(path)).mkdir();
			if (sucesso)
				System.out.println("Diret�rio: " + path + " criado");
		} catch (Exception e) {// Catch exception if any
			checaDir = false;
			System.err.println("Erro ao criar diret�rio - '" + path + "': "
					+ e.getMessage());
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

		return checaDir;
	}
	
	public String getDefaultPathDiretorioUsuario() {
		/*String nomePastaUsuario = usuarioLogado.getId().toString() + "-"
				+ usuarioLogado.getlogin().toLowerCase() + "-"
				+ vendedorLogado.getNome().toLowerCase().replaceAll(" ", "_");*/
		String nomePastaUsuario = usuarioLogado.getId().toString();
		String arquivosPath = globalSettings.getDefaultArqsFile();
		String path = arquivosPath + nomePastaUsuario
				+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
		return path;
	}

	private void copiaLogoDirDefault( String nomeLogoUsuario , String origem ) {		
		// 08-abr-2012
		// Trecho de c�digo do programa Open Source - File Explorer
		// http://sourceforge.net/projects/fileexplorer/

		int lastFileSep = origem.lastIndexOf(File.separator) + 1;
		int lastDot = origem.lastIndexOf('.');

		// Set to end of file when no file extension exists
		if (lastDot == -1) {
			lastDot = origem.length();
		}

		String filename = origem.substring(lastFileSep, lastDot);
		String fileExt = origem.substring(lastDot, origem.length());

		// Atualiza o nome do arquivo no bd
		String fileNamePlusExt = nomeLogoUsuario + fileExt;
		
		String destino = getLogosPath();/* + nomeLogoUsuario + fileExt;*/
		
		copiaArquivosPServidor( origem , destino , fileNamePlusExt);	
	}
	
	public void copiaArquivosPServidor( String origem , String destino , String filename ) {
		String[] command;
		// For now add rntime exec in here
		// OS Specific ways of launching file into default handler

		int exitCode = -1;
		
		try {

			if (System.getProperty("os.name").contains("Win")) {
				// Force windows to COPY to the correct path
				if ((new File(destino)).isDirectory() == true)
					if (destino.endsWith(File.separator) == true)
						destino = destino + filename;
					else
						destino = destino + File.separator + filename;
								
				if ((new File(origem)).isDirectory() == true) {
					origem = "\"" + origem + "\"";
					destino = "\"" + destino + "\"";
					command = new String[14];
					command[0] = "cmd.exe";
					command[1] = "/c";
					command[2] = "XCOPY";
					command[5] = "/S"; // Copy Folders & Subfolder
					command[6] = "/E"; // Include Empty Folders & Subfolder
					command[7] = "/H"; // Copy System & hidden
					command[8] = "/C"; // Continue if Error
					command[9] = "/I"; // Assume dest is Folder if selecting
										// multiple and dest not exists
					command[10] = "/K"; // Copy attributes
					command[11] = "/X"; // Copy file audit settings
					command[12] = "/F"; // Output files coppied
					command[13] = "/Y"; // Forece Ovewrite if destination files
										// exist

/*					// Force windows to COPY to the correct path
					if ((new File(destino)).isDirectory() == true) {
						if (destino.endsWith(File.separator) == true)
							destino = destino + filename;
						else
							destino = destino + File.separator + filename;
					}*/

					command[3] = origem;
					command[4] = destino;

				} else {
					origem = "\"" + origem + "\"";
					destino = "\"" + destino + "\"";
					// File based Copy
					command = new String[6];
					command[0] = "cmd.exe";
					command[1] = "/c";
					command[2] = "COPY";
					command[3] = "/Y"; // Do not prompt
					command[4] = origem;
					command[5] = destino;
				}
			} else {
				// Not MAC or Windows
				// Assign default null values
				command = new String[1];
				command[0] = " ";
			}

			exitCode = runcommand(command);

			if (exitCode != 0) {
				System.out
						.println("Sess�o de c�pia de CORTAR foi fechado de maneira 'suja', insegura, para remover os arquivos originais");
				JOptionPane.showMessageDialog(null,
						"Erro na COPIA dos arquivos originais " + origem,
						"alert", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			System.out.println("PasteWorker.java:pasteHardWork - IOException");
			System.err.println(e.getMessage());
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		} catch (Exception e) {
			System.out.println("PasteWorker.java:pasteHardWork - Exception");
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

	}
	
	public void copiaLogoDirDefault(PIVClientModel cliente, String origem, String fileExt) {
		String nomeLogoUsuario = cliente.getId().toString();/* + "_"
		+ cliente.getNomefantasia();*/
		/*return */copiaLogoDirDefault(nomeLogoUsuario, origem);
	}
	
	public void copiaLogoDirDefault(String origem) {
		/*return */copiaLogoDirDefault("logo_empresa", origem);
	}

	private int runcommand(String[] command) throws IOException, Exception {
		for (int i = 0; i < command.length - 1; i++) {
			System.out.print(command[i] + " ");
		}

		Process p = Runtime.getRuntime().exec(command);

		InputStream is = p.getInputStream();
		try {
			int b;
			while ((b = is.read()) != -1)
				System.out.write(b);
		} finally {
			is.close();
		}
		int exitCode = p.waitFor();
		return exitCode;
	}

	public boolean criaDiretorio(String dir) {
		boolean checaDir = true;
		try {
			boolean sucesso = (new File(dir)).mkdir();
			if (sucesso)
				System.out.println("Diret�rio: " + dir + " criado");
		} catch (Exception e) {// Catch exception if any
			checaDir = false;
			System.err.println("Erro ao criar diret�rio - '" + dir + "': "
					+ e.getMessage());
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		return checaDir;
	}
	
	public String getLogosPath() {
		return this.logosPath;
	}

	public String getArquivosDeSomPath() {
		return this.arquivosDeSomPath;
	}

	public String getArquivosDeClientePath() {
		return this.arquivosDeClientePath;
	}
	
	public String getDiretorioTemp() {
		String dir = System.getProperty("user.home") + "\\PlayIV_temp\\";
		File temp = new File(dir);
		if(!temp.isDirectory())
			temp.mkdir();
		return dir;
		/*if(PIVGlobalSettings.isWindows()) {
			return "%temp%\\";
		} else return "";*/
	}

	public String getPath_calendario() {
		return path_calendario;
	}

	public String getTemplateFilePath() {
		return templateFilePath;
	}

}