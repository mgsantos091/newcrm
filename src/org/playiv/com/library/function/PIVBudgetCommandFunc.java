package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVSalesMngtGFController;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVBudgetCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	/*private PIVBusRelMainCFController organizaCliente;*/
	
	/*private PIVGerRelComPrincip telaPrincipal;*/
	
	public PIVBudgetCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		if(this.elemento!=null) {
			new PIVSalesMngtGFController( ((PIVBusRelElementComponent) elemento).getOrganizacliente() , ((PIVBusRelElementComponent) elemento).getCliente() );
			System.out.println(elemento + " tela 2 executado");
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
		/*if(this.elemento!=null)
			this.organizaCliente = ((PIVBusRelElementComponent) elemento).getOrganizacliente();*/
	}
	
/*	//temporario
	public void setGerRelComPrincip(PIVGerRelComPrincip telaPrincipal) {
		this.telaPrincipal = telaPrincipal;
	}*/

}