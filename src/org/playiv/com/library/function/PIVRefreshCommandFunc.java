package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMngtGFController;

public class PIVRefreshCommandFunc implements IPIVCommandFunc {

	private PIVBusRelMngtGFController busRelController;
	
	public PIVRefreshCommandFunc( ) {
		
	}
	
	public PIVRefreshCommandFunc(PIVBusRelMngtGFController controller) {
		this.busRelController = controller;
	}

	@Override
	public void execute() {
		
		System.out.println(this + " executado");
		
		this.busRelController.carregar();
		
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {

	}

}