package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;

public class PIVAddCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;
	
	private PIVBusRelMainCFController organizaCliente;
	
	public PIVAddCommandFunc( PIVBusRelMainCFController organizaCliente ) {
		this.organizaCliente = organizaCliente;
	}

	@Override
	public void execute() {
		
		System.out.println(this + " executado");
		
		PIVClientMngtDFController controller = new PIVClientMngtDFController(organizaCliente);
		//controller.getInternalFrame().setModal(true);
		//controller.getInternalFrame().setClosable(false);
		controller.getFrame().setMaximizable(false);
		
		// evento � registro dentro do pr�prio 'PIVClientMngtDFController', quando o mesmo possui associa��o com algum objeto do tipo 'PIVBusRelMainCFController' (indicando o v�nculo com o m�dulo de gerenciamento de relacionamento comercial)

		System.out.println(elemento + " adicionado");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}