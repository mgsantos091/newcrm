package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVNetCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	private PIVDesktopFunc funcoesDesktop = PIVDesktopFunc.getInstance();
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		PIVClientModel cliente = null;
		if(elemento!=null)
			cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
		funcoesDesktop.lancarBrowser(cliente);
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}
