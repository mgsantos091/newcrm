package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVSalesMngtGFController;
import org.playiv.com.swing.general.PIVSalesElementComponent;

public class PIVSalesBudgetCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVSalesBudgetCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		if(this.elemento!=null) {
			new PIVSalesMngtGFController( ( (PIVSalesElementComponent) elemento).getVendedor() );
			System.out.println(elemento + " tela 2 executado");
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Selecione um vendedor");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}