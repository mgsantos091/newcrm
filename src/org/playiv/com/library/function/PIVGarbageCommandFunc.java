package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVGarbageCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	private PIVBusRelMainCFController organizaCliente;
	
	/*private PIVGerRelComPrincip telaPrincipal;*/
	
	public PIVGarbageCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		if(this.elemento!=null) {
			Integer opt = JOptionPane.showConfirmDialog(MDIFrame.getInstance(),"Confirma a adi��o do " + organizaCliente.getNivelDesc() + " a lixeira?");
			if(opt == JOptionPane.YES_OPTION) {
				organizaCliente.addElemLixera(((PIVBusRelElementComponent)elemento).getCliente()); // o registro do evento esta no m�todo 'addElemLixera' de 'PIVBusRelMainCFController'
				/*this.telaPrincipal.getLblLixo().setIcon(new ImageIcon(PIVLixoCommand.class.getResource("/images/gerenciamento_relcomercial/lixo/lixo_cheio.png")));
				this.telaPrincipal.repaint();*/
				System.out.println(elemento + " removido");
			}
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
		if(this.elemento!=null)
			this.organizaCliente = ((PIVBusRelElementComponent)elemento).getOrganizacliente();
	}
	
/*	//temporario
	public void setGerRelComPrincip(PIVGerRelComPrincip telaPrincipal) {
		this.telaPrincipal = telaPrincipal;
	}*/

}