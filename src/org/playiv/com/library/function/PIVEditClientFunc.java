package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVEditClientFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVEditClientFunc( ) {

	}	

	@Override
	public void execute( ) {
		System.out.println( this + " executado" );
		if( this.elemento!=null ) {
			
			PIVClientModel cliente = ((PIVBusRelElementComponent)elemento).getCliente();
			new PIVClientMngtDFController(cliente.getId());

			System.out.println( elemento + " chamado para editar" );
		} else
			JOptionPane.showMessageDialog(MDIFrame.getSelectedFrame(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}