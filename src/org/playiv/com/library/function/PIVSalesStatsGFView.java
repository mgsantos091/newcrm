package org.playiv.com.library.function;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.util.java.Consts;

public class PIVSalesStatsGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	private GridControl grid = new GridControl();
	private JPanel buttonsPanel = new JPanel();
	private ReloadButton reloadButton = new ReloadButton();
	private FlowLayout flowLayout1 = new FlowLayout();
	private ExportButton exportButton = new ExportButton();
	private FilterButton filterButton = new FilterButton();
	
	private ComboColumn colNivelCliente = new ComboColumn();
	private IntegerColumn colIdVendedor = new IntegerColumn();
	private TextColumn colNomeVendedor = new TextColumn();
	private IntegerColumn colIdCliente = new IntegerColumn();
	private TextColumn colRazaoSocialCliente = new TextColumn();
	private TextColumn colNomeFantasiaCliente = new TextColumn();
	private IntegerColumn colQtdLigacoes = new IntegerColumn();
	private IntegerColumn colQtdEmails = new IntegerColumn();
	private IntegerColumn colQtdObs = new IntegerColumn();
	private IntegerColumn colQtdPropostas = new IntegerColumn();
	private IntegerColumn colDiasNesteNivel = new IntegerColumn();
	private DateColumn colFilDataInicio = new DateColumn();
	private DateColumn colFilDataFim = new DateColumn();
	
	public PIVSalesStatsGFView(PIVSalesStatsGFController controller) {
		try {
			
			jbInit();
			setSize(400, 300);
			grid.setController(controller);
			grid.setGridDataLocator(controller);
			grid.setShowFilterPanelOnGrid(false);

			setVisible(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {

		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setExportButton(exportButton);
		grid.setReloadButton(reloadButton);
		grid.setFilterButton(filterButton);
		grid.setRowHeight(40);

		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSellerGridModel");
		
		/*this.colNivelCliente = new ComboColumn();*/
		this.colNivelCliente.setColumnName("nivelComercial");
		this.colNivelCliente.setDomainId("NIVEL_COMERCIAL");
		this.colNivelCliente.setColumnSortable(true);
		this.colNivelCliente.setSortingOrder(1);
		this.colNivelCliente.setSortVersus(Consts.DESC_SORTED);
		grid.getColumnContainer().add(this.colNivelCliente);
		
		/*this.colIdVendedor = new IntegerColumn();*/
		this.colIdVendedor.setColumnName("idVendedor");
		grid.getColumnContainer().add(this.colIdVendedor);
		
		/*this.colNomeVendedor = new TextColumn();*/
		this.colNomeVendedor.setColumnName("nomeVendedor");
		grid.getColumnContainer().add(this.colNomeVendedor);
		
		/*this.colIdCliente = new IntegerColumn();*/
		this.colIdCliente.setColumnName("id_cliente");
		grid.getColumnContainer().add(this.colIdCliente);
		
		/*this.colRazaoSocialCliente = new TextColumn();*/
		this.colRazaoSocialCliente.setColumnName("razaosocial");
		grid.getColumnContainer().add(this.colRazaoSocialCliente);
		
		/*this.colNomeFantasiaCliente = new TextColumn();*/
		this.colNomeFantasiaCliente.setColumnName("nomefantasia");
		grid.getColumnContainer().add(this.colNomeFantasiaCliente);
		
		/*this.colQtdLigacoes = new IntegerColumn();*/
		this.colQtdLigacoes.setColumnName("qtdLigacoes");
		grid.getColumnContainer().add(this.colQtdLigacoes);
		
		/*this.colQtdEmails = new IntegerColumn();*/
		this.colQtdEmails.setColumnName("qtdEmails");
		grid.getColumnContainer().add(this.colQtdEmails);
		
		/*this.colQtdObs = new IntegerColumn();*/
		this.colQtdObs.setColumnName("qtdObs");
		grid.getColumnContainer().add(this.colQtdObs);
		
		/*this.colQtdPropostas = new IntegerColumn();*/
		this.colQtdPropostas.setColumnName("qtdPropostas");
		grid.getColumnContainer().add(this.colQtdPropostas);
		
		/*this.colDiasNesteNivel = new IntegerColumn();*/
		this.colDiasNesteNivel.setColumnName("diasNesteNivel");
		grid.getColumnContainer().add(this.colDiasNesteNivel);
		
		this.colFilDataInicio.setColumnName("filtro_dtinicio");
		this.colFilDataInicio.setColumnFilterable(true);
		this.colFilDataInicio.setColumnVisible(false);
		grid.getColumnContainer().add(this.colFilDataInicio);
		
		this.colFilDataFim.setColumnName("filtro_dtfim");
		this.colFilDataFim.setColumnFilterable(true);
		this.colFilDataFim.setColumnVisible(false);
		grid.getColumnContainer().add(this.colFilDataFim);
		
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(exportButton, null);
		buttonsPanel.add(filterButton, null);
		
		this.getContentPane().add(grid, BorderLayout.CENTER);
	    this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);

	}

	public GridControl getGrid() {
		return grid;
	}

}