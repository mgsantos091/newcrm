package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVMailContactGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVMailCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVMailCommandFunc( ) {

	}
	
	@Override
	public void execute() {
		System.out.println(this + " executado");
		if(this.elemento!=null) {
			PIVClientModel cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
			PIVBusRelMainCFController organizaCliente = ((PIVBusRelElementComponent)this.elemento).getOrganizacliente();
			new PIVMailContactGFController(cliente,organizaCliente); // o registro do evento esta dentro da classe
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}