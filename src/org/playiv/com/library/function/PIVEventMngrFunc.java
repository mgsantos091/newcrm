package org.playiv.com.library.function;

import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVEventRegisterModel;

public class PIVEventMngrFunc {

	private PIVDao pdao = PIVDao.getInstance();

	private static PIVEventMngrFunc eventmngr;

	private PIVUserSession userSession;

	private PIVEventMngrFunc() {
		this.userSession = PIVUserSession.getInstance();
	}

	public static PIVEventMngrFunc getInstance() {
		if (eventmngr == null)
			eventmngr = new PIVEventMngrFunc();
		return eventmngr;
	}

	// m�todo utilizado para realizar registro de eventos no playiv
	public void registerEvent(PIVClientModel cliente,
			Integer idevento ,
			PIVBusRelMainCFController.nivel nivel, evento evento,
			String descricao) {

		int nivelcomercial = 0;
		if(nivel!=null)
			nivelcomercial = PIVBusRelMainCFController.getNivelCod(nivel);

		PIVEventRegisterModel novoEvento = new PIVEventRegisterModel();
		novoEvento.setVendedor(this.userSession.getVendedorSessao());
		novoEvento.setCliente(cliente);
		novoEvento.setIdevento(idevento);
		novoEvento.setIdnivelcomercial(nivelcomercial);
		novoEvento.setNomeevento(getNomeEvento(evento));
		novoEvento.setDescricaoevento(descricao);

		pdao.save(novoEvento);

	}

	public enum evento {
		NOVO_CLIENTE, SUBIR_NIVEL_CLIENTE, LIGACAO, EMAIL, REGISTRO_OBSERVACAO, CUBO_CONHECIMENTO, CALENDARIO, LIXEIRA
	}

	public static String getNomeEvento(evento evento) {
		switch (evento) {
		case NOVO_CLIENTE:
			return "Novo cliente adicionado";
		case SUBIR_NIVEL_CLIENTE:
			return "Aumento do relacionamento comercial com cliente";
		case LIGACAO:
			return "Liga��o realizada para o cliente";
		case EMAIL:
			return "E-mail enviado ao cliente";
		case REGISTRO_OBSERVACAO:
			return "Nova observa��o (texto ou texto/�udio) adicionado referente ao cliente";
		case CUBO_CONHECIMENTO:
			return "Arquivo adiconado/removido referente ao cubo de conhecimento do cliente";
		case CALENDARIO:
			return "Evento adicionado/removido do calend�rio";
		case LIXEIRA:
			return "Cliente adicionado � lixeira";
		}
		return "";
	}

	public static Integer getCodEvento(evento evento) {
		switch (evento) {
		case NOVO_CLIENTE:
			return 1;
		case SUBIR_NIVEL_CLIENTE:
			return 2;
		case LIGACAO:
			return 3;
		case EMAIL:
			return 4;
		case REGISTRO_OBSERVACAO:
			return 5;
		case CUBO_CONHECIMENTO:
			return 6;
		case CALENDARIO:
			return 7;
		case LIXEIRA:
			return 8;
		default :
			return 0;
		}
	}
	
	public static evento getEventoCod( Integer idevento ) {
		switch (idevento) {
		case 1:
			return evento.NOVO_CLIENTE;
		case 2:
			return evento.SUBIR_NIVEL_CLIENTE;
		case 3:
			return evento.LIGACAO;
		case 4:
			return evento.EMAIL;
		case 5:
			return evento.REGISTRO_OBSERVACAO;
		case 6:
			return evento.CUBO_CONHECIMENTO;
		case 7:
			return evento.CALENDARIO;
		case 8:
			return evento.LIXEIRA;
		default:
			return null;
		}
	}

}