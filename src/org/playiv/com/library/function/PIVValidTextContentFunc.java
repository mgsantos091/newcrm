package org.playiv.com.library.function;

import org.openswing.swing.form.client.FormController;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.IPIVFormController;

public abstract class PIVValidTextContentFunc {

	private IPIVFormController formController;

	private PIVDao pdao = PIVDao.getInstance();

	public PIVDao getPdao() {
		return pdao;
	}

	public PIVValidTextContentFunc( ) { }
	
	public abstract boolean validaConteudo(String text,Integer pk);

	public boolean possuiController( ) {
		return this.formController != null;
	}

	public void setFormController(IPIVFormController formController) {
		this.formController = formController;
	}

}