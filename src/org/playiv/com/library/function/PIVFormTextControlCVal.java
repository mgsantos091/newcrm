package org.playiv.com.library.function;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import org.openswing.swing.client.FormattedTextControl;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.swing.general.PIVTextControlCValidation;

public class PIVFormTextControlCVal extends FormattedTextControl implements PIVTextControlCValidation {
	
	private static final long serialVersionUID = 1L;

	private IPIVFormController formController;

	private ArrayList<PIVValidTextContentFunc> validFunctions = new ArrayList<PIVValidTextContentFunc>();
	
	@Override
	public final void addValidFunction(PIVValidTextContentFunc function) {
		function.setFormController(this.formController);
		this.validFunctions.add(function);
	}
	
	@Override
	public final void removeValidFunction(PIVValidTextContentFunc function) {
		this.validFunctions.remove(function);
	}
	
	public PIVFormTextControlCVal( ) {
		super( );
		inicializarFocusListener( );
	}

	private void inicializarFocusListener() {
		
		getBindingComponent().addFocusListener(new FocusListener( ) {
			@Override
			public void focusLost(FocusEvent e) {
				for(PIVValidTextContentFunc funcao : validFunctions) {
					boolean isValidado = funcao.validaConteudo((String)getValue(),formController.getPk());
					if(!isValidado) {
						setText("");
						break;
					}
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public PIVFormTextControlCVal(IPIVFormController formController) {
		this( );		
		this.formController = formController;
	}
	
}