package org.playiv.com.library.function;

import javax.swing.JOptionPane;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVElementUPCommandFunc implements IPIVCommandFunc {
	
	private IPIVElementPanel elemento;
	
	public PIVElementUPCommandFunc( ) {

	}	

	@Override
	public void execute( ) {
		System.out.println( this + " executado" );
		if( this.elemento!=null ) {
			
			PIVBusRelMainCFController organizacliente = ((PIVBusRelElementComponent)elemento).getOrganizacliente();
			
			PIVClientModel cliente = ((PIVBusRelElementComponent)elemento).getCliente();
			organizacliente.sobeNivel(cliente, ((PIVBusRelElementComponent)elemento).getNivel_classificao()); // o evento � registrado dentro de 'PIVBusRelMainCFController' atrav�s do m�todo 'sobeNivel' 

			System.out.println( elemento + " movido para o pr�ximo box" );
		} else
			JOptionPane.showMessageDialog(MDIFrame.getSelectedFrame(),"Selecione um cliente");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}