package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVClientMoreInfoDFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVClientMoreInfoCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	public PIVClientMoreInfoCommandFunc() {

	}

	@Override
	public void execute() {
		System.out.println(this + " executado");
		if (this.elemento != null) {
			PIVClientModel cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
			new PIVClientMoreInfoDFController(cliente);
		}

	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}