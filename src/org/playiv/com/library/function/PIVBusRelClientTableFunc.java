package org.playiv.com.library.function;

import javax.swing.JTable;

import org.playiv.com.library.general.PIVRelBusCell;
import org.playiv.com.swing.general.PIVBusRelDefaultTableCellRenderer;

public class PIVBusRelClientTableFunc extends JTable {
	
	private PIVBusRelTableModelFunc pivtablemodel;
	private PIVBusRelDefaultTableCellRenderer pivrenderer;

	private static final long serialVersionUID = 1L;

	public PIVBusRelClientTableFunc( final int LINHAS_TABELAS , final int COLUNAS_TABELAS ) {
		super();

		PIVRelBusCell[][] RelacionamentoComercial = new PIVRelBusCell[LINHAS_TABELAS][COLUNAS_TABELAS];

		pivrenderer = new PIVBusRelDefaultTableCellRenderer();
		setDefaultRenderer(Object.class, pivrenderer);

		pivtablemodel = new PIVBusRelTableModelFunc(
				RelacionamentoComercial, pivrenderer);
		setModel(pivtablemodel);
	}

	public void addCelulaPintada() {
		this.pivtablemodel.addCelulaPintada();
		this.repaint();
	}

	public void removeCelulaPintada() {
		this.pivtablemodel.removeCelulaPintada();
		this.repaint();
	}

	public int getTotalElementos( ) {
		return this.pivtablemodel.getTotalElementos();
	}

}
