package org.playiv.com.library.function;

import java.util.ArrayList;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;


public class PIVClientFunc {

	public PIVClientFunc() {

	}

	public static PIVContactModel getResponsavel(PIVClientModel cliente) {

		if (cliente == null)
			throw new IllegalArgumentException();

		Integer idCliente = cliente.getId();

		PIVDao pdao = PIVDao.getInstance();

		String sqlCode = "from org.playiv.com.mvc.model.PIVContactModel as Contatos where Contatos.cliente = '" + idCliente + "' and  Contatos.responsavel = true ";
		Session session = pdao.getSession();
		
		@SuppressWarnings("unchecked")
		ArrayList<PIVContactModel> contatos = (ArrayList<PIVContactModel>) session
				.createQuery(sqlCode).list();
		
		session.close();
		
		for (PIVContactModel contato : contatos)
				return contato;
		
		return null;
	}

	public static PIVContactModel getContato(PIVClientModel cliente) {

		if (cliente == null)
			throw new IllegalArgumentException();

		Integer idCliente = cliente.getId();

		PIVDao pdao = PIVDao.getInstance();

		String sqlCode = "from org.playiv.com.mvc.model.PIVContactModel as Contatos where Contatos.cliente = '" + idCliente + "' and  Contatos.responsavel = false ";
		Session session = pdao.getSession();
		
		@SuppressWarnings("unchecked")
		ArrayList<PIVContactModel> contatos = (ArrayList<PIVContactModel>) session
				.createQuery(sqlCode).list();
		
		session.close();
		
		for (PIVContactModel contato : contatos)
			return contato;

		return null;
	}	
	
}