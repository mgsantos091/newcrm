package org.playiv.com.library.function;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVSellerMngtCFController;
import org.playiv.com.mvc.controller.PIVUserMngtDFController;

public class PIVAddSellerCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;
	
	private PIVSellerMngtCFController controller;
	
	public PIVAddSellerCommandFunc( PIVSellerMngtCFController controller ) {
		this.controller = controller;
	}

	@Override
	public void execute() {
		System.out.println(this + " executado");
		
		PIVUserMngtDFController cadastroUsuarioVendedor = new PIVUserMngtDFController(controller);
		//controller.getInternalFrame().setModal(true);
		//controller.getInternalFrame().setClosable(false);
		cadastroUsuarioVendedor.getFrame().setMaximizable(false);
		
		// evento � registro dentro do pr�prio 'PIVClientMngtDFController', quando o mesmo possui associa��o com algum objeto do tipo 'PIVBusRelMainCFController' (indicando o v�nculo com o m�dulo de gerenciamento de relacionamento comercial)

		System.out.println(elemento + " adicionado");
	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}