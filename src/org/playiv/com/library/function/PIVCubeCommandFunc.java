package org.playiv.com.library.function;

import javax.swing.ImageIcon;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;
import org.playiv.com.swing.general.PIVFileExplorerInternalFrameAdapter;

public class PIVCubeCommandFunc implements IPIVCommandFunc {

	private IPIVElementPanel elemento;

	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();

	public PIVCubeCommandFunc() {

	}

	@Override
	public void execute() {
		System.out.println(this + " executado");

		if (this.elemento != null) {

			PIVClientModel cliente = ((PIVBusRelElementComponent)this.elemento).getCliente();
			String nomePastaUsuario = cliente.getId().toString()/* + "_"
					+ cliente.getNomefantasia().replace(" ", "_")*/;
			String pathArquivos = gerenciarDiretorio.getArquivosDeClientePath()
					+ nomePastaUsuario
					+ (PIVGlobalSettings.isWindows() ? "\\" : "//");

			if (gerenciarDiretorio.criaDiretorio(pathArquivos)) {

				org.playiv.com.library.fileexplorer.FileExplorer adaptee = new org.playiv.com.library.fileexplorer.FileExplorer(pathArquivos,new ImageIcon(PIVFileExplorerInternalFrameAdapter.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_16x16.png")));
				PIVFileExplorerInternalFrameAdapter.incluirFileExplorer(adaptee);

			}

		}

	}

	@Override
	public void setElemento(IPIVElementPanel elemento) {
		this.elemento = elemento;
	}

}