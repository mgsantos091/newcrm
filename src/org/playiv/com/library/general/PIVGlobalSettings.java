package org.playiv.com.library.general;

import java.io.FileInputStream;
import java.net.URL;

//import java.io.File;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVLocalConfModel;
//import org.playiv.com.mvc.view.PIVClientMngtDFView;


public class PIVGlobalSettings {
	
	private static PIVGlobalSettings instance;
	
	private final String DEFAULT_PLAYIV_PATH;
	private final String DEFAULT_ARQ_PATH;
	private final String DEFAULT_LOG_PATH;
	private final String DEFAULT_DATABASE_PATH;
	private final String DEFAULT_RTF_OUTPUT_PATH;
	private final String DEFAULT_RTF_TEMPLATE_PATH;
	private final URL DEFAULT_RTF_RESOURCE;
	private final String DEFAULT_CALENDARIO_PATH;

	private PIVGlobalSettings( ) {
//		DEFAULT_RTF_RESOURCE = "proposta_comercial.rtf";
		DEFAULT_RTF_RESOURCE = PIVGlobalSettings.class.getResource("/proposta_comercial.rtf");
		if(isWindows()) {
//			DEFAULT_PLAYIV_PATH = "\\\\127.0.0.1\\playiv_local\\";
			DEFAULT_PLAYIV_PATH = System.getProperty("user.home") + "\\playiv\\";
			DEFAULT_ARQ_PATH = DEFAULT_PLAYIV_PATH + "arquivos\\";
			DEFAULT_LOG_PATH = DEFAULT_PLAYIV_PATH + "log\\";
			DEFAULT_DATABASE_PATH = DEFAULT_PLAYIV_PATH + "databases\\hsqldb\\";
			DEFAULT_RTF_OUTPUT_PATH = DEFAULT_PLAYIV_PATH + "output\\";
			DEFAULT_RTF_TEMPLATE_PATH = DEFAULT_PLAYIV_PATH + "rtf\\template\\";
			DEFAULT_CALENDARIO_PATH = DEFAULT_PLAYIV_PATH + "calendario\\";
		} else {
			DEFAULT_PLAYIV_PATH = "////home//playiv//";
			DEFAULT_ARQ_PATH = DEFAULT_PLAYIV_PATH + "arquivos//";
			DEFAULT_LOG_PATH = DEFAULT_PLAYIV_PATH + "log//";
			DEFAULT_DATABASE_PATH = DEFAULT_PLAYIV_PATH + "databases//hsqldb//";
			DEFAULT_RTF_OUTPUT_PATH = DEFAULT_PLAYIV_PATH + "output//";
			DEFAULT_RTF_TEMPLATE_PATH = DEFAULT_PLAYIV_PATH + "rtf//template//";
			DEFAULT_CALENDARIO_PATH = DEFAULT_PLAYIV_PATH + "calendario//";
		}
	}
	
	public static PIVGlobalSettings getInstance( ) {
		if( instance == null ) instance = new PIVGlobalSettings();
		return instance;
	}
	
	public String getDefaultArqsFile( ) {
		return this.DEFAULT_ARQ_PATH;
	}
	
	public String getDefaultPlayIVPath( ) {
		return this.DEFAULT_PLAYIV_PATH;
	}
	
	public String getDEFAULT_LOG_PATH() {
		return DEFAULT_LOG_PATH;
	}
	
	public String getDefaultDatabasePath( ) {
		return this.DEFAULT_DATABASE_PATH;
	}
	
	public PIVLocalConfModel getConfiguracoesLocais( ) {
		PIVDao pdao = PIVDao.getInstance();
		Session session = pdao.getSession();
		PIVLocalConfModel confLocal = (PIVLocalConfModel) session.createQuery("from org.playiv.com.mvc.model.PIVLocalConfModel as confLocal").uniqueResult();
		session.close();
		return confLocal;
	}
	
	// Trecho de c�digo do site Mkyong.com
	// How to detect OS in Java � System.getProperty(�os.name�)
	// http://www.mkyong.com/java/how-to-detect-os-in-java-systemgetpropertyosname/

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}
 
	public static boolean isMac() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("mac") >= 0);
	}
 
	public static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
	}
 
	public static boolean isSolaris() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("sunos") >= 0);
	}
	
//	public String getDEFAULT_RTF_TEMPLATE_PATH() {
//		return DEFAULT_RTF_TEMPLATE_PATH;
//	}
	
	public String getDEFAULT_RTF_OUTPUT_PATH() {
		return DEFAULT_RTF_OUTPUT_PATH;
	}

	public URL getDEFAULT_RTF_RESOURCE() {
		return DEFAULT_RTF_RESOURCE;
	}
	
	public String getDEFAULT_CALENDARIO_PATH() {
		return DEFAULT_CALENDARIO_PATH;
	}

	public String getDEFAULT_RTF_TEMPLATE_PATH() {
		return DEFAULT_RTF_TEMPLATE_PATH;
	}

}