package org.playiv.com.library.general;

import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVSalesElementComponent;

public class PIVSellerList {

	private final PIVSellerModel vendedor;

	private final IPIVElementPanel elemento;

	public PIVSellerList( PIVSellerModel vendedor , IPIVElementPanel elemento ) {
		this.vendedor = vendedor;
		this.elemento = elemento;
	}

	public PIVSellerModel getVendedor() {
		return vendedor;
	}

	public IPIVElementPanel getElemento() {
		return elemento;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PIVSalesElementComponent) {
			PIVSellerModel vendedor = ((PIVSalesElementComponent) obj).getVendedor();
			if(vendedor.getId() == this.vendedor.getId()) return true;
		} else if(obj instanceof PIVSellerList) {
			PIVSellerModel vendedor = (PIVSellerModel) ((PIVSellerList)obj).getVendedor();
			if(vendedor.getId() == this.vendedor.getId()) return true;
		} else if(obj instanceof PIVSellerModel) {
			PIVSellerModel vendedor = (PIVSellerModel) obj;
			if(vendedor.getId() == this.vendedor.getId()) return true;
		}
		return false;
	}

}