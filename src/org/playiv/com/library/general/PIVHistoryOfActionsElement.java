package org.playiv.com.library.general;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVEventRegisterModel;

public class PIVHistoryOfActionsElement extends JComponent {

	private static final long serialVersionUID = 1L;

	private PIVBusRelMainCFController organizacliente;
	
	private PIVEventRegisterModel evento;

	private JLabel lblImgElem;
	
	public PIVHistoryOfActionsElement( PIVEventRegisterModel evento ) {
	
		setLayout(new MigLayout());
		
		this.evento = evento;
				
		lblImgElem = new JLabel();
		
		if( evento != null ) {
		
			lblImgElem.setHorizontalAlignment(SwingConstants.CENTER);
			lblImgElem.setToolTipText(evento.getNomeevento());
			lblImgElem.setIcon(getIconByEvent(PIVEventMngrFunc.getEventoCod(evento.getIdevento())));
		
		}
		
		add(lblImgElem,"w 75! , h 75! , center");

	}
	
	/*public void setCliente( PIVClientModel cliente ) {
		this.cliente = cliente;
		lblNmElemen.setText(cliente.getNomefantasia());
		String imgpath = gerenciarDiretorio.getLogosPath()
				+ cliente.getNomeimagem();
		ImageIcon icone = new ImageIcon(imgpath);
		System.out.println(imgpath);
		lblImgElem.setIcon(icone);
	}

	public PIVClientModel getCliente( ) {
		return this.cliente;
	}*/

	public PIVBusRelMainCFController getOrganizacliente() {
		return organizacliente;
	}
	
	public ImageIcon getIconByEvent( PIVEventMngrFunc.evento evento ) {
		switch(evento) {
			case CALENDARIO:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_32x32.png"));
			case CUBO_CONHECIMENTO:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_32x32.png"));
			case EMAIL:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/email_32x32.png"));
			case LIGACAO:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_32x32.png"));
			case LIXEIRA:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/removercliente_32x32.png"));
			case NOVO_CLIENTE:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/addcliente_32x32.png"));
			case REGISTRO_OBSERVACAO:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_32x32.png"));
			case SUBIR_NIVEL_CLIENTE:
				return new ImageIcon(PIVHistoryOfActionsElement.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/up_32x32.png"));
			default:
				return null;
		}
	}
	
	public JLabel getLblImgElem() {
		return lblImgElem;
	}
	
	@Override
	public String toString() {
		return "";
	}
	
	public PIVEventRegisterModel getEvento() {
		return evento;
	}
	
}