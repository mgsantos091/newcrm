package org.playiv.com.library.general;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JLabel;

import org.playiv.com.mvc.model.PIVEventRegisterModel;


public class PIVHistoryOfActionsTableCellRow {
	
	private String dia = new String();
	
	private List<PIVHistoryOfActionsElement> colunas = new ArrayList<PIVHistoryOfActionsElement>();
	
	public PIVHistoryOfActionsTableCellRow( Timestamp dia ) {
		// incializa as variáveis
		this.dia = new SimpleDateFormat("d-MMM",new Locale("pt","BRA")).format(new Date(dia.getTime()));
	}
	
	public void addEvento( PIVEventRegisterModel evento ) {
		PIVHistoryOfActionsElement elemento = new PIVHistoryOfActionsElement(evento);
		colunas.add(elemento);
	}
	
	public Integer getColumnLength( ) {
		return colunas.size();
	}
	
	public Object getElementAt(int columnIndex) {

		if(columnIndex==0) return dia;
		else 
			columnIndex--;

		if(columnIndex < colunas.size()&&columnIndex>=0)
			return colunas.get(columnIndex);
		else
			return null;
	}
	
}
