package org.playiv.com.library.general;

public class PIVRelBusCell {
	
	private Boolean usado;

	public PIVRelBusCell(  ) {
		this.usado = false;
	}
	
	public Boolean isUsado() {
		return usado;
	}

	public void setUsado(Boolean usado) {
		this.usado = usado;
	}

	@Override
	public String toString() {
		return "";
	}
}