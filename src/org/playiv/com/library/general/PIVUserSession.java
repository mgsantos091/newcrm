package org.playiv.com.library.general;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVUserModel;
import org.playiv.com.mvc.model.PIVSellerModel;

//M�todo de Bill Pugh para Singleton Pattern
public class PIVUserSession {

	private PIVUserModel usuarioSessao;

	private PIVSellerModel vendedorSessao;

	// Construtor com o modificador de acesso private n�o permite a
	// possibilidade de instancia��o por outra classe
	private PIVUserSession() {
		PIVDao pdao = PIVDao.getInstance();
		Session session = pdao.getSession();
		
		// Busca usu�rio
//		String baseSQL = "from org.playiv.com.mvc.model.PIVUserModel as Usuario where Usuario.id = '1'";
		String baseSQL = "from org.playiv.com.mvc.model.PIVUserModel as Usuario order by Usuario.id asc";
		this.usuarioSessao = (PIVUserModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
		
		if(this.usuarioSessao==null) {
			
			PIVUserModel novoUsuarioPadrao = new PIVUserModel();
			novoUsuarioPadrao.setNome("playiv-teste");
			novoUsuarioPadrao.setlogin("teste");
//			novoUsuarioPadrao.setUsuario_voip("09650");
//			novoUsuarioPadrao.setSenha_voip("5066739837");
			novoUsuarioPadrao.setUsuario_email("testeplayiv@gmail.com");
			novoUsuarioPadrao.setSenha_email("sltmmzzmremkkmah");
			novoUsuarioPadrao.setUsuariovendedor(true);
//			novoUsuarioPadrao.setUsuariogerente(true);
			pdao.save(novoUsuarioPadrao);
			this.usuarioSessao = novoUsuarioPadrao;
			
			PIVSellerModel novoVendedorPadrao = new PIVSellerModel(this.usuarioSessao);
			pdao.save(novoVendedorPadrao);
			this.vendedorSessao = novoVendedorPadrao;
			
		} else {

			// Busca vendedor
//			baseSQL = "from org.playiv.com.mvc.model.PIVSellerModel as Vendedor where Vendedor.id = '1'";
			baseSQL = "from org.playiv.com.mvc.model.PIVSellerModel as Vendedor order by Vendedor.id asc";
			this.vendedorSessao = (PIVSellerModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
			
			if(this.vendedorSessao==null) {
				PIVSellerModel novoVendedorPadrao = new PIVSellerModel(this.usuarioSessao);
				pdao.save(novoVendedorPadrao);
				this.vendedorSessao = novoVendedorPadrao;
			}
			
		}
		
		session.close();
		
	}

	// PIVSessaoUsuarioHolder � carregada na primeira execu��o de
	// PIVSessaoUsuario.getInstance()
	// ou no primeiro acesso ao PIVSessaoUsuarioHolder.psessao, n�o antes
	private static class PIVSessaoUsuarioHolder {
		public static final PIVUserSession psessao = new PIVUserSession();
	}

	public static PIVUserSession getInstance() {
		return PIVSessaoUsuarioHolder.psessao;
	}

	public PIVSellerModel getVendedorSessao() {
		return this.vendedorSessao;
	}

	public PIVUserModel getUsuarioSessao() {
		return this.usuarioSessao;
	}
	
}