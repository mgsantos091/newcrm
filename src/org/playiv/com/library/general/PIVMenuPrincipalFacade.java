package org.playiv.com.library.general;

import org.openswing.swing.mdi.client.ClientFacade;
import org.playiv.com.library.jasperreports.PIVReportFactory;
import org.playiv.com.mvc.controller.PIVBusRelGarbageMngtGFController;
import org.playiv.com.mvc.controller.PIVCampaignCatMngtGFController;
import org.playiv.com.mvc.controller.PIVCampaignMngtGFController;
import org.playiv.com.mvc.controller.PIVCarrierMngtGFController;
import org.playiv.com.mvc.controller.PIVCatMngtGFController;
import org.playiv.com.mvc.controller.PIVClientMngtGFController;
import org.playiv.com.mvc.controller.PIVLocalConfDFController;
import org.playiv.com.mvc.controller.PIVPaymentMethodGFController;
import org.playiv.com.mvc.controller.PIVProdMngtGFController;
import org.playiv.com.mvc.controller.PIVSalesLevelMngtGFController;
import org.playiv.com.mvc.controller.PIVUserMngtGFController;
import org.playiv.com.mvc.view.PIVBusRelMainCFView;
import org.playiv.com.mvc.view.PIVSellerMngtCFView;

public class PIVMenuPrincipalFacade implements ClientFacade {

	private PIVReportFactory reportFactory = PIVReportFactory.getInstance();
	
	public PIVMenuPrincipalFacade( ) {

	}

	public void cadCliente( ) {
		new PIVClientMngtGFController();
	}

	public void cadUsuario( ) {
		new PIVUserMngtGFController( );
	}

	public void gerRelCom( ) {
		new PIVBusRelMainCFView();
	}
	
	public void gerVends( ) {
		new PIVSellerMngtCFView( );
	}

	public void cadProduto( ) {
		new PIVProdMngtGFController();
	}

	public void cadCategoria( ) {
		new PIVCatMngtGFController();
	}
	
	public void gerConfLocal( ) {
		new PIVLocalConfDFController();
	}
	
	public void cadCampanha( ) {
		new PIVCampaignMngtGFController( );
	}
	
	public void cadCategoriaCampanha( ) {
		new PIVCampaignCatMngtGFController( );
	}
	
	public void cadTransportadora( ) {
		new PIVCarrierMngtGFController( );
	}
	
	public void cadFormaPagamento( ) {
		new PIVPaymentMethodGFController( );
	}
	
	public void cadEstagioVenda( ) {
		new PIVSalesLevelMngtGFController( );
	}
	
	public void gerNaoVendas( ) {
		new PIVBusRelGarbageMngtGFController( );
	}
	
	public void relVendasCli( ) {
		reportFactory.abrirRelatorioVendasPorClientes();
	}
	
	public void relVendasVend( ) {
		reportFactory.abrirRelatorioVendasPorVendedor();
	}
	
	public void relCliVend( ) {
		reportFactory.abrirRelatorioClienterPorVendedor();
	}
	
	public void relVendasProd( ) {
		reportFactory.abrirRelatorioVendasPorProduto();
	}

}