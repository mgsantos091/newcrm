package org.playiv.com.library.general;

public class PIVMaskResources {
	
	public enum mask {
		CNPJ, CPF, CEP, TEL, CEL
	}
	
	public static String getMask( mask maskenum ) {
		switch(maskenum) {
			case CNPJ:
				return "##.###.###/####-##";
			case CPF:
				return "###.###.###-##";
			case CEP:
				return "##.###-###";
			case TEL:
				return "(###) ####-####";
			case CEL:
				return "(###) #####-####";
			default:
				return "";
		}
	}

}