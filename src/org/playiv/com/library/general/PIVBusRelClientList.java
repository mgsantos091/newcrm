package org.playiv.com.library.general;

import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVBusRelClientList {

	private final PIVClientModel cliente;

	private final PIVBusRelElementComponent elemento;

	public PIVBusRelClientList( PIVClientModel cliente , PIVBusRelElementComponent elemento ) {
		this.cliente = cliente;
		this.elemento = elemento;
	}

	public PIVClientModel getCliente() {
		return cliente;
	}

	public PIVBusRelElementComponent getElemento() {
		return elemento;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PIVBusRelElementComponent) {
			PIVClientModel cliente = ((PIVBusRelElementComponent) obj).getCliente();
			if(cliente.getId() == this.cliente.getId()) return true;
		} else if(obj instanceof PIVBusRelClientList) {
			PIVClientModel cliente = (PIVClientModel) ((PIVBusRelClientList)obj).getCliente();
			if(cliente.getId() == this.cliente.getId()) return true;
		} else if(obj instanceof PIVClientModel) {
			PIVClientModel cliente = (PIVClientModel) obj;
			if(cliente.getId() == this.cliente.getId()) return true;
		}
		return false;
	}

}