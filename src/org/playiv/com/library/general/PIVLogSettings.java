package org.playiv.com.library.general;

import java.io.File;
import java.io.IOException;  
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;  
import org.apache.log4j.Level;  
import org.apache.log4j.Appender;  
import org.apache.log4j.FileAppender;  
import org.apache.log4j.PatternLayout;  
import org.apache.log4j.BasicConfigurator; 
import org.openswing.swing.mdi.client.MDIFrame;

public class PIVLogSettings {
	
	private PIVGlobalSettings globalSettings = PIVGlobalSettings.getInstance();
	
	private Logger logger = Logger.getLogger(PIVLogSettings.class);
	
	private final String LOG_FILE = "playiv-error.log";
	private String ip;
	
	private static PIVLogSettings logsettings;
	
	private PIVLogSettings( ) {
		
		ip = "DESCONHECIDO";
		try {
			ip = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		BasicConfigurator.configure();
		logger.setLevel(Level.ALL);
		
		Appender fileAppender = null;
		try {
			fileAppender = new FileAppender(new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN), globalSettings.getDEFAULT_LOG_PATH() + LOG_FILE);
			logger.addAppender(fileAppender);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(), "N�o foi poss�vel criar o arquivo " + LOG_FILE + " em: '" + globalSettings.getDEFAULT_LOG_PATH() + "'.\n" +
					" N�o vai ser poss�vel realizar o log dos erros no sistema.\n" +
					"Entre em contato com o administrador.\n" + 
					e.getMessage());
			e.printStackTrace();
		}

	}
	
	public static PIVLogSettings getInstance( ) {
		if( logsettings == null ) logsettings = new PIVLogSettings();
		return logsettings;
	}
	
	public void iniciar( ) {
		Format formatter = new SimpleDateFormat("dd/mm/aaaa �s hh:mm:ss");
		logger.info("DEBUG PLAYIV - " + ip + " - " + formatter.format(new Date()) + " - INICIALIZANDO APLICA��O...");
	}
	
	public void encerrar( ) {
		Format formatter = new SimpleDateFormat("dd/mm/aaaa �s hh:mm:ss");
		logger.info("DEBUG PLAYIV - " + ip + " - " + formatter.format(new Date()) + " - ENCERRANDO APLICA��O...");
	}
	
	public void error(Object message , Throwable t) {
		if(this.logger!=null) {
			this.logger.error(message, t);
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");
	}
	
	public void error( Object message ) {
		if(this.logger!=null) {
			this.logger.error(message);
		} else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");
	}
	
	public void info( Object message ) {
		if(this.logger!=null) {
			this.logger.info(message);
		}/* else
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Erro detectado: " + message + " n�o foi poss�vel realizar o log do erro, favor entrar em contato com o administrador.");*/
	}
	
	public void debug( Object message ) {
		if(this.logger!=null) {
			this.logger.debug(message);
		}
	}

}