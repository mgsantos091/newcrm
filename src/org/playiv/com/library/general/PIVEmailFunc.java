package org.playiv.com.library.general;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVLocalConfModel;

public class PIVEmailFunc {
	
	private static PIVEmailFunc instance;
	
	private PIVUserSession sessao_usuario = PIVUserSession.getInstance();
	
	private PIVDao pdao = PIVDao.getInstance();
	
	private PIVLocalConfModel configuracao_local;
	
	private PIVEmailFunc( ) {
		
		// carrega as configuracoes locais
		
		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = pdao.getSession();

		PIVLocalConfModel vo = (PIVLocalConfModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();
		
		this.configuracao_local = vo;
		
	}
	
	public static PIVEmailFunc getInstance( ) {
		if( instance == null ) instance = new PIVEmailFunc();
		return instance;
	}
	
	public HtmlEmail createHtmlEmail( ) {
		
		String hostname = configuracao_local.getEmail_server_smpt_host();
		int port = configuracao_local.getEmail_server_smpt_port();
		boolean ssl = configuracao_local.isEmail_server_ssl();
		
		String usuario = sessao_usuario.getUsuarioSessao().getUsuario_email();
		String password = sessao_usuario.getUsuarioSessao().getSenha_email();
		
		HtmlEmail email = new HtmlEmail();
		email.setHostName(hostname);
		email.setSmtpPort(port);
		email.setAuthenticator(new DefaultAuthenticator(usuario, password));
		email.setSSL(ssl);
		
		return email;
	}
	
	public String sendEmail( Email email ) throws EmailException {
		return email.send();
	}
	
}
