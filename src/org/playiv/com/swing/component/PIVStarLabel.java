package org.playiv.com.swing.component;

public class PIVStarLabel extends PIVAutoReSizeLabel {

	private static final long serialVersionUID = 1L;
	
	private Integer nivel;

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

}
