package org.playiv.com.swing.general;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.component.PIVAutoReSizeLabel;

public class PIVBusRelElementComponent extends JComponent implements IPIVElementPanel {

	private static final long serialVersionUID = 1L;

	private PIVBusRelMainCFController organizacliente;

	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();
	
	private PIVBusRelLevelLayerPanel boxRelacionado;
	private PIVClientModel cliente;

	private PIVAutoReSizeLabel lblImgElem;
	private JLabel lblNmElemen;
	
	/*private final Integer NUMESTRELAS = 5;
	
	private ArrayList<PIVStarLabel> estrelasLabel = new ArrayList<PIVStarLabel>();*/

	private Integer nivel_classificao;

	public PIVBusRelElementComponent( PIVBusRelMainCFController organizacliente /*, Integer nivelClassificao*/ ) {

		setLayout(new MigLayout());
		
		this.organizacliente = organizacliente;
		
		lblImgElem = new PIVAutoReSizeLabel();
		lblImgElem.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lblImgElem,"w 100! , h 75! , top");
		
		lblNmElemen = new JLabel();
		lblNmElemen.setForeground(new Color(0,0,255));
		lblNmElemen.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lblNmElemen,"w 100! , south");

		desfazSeleciona( );
		
		/*nivel_classificao = nivelClassificao;
		
		for(int x=1;x<=NUMESTRELAS;x++) {
			PIVStarLabel lblEstrelaVazia = new PIVStarLabel();
			lblEstrelaVazia.setNivel(x);
			estrelasLabel.add(lblEstrelaVazia);
			if(x<=nivel_classificao)
				lblEstrelaVazia.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/gerenciamento_relcomercial/nivel_comercial/estrela_cheia.png")));
			else
				lblEstrelaVazia.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/gerenciamento_relcomercial/nivel_comercial/estrela_vazia.png")));
			lblEstrelaVazia.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount()>=2) {
						PIVStarLabel lblEstrela = (PIVStarLabel) e.getSource();
						PIVBusRelModel nivelRelacionamentoComercial = PIVBusRelElementComponent.this.organizacliente.getNivelRelacionamentoSelecionado();
						nivel_classificao = lblEstrela.getNivel();
						nivelRelacionamentoComercial.setNivel_classificao(nivel_classificao);
						PIVDao.getInstance().update(nivelRelacionamentoComercial);
						for(PIVStarLabel lblEstrelaIterate : estrelasLabel) {
							Integer nivelClassificao = lblEstrelaIterate.getNivel();
							if(nivelClassificao <= nivel_classificao)
								lblEstrelaIterate.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/gerenciamento_relcomercial/nivel_comercial/estrela_cheia.png")));
							else
								lblEstrelaIterate.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/gerenciamento_relcomercial/nivel_comercial/estrela_vazia.png")));
						}
						PIVBusRelElementComponent.this.repaint();
					}
				}
			});
			add(lblEstrelaVazia,"w 18! , h 18! , top");
		}*/
		
	}
	
/*	public PIVBusRelElementComponent( PIVClientModel cliente ) {
		
		setLayout(new MigLayout());
		
		setCliente(cliente);
		
		lblImgElem = new PIVAutoReSizeLabel();
		lblImgElem.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgElem.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		add(lblImgElem,"w 150! , h 150! , top");
		
		lblNmElemen = new JLabel();
		lblNmElemen.setHorizontalAlignment(SwingConstants.CENTER);
		lblNmElemen.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		
		add(lblNmElemen,"w 150! , south");

	}*/

	public void setCliente( PIVClientModel cliente ) {
		this.cliente = cliente;
		if(this.cliente.getNomeimagem()==null||this.cliente.getNomeimagem().equals("")) {
			int nivel = PIVBusRelMainCFController.getNivelCod(this.organizacliente.getNivelcomercial());
//			lblImgElem.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/cadastros/cliente/guerrero_padrao.png")));
			lblImgElem.setIcon(new ImageIcon(PIVBusRelElementComponent.class.getResource("/images/cadastros/cliente/build" + nivel + "_128x128.png")));
		}
		else {
			String imgpath = gerenciarDiretorio.getLogosPath()
					+ cliente.getNomeimagem();
			ImageIcon icone = new ImageIcon(imgpath);
			System.out.println(imgpath);
			lblImgElem.setIcon(icone);
		}
		lblNmElemen.setText(cliente.getNomefantasia());
	}

	public PIVClientModel getCliente( ) {
		return this.cliente;
	}

	public PIVBusRelLevelLayerPanel getBoxRelacionado() {
		return boxRelacionado;
	}

	public void setBoxRelacionado(PIVBusRelLevelLayerPanel boxRelacionado) {
		this.boxRelacionado = boxRelacionado;
	}

	public PIVBusRelMainCFController getOrganizacliente() {
		return organizacliente;
	}
	
	public void seleciona( ) {
		this.lblImgElem.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		this.lblNmElemen.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		this.lblNmElemen.setForeground(Color.BLACK);
	}
	
	public void desfazSeleciona( ) {
		this.lblImgElem.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		this.lblNmElemen.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		this.lblNmElemen.setForeground(Color.BLACK);
	}

	public Integer getNivel_classificao() {
		return nivel_classificao;
	}
	
}