package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.hibernate.Query;
import org.hibernate.Session;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.lookup.client.LookupParent;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.columns.client.CodLookupColumn;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DecimalColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.table.java.GridDataLocator;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.controller.PIVCampaignLUFController;
import org.playiv.com.mvc.controller.PIVProductLUFController;
import org.playiv.com.mvc.controller.PIVProductSaleGFController;
import org.playiv.com.mvc.controller.PIVSalesMoreInfoDFController;
import org.playiv.com.mvc.model.PIVCampaignModel;
import org.playiv.com.mvc.model.PIVProductModel;
import org.playiv.com.mvc.model.PIVSalesDetailModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.model.PIVSalesXCampaignModel;
import org.playiv.com.mvc.view.PIVUserNoteDFView;

public class PIVProductSalesPanel extends JPanel implements LookupParent {

	private static final long serialVersionUID = 1L;

	private JPanel buttonsPanel;

	private SaveButton saveButton;
	private InsertButton insertButton;
	private EditButton editButton;
	private DeleteButton deleteButton;
	private ReloadButton reloadButton;
	
	private JButton expandirButton;
	private JButton detalhesButton;
	/*private JButton campanhaButton;
	private JButton observacaoButton;*/
	
	private GridControl grid = new GridControl();

	private PIVDao pdao = PIVDao.getInstance();

	private PIVProductSaleGFController controller;

	private JPanel jpCentroPainel = new JPanel();

	public JPanel getJpCentroPainel() {
		return jpCentroPainel;
	}

	public PIVProductSalesPanel(final PIVProductSaleGFController controller, boolean showMaximiza)
			throws ParseException {

		this.controller = controller;
		this.controller.setGrid(grid);

		/*grid.mergeCells(new int[]{0,1},new int[]{0,1});*/
		grid.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		/*grid.setPreferredSize(new Dimension(400,300));*/
		
		/*grid.setSize(new Dimension(400,300));*/
		
		setLayout(new MigLayout());

		/*JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));*/

		/*JLabel lblTituloPainel = new JLabel("VENDAS");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		lblTituloPainel.setFont(new Font("Arial", Font.BOLD, 12));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");*/

		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[][grow]", "[top][grow,fill][]"));

		/* JLabel lblImgTopico = new JLabel(); */
		/* lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK)); */
		/*
		 * lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		 * lblImgTopico.setIcon(new ImageIcon(PIVContactPanel.class.getResource(
		 * "/images/cadastros/cliente/contato_60x60.png")));
		 * jpCentroPainel.add(lblImgTopico, "w 60! , h 60! , left");
		 */

		buttonsPanel = new JPanel();
		buttonsPanel.setBackground(new Color(245, 245, 245));

		jpCentroPainel.add(buttonsPanel,
				"h 50! , growx , spanx , bottom , wrap");

		grid.setBackground(new Color(245, 245, 245));
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSalesDetailModel");
		grid.setAutoLoadData(true);
		grid.setVisibleStatusPanel(false);

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		insertButton = new InsertButton();
		if(showMaximiza)
			insertButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					expandirButton.setEnabled(false);
				}
			});
		buttonsPanel.add(insertButton);

		grid.setInsertButton(insertButton);

		editButton = new EditButton();
		buttonsPanel.add(editButton);
		grid.setEditButton(editButton);

		deleteButton = new DeleteButton();
		buttonsPanel.add(deleteButton);
		grid.setDeleteButton(deleteButton);

		saveButton = new SaveButton();
		if(showMaximiza)
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					expandirButton.setEnabled(true);
				}
			});
		buttonsPanel.add(saveButton);
		grid.setSaveButton(saveButton);

		if(showMaximiza) {
			
			expandirButton = new JButton();
			expandirButton.setPreferredSize(new Dimension(32,32));
			expandirButton.setIcon(new ImageIcon(PIVProductSalesPanel.class.getResource("/images/cadastros/vendas/expandir_24x24.png")));
			expandirButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					PIVProductSalesDetailPanel frame = new PIVProductSalesDetailPanel(PIVProductSalesPanel.this.controller.getVendasPai(),PIVProductSalesPanel.this.controller.getCliente(),PIVProductSalesPanel.this);
					MDIFrame.add(frame);
				}
			});
			buttonsPanel.add(expandirButton);
		
		}
		
		/*campanhaButton = new JButton();
		campanhaButton.setPreferredSize(new Dimension(32,32));
		campanhaButton.setIcon(new ImageIcon(PIVProductSalesPanel.class.getResource("/images/cadastros/campanha/campanha_24x24.png")));
		campanhaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVCampaignLUFController controller = new PIVCampaignLUFController();
				controller.openLookupFrame(MDIFrame.getSelectedFrame(), PIVProductSalesPanel.this);

				PIVCampaignModel campanha = (PIVCampaignModel) controller.getLookupVO();
				if(campanha!=null) {
					String sql = "from org.playiv.com.mvc.model.PIVSalesXCampaignModel where venda.id = " + PIVProductSalesPanel.this.controller.getVendasPai().getId();
					Session sessao = pdao.getSession();
					PIVSalesXCampaignModel vendaXCampanha = (PIVSalesXCampaignModel) sessao.createQuery(sql).uniqueResult();
					sessao.close();
					if(vendaXCampanha!=null) {
						int option = JOptionPane
								.showConfirmDialog(
										MDIFrame.getSelectedFrame(),
										"J� existe uma campanha atrelado a esta proposta comercial (" + vendaXCampanha.getCampanha().getNome_campanha() + "), deseja alter�-la?",
										"Alterar campanha?",
										JOptionPane.YES_NO_OPTION);
						if (option == JOptionPane.YES_OPTION) {
							vendaXCampanha.setCampanha(campanha);
							pdao.update(vendaXCampanha);
						}
					} else
					{
						vendaXCampanha = new PIVSalesXCampaignModel();
						vendaXCampanha.setCampanha(campanha);
						vendaXCampanha.setVenda(PIVProductSalesPanel.this.controller.getVendasPai());
						pdao.save(vendaXCampanha);
					}
				}
			}
		});
		
		buttonsPanel.add(campanhaButton);
		
		observacaoButton = new JButton();
		observacaoButton.setPreferredSize(new Dimension(32,32));
		observacaoButton.setIcon(new ImageIcon(PIVProductSalesPanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_24x24.png")));
		observacaoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVSalesNotePanel anotacaoPainel = new PIVSalesNotePanel( controller.getVendasPai( ) );
				MDIFrame.add(anotacaoPainel);
			}
		});
		
		buttonsPanel.add(observacaoButton);*/
		
		detalhesButton = new JButton("detalhes");
		detalhesButton.setPreferredSize(new Dimension(32,32));
		detalhesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVSalesMoreInfoDFController controller = new PIVSalesMoreInfoDFController(PIVProductSalesPanel.this.controller.getVendasPai());
			}
		});
		
		buttonsPanel.add(detalhesButton);
		
		reloadButton = new ReloadButton();
		buttonsPanel.add(reloadButton);
		grid.setReloadButton(reloadButton);

		LookupController produtoLookupController = new PIVProductLUFController();

		grid.setLockedRowsOnBottom(1);
		
		IntegerColumn colItem = new IntegerColumn();
		colItem.setColumnName("item");
		colItem.setColumnRequired(false);
		colItem.setEditableOnEdit(false);
		colItem.setEditableOnInsert(false);
		colItem.setPreferredWidth(35);
		grid.getColumnContainer().add(colItem);
		
		CodLookupColumn colProdutoId = new CodLookupColumn();
		colProdutoId.setLookupController(produtoLookupController);
		colProdutoId.setColumnName("produto.id");
		colProdutoId.setColumnRequired(true);
		colProdutoId.setEditableOnEdit(true);
		colProdutoId.setEditableOnInsert(true);
		colProdutoId.setPreferredWidth(70);
		grid.getColumnContainer().add(colProdutoId);

		TextColumn colProdutoNome = new TextColumn();
		colProdutoNome.setColumnName("produto.nome");
		colProdutoNome.setEditableOnEdit(false);
		colProdutoNome.setEditableOnInsert(false);
		colProdutoNome.setPreferredWidth(100);
		grid.getColumnContainer().add(colProdutoNome);

		CurrencyColumn colValorVenda = new CurrencyColumn();
		colValorVenda.setEditableOnEdit(false);
		colValorVenda.setEditableOnInsert(false);
		colValorVenda.setDecimals(4);
		colValorVenda.setColumnName("valvenda");
		colValorVenda.setPreferredWidth(75);
		grid.getColumnContainer().add(colValorVenda);

		DecimalColumn colQtdItens = new DecimalColumn();
		colQtdItens.setColumnRequired(true);
		colQtdItens.setEditableOnEdit(true);
		colQtdItens.setEditableOnInsert(true);
		colQtdItens.setDecimals(4);
		colQtdItens.setColumnName("qtditens");
		colQtdItens.setColumnRequired(true);
		colQtdItens.setGrouping(true);
		colQtdItens.setPreferredWidth(75);
		grid.getColumnContainer().add(colQtdItens);

		CurrencyColumn colValSubTotal = new CurrencyColumn();
		colValSubTotal.setEditableOnEdit(false);
		colValSubTotal.setEditableOnInsert(false);
		colValSubTotal.setDecimals(4);
		colValSubTotal.setColumnName("valsubtotal");
		colValSubTotal.setGrouping(true);
		colValSubTotal.setPreferredWidth(100);
		grid.getColumnContainer().add(colValSubTotal);
		
		DecimalColumn colDescontoPerc = new DecimalColumn();
		colDescontoPerc.setColumnRequired(false);
		colDescontoPerc.setEditableOnEdit(true);
		colDescontoPerc.setEditableOnInsert(true);
		colDescontoPerc.setDecimals(4);
		colDescontoPerc.setColumnName("porcdesconto");
		colDescontoPerc.setGrouping(true);
		colDescontoPerc.setPreferredWidth(50);
//		colDescontoPerc.setTextAlignment(SwingConstants.LEFT);
		colDescontoPerc.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		grid.getColumnContainer().add(colDescontoPerc);
		
		DecimalColumn colDescontoValor = new DecimalColumn();
		colDescontoValor.setColumnRequired(false);
		colDescontoValor.setEditableOnEdit(true);
		colDescontoValor.setEditableOnInsert(true);
		colDescontoValor.setDecimals(4);
		colDescontoValor.setColumnName("valdesconto");
		colDescontoValor.setGrouping(true);
		colDescontoValor.setPreferredWidth(50);
//		colDescontoValor.setTextAlignment(SwingConstants.LEFT);
		colDescontoValor.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		grid.getColumnContainer().add(colDescontoValor);
		
		CurrencyColumn colValTotal = new CurrencyColumn();
		colValTotal.setEditableOnEdit(false);
		colValTotal.setEditableOnInsert(false);
		colValTotal.setDecimals(4);
		colValTotal.setColumnName("valtotal");
		colValTotal.setGrouping(true);
		colValTotal.setPreferredWidth(100);
		grid.getColumnContainer().add(colValTotal);

		grid.setController(controller);
		grid.setGridDataLocator(controller);

		grid.setShowFilterPanelOnGrid(false);

		grid.setAllowInsertInEdit(true);
	
		grid.setBottomGridDataLocator(new GridDataLocator() {

			private static final long serialVersionUID = 1L;

			public Response loadData(int action, int startIndex,
					Map filteredColumns, ArrayList currentSortedColumns,
					ArrayList currentSortedVersusColumns,
					Class valueObjectType, Map otherGridParams) {
				ArrayList rows = getValoresTotais();
				return new VOListResponse(rows, false, rows.size());
			}
		});
		
		grid.setBottomGridController(new GridController() {
			/**
	         * Method used to define the background color for each cell of the grid.
	         * @param rowNumber selected row index
	         * @param attributedName attribute name related to the column currently selected
	         * @param value object contained in the selected cell
	         * @return background color of the selected cell
	         */
	        public Color getBackgroundColor(int row,String attributedName,Object value) {
	          return new Color(220,220,220);
	        }

	        /**
	         * Method used to define the font to use for each cell of the grid.
	         * @param rowNumber selected row index
	         * @param attributeName attribute name related to the column currently selected
	         * @param value object contained in the selected cell
	         * @param defaultFont default font currently in used with this column
	         * @return font to use for the current cell; null means default font usage; default value: null
	         */
	        public Font getFont(int row,String attributeName,Object value,Font defaultFont) {
	          if (attributeName.equals("valvenda") || attributeName.equals("valdesconto") || attributeName.equals("qtditens") || attributeName.equals("valtotal"))
	            return new Font(defaultFont.getFontName(),Font.BOLD,defaultFont.getSize());
	          else
	            return null;
	        }

	      });

		jpCentroPainel.add(grid, "grow , span");

		super.add(jpCentroPainel, "dock center , grow");

	}

	public GridControl getGrid() {
		return this.grid;
	}

	/*
	 * public void setEnabledGridButtons(int mode) { switch (mode) { case
	 * Consts.READONLY: insertButton.setEnabled(false);
	 * deleteButton.setEnabled(false); reloadButton.setEnabled(false);
	 * saveButton.setEnabled(false); editButton.setEnabled(false); break; case
	 * Consts.INSERT: insertButton.setEnabled(true);
	 * deleteButton.setEnabled(true); reloadButton.setEnabled(true);
	 * saveButton.setEnabled(false); editButton.setEnabled(true); case
	 * Consts.EDIT: insertButton.setEnabled(true);
	 * deleteButton.setEnabled(true); reloadButton.setEnabled(true);
	 * saveButton.setEnabled(false); editButton.setEnabled(true); break; }
	 * grid.reloadData(); }
	 */

	private ArrayList getValoresTotais() {

		ArrayList rows = new ArrayList();

		PIVSalesMainModel vendapai = controller.getVendasPai();
		
		PIVSalesDetailModel item = new PIVSalesDetailModel();

		String baseSQL = "select " +
					"SUM(VendasDetalhe.qtditens) , " +
					"SUM(VendasDetalhe.valtotal) , " +
					"SUM(VendasDetalhe.valsubtotal) , " +
					"SUM(VendasDetalhe.valdesconto) , " +
					"100 - (SUM(VendasDetalhe.valtotal) / SUM(VendasDetalhe.valvenda*VendasDetalhe.qtditens) * 100) " +
				"from org.playiv.com.mvc.model.PIVSalesDetailModel as VendasDetalhe where VendasDetalhe.vendaPai.id = "
				+ vendapai.getId()
				+ " and VendasDetalhe.ativo = true ";

		Session session = pdao.getSession();
		Query query = session.createQuery(baseSQL);

		double totalQtdItens = 0d;
		double totalValorTotalItens = 0d;
		double totalValorSubTotalItens = 0d;
		double totalValorDescontoItens = 0d;
		double totalDescontoTotalItens = 0d;

		for (Iterator it = query.iterate(); it.hasNext();) {
			Object[] row = (Object[]) it.next();
			if(row!=null) {
				totalQtdItens = (Double) (row[0]==null?0d:row[0]);
				totalValorTotalItens = (Double) (row[0]==null?0d:row[1]);
				totalValorSubTotalItens = (Double) (row[0]==null?0d:row[2]);
				totalValorDescontoItens = (Double) (row[0]==null?0d:row[3]);
				totalDescontoTotalItens = (Double) (row[0]==null?0d:row[4]);
			}
		}
		
		session.close();

		Double valorDespesas = vendapai.getValordespesa() == null ? 0d : vendapai.getValordespesa();
		Double valorFrete = vendapai.getValorfrete() == null ? 0d : vendapai.getValorfrete();
		
		vendapai.setPorcdesconto(totalDescontoTotalItens);
		vendapai.setValordesconto(totalValorDescontoItens);
		vendapai.setValorsubtotal(totalValorSubTotalItens);
		vendapai.setValortotal(totalValorTotalItens + valorDespesas + valorFrete);

		if(vendapai.getCampanha()!=null) pdao.update(vendapai.getCampanha());
		pdao.update(vendapai);

		PIVProductModel produto = new PIVProductModel();
		produto.setNome("Totais:");
		
		item.setProduto(produto);
		item.setAtivo(true);
		item.setQtditens(totalQtdItens);
		item.setValtotal(totalValorTotalItens);
		item.setValsubtotal(totalValorSubTotalItens);
		item.setValdesconto(totalValorDescontoItens);
		item.setPorcdesconto(totalDescontoTotalItens);

		rows.add(item);

		return rows;

	}
	
	public void setBackground( Color bg ) {
		super.setBackground( bg );
		if(this.jpCentroPainel!=null)
			this.jpCentroPainel.setBackground(bg);
		if(this.buttonsPanel!=null)
			this.buttonsPanel.setBackground(bg);
	}

	@Override
	public void setValue(String attributeName, Object value) { }

	@Override
	public ValueObject getValueObject() { return null; }

	@Override
	public Object getLookupCodeParentValue() { return null; }
	
}