package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.hibernate.Query;
import org.hibernate.Session;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;

public class PIVGeneralGadget extends PIVMainGadget {

	private static final long serialVersionUID = 1L;

	public PIVGeneralGadget() {
		setGadgetName("INFORMA��ES GERAIS");
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
		setSize(new Dimension(750,200));
	}

	class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;

		private JFreeChart createChart(CategoryDataset categorydataset) {

			JFreeChart jfreechart = ChartFactory.createBarChart3D("", "", "",
					categorydataset, PlotOrientation.VERTICAL, false, true,
					false);

			CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();

			BarRenderer3D custombarrenderer3d = new BarRenderer3D();
			custombarrenderer3d
					.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
			custombarrenderer3d.setBaseItemLabelsVisible(true);
			custombarrenderer3d.setItemLabelAnchorOffset(10D);
			custombarrenderer3d
					.setBasePositiveItemLabelPosition(new ItemLabelPosition(
							ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
			custombarrenderer3d.setBaseItemLabelsVisible(true);
			custombarrenderer3d.setMaximumBarWidth(0.050000000000000003D);
			
			GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
			custombarrenderer3d.setSeriesPaint(0, gradientpaint);
			
			categoryplot.setRenderer(custombarrenderer3d);

			DecimalFormat decimalformat = new DecimalFormat("####");
			decimalformat.setNegativePrefix("(");
			decimalformat.setNegativeSuffix(")");
			
			/*TickUnits tickunits = new TickUnits();
			tickunits.add(new NumberTickUnit(5, decimalformat));
			tickunits.add(new NumberTickUnit(10, decimalformat));
			tickunits.add(new NumberTickUnit(20, decimalformat));
			tickunits.add(new NumberTickUnit(50, decimalformat));
			tickunits.add(new NumberTickUnit(100, decimalformat));
			tickunits.add(new NumberTickUnit(200, decimalformat));
			tickunits.add(new NumberTickUnit(500, decimalformat));
			tickunits.add(new NumberTickUnit(1000, decimalformat));
			tickunits.add(new NumberTickUnit(2000, decimalformat));*/
			
			/*ValueAxis valueaxis = categoryplot.getRangeAxis();
			valueaxis.setStandardTickUnits(tickunits);*/
			
			NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
			numberaxis.setNumberFormatOverride(NumberFormat.getIntegerInstance());
			numberaxis.setUpperMargin(0.10000000000000001D);
			numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			/*numberaxis.setRange(0, 200);*/
			/*numberaxis.setStandardTickUnits(tickunits);*/

			ChartUtilities.applyCurrentTheme(jfreechart);

			return jfreechart;

		}
		
		public Conteudo( )
		{
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			CategoryDataset categorydataset = createDataset();
			
			JFreeChart jfreechart = createChart(categorydataset);
			
			ChartPanel chartpanel = new ChartPanel(jfreechart);
			/*chartpanel.setPreferredSize(new Dimension(500, 270));*/
			add(chartpanel,BorderLayout.CENTER);
			
		}
		
		private CategoryDataset createDataset()
		{
			
			DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();
			
			String baseSQL = "select idnivelcomercial , COUNT(*) "
					+ "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento where " 
					+ "Relacionamento.vendedor.id = '" + PIVUserSession.getInstance().getVendedorSessao().getId() + "' " 
					+ "and Relacionamento.completado = false "
					+ "and Relacionamento.ativo = true " 
					+ "group by idnivelcomercial";
			
			Session session = PIVDao.getInstance().getSession();
			Query query = session.createQuery(baseSQL);
			
			long qtdSuspect = 0;
			long qtdProspect = 0;
			long qtdForecast = 0;
			long qtdCustomer = 0;
			long qtdempresas = 0;
			long qtdContatos = 0;
			long qtdVendasAberto = 0;
			long qtdVendasFechado = 0;

			for (@SuppressWarnings("rawtypes")
			Iterator it = query.iterate(); it.hasNext();) {
				Object[] row = (Object[]) it.next();
				if(row!=null) {
					switch((Integer)row[0]) {
						case 1:
							qtdSuspect = (Long) row[1];
							break;
						case 2:
							qtdProspect = (Long) row[1];
							break;
						case 3:
							qtdForecast = (Long) row[1];
							break;
						case 4:
							qtdCustomer = (Long) row[1];
							break;
						default:
							break;
					}
				}
			}
			
			qtdempresas += qtdSuspect+qtdProspect+qtdForecast+qtdCustomer;
			
			session.close();
			
			baseSQL = "select COUNT(*) from org.playiv.com.mvc.model.PIVContactModel as Contato , org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento " 
			+ "where Relacionamento.cliente.id = Contato.cliente.id "
			+ "and Relacionamento.vendedor.id = '" + PIVUserSession.getInstance().getVendedorSessao().getId() + "' " 
			+ "and Relacionamento.completado = false "
			+ "and Relacionamento.ativo = true "
			+ "and Contato.ativo = true";
			
			session = PIVDao.getInstance().getSession();
			query = session.createQuery(baseSQL);
			
			for (@SuppressWarnings("rawtypes")
			Iterator it = query.iterate(); it.hasNext();) {
				qtdContatos = (Long) it.next();
			}
			
			session.close();
			
			// Solu��o nada bonita mas � a mais pr�tica......
			// melhorar no futuro.....
			Calendar calendario = Calendar.getInstance();
			calendario.add(Calendar.MONTH,-1);
			
			Date umMesAtras = new Date(calendario.getTimeInMillis());
			
			baseSQL = "select Venda.vendaefetivada , COUNT(*) from org.playiv.com.mvc.model.PIVSalesMainModel as Venda " 
					+ "where Venda.vendedor.id = '" + PIVUserSession.getInstance().getVendedorSessao().getId() + "' " 
					+ "and Venda.dataregistro >= :data_limite "
					+ "and Venda.ativo >= true "
					+ "group by Venda.vendaefetivada ";
			
			session = PIVDao.getInstance().getSession();
			query = session.createQuery(baseSQL).setParameter("data_limite", umMesAtras);
			
			for (@SuppressWarnings("rawtypes")
			Iterator it = query.iterate(); it.hasNext();) {
				Object[] row = (Object[]) it.next();
				if(row!=null) {
					if((Boolean)row[0].equals(false))
						qtdVendasAberto = (Long) row[1];
					else
						qtdVendasFechado = (Long) row[1];
				}
			}
			
			session.close();
			
			categorydataset.addValue(qtdempresas, "", "Empresas");
			categorydataset.addValue(qtdContatos,"","Contatos");
			categorydataset.addValue(qtdSuspect, "", "Suspect");
			categorydataset.addValue(qtdProspect, "", "Prospect");
			categorydataset.addValue(qtdForecast, "", "Forecast");
			categorydataset.addValue(qtdCustomer, "", "Customer");
			categorydataset.addValue(qtdContatos,"","Contatos");
			categorydataset.addValue(qtdVendasAberto,"","Prop. A");
			categorydataset.addValue(qtdVendasFechado,"","Prop. F");
			
			return categorydataset;
			
		}
		
	}

}