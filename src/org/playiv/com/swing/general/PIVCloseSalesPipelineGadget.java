package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.hibernate.Query;
import org.hibernate.Session;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;

public class PIVCloseSalesPipelineGadget extends PIVMainGadget {

	private static final long serialVersionUID = 1L;

	public PIVCloseSalesPipelineGadget() {
		setGadgetName("PIPELINE DE VENDAS (oportunidades fechadas)");
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
		setSize(new Dimension(400,200));
	}

	class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;

		private JFreeChart createChart(CategoryDataset categorydataset) {

			JFreeChart jfreechart = ChartFactory.createWaterfallChart("", "", "", categorydataset, PlotOrientation.VERTICAL, false, true, false);
			
			CategoryPlot categoryplot = (CategoryPlot)jfreechart.getPlot();
			
			DecimalFormat decimalformat = new DecimalFormat("##,####");
			decimalformat.setNegativePrefix("(");
			decimalformat.setNegativeSuffix(")");
			
			TickUnits tickunits = new TickUnits();
			tickunits.add(new NumberTickUnit(5D, decimalformat));
			tickunits.add(new NumberTickUnit(10D, decimalformat));
			tickunits.add(new NumberTickUnit(20D, decimalformat));
			tickunits.add(new NumberTickUnit(50D, decimalformat));
			tickunits.add(new NumberTickUnit(100D, decimalformat));
			tickunits.add(new NumberTickUnit(200D, decimalformat));
			tickunits.add(new NumberTickUnit(500D, decimalformat));
			tickunits.add(new NumberTickUnit(1000D, decimalformat));
			tickunits.add(new NumberTickUnit(2000D, decimalformat));
			tickunits.add(new NumberTickUnit(5000D, decimalformat));
			tickunits.add(new NumberTickUnit(15000D, decimalformat));
			tickunits.add(new NumberTickUnit(30000D, decimalformat));
			tickunits.add(new NumberTickUnit(60000D, decimalformat));
			tickunits.add(new NumberTickUnit(120000D, decimalformat));
			tickunits.add(new NumberTickUnit(500000D, decimalformat));
			tickunits.add(new NumberTickUnit(1000000D, decimalformat));
			
			ValueAxis valueaxis = categoryplot.getRangeAxis();
			valueaxis.setStandardTickUnits(tickunits);

			DecimalFormat decimalformat1 = new DecimalFormat("R$ ##,###.0000");
			decimalformat1.setNegativePrefix("(");
			decimalformat1.setNegativeSuffix(")");
			
			/*BarRenderer barrenderer = (BarRenderer)categoryplot.getRenderer();
			barrenderer.setDrawBarOutline(false);
			barrenderer.setBase(5D);*/
			/*barrenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", decimalformat1));*/
			/*barrenderer.setBaseItemLabelsVisible(true);*/
			
			return jfreechart;

		}
		
		public Conteudo( )
		{
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			CategoryDataset categorydataset = createDataset();
			
			JFreeChart jfreechart = createChart(categorydataset);
			
			ChartPanel chartpanel = new ChartPanel(jfreechart);
			/*chartpanel.setPreferredSize(new Dimension(500, 270));*/
			add(chartpanel,BorderLayout.CENTER);
			
		}
		
		private CategoryDataset createDataset()
		{
			
			DefaultCategoryDataset categorydataset = new DefaultCategoryDataset();
			
			String baseSQL = "select Relacionamento.idnivelcomercial , SUM(Venda.valortotal) "
					+ "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento , org.playiv.com.mvc.model.PIVSalesMainModel as Venda where "
					+ "Venda.cliente.id = Relacionamento.cliente.id "
					+ "and Relacionamento.vendedor.id = '" + PIVUserSession.getInstance().getVendedorSessao().getId() + "' " 
					+ "and Relacionamento.completado = false "
					+ "and Relacionamento.ativo = true "
					+ "and Venda.vendaefetivada = true "
					+ "group by Relacionamento.idnivelcomercial";
			
			Session session = PIVDao.getInstance().getSession();
			Query query = session.createQuery(baseSQL);
			
			Double valSuspect = 0d;
			Double valProspect = 0d;
			Double valForecast = 0d;
			Double valCustomer = 0d;
			Double valTotal = 0d;
			
			for (@SuppressWarnings("rawtypes")
			Iterator it = query.iterate(); it.hasNext();) {
				Object[] row = (Object[]) it.next();
				if(row!=null) {
					switch((Integer)row[0]) {
						case 1:
							valSuspect = (Double) row[1];
							break;
						case 2:
							valProspect = (Double) row[1];
							break;
						case 3:
							valForecast = (Double) row[1];
							break;
						case 4:
							valCustomer = (Double) row[1];
							break;
						default:
							break;
					}
				}
			}
			
			valTotal += valSuspect+valProspect+valForecast+valCustomer;
			
			session.close();
			
			categorydataset.addValue(valSuspect, "Vendas", "Suspect");
			categorydataset.addValue(valProspect, "Vendas", "Prospect");
			categorydataset.addValue(valForecast, "Vendas", "Forecast");
			categorydataset.addValue(valCustomer, "Vendas", "Customer");
			categorydataset.addValue(valTotal, "Vendas","Total");

			return categorydataset;
			
		}
		
	}

}