package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

public class PIVBusRelSalesPerformedFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private JTextField txRazaoSocial;
	private JTextField txNomeFantasia;
	private JTextField txNomeContato;
	private JTextField txTempoFinal;
	private JTextField txTempoMedio;
	private JTextField txQtdReunioes;
	private JTextField txQtdEmails;
	private JTextField txQtdTelefonemas;
	
/*	public static void main(String args[]) {
		JFrame frame = new PIVBusRelSalesPerformedFrame();
		frame.setPreferredSize(new Dimension(600,300));
		frame.pack();
		frame.setVisible(true);
	}*/
	
	public PIVBusRelSalesPerformedFrame( ) {
		initialize();
	}
	
	private void initialize() {
		
		PIVBackgroundPanel bkgPanel = new PIVBackgroundPanel(Toolkit.getDefaultToolkit().createImage(PIVBusRelSalesPerformedFrame.class.getResource("/images/gerenciamento_relcomercial/outros/venda-efetivada_600x300.jpg")));
		bkgPanel.setLayout(new BorderLayout());
		
		JPanel jpLateral = new JPanel();
		jpLateral.setPreferredSize(new Dimension(140,0));
		/*jpLateral.setBorder(BorderFactory.createLineBorder(Color.RED));*/
		bkgPanel.add(jpLateral,BorderLayout.WEST);
		
		JPanel jpTopo = new JPanel();
		jpTopo.setPreferredSize(new Dimension(0,50));
		/*jpTopo.setBorder(BorderFactory.createLineBorder(Color.BLUE));*/
		bkgPanel.add(jpTopo,BorderLayout.NORTH);
		
		JPanel jpCentro = new JPanel();
		jpCentro.setLayout(new MigLayout("", "[right][grow,left][right][grow,left][grow]", ""));
		jpCentro.setPreferredSize(new Dimension(460,250));
		
		JLabel lbRazaoSocial = new JLabel("Raz�o Social:");
		jpCentro.add(lbRazaoSocial, "");
		
		txRazaoSocial = new JTextField();
		txRazaoSocial.setEnabled(false);
		lbRazaoSocial.setLabelFor(txRazaoSocial);
		jpCentro.add(txRazaoSocial,"spanx 3 , growx , wrap");
		
		JLabel lbNomeFantasia = new JLabel("Nome Fantasia:");
		jpCentro.add(lbNomeFantasia, "");
		
		txNomeFantasia = new JTextField();
		txNomeFantasia.setEnabled(false);
		lbNomeFantasia.setLabelFor(txNomeFantasia);
		jpCentro.add(txNomeFantasia,"spanx 3 , growx , wrap");
		
		JLabel lbNomeContato = new JLabel("Contato Respons�vel:");
		jpCentro.add(lbNomeContato, "");
		
		txNomeContato = new JTextField();
		txNomeContato.setEnabled(false);
		lbNomeContato.setLabelFor(txNomeContato);
		jpCentro.add(txNomeContato,"spanx 3 , growx , wrap");
		
		JLabel lbTempoFinal = new JLabel("Tempo total para completar a venda:");
		jpCentro.add(lbTempoFinal, "");
		
		txTempoFinal = new JTextField();
		txTempoFinal.setEnabled(false);
		lbTempoFinal.setLabelFor(txTempoFinal);
		jpCentro.add(txTempoFinal,"growx , wrap");
		
		JLabel lbTempoMedio = new JLabel("Tempo m�dio em cada camada");
		jpCentro.add(lbTempoMedio, "");
		
		txTempoMedio = new JTextField();
		txTempoMedio.setEnabled(false);
		lbTempoMedio.setLabelFor(txTempoMedio);
		jpCentro.add(txTempoMedio,"growx , wrap");
		
		JLabel lbQtdReunioes = new JLabel("Quantidade total de reuni�es:");
		jpCentro.add(lbQtdReunioes, "");
		
		txQtdReunioes = new JTextField();
		txQtdReunioes.setEnabled(false);
		lbQtdReunioes.setLabelFor(txQtdReunioes);
		jpCentro.add(txQtdReunioes,"growx , wrap");
		
		JLabel lbQtdEmails = new JLabel("Quantidade total de e-mails:");
		jpCentro.add(lbQtdEmails, "");
		
		txQtdEmails = new JTextField();
		txQtdEmails.setEnabled(false);
		lbQtdEmails.setLabelFor(txQtdEmails);
		jpCentro.add(txQtdEmails,"growx , wrap");
		
		JLabel lbQtdTelefonemas = new JLabel("Quantidade total de telefonemas:");
		jpCentro.add(lbQtdTelefonemas, "");
		
		txQtdTelefonemas = new JTextField();
		txQtdTelefonemas.setEnabled(false);
		lbQtdTelefonemas.setLabelFor(txQtdTelefonemas);
		jpCentro.add(txQtdTelefonemas, "growx , wrap");
		
		JButton btCloseFrame = new JButton("Fechar");
		btCloseFrame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVBusRelSalesPerformedFrame.this.dispose();
			}
		});
		jpCentro.add(btCloseFrame);
		
		/*jpCentro.setLayout(new MigLayout());*/
		/*jpCentro.setBorder(BorderFactory.createLineBorder(Color.GREEN));*/
		bkgPanel.add(jpCentro,BorderLayout.CENTER);
		
		add(bkgPanel);

		setSize(new Dimension(600,300));
		setLocationRelativeTo(null);
		setResizable(false);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		pack();
		setVisible(true);
	}
	
	public JTextField getTxRazaoSocial() {
		return txRazaoSocial;
	}

	public JTextField getTxNomeFantasia() {
		return txNomeFantasia;
	}

	public JTextField getTxNomeContato() {
		return txNomeContato;
	}

	public JTextField getTxTempoFinal() {
		return txTempoFinal;
	}

	public JTextField getTxTempoMedio() {
		return txTempoMedio;
	}

	public JTextField getTxQtdReunioes() {
		return txQtdReunioes;
	}

	public JTextField getTxQtdEmails() {
		return txQtdEmails;
	}

	public JTextField getTxQtdTelefonemas() {
		return txQtdTelefonemas;
	}
	
}
