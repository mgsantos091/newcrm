package org.playiv.com.swing.general;

import org.playiv.com.library.function.PIVValidTextContentFunc;

public interface PIVTextControlCValidation {

	public abstract void addValidFunction(PIVValidTextContentFunc function);

	public abstract void removeValidFunction(
			PIVValidTextContentFunc function);

}