package org.playiv.com.swing.general;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.client.FormattedTextControl;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVCpfCnpjTextControl extends FormattedTextControl {

	private static final long serialVersionUID = 1L;
	
	private PIVDao pdao = PIVDao.getInstance();

	public PIVCpfCnpjTextControl() {
		super();
		iniciaListeners();
	}

	private boolean validaConteudo(String text) {

		PIVClientModel cliente;

		if (text.equals(""))
			return false;

		String sqlCode = "from org.playiv.com.mvc.model.PIVClientModel as Cliente where Cliente.cnpjcpf = '"
				+ text + "'";

		Session session = pdao.getSession();
		cliente = (PIVClientModel) session.createQuery(sqlCode).uniqueResult();
		// Cliente encontrado
		if (cliente != null) {
			// Cliente desativado
			if(!cliente.isAtivo()) {
				int option = JOptionPane.showConfirmDialog(
						null,
						"Foi encontrado um cliente desativado"
								+ " - "
								+ (cliente.isPjuridica() ? " pessoa jur�dica "
										: " pessoa f�sica ") + " - "
								+ " com o mesmo "
								+ (cliente.isPjuridica() ? "CNPJ" : "CPF\n")
								+ "Deseja ativa-lo e carregar seus dados?",
						"Ativar cliente desativado",
						JOptionPane.YES_NO_OPTION);
				if (option == JOptionPane.YES_OPTION) {
					cliente.setAtivo(true);
					pdao.update(cliente);
					new PIVClientMngtDFController(null, cliente.getId());
				}
			}
			int option = JOptionPane.showConfirmDialog(
					null,
					"Foi encontrado um cliente"
							+ " - "
							+ (cliente.isPjuridica() ? " pessoa jur�dica "
									: " pessoa f�sica ") + " - "
							+ " com o mesmo "
							+ (cliente.isPjuridica() ? "CNPJ" : "CPF\n")
							+ "Deseja carregar os dados do cliente?",
					"Carregar dados de cliente encontrado",
					JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {
				new PIVClientMngtDFController(null, cliente.getId());
				return false;
			} else
				return false;
		}
		return true;
	}

	private void iniciaListeners() {

		/*
		 * JFormattedTextField textBox = (JFormattedTextField)
		 * getBindingComponent(); textBox.addKeyListener(new KeyAdapter() {
		 * 
		 * @Override public void keyPressed(KeyEvent e) { if (e.getKeyCode() ==
		 * KeyEvent.VK_TAB) { transferFocus(); if (getText().trim().length() >
		 * 0) // cod box is empty: focus is automatically setted to // the
		 * component next to lookup button SwingUtilities.invokeLater(new
		 * Runnable() { public void run() { FocusManager.getCurrentManager()
		 * .getFocusOwner().transferFocus(); } }); } } });
		 */

		getBindingComponent().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				String value = (String) getValue();
				if (value == null)
					return;
				String text = value.toString();
				
				 boolean conteudoValido = validaConteudo(text);
				 
				 if (!conteudoValido) { setText(""); }

			}

			@Override
			public void focusGained(FocusEvent e) {
/*
				if (!getBindingComponent().hasFocus())
					// set focus to the component inside this...
					getBindingComponent().requestFocus();
*/
			}
		});

	}

}