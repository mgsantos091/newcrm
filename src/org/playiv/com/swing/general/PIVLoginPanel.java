package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.PasswordControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.permissions.client.LoginController;
import org.playiv.com.library.general.PIVLogSettings;

public class PIVLoginPanel extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private LoginController controller;

	private TextControl txtLoginUsuario;

	private PasswordControl txtSenhaUsuario;

	public PIVLoginPanel( LoginController controller ) {
		this.controller = controller;
		initialize( );
	}

	private void initialize() {
				
		// inicializa outros objetos
		Font fonteLabel = new Font("Arial", Font.BOLD, 16);
		Font fonteTextField = new Font("Arial", Font.PLAIN , 12);
		
		PIVBackgroundPanel bkgPanel = new PIVBackgroundPanel(Toolkit.getDefaultToolkit().createImage(PIVLoginPanel.class.getResource("/images/playiv-login.jpg")));
		bkgPanel.setLayout(new BorderLayout());
		
		JPanel jpLateral = new JPanel();
		jpLateral.setPreferredSize(new Dimension(60,0));
		bkgPanel.add(jpLateral,BorderLayout.WEST);
		
		JPanel jpTopo = new JPanel();
		jpTopo.setPreferredSize(new Dimension(0,120));
		bkgPanel.add(jpTopo,BorderLayout.NORTH);
		
		JPanel jpCentro = new JPanel();
		jpCentro.setLayout(new MigLayout("", "[right][grow,fill]", "[][][grow,fill]"));
		jpCentro.setPreferredSize(new Dimension(460,250));
		/*jpCentro.setBorder(BorderFactory.createLineBorder(Color.WHITE));*/
		bkgPanel.add(jpCentro,BorderLayout.CENTER);
		
		JLabel lblLoginUsuario = new JLabel("Usu�rio:");
		lblLoginUsuario.setForeground(Color.WHITE);
		lblLoginUsuario.setFont(fonteLabel);
		jpCentro.add(lblLoginUsuario, "cell 0 0");
		
		txtLoginUsuario = new TextControl();
		txtLoginUsuario.setFont(fonteTextField);
		txtLoginUsuario.setMaxCharacters(30);
		jpCentro.add(txtLoginUsuario,"cell 1 0 2 1,grow");

		JLabel lblSenhaUsuario = new JLabel("Senha:");
		lblSenhaUsuario.setForeground(Color.WHITE);
		lblSenhaUsuario.setFont(fonteLabel);
		jpCentro.add(lblSenhaUsuario, "cell 0 1");
		
		txtSenhaUsuario = new PasswordControl();
		txtSenhaUsuario.setFont(fonteTextField);
		txtSenhaUsuario.setMaxCharacters(30);
		jpCentro.add(txtSenhaUsuario,"cell 1 1 2 1,grow");
		
		JPanel jpBotoes = new JPanel();
		jpBotoes.setOpaque(false);
		
		FlowLayout flowLayout = (FlowLayout) jpBotoes.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		jpBotoes.setSize(new Dimension(0,50));
		/*jpBotoes.setBorder(BorderFactory.createLineBorder(Color.RED));*/
		
		jpCentro.add(jpBotoes,"cell 0 2 2 1,grow");
		
		JButton btnLogar = new JButton("Logar");
		btnLogar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String login = (String) PIVLoginPanel.this.txtLoginUsuario.getValue();
				String pwd = (String) PIVLoginPanel.this.txtSenhaUsuario.getValue();
				
				HashMap map = new HashMap();
				map.put("password",pwd);
				map.put("username",login);
				
				boolean ok = false;
				try {
					ok = PIVLoginPanel.this.controller.authenticateUser(map);
				} catch (Exception e1) {
					PIVLogSettings.getInstance().error(e1.getMessage(),e1);
					e1.printStackTrace();
				}
				
				if(ok) {
					PIVLoginPanel.this.controller.loginSuccessful(map);
					dispose();
				} else JOptionPane.showMessageDialog(null, "Usu�rio ou senha incorretos, favor tentar novamente", "Falha no processo de login",JOptionPane.ERROR_MESSAGE);
				
			}
		});
		jpBotoes.add(btnLogar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVLoginPanel.this.dispose();
			}			
		});
		
		jpBotoes.add(btnCancelar);
		
		getContentPane().add(bkgPanel);
		
		setSize(new Dimension(439,360));
		
		// busca a dimens�o da tela para centralizar o dialog
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Dimension screenSize = toolkit.getScreenSize();
		final int x = (screenSize.width - getWidth()) / 2;
		final int y = (screenSize.height - getHeight()) / 2;
		
		setResizable(false);
		setUndecorated(true);
		/*setLocationRelativeTo(null);*/
		setLocation(x, y);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		/*pack();*/
		setVisible(true);
		
	}
	
	/*public static void main(String args[]) {
		JDialog frame = new PIVLoginPanel();
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}*/

}