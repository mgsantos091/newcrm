package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.lookup.client.LookupParent;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ValueObject;
import org.playiv.com.library.function.PIVBusRelClientTableFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVClientMngtLUFController;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVBusRelLevelLayerPanel extends JPanel implements LookupParent {

	private static final long serialVersionUID = 1L;

	private final String TITULO;
	private final int LINHAS_TABELAS;
	private final int COLUNAS_TABELAS;

	private PIVBusRelClientTableFunc table;

	private PIVBusRelLevelLayerPanel boxPosterior;

	private JLabel lbQtdElementos;

	private PIVClientModel cliente;

	private PIVBusRelMainCFController organizacliente;

	private JLabel lbNivelPrint;

	/**
	 * Create the application.
	 */
	public PIVBusRelLevelLayerPanel(final String TITULO, final int LINHAS_TABELAS , final int COLUNAS_TABELAS) {
		super();
		this.TITULO = TITULO;
		this.LINHAS_TABELAS = LINHAS_TABELAS;
		this.COLUNAS_TABELAS= COLUNAS_TABELAS;
		initialize();
		atualizaQuantElementos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setLayout(new MigLayout("", "[][left][][][grow]", "[13px][][]"));
		setBackground(Color.WHITE);

		lbNivelPrint = new JLabel();
		lbNivelPrint.setForeground(new Color(47, 79, 79));
		add(lbNivelPrint, "cell 0 0,alignx left");
		
		JLabel lbCustomer = new JLabel(this.TITULO);
		lbCustomer.setForeground(new Color(47, 79, 79));
		lbCustomer.setFont(new Font("Arial", Font.BOLD, 10));
		add(lbCustomer, "cell 1 0,alignx left");

		JLabel lbN1 = new JLabel("n\u00B0");
		lbN1.setForeground(new Color(47, 79, 79));
		lbN1.setFont(new Font("Arial", Font.BOLD, 10));
		add(lbN1, "cell 2 0");

		lbQtdElementos = new JLabel("");
		lbQtdElementos.setForeground(new Color(47, 79, 79));
		lbQtdElementos.setFont(new Font("Arial", Font.BOLD, 10));
		lbQtdElementos.setHorizontalAlignment(SwingConstants.LEFT);
		lbQtdElementos.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(lbQtdElementos, "spanx 2,growx,alignx left");

		JButton btnEscElemen = new JButton("+");
		btnEscElemen.setBackground(Color.WHITE);
		btnEscElemen.setForeground(new Color(27,75,18));
		btnEscElemen.setBorder(null);
		//btnEscElemen.setIcon(new ImageIcon());
		btnEscElemen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evento) {

				PIVClientMngtLUFController controller = new PIVClientMngtLUFController();
				controller.openLookupFrame(MDIFrame.getSelectedFrame(),
						PIVBusRelLevelLayerPanel.this);

				cliente = (PIVClientModel) controller.getLookupVO();
				if(cliente!=null) {
					organizacliente.add(cliente,true,true/*,0*/);
					PIVEventMngrFunc.getInstance().registerEvent( // registra o evento
							cliente,
							PIVEventMngrFunc.getCodEvento(PIVEventMngrFunc.evento.NOVO_CLIENTE),
							PIVBusRelLevelLayerPanel.this.organizacliente.getNivelcomercial(),
							PIVEventMngrFunc.evento.NOVO_CLIENTE,
							"Cliente - "
									+ cliente.getId().toString()
									+ "_"
									+ cliente.getRazaosocial()
									+ " - adicionado no n�vel comercial "
									+ PIVBusRelMainCFController
											.getNivelDesc(PIVBusRelLevelLayerPanel.this.organizacliente
													.getNivelcomercial()));
				}
			}
			/*
			 * @Override public void actionPerformed(ActionEvent evento) {
			 * 
			 * PIVGerRelComBox.this.controller = new PIVClienteGFController(
			 * true);
			 * 
			 * PIVGerRelComBox.this.grid = controller.getGrid();
			 * 
			 * PIVGerRelComBox.this.grid .addInternalFrameListener(new
			 * InternalFrameAdapter() { private PIVRelComElemComp elemento;
			 * 
			 * @Override public void internalFrameClosing( InternalFrameEvent
			 * event) { PIVGerRelComBox.this.cliente = controller .getVORef();
			 * 
			 * Icon imgElemento = new ImageIcon(globalSettings
			 * .getDefaultImgPath() + cliente.getNomeimagem()); String
			 * nomeElemento = cliente.getNomefantasia();
			 * 
			 * elemento = new PIVRelComElemComp(imgElemento, nomeElemento);
			 * elemento.setCliente(PIVGerRelComBox.this.cliente);
			 * elemento.addMouseListener(new MouseListener() {
			 * 
			 * @Override public void mouseReleased(MouseEvent e) { // TODO
			 * Auto-generated method stub
			 * 
			 * }
			 * 
			 * @Override public void mousePressed(MouseEvent e) { // TODO
			 * Auto-generated method stub
			 * 
			 * }
			 * 
			 * @Override public void mouseExited(MouseEvent e) { // TODO
			 * Auto-generated method stub
			 * 
			 * }
			 * 
			 * @Override public void mouseEntered(MouseEvent e) { // TODO
			 * Auto-generated method stub
			 * 
			 * }
			 * 
			 * @Override public void mouseClicked(MouseEvent e) {
			 * System.out.println(elemento + " clicado"); PIVGerRelComBox.this
			 * .setElementoSelecionado(elemento);
			 * elemento.setBoxRelacionado(PIVGerRelComBox.this); } });
			 * PIVGerRelComBox.this.addCelulaPintada(elemento); } }); }
			 */

			/*
			 * try { frameSignal.await(); } catch (InterruptedException e) {
			 * e.printStackTrace(); }
			 * 
			 * 
			 * cliente = controller.getVORef();
			 * 
			 * System.out.println(cliente.getNomefantasia());
			 */

		});

		add(btnEscElemen, "cell 3 0 2 1,width 45!,alignx right,height 15!");

		table = new PIVBusRelClientTableFunc( this.LINHAS_TABELAS , this.COLUNAS_TABELAS );
		table.setEnabled(false);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 1 && SwingUtilities.isLeftMouseButton(e)) {
					organizacliente.setFocusPainelRelacionado();
				}
			}
		});
		
		add(table, "cell 0 1 5 1,grow");
		
	}

	public void atualizaQuantElementos() {
		this.lbQtdElementos.setText(String.valueOf(this.table
				.getTotalElementos()));
	}

	public PIVBusRelClientTableFunc getTable() {
		return table;
	}

	public void addCelulaPintada() {
		this.table.addCelulaPintada();
		atualizaQuantElementos();
	}

	public void removeCelulaPintada( ) {
		this.table.removeCelulaPintada();
		atualizaQuantElementos();
	}

	public void moverProximoBox( ) {
		if (!(this.boxPosterior == null)) {
			this.removeCelulaPintada();
			this.boxPosterior.addCelulaPintada();
		}
	}

	public void setBoxPosterior(PIVBusRelLevelLayerPanel boxPosterior) {
		this.boxPosterior = boxPosterior;
	}

	@Override
	public Object getLookupCodeParentValue() {
		return cliente.getId();
	}

	@Override
	public ValueObject getValueObject() {
		return cliente;
	}

	@Override
	public void setValue(String arg0, Object arg1) {
		System.out.println("setValue on PIVGerRelComBox executed | " + arg0
				+ " | " + arg1);
	}

	public void setOrganizacliente(PIVBusRelMainCFController organizacliente) {
		this.organizacliente = organizacliente;
	}
	
	public JLabel getLbNivelPrint() {
		return lbNivelPrint;
	}
	
}