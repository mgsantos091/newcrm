package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.miginfocom.swing.MigLayout;

import org.hibernate.Session;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.openswing.swing.util.client.ClientUtils;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVFormTextControlCVal;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.function.PIVValidTextContentFunc;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.component.PIVAutoReSizeLabel;
import org.playiv.com.swing.component.PIVImagePreviewLabel;

public class PIVClientPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();

	private PIVAutoReSizeLabel lblImgEmp;

	@SuppressWarnings("rawtypes")
	private JComboBox cmbBoxCnpjCpf;

	private PIVFormTextControlCVal frmtdTxtCnpjCpf;

	public PIVClientPanel(PIVClientMngtDFController controller)
			throws ParseException {

		super.setLayout( new MigLayout( ) );

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("EMPRESA");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("",
				"[right][grow][right][grow][]", ""));

		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico.setIcon(new ImageIcon(PIVClientPanel.class.getResource("/images/cadastros/cliente/agencia_64x64.png")));

		// lblImgTopico.setIcon(new ImageIcon(PIVClientePanel.class
		// .getResource("/resources/agency.png")));
		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , left , span 2");

		JButton btnEscImgEmp = new JButton("...");
		btnEscImgEmp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				FileNameExtensionFilter filtroImagem = new FileNameExtensionFilter(
						"Imagens", "gif", "jpg", "png");

				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(filtroImagem);

				chooser.setCurrentDirectory(new File(System
						.getProperty("user.home")));

				chooser.setAccessory(new PIVImagePreviewLabel(chooser));

				int res = chooser.showOpenDialog(null);
				if (res == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					PIVClientPanel.this.lblImgEmp.setIcon(new ImageIcon(file
							.getAbsolutePath()));
					System.out.println(file.getAbsolutePath());
				}
			}
		});
		jpCentroPainel.add(btnEscImgEmp, "top , right");

		lblImgEmp = new PIVAutoReSizeLabel();
		lblImgEmp.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblImgEmp.setHorizontalAlignment(SwingConstants.CENTER);
		/*lblImgEmp.setIcon(new ImageIcon(gerenciarDiretorio.getLogosPath()
				+ "guerrero_padrao.jpg"));*/
//		lblImgEmp.setIcon(new ImageIcon(PIVClientPanel.class.getResource("/images/cadastros/cliente/guerrero_padrao.png")));
		lblImgEmp.setIcon(new ImageIcon(PIVClientPanel.class.getResource("/images/cadastros/cliente/build1_128x128.png")));
		jpCentroPainel.add(lblImgEmp, "w 120! , h 120! , left , span 2 , wrap");

		LabelControl lblIdEmp = new LabelControl("Codigo");
		jpCentroPainel.add(lblIdEmp);

		NumericControl txtIdEmp = new NumericControl();
		txtIdEmp.setColumns(5);
		txtIdEmp.setEnabledOnEdit(false);
		txtIdEmp.setEnabledOnInsert(false);
		txtIdEmp.setAttributeName("id");
		txtIdEmp.setLinkLabel(lblIdEmp);
//		lblIdEmp.setLabelFor(txtIdEmp);
		jpCentroPainel.add(txtIdEmp);

		cmbBoxCnpjCpf = new JComboBox(new Object[] { "CNPJ", "CPF" });
		cmbBoxCnpjCpf.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
				try {
					if (event.getItem() == "CNPJ")
						PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
								PIVMaskResources.mask.CNPJ);
					else
						PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
								PIVMaskResources.mask.CPF);
				} catch (ParseException e) {
					e.printStackTrace();
					PIVLogSettings.getInstance().error(e.getMessage(), e);
				}
			}
		});
		jpCentroPainel.add(cmbBoxCnpjCpf);

		frmtdTxtCnpjCpf = new PIVFormTextControlCVal( controller );
		frmtdTxtCnpjCpf.addValidFunction(new PIVValidTextContentFunc() {
			@Override
			public boolean validaConteudo(String text, Integer pk) {
				
				PIVClientModel cliente;

				if (text.equals(""))
					return false;

				if(pk==null) pk = 0;
				String sqlCode = "from org.playiv.com.mvc.model.PIVClientModel as Cliente where Cliente.cnpjcpf = '"
						+ text + "' and Cliente.id <> " + pk;

				Session session = getPdao().getSession();
				cliente = (PIVClientModel) session.createQuery(sqlCode)
						.uniqueResult();
				session.clear();
				// Cliente encontrado
				if (cliente != null) {
					// Cliente desativado
					if (!cliente.isAtivo()) {
						int option = JOptionPane
								.showConfirmDialog(
										null,
										"Foi encontrado um cliente desativado"
												+ " - "
												+ (cliente.isPjuridica() ? " pessoa jur�dica "
														: " pessoa f�sica ")
												+ " - "
												+ " com o mesmo "
												+ (cliente.isPjuridica() ? "CNPJ"
														: "CPF\n")
												+ "Deseja ativa-lo e carregar seus dados?",
										"Ativar cliente desativado",
										JOptionPane.YES_NO_OPTION);
						if (option == JOptionPane.YES_OPTION) {
							cliente.setAtivo(true);
							getPdao().update(cliente);
							new PIVClientMngtDFController(null, cliente.getId());
						}
					} else {
						int option = JOptionPane
								.showConfirmDialog(
										null,
										"Foi encontrado um cliente"
												+ " - "
												+ (cliente.isPjuridica() ? " pessoa jur�dica "
														: " pessoa f�sica ")
												+ " - "
												+ " com o mesmo "
												+ (cliente.isPjuridica() ? "CNPJ"
														: "CPF\n")
												+ "Deseja carregar os dados do cliente?",
										"Carregar dados de cliente encontrado",
										JOptionPane.YES_NO_OPTION);
						if (option == JOptionPane.YES_OPTION) {
							new PIVClientMngtDFController(null, cliente.getId());
						}
					}
					return false;
				}
				return true;
			}
		});
		PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
				PIVMaskResources.mask.CNPJ);

		// frmtdTxtCnpjCpf =
		// PIVTextControlFactory.criaCpfCnpjTextControl(PIVMaskResources.mask.CNPJ);
		/*
		 * frmtdTxtCnpjCpf = PIVTextControlFactory
		 * .criaFormattedTextControl(PIVMaskResources.mask.CNPJ);
		 */
		// PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,
		// PIVMaskResources.mask.CNPJ);
		frmtdTxtCnpjCpf.setAttributeName("cnpjcpf");
		jpCentroPainel.add(frmtdTxtCnpjCpf, "grow , wrap");
		
		LabelControl lblRazaoSocial = new LabelControl("Raz�o Social");
		jpCentroPainel.add(lblRazaoSocial);

		TextControl txtRazaoSocial = new TextControl();
		txtRazaoSocial.setMaxCharacters(120);
		txtRazaoSocial.setTrimText(true);
		txtRazaoSocial.setAttributeName("razaosocial");
		txtRazaoSocial.setRequired(true);
		txtRazaoSocial.setLinkLabel(lblRazaoSocial);
//		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		jpCentroPainel.add(txtRazaoSocial, "grow , span 2 , wrap");

		LabelControl lblNomeFantasia = new LabelControl("Nome Fantasia");
		jpCentroPainel.add(lblNomeFantasia);

		TextControl txtNomeFantasia = new TextControl();
		txtNomeFantasia.setMaxCharacters(60);
		txtNomeFantasia.setTrimText(true);
		txtNomeFantasia.setAttributeName("nomefantasia");
		txtNomeFantasia.setRequired(true);
//		lblNomeFantasia.setLabelFor(txtNomeFantasia);
		txtNomeFantasia.setLinkLabel(lblNomeFantasia);
		jpCentroPainel.add(txtNomeFantasia, "grow , span , wrap");

		LabelControl lblWebsite = new LabelControl("Website");
		jpCentroPainel.add(lblWebsite);

		TextControl txtWebSite = new TextControl();
		txtWebSite.setMaxCharacters(120);
		txtWebSite.setTrimText(true);
		txtWebSite.setAttributeName("website");
//		lblWebsite.setLabelFor(txtWebSite);
		txtWebSite.setLinkLabel(lblWebsite);
		jpCentroPainel.add(txtWebSite, "grow");

		LabelControl lblEmail = new LabelControl("Email");
		jpCentroPainel.add(lblEmail);

		TextControl txtEmail = new TextControl();
		txtEmail.setMaxCharacters(120);
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("email");
//		lblWebsite.setLabelFor(txtWebSite);
		txtEmail.setLinkLabel(lblEmail);
		jpCentroPainel.add(txtEmail, "grow , wrap");
		
		LabelControl lblLeadFonte = new LabelControl("Fonte do lead");
		jpCentroPainel.add(lblLeadFonte,"");
		
		ComboBoxControl cboLeadFonte = new ComboBoxControl();
		cboLeadFonte.setAttributeName("fonte");
		cboLeadFonte.setDomainId("FONTE_LEAD");
		cboLeadFonte.setRequired(true);
//		lblLeadFonte.setLabelFor(cboLeadFonte);
		cboLeadFonte.setLinkLabel(lblLeadFonte);
		jpCentroPainel.add(cboLeadFonte,"growx");
		
		LabelControl lblLeadAtividade = new LabelControl("Tipo de neg�cio");
		jpCentroPainel.add(lblLeadAtividade,"");
		
		ComboBoxControl cboLeadNegocio = new ComboBoxControl();
		cboLeadNegocio.setAttributeName("atividade");
		cboLeadNegocio.setDomainId("ATIVIDADE_LEAD");
		cboLeadNegocio.setRequired(true);
//		lblLeadAtividade.setLabelFor(cboLeadNegocio);
		cboLeadNegocio.setLinkLabel(lblLeadAtividade);
		jpCentroPainel.add(cboLeadNegocio,"growx,wrap");
		
		LabelControl lblTel = new LabelControl("Telefone");
		jpCentroPainel.add(lblTel);

		FormattedTextControl txtTel = PIVTextControlFactory
				.criaFormattedTextControl(PIVMaskResources.mask.TEL);
		txtTel.setAttributeName("telefone");
		txtTel.setLinkLabel(lblTel);
//		lblTel.setLabelFor(txtTel);
		jpCentroPainel.add(txtTel, "growx");

		super.add(jpCentroPainel, "dock center , grow");
	}

	public boolean isPJuridica() {
		return this.cmbBoxCnpjCpf.getSelectedItem() == "CNPJ";
	}

	public void setPJuridica(boolean isJuridica) {
		this.cmbBoxCnpjCpf.setSelectedItem(isJuridica ? "CNPJ" : "CPF");
	}

	public void setEmpIcon(String filename) {
		if(filename!=null&&!filename.equals("")) {
			String fullPathFile = gerenciarDiretorio.getLogosPath() + filename;
			this.lblImgEmp.setIcon(new ImageIcon(fullPathFile));
			System.out.println(fullPathFile);
		}
	}

	public String getFileNameIcon() {
		return this.lblImgEmp.getIcon() == null || this.lblImgEmp.getIcon().toString().endsWith("build1_128x128.png") ? ""
				: this.lblImgEmp
						.getIcon()
						.toString()
						.substring(
								this.lblImgEmp.getIcon().toString()
										.lastIndexOf(File.separator) + 1);
	}

	public String getFullPathIcon() {
		return this.lblImgEmp.getIcon() == null ? "" : this.lblImgEmp.getIcon()
				.toString();
	}

}