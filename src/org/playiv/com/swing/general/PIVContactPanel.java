package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.table.columns.client.CheckBoxColumn;
import org.openswing.swing.table.columns.client.FormattedTextColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.mvc.controller.PIVClientContactGFController;

public class PIVContactPanel extends Form {

	private static final long serialVersionUID = 1L;

	private JPanel buttonsPanel;

	private SaveButton saveButton;
	private InsertButton insertButton;
	private EditButton editButton;
	private DeleteButton deleteButton;
	private ReloadButton reloadButton;

	private GridControl grid;

//	private JPanel jpCentroPainel;

	public PIVContactPanel(Integer pk, PIVClientContactGFController controller, boolean onlyGrid)
			throws ParseException {

		setLayout(new MigLayout("", "[][grow]", "[top][grow]"));
		
//		jpCentroPainel = new Form();
//		jpCentroPainel.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));

		grid = new GridControl();

		if(!onlyGrid)
			this.organizaCabecalho();

		grid.setBackground(new Color(245, 245, 245));
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVContactModel");
		grid.setAutoLoadData(false);
		grid.setVisibleStatusPanel(false);

		TextColumn colNome = new TextColumn();
		colNome.setEditableOnEdit(true);
		colNome.setEditableOnInsert(true);
		colNome.setMaxCharacters(120);
		colNome.setPreferredWidth(250);
		colNome.setColumnFilterable(true);
		colNome.setColumnSortable(true);
		colNome.setColumnRequired(true);
		colNome.setTrimText(true);
		colNome.setColumnName("nome");
		grid.add(colNome);

		TextColumn colFuncao = new TextColumn();
		colFuncao.setColumnName("funcao");
		colFuncao.setEditableOnEdit(true);
		colFuncao.setEditableOnInsert(true);
		colFuncao.setMaxCharacters(60);
		colFuncao.setPreferredWidth(150);
		colFuncao.setColumnFilterable(false);
		colFuncao.setColumnSortable(false);
		colFuncao.setColumnRequired(false);
		colFuncao.setTrimText(true);
		grid.add(colFuncao);

		FormattedTextColumn frmtdColTel = PIVTextControlFactory
				.criaFormaattedTextColumn(PIVMaskResources.mask.TEL);
		frmtdColTel.setEditableOnEdit(true);
		frmtdColTel.setEditableOnInsert(true);
		frmtdColTel.setPreferredWidth(90);
		frmtdColTel.setColumnFilterable(false);
		frmtdColTel.setColumnSortable(false);
		frmtdColTel.setColumnRequired(true);
		frmtdColTel.setColumnName("telefone");
		grid.add(frmtdColTel);

		FormattedTextColumn frmtdColCel = PIVTextControlFactory
				.criaFormaattedTextColumn(PIVMaskResources.mask.CEL);
		frmtdColCel.setEditableOnEdit(true);
		frmtdColCel.setEditableOnInsert(true);
		frmtdColCel.setPreferredWidth(90);
		frmtdColCel.setColumnFilterable(false);
		frmtdColCel.setColumnSortable(false);
		frmtdColCel.setColumnRequired(false);
		frmtdColCel.setColumnName("celular");
		grid.add(frmtdColCel);

		TextColumn colEmail = new TextColumn();
		colEmail.setEditableOnEdit(true);
		colEmail.setEditableOnInsert(true);
		colEmail.setPreferredWidth(150);
		colEmail.setColumnFilterable(false);
		colEmail.setColumnSortable(false);
		colEmail.setColumnRequired(false);
		colEmail.setTrimText(true);
		colEmail.setColumnName("email");
		grid.add(colEmail);

		CheckBoxColumn colResponsavel = new CheckBoxColumn();
		colResponsavel.setEditableOnEdit(true);
		colResponsavel.setEditableOnInsert(true);
		colResponsavel.setPreferredWidth(50);
		colResponsavel.setColumnFilterable(false);
		colResponsavel.setColumnSortable(false);
		colResponsavel.setColumnRequired(false);
		colResponsavel.setColumnName("responsavel");
		grid.add(colResponsavel);

		grid.setController(controller);
		grid.setGridDataLocator(controller);

		grid.setShowFilterPanelOnGrid(false);
		
		/*grid.setAllowInsertInEdit(true);*/
		
//		jpCentroPainel.add(grid, "grow, span");
		add(grid, "dock center, grow, span");

//		super.add(jpCentroPainel, "span , grow ");

	}

	public GridControl getGrid() {
		return this.grid;
	}

	public void setEnabledGridButtons(int mode) {
		switch (mode) {
		case Consts.READONLY:
			if(insertButton!=null)
				insertButton.setEnabled(false);
			if(deleteButton!=null)
				deleteButton.setEnabled(false);
			if(reloadButton!=null)
				reloadButton.setEnabled(false);
			if(saveButton!=null)
				saveButton.setEnabled(false);
			if(editButton!=null)
				editButton.setEnabled(false);
			break;
		case Consts.INSERT:
			if(insertButton!=null)
				insertButton.setEnabled(true);
			if(deleteButton!=null)
				deleteButton.setEnabled(true);
			if(reloadButton!=null)
				reloadButton.setEnabled(true);
			if(saveButton!=null)
				saveButton.setEnabled(false);
			if(editButton!=null)
				editButton.setEnabled(true);
			break;
		case Consts.EDIT:
			if(insertButton!=null)
				insertButton.setEnabled(true);
			if(deleteButton!=null)
				deleteButton.setEnabled(true);
			if(reloadButton!=null)
				reloadButton.setEnabled(true);
			if(saveButton!=null)
				saveButton.setEnabled(false);
			if(editButton!=null)
				editButton.setEnabled(true);
			break;
		}
		grid.reloadData();
	}
	
	private void organizaCabecalho( ) {
		
		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("CONTATO");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		add(jpNomePainel, "h 25! , dock north , growx");

//		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
//		jpCentroPainel.setBackground(new Color(245, 245, 245));
//		jpCentroPainel.setLayout(new MigLayout("", "[][grow]", "[top][grow]"));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBackground(new Color(245, 245, 245));
		
//		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
//		lblImgTopico.setHorizontalAlignmentC(SwingConstants.CENTER);
//		lblImgTopico.setIcon(new ImageIcon(PIVProductSalesPanel.class.getResource("/images/cadastros/cliente/contato_60x60.png")));
//		jpCentroPainel.add(lblImgTopico, "w 60! , h 60! , left");
//		add(lblImgTopico, "w 60! , h 60! , left");
		
		buttonsPanel = new JPanel();
		buttonsPanel.setBackground(new Color(245, 245, 245));

//		jpCentroPainel.add(buttonsPanel,
//				"h 50! , growx , spanx , bottom , north");
		add(buttonsPanel,
				"h 50! , growx , spanx , bottom ");

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		insertButton = new InsertButton();
		buttonsPanel.add(insertButton);

		grid.setInsertButton(insertButton);

		editButton = new EditButton();
		buttonsPanel.add(editButton);
		grid.setEditButton(editButton);

		deleteButton = new DeleteButton();
		buttonsPanel.add(deleteButton);
		grid.setDeleteButton(deleteButton);

		saveButton = new SaveButton();
		buttonsPanel.add(saveButton);
		grid.setSaveButton(saveButton);

		reloadButton = new ReloadButton();
		buttonsPanel.add(reloadButton);
		grid.setReloadButton(reloadButton);
		
	}

}