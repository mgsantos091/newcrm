package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.playiv.com.mvc.controller.PIVVoipRecordDFController;

public class PIVVoipTimerPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private WatcherGUI watcherPanel;
	
	public PIVVoipTimerPanel(PIVVoipRecordDFController controller)
			throws ParseException {

		super.setLayout( new MigLayout( ) );

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("MARCADOR DO TEMPO");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new BorderLayout());
		
		watcherPanel = new WatcherGUI();
		jpCentroPainel.add(watcherPanel, BorderLayout.CENTER);
		
		super.add(jpCentroPainel, "dock center , grow");

	}
	
	class WatcherGUI extends JPanel {
		
		private static final long serialVersionUID = -35363535177961727L;
		
		// GUI Components
	    private JPanel panel;
	    private JLabel timeLabel;

	    private JPanel buttonPanel;
	    private JButton startButton;
	    private JButton resetButton;
	    private JButton stopButton;

	    // Properties of Program.
	    private byte seconds = 0;
	    private byte minutes = 0;
	    private short hours = 0;

	    private DecimalFormat timeFormatter;

	    private Timer timer;
	    
	    private Timestamp inicio;
	    private Timestamp fim;

	    private ActionListener start = new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	if(inicio == null) inicio = new Timestamp((new Date()).getTime());
            	fim = null;
            	
                timer.start();

            }
        };
	    
	    public WatcherGUI() {
	        panel = new JPanel();
	        panel.setLayout(new BorderLayout());

	        timeLabel = new JLabel();
	        timeLabel.setFont(new Font("Consolas", Font.PLAIN, 42));
	        timeLabel.setHorizontalAlignment(JLabel.CENTER);
	        panel.add(timeLabel);

	        buttonPanel = new JPanel();
	        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

	        startButton = new JButton("Iniciar");
	        startButton.addActionListener(start);
	        buttonPanel.add(startButton);

	        resetButton = new JButton("Zerar");
	        resetButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {

	            	inicio = null;
	            	fim = null;
	            	
	                timer.stop();

	                seconds = 0;
	                minutes = 0;
	                hours = 0;

	                atualizaLabel();
	            }
	        });

	        buttonPanel.add(resetButton);

	        stopButton = new JButton("Parar");
	        stopButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	            	fim = new Timestamp((new Date()).getTime());
	                timer.stop();
	            }
	        });

	        buttonPanel.add(stopButton);

	        panel.add(buttonPanel, BorderLayout.SOUTH);

	        timeFormatter = new DecimalFormat("00");

	        timer = new Timer(1000, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	if( seconds < 59 ) {
	            		seconds++;
	            	} else {
	            		if( minutes > 59 ) {
	            			seconds = 0;
	            			minutes = 0;
	            			hours++;
	            		} else {
	            			seconds = 0;
	            			minutes++;
	            		}
	            	}
	            	atualizaLabel();
	            }
	        });

	        atualizaLabel();

	        add(panel);
	        
	    }

		public Timestamp getInicio() {
			return inicio;
		}

		public Timestamp getFim() {
			return fim;
		}
		
		private void atualizaLabel() {
	        timeLabel.setText(timeFormatter.format(hours) + ":"
	                + timeFormatter.format(minutes) + ":"
	                + timeFormatter.format(seconds));
		}
		
		private void setTime(Long time) {
			if(time == null) return;
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			cal.setTimeInMillis(time);
			hours = (short) cal.get(Calendar.HOUR);
			minutes = (byte) cal.get(Calendar.MINUTE);
			seconds = (byte) cal.get(Calendar.SECOND);
			atualizaLabel();
		}

		public void setInicio(Timestamp inicio) {
			this.inicio = inicio;
		}

		public void setFim(Timestamp fim) {
			this.fim = fim;
		}
		
		public void start() {
			start.actionPerformed(null);
		}
		
	}

	public Timestamp getInicioTempo() {
		return watcherPanel.getInicio();
	}
	
	public Timestamp getFimTempo() {
		return watcherPanel.getFim();
	}
	
	public void setTime(long time) {
		this.watcherPanel.setTime(time);
	}
	
	public void setInicioTempo(Timestamp inicio) {
		this.watcherPanel.setInicio(inicio);
	}

	public void setFimTempo(Timestamp fim) {
		this.watcherPanel.setFim(fim);
	}
	
	public void startTimer() {
		this.watcherPanel.start();
	}
	
}