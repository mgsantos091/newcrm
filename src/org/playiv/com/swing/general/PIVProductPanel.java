package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CodLookupControl;
import org.openswing.swing.client.CurrencyControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;
import org.playiv.com.mvc.controller.PIVCatLUFController;

public class PIVProductPanel extends JPanel {

	private static final long serialVersionUID = 1L;

/*	public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		PIVProdutoPanel pproduto = new PIVProdutoPanel();
		frame.add(pproduto,BorderLayout.CENTER);

		frame.pack();
		frame.setVisible(true);
	}
*/
	public PIVProductPanel( ) {

		super.setLayout(new MigLayout());
		
		super.setPreferredSize(new Dimension(400, 350));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
	
		JLabel lblTituloPainel = new JLabel("PRODUTO");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);
		
		super.add(jpNomePainel,"h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][left,grow][right][left,grow][grow]", ""));
		
		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico.setIcon(new ImageIcon(PIVAddressPanel.class.getResource("/images/cadastros/produto/produto_64x64.png")));
		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , spany 3 , gaptop 5 , gapright 5 , top , east ");
		/*jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , gaptop 5 , gapright 5 , top , east ");*/
		
		LabelControl lblIdProduto = new LabelControl("Codigo:");
		jpCentroPainel.add(lblIdProduto,"gaptop 5");
		
		NumericControl txtIdProduto = new NumericControl();
		txtIdProduto.setColumns(6);
		txtIdProduto.setEnabledOnEdit(false);
		txtIdProduto.setEnabledOnInsert(false);
		txtIdProduto.setAttributeName("id");
		
		lblIdProduto.setLabelFor(txtIdProduto);
		jpCentroPainel.add(txtIdProduto,"wrap , gaptop 5");

		LabelControl lblNomeProduto = new LabelControl("Nome:");
		jpCentroPainel.add(lblNomeProduto);
		
		TextControl txtNomeProduto = new TextControl();
		txtNomeProduto.setMaxCharacters(255);
		txtNomeProduto.setTrimText(true);
		txtNomeProduto.setRequired(true);
		txtNomeProduto.setAttributeName("nome");
		lblNomeProduto.setLabelFor(txtNomeProduto);
		jpCentroPainel.add(txtNomeProduto,"growx , spanx , wrap");
		
		LabelControl lblProduto = new LabelControl("Descri��o:");
		jpCentroPainel.add(lblProduto,"");
		
		TextAreaControl txtDescProduto = new TextAreaControl();
		txtDescProduto.setAttributeName("descricao");
		
		lblProduto.setLabelFor(txtDescProduto);
		jpCentroPainel.add(txtDescProduto,"h 50! , growx , spanx , wrap");
		
		LabelControl lblValCompra = new LabelControl("Valor de Compra:");
		jpCentroPainel.add(lblValCompra,"");
		
		CurrencyControl curValCompraProduto = new CurrencyControl();
		curValCompraProduto.setDecimals(4);
		
		curValCompraProduto.setAttributeName("valcompra");
		
		lblValCompra.setLabelFor(curValCompraProduto);
		jpCentroPainel.add(curValCompraProduto,"wrap");
		
		LabelControl lblValVenda = new LabelControl("Valor de Venda:");
		jpCentroPainel.add(lblValVenda);
		
		CurrencyControl curValVendaProduto = new CurrencyControl();
		curValVendaProduto.setDecimals(4);
		curValVendaProduto.setAttributeName("valvenda");
		
		lblValCompra.setLabelFor(curValVendaProduto);
		jpCentroPainel.add(curValVendaProduto,"wrap 35");
		
		LabelControl lblCodCategoria = new LabelControl("Id Categoria:");
		jpCentroPainel.add(lblCodCategoria);

		CodLookupControl codCategoria = new CodLookupControl();
		codCategoria.setLookupController(new PIVCatLUFController());
		codCategoria.setLookupButtonVisible(true);
		codCategoria.setAttributeName("categoria.id");
		codCategoria.setCanCopy(true);
		codCategoria.setMaxCharacters(5);
		codCategoria.setRequired(true);
		
		lblCodCategoria.setLabelFor(codCategoria);
		jpCentroPainel.add(codCategoria,"wrap");
		
		LabelControl lblNomeCategoria = new LabelControl("Nome Categoria:");
		jpCentroPainel.add(lblNomeCategoria);
		
		TextControl txtNomeCategoria = new TextControl();
		txtNomeCategoria.setEnabledOnEdit(false);
		txtNomeCategoria.setEnabledOnInsert(false);
		txtNomeCategoria.setAttributeName("categoria.nome");
		txtNomeCategoria.setMaxCharacters(255);
		txtNomeCategoria.setTrimText(true);
		
		lblNomeCategoria.setLabelFor(txtNomeCategoria);
		jpCentroPainel.add(txtNomeCategoria,"growx , spanx");
		
		super.add(jpCentroPainel,"dock center , grow , span");

	}
	
}