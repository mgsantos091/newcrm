package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVFormTextControlCVal;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.function.PIVValidTextContentFunc;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.mvc.controller.PIVLocalConfDFController;
import org.playiv.com.swing.component.PIVAutoReSizeLabel;
import org.playiv.com.swing.component.PIVImagePreviewLabel;

import br.com.caelum.stella.MessageProducer;
import br.com.caelum.stella.ResourceBundleMessageProducer;
import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import br.com.caelum.stella.validation.Validator;

public class PIVLocalConfPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private PIVFormTextControlCVal frmtdTxtCnpj;
	private PIVAutoReSizeLabel lblImgEmp;

	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();
	
	public PIVLocalConfPanel(PIVLocalConfDFController controller) throws ParseException {

		setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("EMPRESA");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][grow][right][grow][grow]", "[][][][][][][]"));
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		/*jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , gaptop 5 , gapright 5 , top , east ");*/
		
		JButton btnEscImgEmp = new JButton("...");
		btnEscImgEmp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				FileNameExtensionFilter filtroImagem = new FileNameExtensionFilter(
						"Imagens", "gif", "jpg", "png");

				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(filtroImagem);

				chooser.setCurrentDirectory(new File(System
						.getProperty("user.home")));

				chooser.setAccessory(new PIVImagePreviewLabel(chooser));

				int res = chooser.showOpenDialog(null);
				if (res == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					PIVLocalConfPanel.this.lblImgEmp.setIcon(new ImageIcon(file
							.getAbsolutePath()));
					System.out.println(file.getAbsolutePath());
				}
			}
		});
		jpCentroPainel.add(btnEscImgEmp, "cell 0 0,alignx right,aligny top");

		lblImgEmp = new PIVAutoReSizeLabel();
		lblImgEmp.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblImgEmp.setHorizontalAlignment(SwingConstants.CENTER);
		jpCentroPainel.add(lblImgEmp, "cell 1 0 2 1,width 120!,alignx left,height 120!");

/*		LabelControl lblIdEmp = new LabelControl("Codigo:");
		jpCentroPainel.add(lblIdEmp);

		NumericControl txtIdEmp = new NumericControl();
		txtIdEmp.setColumns(5);
		txtIdEmp.setEnabledOnEdit(false);
		txtIdEmp.setEnabledOnInsert(false);
		txtIdEmp.setAttributeName("id");
		lblIdEmp.setLabelFor(txtIdEmp);
		jpCentroPainel.add(txtIdEmp,"wrap");*/
		
				JLabel lblImgTopico = new JLabel();
				lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
				lblImgTopico.setIcon(new ImageIcon(PIVLocalConfPanel.class.getResource("/images/configuracao/configuracoes_locais/configuracao_64x64.png")));
				jpCentroPainel.add(lblImgTopico, "cell 4 0,width 120!,gapright 5,height 120!,aligny top,gapy 5");

		LabelControl lblRazaoSocial = new LabelControl("Raz�o Social:");
		jpCentroPainel.add(lblRazaoSocial, "cell 0 1");

		TextControl txtRazaoSocial = new TextControl();
		txtRazaoSocial.setMaxCharacters(120);
		txtRazaoSocial.setTrimText(true);
		txtRazaoSocial.setAttributeName("razaosocial");
		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		jpCentroPainel.add(txtRazaoSocial, "cell 1 1 5 1,growx");

		LabelControl lblNomeFantasia = new LabelControl("Nome Fantasia:");
		jpCentroPainel.add(lblNomeFantasia, "cell 0 2");

		TextControl txtNomeFantasia = new TextControl();
		txtNomeFantasia.setMaxCharacters(60);
		txtNomeFantasia.setTrimText(true);
		txtNomeFantasia.setAttributeName("nomefantasia");
		lblNomeFantasia.setLabelFor(txtNomeFantasia);
		jpCentroPainel.add(txtNomeFantasia, "cell 1 2 5 1,growx");
		
		LabelControl lblCnpj = new LabelControl("Cnpj:");
		jpCentroPainel.add(lblCnpj, "cell 0 3");

		frmtdTxtCnpj = new PIVFormTextControlCVal(controller);
		frmtdTxtCnpj.addValidFunction(new PIVValidTextContentFunc() {
			@Override
			public boolean validaConteudo(String cnpj,Integer pk) {
				if (((String) cnpj).equals("")) return true; // s� valida se existir algum valor
				Validator<String> validator;
				try {
					// Valida CNPJ
					ResourceBundle resourceBundle = ResourceBundle.getBundle("PlayIVValidationMessages",new Locale("pt","BR"));
					MessageProducer messageProducer = new ResourceBundleMessageProducer(resourceBundle);
					boolean isFormatted = false;
					validator = new CNPJValidator(messageProducer,isFormatted);
					validator.assertValid(cnpj);
				} catch( InvalidStateException e ) {
					StringBuilder msgError = new StringBuilder("");
		            for (ValidationMessage message : e.getInvalidMessages()) {
		            	msgError.append(message.getMessage()+"\n");
		            }
		            JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Houve um erro na valida��o do CNPJ \n" + msgError);
		            PIVLogSettings.getInstance().error(e.getMessage(), e);
					return false;
				}
				return true;
			}
		});
		PIVTextControlFactory.mudaMascara(frmtdTxtCnpj, PIVMaskResources.mask.CNPJ);

		//frmtdTxtCnpjCpf = PIVTextControlFactory.criaCpfCnpjTextControl(PIVMaskResources.mask.CNPJ);
		/*frmtdTxtCnpjCpf = PIVTextControlFactory
				.criaFormattedTextControl(PIVMaskResources.mask.CNPJ);*/
		//PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf, PIVMaskResources.mask.CNPJ);
		
		frmtdTxtCnpj.setAttributeName("cnpj");
		lblCnpj.setLabelFor(frmtdTxtCnpj);
		jpCentroPainel.add(frmtdTxtCnpj, "cell 1 3 3 1,growx");

		LabelControl lblWebsite = new LabelControl("Website:");
		jpCentroPainel.add(lblWebsite, "cell 0 4");

		TextControl txtWebSite = new TextControl();
		txtWebSite.setMaxCharacters(120);
		txtWebSite.setTrimText(true);
		txtWebSite.setAttributeName("website");
		lblWebsite.setLabelFor(txtWebSite);
		jpCentroPainel.add(txtWebSite, "cell 1 4 5 1,growx");

		LabelControl lblEmail = new LabelControl("Email:");
		jpCentroPainel.add(lblEmail, "cell 0 5");

		TextControl txtEmail = new TextControl();
		txtEmail.setMaxCharacters(120);
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("email");
		lblWebsite.setLabelFor(txtWebSite);
		jpCentroPainel.add(txtEmail, "cell 1 5 5 1,growx");

		super.add(jpCentroPainel, "center , grow");

	}

	public void setEmpIcon(String filename) {
		String fullPathFile = gerenciarDiretorio.getLogosPath() + filename;
		this.lblImgEmp.setIcon(new ImageIcon(fullPathFile));
		System.out.println(fullPathFile);
	}

	public String getFileNameIcon() {
		return this.lblImgEmp.getIcon() == null ? ""
				: this.lblImgEmp.getIcon().toString().substring(this.lblImgEmp.getIcon().toString().lastIndexOf("\\") + 1);
	}

	public String getFullPathIcon() {
		return this.lblImgEmp.getIcon() == null ? ""
				: this.lblImgEmp.getIcon().toString();
	}
	
}