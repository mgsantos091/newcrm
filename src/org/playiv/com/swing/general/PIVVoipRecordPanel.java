package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;
import org.playiv.com.mvc.controller.PIVVoipRecordDFController;

public class PIVVoipRecordPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public PIVVoipRecordPanel(PIVVoipRecordDFController controller)
			throws ParseException {

		super.setLayout( new MigLayout( ) );

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("DADOS DA LIGA��O");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("",
				"[right][grow][]", ""));

		LabelControl lblIdLigacao = new LabelControl("Codigo");
		jpCentroPainel.add(lblIdLigacao);

		NumericControl txtIdLigacao = new NumericControl();
		txtIdLigacao.setColumns(5);
		txtIdLigacao.setEnabledOnEdit(false);
		txtIdLigacao.setEnabledOnInsert(false);
		txtIdLigacao.setAttributeName("id");
		txtIdLigacao.setLinkLabel(lblIdLigacao);
//		lblIdEmp.setLabelFor(txtIdEmp);
		jpCentroPainel.add(txtIdLigacao,"wrap");
		
		LabelControl lblNomeCliente = new LabelControl("Cliente");
		jpCentroPainel.add(lblNomeCliente);
		
		TextControl txtNomeCliente = new TextControl();
		txtNomeCliente.setEnabledOnEdit(false);
		txtNomeCliente.setEnabledOnInsert(false);
		txtNomeCliente.setAttributeName("cliente.nomefantasia");
		txtNomeCliente.setLinkLabel(lblNomeCliente);
		jpCentroPainel.add(txtNomeCliente,"growx,wrap");
		
		LabelControl lblNumero = new LabelControl("N�mero");
		jpCentroPainel.add(lblNumero);
		
		TextControl txtNumero = new TextControl();
		txtNumero.setEnabledOnEdit(false);
		txtNumero.setEnabledOnInsert(false);
		txtNumero.setAttributeName("numero");
		txtNumero.setRequired(true);
		txtNumero.setLinkLabel(lblNumero);
		jpCentroPainel.add(txtNumero,"wrap");
		
//		LabelControl lbDuracao = new LabelControl("Dura��o");
//		jpCentroPainel.add(lbDuracao);
//		
//		DateControl txtDuracao = new DateControl(Consts.TYPE_TIME, Resources.DMY, '-', false, Resources.HH_MM_SS, TimeZone.getTimeZone("GMT"));
//		txtDuracao.setEnabledOnEdit(false);
//		txtDuracao.setEnabledOnInsert(false);
//		txtDuracao.setAttributeName("duracao");
//		txtDuracao.setLinkLabel(lblNumero);
//		jpCentroPainel.add(txtDuracao,"wrap");

		LabelControl lblStatusLigacao = new LabelControl("Status da Liga��o");
		jpCentroPainel.add(lblStatusLigacao,"");
		
		ComboBoxControl cboStatusLigacao = new ComboBoxControl();
		cboStatusLigacao.setAttributeName("status");
		cboStatusLigacao.setDomainId("STATUS_LIGACAO");
		cboStatusLigacao.setRequired(true);
		cboStatusLigacao.setLinkLabel(lblStatusLigacao);
		jpCentroPainel.add(cboStatusLigacao,"growx,wrap");
		
		LabelControl lblAnotacao = new LabelControl("Anota��o:");
		jpCentroPainel.add(lblAnotacao, "");

		TextAreaControl txtAnotacao = new TextAreaControl();
		txtAnotacao.setMaxCharacters(10000);
		txtAnotacao.setAttributeName("anotacao");
		txtAnotacao.setRequired(false);
		txtAnotacao.setLinkLabel(lblAnotacao);

		jpCentroPainel.add(txtAnotacao, "h 250! , growx , spanx");

		super.add(jpCentroPainel, "dock center , grow");
		
	}

}