package org.playiv.com.swing.general;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVHistoryOfActionsTableCellRow;
import org.playiv.com.mvc.model.PIVEventRegisterModel;

public class PIVHistoryOfActionsTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private List<PIVHistoryOfActionsTableCellRow> linhas = new ArrayList<PIVHistoryOfActionsTableCellRow>();
	private int numeroDeColunas;
	
	public static void main(String args[]) {
		
		String sql = "from Evento in class org.playiv.com.mvc.model.PIVEventRegisterModel ";
		Session session = PIVDao.getInstance().getSession();
		@SuppressWarnings("unchecked")
		ArrayList<PIVEventRegisterModel> eventos = (ArrayList<PIVEventRegisterModel>) session.createQuery(sql).list();
		session.close();
		
		/*TableModel tableModel = new PIVHistoryOfActionsTableModel( eventos );*/
		
		JFrame frame = new JFrame();
		frame.setPreferredSize(new Dimension(500,300));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		/*frame.setLayout(new FlowLayout(FlowLayout.LEADING));*/
	
		/*tblTeste.setModel(tableModel);*/
		PIVHistoryOfActionsTableFunc tblTeste = new PIVHistoryOfActionsTableFunc();
		tblTeste.carregar(eventos);
		/*tblTeste.repaint();*/
		
		JScrollPane scrollPane = new JScrollPane();
		/*scrollPane.setBackground(Color.WHITE);*/
		scrollPane.setViewportView(tblTeste);
		frame.add(scrollPane, BorderLayout.CENTER);
		
		/*frame.setBackground(Color.WHITE);*/
		
		frame.pack();
		
		frame.setVisible(true);
		
	}
	
	public PIVHistoryOfActionsTableModel( ) {
		
	}
	
	// construtor sobrescrito para popular a tabela de acordo com os elementos passados
	public PIVHistoryOfActionsTableModel( ArrayList<PIVEventRegisterModel> eventos ) {		
		this( );
		carregar(eventos);
	}
	
	public void carregar( ArrayList<PIVEventRegisterModel> eventos ) {
		
		int firstRow = getRowCount();
		
		int numeroDeColunas = 0;
		int numeroDeColunasASerChecada = 0;
		
		Collections.sort(eventos,PIVEventRegisterModel.ORDERNA_POR_DATA); // ordena por data
		Timestamp ultimaDataASerChecada = eventos.get(0).getDataregistro();
		for(PIVEventRegisterModel evento : eventos) { // encontra o maior numero de colunas por dia
			Timestamp data = evento.getDataregistro();
			if(checaDiaDoAno(data,ultimaDataASerChecada)) // caso seja o mesmo dia
				numeroDeColunasASerChecada ++;
			else numeroDeColunasASerChecada = 1;
			if(numeroDeColunasASerChecada>numeroDeColunas) numeroDeColunas = numeroDeColunasASerChecada;
			ultimaDataASerChecada = data;
		}
		this.numeroDeColunas = numeroDeColunas + 1;

		// inicia o processo de constru��o din�mica de colunas da tabela
		PIVHistoryOfActionsTableCellRow linha = new PIVHistoryOfActionsTableCellRow(eventos.get(0).getDataregistro());
		ultimaDataASerChecada = eventos.get(0).getDataregistro();
		for(PIVEventRegisterModel evento : eventos) {
			Timestamp data = evento.getDataregistro();
			if(checaDiaDoAno(data,ultimaDataASerChecada)) // caso seja o mesmo dia
				linha.addEvento(evento);
			else {
				if(linha.getColumnLength()<this.numeroDeColunas) // regulariza a quantidade de colunas
					for(int x=0;x<=this.numeroDeColunas-linha.getColumnLength();x++)
						linha.addEvento(null);
				this.linhas.add(linha);
				linha = new PIVHistoryOfActionsTableCellRow(data);
				linha.addEvento(evento);
			}
			ultimaDataASerChecada = data;
		}
		this.linhas.add(linha);
		
		int lastRow = getRowCount() - 1;
		
		fireTableRowsInserted(firstRow, lastRow);
		
		fireTableStructureChanged();
		
	}
	
	private boolean checaDiaDoAno( Timestamp dia1 , Timestamp dia2 ) {
		
		Calendar calendario = Calendar.getInstance();
		calendario.setTimeInMillis(dia1.getTime());
		int diaAComparar = calendario.get(Calendar.DAY_OF_MONTH);
		int mesAComparar = calendario.get(Calendar.MONTH);
		int anoAComparar = calendario.get(Calendar.YEAR);
		
		calendario.setTimeInMillis(dia2.getTime());
		int diaACompararAnterior = calendario.get(Calendar.DAY_OF_MONTH);
		int mesACompararAnterior = calendario.get(Calendar.MONTH);
		int anoACompararAnterior = calendario.get(Calendar.YEAR);
		
		/*int difDias = (int) ((data.getTime() - ultimaDataASerChecada.getTime()) / (1000 * 60 * 60 * 24));*/
		/*if(difDias==0)*/
		if(diaAComparar==diaACompararAnterior&&mesAComparar==mesACompararAnterior&&anoAComparar==anoACompararAnterior)
			return true;
		else
			return false;

	}
	
	@Override
	public int getColumnCount() {
		return numeroDeColunas;
		/*int numeroDeColunas = 0;
		for(PIVHistoryOfActionsTableCellRow linha : linhas) {
			if(numeroDeColunas < linha.getColumnLength())
				numeroDeColunas = linha.getColumnLength();
		}
		return numeroDeColunas;*/
	}
	
	@Override
	public int getRowCount() {
		return linhas.size();
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return "";
	};
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
		/*return getValueAt(0, columnIndex).getClass();*/
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		PIVHistoryOfActionsTableCellRow elemento = (PIVHistoryOfActionsTableCellRow) linhas.get(rowIndex);
		return elemento.getElementAt(columnIndex);
	}
	
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {};
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public PIVHistoryOfActionsTableCellRow getRow( int rowIndex ) {
		return this.linhas.get(rowIndex);
	}
	
	public void addRow( PIVHistoryOfActionsTableCellRow row ) {
		this.linhas.add(row);
		int lastIndex = getRowCount() - 1;
		fireTableRowsInserted(lastIndex, getRowCount() - 1);
	}
	
	public void removeRow( int rowIndex ) {
		linhas.remove(rowIndex);
		fireTableRowsDeleted(rowIndex,rowIndex);
	}
	
	public void addRowList( List<PIVHistoryOfActionsTableCellRow> rows ) {
		int oldSize = getRowCount();
		rows.addAll(rows);
		fireTableRowsInserted(oldSize, getRowCount() - 1);
	}

	public void clean() {
		linhas.clear();
		fireTableDataChanged();
	}
	
	public boolean isEmpty() {
		return linhas.isEmpty();
	}

}