package org.playiv.com.swing.general;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import org.playiv.com.mvc.controller.PIVSellerMngtCFController;
import org.playiv.com.swing.component.PIVCustomFlowLayout;

public class PIVSellerMngtBCGMatrixPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private PIVSellerMngtCFController controller;

	public PIVSellerMngtBCGMatrixPanel( ) {
		inicializaListeners( );
		setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
	}

	public PIVSellerMngtCFController getController() {
		return this.controller;
	}

	public void setController( PIVSellerMngtCFController controller ) {
		this.controller = controller;
	}

	private void inicializaListeners() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(controller!=null)
					controller.setElementoSelecionado(null);
			}
		});
	}

}