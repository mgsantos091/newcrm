package org.playiv.com.swing.general;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import net.miginfocom.swing.MigLayout;

import org.playiv.com.library.general.IPIVElementPanel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.component.PIVAutoReSizeLabel;

public class PIVSalesElementComponent extends JComponent implements IPIVElementPanel {

	private static final long serialVersionUID = 1L;

	private PIVSellerModel vendedor;

	private PIVAutoReSizeLabel lblImgElem;
	private JLabel lblNmElemen;

	public PIVSalesElementComponent( ) {

		setLayout(new MigLayout());
		
		lblImgElem = new PIVAutoReSizeLabel();
		lblImgElem.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lblImgElem,"w 100! , h 75! , top");
		
		lblNmElemen = new JLabel();
		lblNmElemen.setForeground(new Color(0,0,255));
		lblNmElemen.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lblNmElemen,"w 100! , south");

		desfazSeleciona( );
			
	}

	public void setVendedor( PIVSellerModel vendedor ) {
		this.vendedor = vendedor;
		lblImgElem.setIcon(new ImageIcon(PIVSalesElementComponent.class.getResource("/images/cadastros/cliente/guerrero_padrao.png")));
		lblNmElemen.setText(vendedor.getNome());
	}

	public PIVSellerModel getVendedor( ) {
		return this.vendedor;
	}

	
	/* (non-Javadoc)
	 * @see org.playiv.com.swing.general.IPIVElementPanel#seleciona()
	 */
	@Override
	public void seleciona( ) {
		this.lblImgElem.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		this.lblNmElemen.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		this.lblNmElemen.setForeground(Color.BLACK);
	}
	
	/* (non-Javadoc)
	 * @see org.playiv.com.swing.general.IPIVElementPanel#desfazSeleciona()
	 */
	@Override
	public void desfazSeleciona( ) {
		this.lblImgElem.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		this.lblNmElemen.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		this.lblNmElemen.setForeground(Color.BLACK);
	}
	
}