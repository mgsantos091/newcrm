package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import net.atlanticbb.tantlinger.shef.HTMLEditorPane;
import net.miginfocom.swing.MigLayout;

import org.openswing.swing.tree.client.TreePanel;
import org.playiv.com.mvc.controller.PIVCubeTreeFrameController;

//import chrriis.dj.nativeswing.swtimpl.components.JHTMLEditor;

public class PIVCubeTreeFrame extends JDialog {
	private static final long serialVersionUID = 1L;
	private TreePanel tree = new TreePanel();
	private JTextField txtAssunto;
	private Boolean ok = Boolean.valueOf(false);
	private String htmlContent;
//	private JHTMLEditor htmlEditor;
	private HTMLEditorPane htmlEditor;

	public TreePanel getTree() {
		return this.tree;
	}

	public PIVCubeTreeFrame(PIVCubeTreeFrameController controller) {
		setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
		setLayout(new BorderLayout());
		
		JPanel emailPanel = new JPanel();
		emailPanel.setLayout(new MigLayout("", "[right][grow][]", "5[]5[]"));
		emailPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		emailPanel.setBackground(new Color(245, 245, 245));
//		emailPanel.setLayout(new MigLayout("", "[right][grow][]", ""));
		
		setTitle("PlayIV - Envio de e-mail");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(
				getGraphicsConfiguration());
		int taskBarSize = scnMax.bottom;
		setSize(screenSize.width - getWidth(), screenSize.height - taskBarSize
				- getHeight());
		
		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("EMAIL");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", 0, 16));
		jpNomePainel.add(lblTituloPainel);

		emailPanel.add(jpNomePainel, "h 25! , dock north , growx , wrap");
		
		JLabel lblAssunto = new JLabel("Assunto: ");
		emailPanel.add(lblAssunto);

		this.txtAssunto = new JTextField();
		emailPanel.add(this.txtAssunto, "growx , wrap");
		
//		add(createCKEditor(),BorderLayout.CENTER);
//		emailPanel.add(createCKEditor(), "dock center , grow , span");
		emailPanel.add(createShefHtmlEditor(), "dock center , grow , span");
		
		JPanel botoesPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		botoesPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		botoesPanel.setBackground(new Color(245, 245, 245));
		
		JButton okButton = new JButton("Confirma");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				htmlContent = htmlEditor.getHTMLContent();
				htmlContent = htmlEditor.getText();
//				System.out.println(htmlContent);
				PIVCubeTreeFrame.this.ok = Boolean.valueOf(true);
				PIVCubeTreeFrame.this.dispose();
			}
		});
		botoesPanel.add(okButton);

		JButton cancelaButton = new JButton("Cancela");
		cancelaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PIVCubeTreeFrame.this.dispose();
			}
		});
		botoesPanel.add(cancelaButton);
		
		// -----
		
		JPanel jpAnexoCorpoPainel = new JPanel();
		jpAnexoCorpoPainel.setLayout(new MigLayout("", "", ""));
		jpAnexoCorpoPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpAnexoCorpoPainel.setBackground(new Color(245, 245, 245));
		
		JPanel jpAnexoNomePainel = new JPanel();
		jpAnexoNomePainel.setBackground(new Color(112, 128, 144));
		jpAnexoNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblAnexoTituloPainel = new JLabel("CUBO DE CONHECIMENTO");
		lblAnexoTituloPainel.setForeground(Color.WHITE);
		lblAnexoTituloPainel.setFont(new Font("Arial Bold", 0, 16));
		jpAnexoNomePainel.add(lblAnexoTituloPainel);
		
		jpAnexoCorpoPainel.add(jpAnexoNomePainel, "h 25! , dock north , growx");
		
		this.tree.setSelectionForeground(Color.BLACK);
		this.tree.setTreeController(controller);
		this.tree.setTreeDataLocator(controller);
		this.tree.setLeavesImageName("node.gif");
		this.tree.setExpandAllNodes(true);
		this.tree.setShowCheckBoxes(true);
		this.tree.setShowCheckBoxesOnLeaves(true);
		this.tree.setShowsRootHandles(true);
		this.tree.setEnabled(true);
		
		jpAnexoCorpoPainel.add(this.tree,"dock center");
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				emailPanel, jpAnexoCorpoPainel);

		splitPane.setDividerLocation(screenSize.width
				- splitPane.getInsets().right - splitPane.getDividerSize()
				- 240);
		
		add(splitPane, BorderLayout.CENTER);
		add(botoesPanel,BorderLayout.SOUTH);
		
		setVisible(true);
	}

	public String getAssuntoEmail() {
		return this.txtAssunto.getText();
	}

	public String getCorpoEmail() {
//		return this.txtConteudo.getText();
		return this.htmlContent;
	}

	public Boolean getOk() {
		return this.ok;
	}

	private JPanel createShefHtmlEditor() {
		htmlEditor = new HTMLEditorPane();
		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.add(htmlEditor,BorderLayout.CENTER);
		return contentPane;
	}
	
/*	private JPanel createCKEditor() {
		JPanel contentPane = new JPanel(new BorderLayout());
		Map<String, String> optionMap = new HashMap<String, String>();
		optionMap
				.put("toolbar",
						"["
								+
								// "  ['Source','-','Save','NewPage','Preview','-','Templates'],"
								// +
//								"  ['Save','Preview'],"
//								+
								// "  ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],"
								// +
								"  ['Cut','Copy','Paste','-','Print'],"
								+ "  ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],"
								+
								// "  ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],"
								// +
								// "  ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],"
								// +
								"  '/',"
								+ "  ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],"
								+ "  ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],"
								+ "  ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],"
								+ "  ['Link','Unlink','Anchor'],"
								+
								// "  ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],"
								// +
								"  ['Image','Table','HorizontalRule','PageBreak'],"
								+ "  '/',"
								+ "  ['Styles','Format','Font','FontSize'],"
								+ "  ['TextColor','BGColor'],"
//								+ "  ['Maximize', 'ShowBlocks','-','About']"
								+ "  ['ShowBlocks','-','About']"
								+ "]");
		// optionMap.put("uiColor", "'#9AB8F3'");
		// optionMap.put("toolbarCanCollapse", "false");
		htmlEditor = new JHTMLEditor(
				JHTMLEditor.HTMLEditorImplementation.CKEditor,
				JHTMLEditor.CKEditorOptions.setOptions(optionMap));
//		htmlEditor.addHTMLEditorListener(new HTMLEditorAdapter() {
//			@Override
//			public void saveHTML(HTMLEditorSaveEvent e) {
//				JOptionPane
//						.showMessageDialog(null,
//								"The data of the HTML editor could be saved anywhere...");
//			}
//		});
		contentPane.add(htmlEditor, BorderLayout.CENTER);
//		 JPanel southPanel = new JPanel(new BorderLayout());
//		 southPanel.setBorder(BorderFactory.createTitledBorder("Custom Controls"));
//		 JPanel middlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
//		 JButton setHTMLButton = new JButton("Set HTML");
//		 middlePanel.add(setHTMLButton);
//		 JButton getHTMLButton = new JButton("Get HTML");
//		 middlePanel.add(getHTMLButton);
//		 southPanel.add(middlePanel, BorderLayout.NORTH);
//		 final JTextArea htmlTextArea = new JTextArea();
//		 htmlTextArea.setCaretPosition(0);
//		 JScrollPane scrollPane = new JScrollPane(htmlTextArea);
//		 scrollPane.setPreferredSize(new Dimension(0, 100));
//		 southPanel.add(scrollPane, BorderLayout.CENTER);
//		 contentPane.add(southPanel, BorderLayout.SOUTH);
//		 getHTMLButton.addActionListener(new ActionListener() {
//		 public void actionPerformed(ActionEvent e) {
//		 htmlTextArea.setText(htmlEditor.getHTMLContent());
//		 htmlTextArea.setCaretPosition(0);
//		 }
//		 });
//		 setHTMLButton.addActionListener(new ActionListener() {
//		 public void actionPerformed(ActionEvent e) {
//		 htmlEditor.setHTMLContent(htmlTextArea.getText());
//		 }
//		 });
//		htmlEditor.setHTMLContent("");
		return contentPane;
	}*/

	public String getHtmlContent() {
		return htmlContent;
	}

}
