package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVUserNoteDFController;
import org.playiv.com.mvc.controller.PIVUserNoteGFController;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVUserNotePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private JPanel buttonsPanel;

	private ReloadButton reloadButton;
	private InsertButton insertButton;
	private DeleteButton deleteButton;
	private ExportButton exportButton1;
	private FilterButton filterButton1;
	
	private GridControl grid;
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colAnotacao = new TextColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();
	
	private PIVClientModel cliente;
	
	private PIVBusRelMainCFController organizaCliente;
	
	private boolean onlyGrid = false;
	
	public PIVUserNotePanel(PIVUserNoteGFController controller,boolean onlyGrid) {
		try {
			this.onlyGrid = onlyGrid;
			this.cliente = controller.getCliente();
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public PIVUserNotePanel(PIVUserNoteGFController pivUserNoteGFController,
			PIVClientModel cliente2, PIVBusRelMainCFController organizaCliente,boolean onlyGrid) {
		this(pivUserNoteGFController,onlyGrid);
		this.organizaCliente = organizaCliente;
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		
		this.setLayout(new BorderLayout());
		
		this.grid = new GridControl();
		
		this.buttonsPanel = new JPanel();
		
		if(!this.onlyGrid) {
		
			reloadButton = new ReloadButton();
			insertButton = new InsertButton();
			deleteButton = new DeleteButton();
			exportButton1 = new ExportButton();
			filterButton1 = new FilterButton();
			
			buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	
			grid.setDeleteButton(deleteButton);
			grid.setExportButton(exportButton1);
			grid.setFilterButton(filterButton1);
			grid.setInsertButton(null);
			grid.setReloadButton(reloadButton);
			
			this.add(buttonsPanel, BorderLayout.NORTH);
			buttonsPanel.add(insertButton, null);
			buttonsPanel.add(reloadButton, null);
			buttonsPanel.add(deleteButton, null);
			buttonsPanel.add(exportButton1, null);
			buttonsPanel.add(filterButton1, null);
			
			insertButton
			.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
					this));
		
		}
		
		grid.setAnchorLastColumn(true);
		
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVUserNoteModel");
		
		this.add(grid, BorderLayout.CENTER);
		
		colId.setColumnName("id");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(25);
		grid.getColumnContainer().add(colId);

		colAnotacao.setColumnName("anotacao");
		colAnotacao.setMaxCharacters(255);
		colAnotacao.setPreferredWidth(50);
		grid.getColumnContainer().add(colAnotacao);
		
		colDataRegistro.setColumnName("dataregistro");
		colDataRegistro.setPreferredWidth(80);
		grid.getColumnContainer().add(colDataRegistro);
		
		/*setSize(500, 300);*/
	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVUserNoteDFController(this.grid, null,cliente,this.organizaCliente);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVUserNotePanel adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVUserNotePanel adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}
	
}
