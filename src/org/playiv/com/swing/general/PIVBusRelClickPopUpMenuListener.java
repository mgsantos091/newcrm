package org.playiv.com.swing.general;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PIVBusRelClickPopUpMenuListener extends MouseAdapter {

	private PIVBusRelPopUpMenu menu;

	public PIVBusRelClickPopUpMenuListener(PIVBusRelPopUpMenu menu) {
		this.menu = menu;
	}

	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger())
			doPop(e);
	}

	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			if (e.getSource() instanceof PIVBusRelElementComponent)
				((PIVBusRelElementComponent) e.getSource()).getOrganizacliente()
						.setElementoSelecionado(
								(PIVBusRelElementComponent) e.getSource());
			else if (e.getSource() instanceof PIVBusRelElementPanel) {
				((PIVBusRelElementPanel) e.getSource())
						.getOrganizaClienteRelacionado()
						.setElementoSelecionado(null);
			}
			doPop(e);
		}
	}

	private void doPop(MouseEvent e) {
		menu.show(e.getComponent(), e.getX(), e.getY());
	}

}