package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.hibernate.Session;
import org.openswing.swing.client.DateControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVSoundMngtFunc;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVUserNoteDFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVUserNoteModel;

public class PIVClientNotePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private LabelControl lblInfo2;

	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc
			.getInstance();
	private NumericControl txtIdAnotacao;

	private PIVClientModel cliente;

	private PIVSoundMngtFunc gerGrvVoz;

	private JButton btnPlayPause;
	private JButton btnGravar;
	private JButton btnStop;

	/*
	 * public static void main(String args[]) { JFrame frame = new JFrame();
	 * frame.setLayout(new BorderLayout());
	 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 * 
	 * PIVProdutoPanel pproduto = new PIVProdutoPanel();
	 * frame.add(pproduto,BorderLayout.CENTER);
	 * 
	 * frame.pack(); frame.setVisible(true); }
	 */
	public PIVClientNotePanel(PIVClientModel cliente,
			final PIVUserNoteDFController controller) {

		this.cliente = cliente;

		setLayout(new MigLayout());

		super.setPreferredSize(new Dimension(600, 550));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("ANOTA��O");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("",
				"[right][left,grow][right][left,grow][grow]", ""));

		JLabel lblImgTopico = new JLabel();
		/* lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK)); */
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico
				.setIcon(new ImageIcon(
						PIVClientNotePanel.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_64x64.png")));
		// lblImgTopico.setIcon(new ImageIcon(PIVClientePanel.class
		// .getResource("/resources/home3.png")));
		jpCentroPainel
				.add(lblImgTopico,
						"w 120! , h 120! , spany 3 , gaptop 5 , gapright 5 , gapleft 5 , top , east ");
		/*
		 * jpCentroPainel.add(lblImgTopico,
		 * "w 120! , h 120! , gaptop 5 , gapright 5 , top , east ");
		 */

		LabelControl lblIdProduto = new LabelControl("Codigo:");
		jpCentroPainel.add(lblIdProduto, "gaptop 5");

		txtIdAnotacao = new NumericControl();
		txtIdAnotacao.setColumns(6);
		txtIdAnotacao.setEnabledOnEdit(false);
		txtIdAnotacao.setEnabledOnInsert(false);
		txtIdAnotacao.setAttributeName("id");

		lblIdProduto.setLabelFor(txtIdAnotacao);
		jpCentroPainel.add(txtIdAnotacao, "wrap , gaptop 5");

		LabelControl lblAnotacao = new LabelControl("Anota��o:");
		jpCentroPainel.add(lblAnotacao, "");

		TextAreaControl txtAnotacao = new TextAreaControl();
		txtAnotacao.setMaxCharacters(10000);
		txtAnotacao.setAttributeName("anotacao");
		txtAnotacao.setRequired(true);

		lblAnotacao.setLabelFor(txtAnotacao);
		jpCentroPainel.add(txtAnotacao, "h 250! , growx , spanx , wrap");

		JPanel jpGrvVozPainel = new JPanel();
		jpGrvVozPainel.setBorder(BorderFactory.createTitledBorder(
				new LineBorder(Color.BLACK), "Observa��o por voz"));
		jpGrvVozPainel.setBackground(new Color(245, 245, 245));
		jpGrvVozPainel.setLayout(new MigLayout("", "[][][]", "[][]"));

		btnPlayPause = new JButton();
		btnPlayPause.setIcon(new ImageIcon(PIVClientNotePanel.class.getResource("/images/gerenciamento_relcomercial/tela-2/playpause_32x32.png")));
		btnPlayPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.executaPlayPause();
			}
		});
		jpGrvVozPainel.add(btnPlayPause, "cell 0 0");

		btnGravar = new JButton();
		btnGravar.setIcon(new ImageIcon(PIVClientNotePanel.class.getResource("/images/gerenciamento_relcomercial/tela-2/mic_32x32.png")));
		btnGravar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.executaGravar();
			}
		});
		jpGrvVozPainel.add(btnGravar, "cell 1 0");

		/*JButton btnPause = new JButton("Pausar");
		btnPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.executaPausar();
			}
		});
		jpGrvVozPainel.add(btnPause);*/

		btnStop = new JButton();
		btnStop.setIcon(new ImageIcon(PIVClientNotePanel.class.getResource("/images/gerenciamento_relcomercial/tela-2/stop_32x32.png")));
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.executaParar();
			}
		});
		jpGrvVozPainel.add(btnStop, "cell 2 0");

		lblInfo2 = new LabelControl("Aguardando a��es...");
		controller.setInfo(lblInfo2);
		
		jpGrvVozPainel.add(lblInfo2, "cell 0 1 3 1");

		jpCentroPainel.add(jpGrvVozPainel, "growx , spanx , wrap");

		LabelControl lblDataRegistro = new LabelControl("Data registro:");
		jpCentroPainel.add(lblDataRegistro);

		DateControl dtDataRegistro = new DateControl();
		dtDataRegistro.setEnabledOnEdit(false);
		dtDataRegistro.setEnabledOnInsert(false);
		dtDataRegistro.setDateType(Consts.TYPE_DATE_TIME);
		dtDataRegistro.setAttributeName("dataregistro");

		lblDataRegistro.setLabelFor(dtDataRegistro);
		jpCentroPainel.add(dtDataRegistro);

		super.add(jpCentroPainel, "dock center , grow , span");
	}

	public Integer getPk() {
		return ((BigDecimal) (txtIdAnotacao.getValue()==null?new BigDecimal(0):txtIdAnotacao.getValue())).intValue();
	}

	public String getNomefantasia() {
		return this.cliente.getNomefantasia();
	}
	
	public void setButtonSoundEnable(boolean enable) {
		this.btnPlayPause.setEnabled(enable);
		this.btnGravar.setEnabled(enable);
		this.btnStop.setEnabled(enable);
	}

}