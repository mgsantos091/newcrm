package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.ParseException;

import org.openswing.swing.mdi.client.InternalFrame;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVProductSaleGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;

public class PIVProductSalesDetailPanel extends InternalFrame {

	private static final long serialVersionUID = 1L;
	private PIVProductSalesPanel painel;
	
	public PIVProductSalesPanel getPainel() {
		return painel;
	}

	public PIVProductSalesDetailPanel( PIVSalesMainModel vendasPai , PIVClientModel cliente , PIVProductSalesPanel painelExpandido ) {

		try {
			PIVProductSaleGFController controller = new PIVProductSaleGFController( vendasPai , cliente );
			controller.setPainelExpandido(painelExpandido);
			painel = new PIVProductSalesPanel(controller, false);
		} catch (ParseException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}

		setTitle("Vendas - Vis�o Detalhada");
		setLayout(new BorderLayout());
		add(painel,BorderLayout.CENTER);
		setSize(new Dimension(800,600));
		setUniqueInstance(true);
		
	}

}