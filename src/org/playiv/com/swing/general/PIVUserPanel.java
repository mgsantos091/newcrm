package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CheckBoxControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.PasswordControl;
import org.openswing.swing.client.TextControl;

public class PIVUserPanel extends JPanel {

	private static final long serialVersionUID = 1L;

/*	public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		PIVUsuarioPanel pusuario = new PIVUsuarioPanel();
		frame.add(pusuario,BorderLayout.CENTER);

		frame.pack();
		frame.setVisible(true);
	}
*/
	
	public PIVUserPanel( ) {

		super.setLayout(new MigLayout());
		
		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
	
		JLabel lblTituloPainel = new JLabel("USU�RIO");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);
		
		super.add(jpNomePainel,"h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][left,grow][right][left,grow][grow]", ""));
		
		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico.setIcon(new ImageIcon(PIVAddressPanel.class.getResource("/images/cadastros/usuario/usuario_64x64.png")));
		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , spany 3 , gapleft 5 , gaptop 5 , gapright 5 , top , east ");
		
		LabelControl lblIdUsuario = new LabelControl("C�digo:");
		jpCentroPainel.add(lblIdUsuario,"gaptop 5");
		
		NumericControl txtIdUsuario = new NumericControl();
		txtIdUsuario.setColumns(6);
		txtIdUsuario.setEnabledOnEdit(false);
		txtIdUsuario.setEnabledOnInsert(false);
		txtIdUsuario.setAttributeName("id");
		txtIdUsuario.setCanCopy(false);
		lblIdUsuario.setLabelFor(txtIdUsuario);
		jpCentroPainel.add(txtIdUsuario,"wrap , gaptop 5");
				
		LabelControl lblNomeUsuario = new LabelControl("Nome:");
		jpCentroPainel.add(lblNomeUsuario,"");
		super.setPreferredSize(new Dimension(400, 300));
		
		TextControl txtNomeUsuario = new TextControl();
		txtNomeUsuario.setMaxCharacters(255);
		txtNomeUsuario.setTrimText(true);
		txtNomeUsuario.setAttributeName("nome");
		lblNomeUsuario.setLabelFor(txtNomeUsuario);
		jpCentroPainel.add(txtNomeUsuario,"growx , spanx , wrap");
		
		LabelControl lblLogin = new LabelControl("Login:");
		jpCentroPainel.add(lblLogin,"");
		
		TextControl txtLogin = new TextControl();
		txtLogin.setMaxCharacters(25);
		txtLogin.setTrimText(true);
		txtLogin.setAttributeName("login");
		lblLogin.setLabelFor(txtLogin);
		jpCentroPainel.add(txtLogin,"growx , spanx , wrap");
		
		LabelControl lblSenha = new LabelControl("Senha");
		jpCentroPainel.add(lblSenha);
		
		PasswordControl txtSenha = new PasswordControl();
		txtSenha.setMaxCharacters(35);
		txtSenha.setTrimText(true);
		txtSenha.setAttributeName("senha");
		txtSenha.setCanCopy(false);
		lblSenha.setLabelFor(txtSenha);
		jpCentroPainel.add(txtSenha,"spanx 2 , growx , wrap");
		
		LabelControl lblVendedor = new LabelControl("Vendedor?");
		jpCentroPainel.add(lblVendedor);
		
		CheckBoxControl chkVendedor = new CheckBoxControl();
		chkVendedor.setAttributeName("usuariovendedor");
		lblVendedor.setLabelFor(chkVendedor);
		jpCentroPainel.add(chkVendedor,"wrap");
		
		LabelControl lblGerente = new LabelControl("Gerente?");
		jpCentroPainel.add(lblGerente);
		
		CheckBoxControl chkGerente = new CheckBoxControl();
		chkGerente.setAttributeName("usuariogerente");
		lblGerente.setLabelFor(chkGerente);
		jpCentroPainel.add(chkGerente,"wrap");
		
		LabelControl lblUsuarioEmail = new LabelControl("Usuario de e-mail:");
		jpCentroPainel.add(lblUsuarioEmail,"");
		
		TextControl txtUsuarioEmail = new TextControl();
		txtUsuarioEmail.setMaxCharacters(255);
		txtUsuarioEmail.setTrimText(true);
		txtUsuarioEmail.setAttributeName("usuario_email");
		lblUsuarioEmail.setLabelFor(txtUsuarioEmail);
		jpCentroPainel.add(txtUsuarioEmail,"growx , spanx , wrap");
		
		LabelControl lblSenhaDeEmail = new LabelControl("Senha de e-mail");
		jpCentroPainel.add(lblSenhaDeEmail,"");
		
		PasswordControl txtSenhaDeEmail = new PasswordControl();
		txtSenhaDeEmail.setMaxCharacters(35);
		txtSenhaDeEmail.setTrimText(true);
		txtSenhaDeEmail.setAttributeName("senha_email");
		txtSenhaDeEmail.setCanCopy(false);
		lblSenhaDeEmail.setLabelFor(txtSenhaDeEmail);
		jpCentroPainel.add(txtSenhaDeEmail,"spanx 2 , growx , wrap");
		
		LabelControl lblUsuarioVoip = new LabelControl("Usuario VOIP:");
		jpCentroPainel.add(lblUsuarioVoip,"");
		
		TextControl txtUsuarioVoip = new TextControl();
		txtUsuarioVoip.setMaxCharacters(255);
		txtUsuarioVoip.setTrimText(true);
		txtUsuarioVoip.setAttributeName("usuario_voip");
		lblUsuarioVoip.setLabelFor(txtUsuarioVoip);
		jpCentroPainel.add(txtUsuarioVoip,"growx , spanx , wrap");
		
		LabelControl lblSenhaVoip = new LabelControl("Senha VOIP:");
		jpCentroPainel.add(lblSenhaVoip,"");
		
		PasswordControl txtSenhaVoip = new PasswordControl();
		txtSenhaVoip.setMaxCharacters(255);
		txtSenhaVoip.setTrimText(true);
		txtSenhaVoip.setAttributeName("senha_voip");
		txtSenhaVoip.setCanCopy(false);
		lblSenhaVoip.setLabelFor(txtSenhaVoip);
		jpCentroPainel.add(txtSenhaVoip,"spanx 2 , growx , wrap");
		
		super.add(jpCentroPainel,"dock center , grow");
	}
	
}