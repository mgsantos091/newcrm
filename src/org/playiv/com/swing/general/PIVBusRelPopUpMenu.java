package org.playiv.com.swing.general;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.playiv.com.library.function.IPIVCommandFunc;
import org.playiv.com.library.function.PIVNoCommandFunc;

public class PIVBusRelPopUpMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;
	
	private IPIVCommandFunc upCom = new PIVNoCommandFunc();
	private IPIVCommandFunc addCom = new PIVNoCommandFunc();
	private IPIVCommandFunc lixoCom = new PIVNoCommandFunc();
	private IPIVCommandFunc cuboCom = new PIVNoCommandFunc();
	private IPIVCommandFunc calcCom = new PIVNoCommandFunc();
	private IPIVCommandFunc netCom = new PIVNoCommandFunc();
	private IPIVCommandFunc calCom = new PIVNoCommandFunc();
	private IPIVCommandFunc emailCom = new PIVNoCommandFunc();
	private IPIVCommandFunc mapaCom = new PIVNoCommandFunc();
	private IPIVCommandFunc anotacaoCom = new PIVNoCommandFunc(); 
	private IPIVCommandFunc orcamentoCom = new PIVNoCommandFunc();
	private IPIVCommandFunc telCom = new PIVNoCommandFunc();
	private IPIVCommandFunc refreshCom = new PIVNoCommandFunc();
	private IPIVCommandFunc editarCom = new PIVNoCommandFunc();
	private IPIVCommandFunc histCom = new PIVNoCommandFunc();
	/*private IPIVCommandFunc detalheCom = new PIVNoCommandFunc();*/

	private JMenuItem upItem;
	private JMenuItem addItem;
	private JMenuItem lixoItem;
	private JMenuItem cuboItem;
	private JMenuItem calcItem;
	private JMenuItem netItem;
	private JMenuItem calItem;
	private JMenuItem emailItem;
	private JMenuItem mapaItem;
	private JMenuItem anotacaoItem;
	private JMenuItem orcamentoItem;
	private JMenuItem telItem;
	private JMenuItem refreshItem;
	private JMenuItem editarItem;
	private JMenuItem histItem;
	
	/*private JMenuItem detalheItem;*/

	public PIVBusRelPopUpMenu(IPIVCommandFunc upCom, IPIVCommandFunc addCom,
			IPIVCommandFunc lixoCom, IPIVCommandFunc cuboCom, IPIVCommandFunc calcCom,
			IPIVCommandFunc netCom, IPIVCommandFunc calCom, IPIVCommandFunc emailCom ,
			IPIVCommandFunc mapaCom, IPIVCommandFunc anotacaoCom , IPIVCommandFunc orcamentoCom ,
			IPIVCommandFunc histCom , IPIVCommandFunc telCom , IPIVCommandFunc refreshCom , IPIVCommandFunc editarCom
			/*, IPIVCommandFunc detalheCom*/) {

		this.upCom = upCom;
		this.addCom = addCom;
		this.lixoCom = lixoCom;
		this.cuboCom = cuboCom;
		this.calcCom = calcCom;
		this.netCom = netCom;
		this.calCom = calCom;
		this.emailCom = emailCom;
		this.mapaCom = mapaCom;
		this.anotacaoCom = anotacaoCom;
		this.orcamentoCom = orcamentoCom;
		this.histCom = histCom;
		this.telCom = telCom;
		this.refreshCom = refreshCom;
		this.editarCom = editarCom;
		/*this.detalheCom = detalheCom;*/

		initialize();

	}

	private void initialize() {

		if (PIVBusRelPopUpMenu.this.upCom != null) {

			upItem = new JMenuItem("Subir de n�vel");
			upItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/up_16x16.png")));

			upItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.upCom.execute();
				}
			});

			add(upItem);

		}

		if (PIVBusRelPopUpMenu.this.addCom != null) {

			addItem = new JMenuItem("Adicionar suspect");
			addItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/addcliente_16x16.png")));

			addItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.addCom.execute();
				}
			});

			add(addItem);

		}

		if(PIVBusRelPopUpMenu.this.editarCom != null) {
			
			editarItem = new JMenuItem("Editar");
			editarItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/edit_16x16.png")));

			editarItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.editarCom.execute();
				}
			});

			add(editarItem);
			
		}
		
		if (PIVBusRelPopUpMenu.this.lixoCom != null) {

			lixoItem = new JMenuItem("Adicionar na lixeira");
			lixoItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/removercliente_16x16.png")));

			lixoItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.lixoCom.execute();
				}
			});

			add(lixoItem);

		}

		if (PIVBusRelPopUpMenu.this.cuboCom != null) {

			cuboItem = new JMenuItem("Checar cubo de conhecimento");
			cuboItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_16x16.png")));

			cuboItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.cuboCom.execute();
				}
			});

			add(cuboItem);

		}

		if (PIVBusRelPopUpMenu.this.calcCom != null) {

			calcItem = new JMenuItem("Chamar calculadora");
			calcItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/calculadora_16x16.png")));

			calcItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.calcCom.execute();
				}
			});

			add(calcItem);

		}

		if (PIVBusRelPopUpMenu.this.netCom != null) {

			netItem = new JMenuItem("Lan�ar browser");
			netItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/internet_16x16.png")));

			netItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.netCom.execute();
				}
			});

			add(netItem);

		}

		if (PIVBusRelPopUpMenu.this.calCom != null) {

			calItem = new JMenuItem("Visualizar calend�rio");
			calItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_16x16.png")));

			calItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.calCom.execute();
				}
			});

			add(calItem);

		}

		if (PIVBusRelPopUpMenu.this.emailCom != null) {

			emailItem = new JMenuItem("Enviar e-mail");
			emailItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/email_16x16.png")));

			emailItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.emailCom.execute();
				}
			});

			add(emailItem);

		}
		
		if (PIVBusRelPopUpMenu.this.mapaCom != null) {

			mapaItem = new JMenuItem("Observar mapa");
			mapaItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/mapa_16x16.png")));

			mapaItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.mapaCom.execute();
				}
			});

			add(mapaItem);

		}
		
		if (PIVBusRelPopUpMenu.this.anotacaoCom != null) {

			anotacaoItem = new JMenuItem("Gerenciar observa��es");
			anotacaoItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_16x16.png")));

			anotacaoItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.anotacaoCom.execute();
				}
			});

			add(anotacaoItem);

		}
		
		if (PIVBusRelPopUpMenu.this.orcamentoCom != null) {

			orcamentoItem = new JMenuItem("Gerenciar or�amentos");
			orcamentoItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_16x16.png")));

			orcamentoItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.orcamentoCom.execute();
				}
			});

			add(orcamentoItem);

		}
		
		if (PIVBusRelPopUpMenu.this.histCom != null) {

			histItem = new JMenuItem("Visualizar hist�rico de a��es");
			histItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/texto_16x16.png")));

			histItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.histCom.execute();
				}
			});

			add(histItem);

		}
		
		if (PIVBusRelPopUpMenu.this.telCom != null) {

			telItem = new JMenuItem("Liga��o VOIP");
			telItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));

			telItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.telCom.execute();
				}
			});

			add(telItem);

		}
		
		if (PIVBusRelPopUpMenu.this.refreshCom != null) {

			refreshItem = new JMenuItem("Recarregar");
			refreshItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/refresh_16x16.png")));

			refreshItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.refreshCom.execute();
				}
			});

			add(refreshItem);

		}
				
		/*if (PIVBusRelPopUpMenu.this.detalheCom != null) {
			
			detalheItem = new JMenuItem("Detalhes do cliente...");
			orcamentoItem.setIcon(new ImageIcon(
					PIVBusRelPopUpMenu.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_16x16.png")));

			detalheItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					PIVBusRelPopUpMenu.this.detalheCom.execute();
				}
			});

			add(detalheItem);
			
		}*/

	}

}