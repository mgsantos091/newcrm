package org.playiv.com.swing.general;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import org.hibernate.Session;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.DateControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.internationalization.java.Resources;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVPaymentMethodModel;

public class PIVPayCondPanel extends JPanel {

	private ComboBoxControl cboFormasPagamento;
	
	private PIVDao pdao = PIVDao.getInstance();

	private Integer qtdParcelas = 0;
	
	private ArrayList<DateControl> parcelasDateControl = new ArrayList<DateControl>();
	
	private JScrollPane scrollPainelParcelas = new JScrollPane();
	private JPanel painelParcelas = new JPanel();
	
	private Domain formasPagamentoDomain;
	
	private int numLimiteParcelas = 0;
	
	private static final long serialVersionUID = 1L;

	/*public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.setSize(new Dimension(400,400));
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new PIVPayCondPanel());
		frame.pack();
		frame.setVisible(true);
	}*/

	public PIVPayCondPanel( ) {
		
		// n�m. limite de parcelas
		
		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = PIVDao.getInstance().getSession();

		PIVLocalConfModel empresa = (PIVLocalConfModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();
		
		numLimiteParcelas = empresa.getLimite_parcelas();
		
		setLayout(new MigLayout("", "[][left][][left]", "[][]"));
		
		scrollPainelParcelas.setViewportView(painelParcelas);
		
		painelParcelas.setLayout(new MigLayout());
		
		JLabel lblFormaPagamento = new JLabel("Forma de pagamento:");
		add(lblFormaPagamento, "cell 0 0");
		
		setFormasPagamentoDominio( );
				
		cboFormasPagamento = new ComboBoxControl();
		cboFormasPagamento.addActionListener(new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    }
		});

		cboFormasPagamento.setDomain(this.formasPagamentoDomain);

		add(cboFormasPagamento, "cell 1 0");

		JLabel lblNumParcelas = new JLabel("Qtd. de parcelas:");
		add(lblNumParcelas, "cell 2 0");
		
		NumericControl txtNumParcelas = new NumericControl( );
		txtNumParcelas.addFocusListener(new FocusAdapter() {			
			@Override
			public void focusLost(FocusEvent e) {
				
				String numparcelas = ((JTextField) e.getSource()).getText();
				
				painelParcelas.removeAll();
				parcelasDateControl = new ArrayList<DateControl>();
				
				if(numparcelas!=null) {
					qtdParcelas = Integer.parseInt(numparcelas);
					if(numLimiteParcelas >= qtdParcelas) {
					/*if(Integer.parseInt(numparcelas) > qtdParcelas) {*/
						/*Integer addDateBoxs = Integer.parseInt(numparcelas) - qtdParcelas;*/
						for(int x=1;x<=qtdParcelas;x++) {
							JLabel lblParcela = new JLabel("Parcela " + x + ":");
							painelParcelas.add(lblParcela);
							DateControl dataParcela = new DateControl();
							dataParcela.setRequired(true);
							dataParcela.setDateType(Consts.TYPE_DATE);
							dataParcela.setFormat(Resources.DMY);
							dataParcela.setDate(new Date());
							/*dataParcela.setLocale( new Locale ( "por" , "bra"  ) );*/
							painelParcelas.add(dataParcela,x%2==0?"wrap":"");
							parcelasDateControl.add(dataParcela);
						}
						PIVPayCondPanel.this.painelParcelas.repaint();
						PIVPayCondPanel.this.repaint();
					} else
						JOptionPane.showMessageDialog(MDIFrame.getInstance(), "O n�mero de parcelas configurado (" + qtdParcelas + ") � maior do que o permitido (" + numLimiteParcelas + "), por favor, configure o valor corretamente" , "N�m. limite de parcelas infrigido" , JOptionPane.ERROR_MESSAGE);
					/*}*/
				}
			}
		});
		add(txtNumParcelas,"cell 3 0");
		
		add(scrollPainelParcelas,"cell 0 1 4 1,push ,grow");
		
	}

	private void setFormasPagamentoDominio() {
		if(formasPagamentoDomain==null) {
			formasPagamentoDomain = new Domain("FORMA_PAGAMENTO");
			Session session = pdao.getSession();
			ArrayList<PIVPaymentMethodModel> formas = (ArrayList<PIVPaymentMethodModel>) session.createQuery(
					"from org.playiv.com.mvc.model.PIVPaymentMethodModel as FormasPagamento " +
					"where FormasPagamento.ativo=true").list();
			session.close();
			for(PIVPaymentMethodModel forma : formas)
				formasPagamentoDomain.addDomainPair(forma.getId(), forma.getForma());
		}
	}
	
	public ArrayList<DateControl> getParcelasDateControl() {
		return parcelasDateControl;
	}

}