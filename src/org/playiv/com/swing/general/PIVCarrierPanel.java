package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVMaskResources;

public class PIVCarrierPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public PIVCarrierPanel( ) {

		super.setLayout(new MigLayout());

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("TRANSPORTADORA");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("",
				"[right][grow][right][grow][]", ""));

		LabelControl lblIdTransp = new LabelControl("Codigo:");
		jpCentroPainel.add(lblIdTransp);

		NumericControl txtIdEmp = new NumericControl();
		txtIdEmp.setColumns(5);
		txtIdEmp.setEnabledOnEdit(false);
		txtIdEmp.setEnabledOnInsert(false);
		txtIdEmp.setAttributeName("id");
		lblIdTransp.setLabelFor(txtIdEmp);
		jpCentroPainel.add(txtIdEmp,"wrap");

		LabelControl lblTxtCnpjCpf = new LabelControl("Cnpj:");
		jpCentroPainel.add(lblTxtCnpjCpf);
				
		FormattedTextControl frmtdTxtCnpjCpf = new FormattedTextControl( );
		try {
			PIVTextControlFactory.mudaMascara(frmtdTxtCnpjCpf,PIVMaskResources.mask.CNPJ);
		} catch (ParseException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
		frmtdTxtCnpjCpf.setAttributeName("cnpj");

		jpCentroPainel.add(frmtdTxtCnpjCpf, "grow , wrap");
		
		LabelControl lblRazaoSocial = new LabelControl("Raz�o Social:");
		jpCentroPainel.add(lblRazaoSocial);

		TextControl txtRazaoSocial = new TextControl();
		txtRazaoSocial.setMaxCharacters(120);
		txtRazaoSocial.setTrimText(true);
		txtRazaoSocial.setAttributeName("razaosocial");
		txtRazaoSocial.setRequired(true);
		lblRazaoSocial.setLabelFor(txtRazaoSocial);
		jpCentroPainel.add(txtRazaoSocial, "grow , span 2 , wrap");

		LabelControl lblNomeFantasia = new LabelControl("Nome Fantasia:");
		jpCentroPainel.add(lblNomeFantasia);

		TextControl txtNomeFantasia = new TextControl();
		txtNomeFantasia.setMaxCharacters(60);
		txtNomeFantasia.setTrimText(true);
		txtNomeFantasia.setAttributeName("nomefantasia");
		txtNomeFantasia.setRequired(false);
		lblNomeFantasia.setLabelFor(txtNomeFantasia);
		jpCentroPainel.add(txtNomeFantasia, "grow , span , wrap");

		LabelControl lblWebsite = new LabelControl("Website:");
		jpCentroPainel.add(lblWebsite);

		TextControl txtWebSite = new TextControl();
		txtWebSite.setMaxCharacters(120);
		txtWebSite.setTrimText(true);
		txtWebSite.setAttributeName("website");
		lblWebsite.setLabelFor(txtWebSite);
		jpCentroPainel.add(txtWebSite, "grow");

		LabelControl lblEmail = new LabelControl("Email:");
		jpCentroPainel.add(lblEmail);

		TextControl txtEmail = new TextControl();
		txtEmail.setMaxCharacters(120);
		txtEmail.setTrimText(true);
		txtEmail.setAttributeName("email");
		lblWebsite.setLabelFor(txtWebSite);
		jpCentroPainel.add(txtEmail, "grow , wrap");
		
		LabelControl lblTelefone = new LabelControl("Telefone:");
		jpCentroPainel.add(lblTelefone);
		
		FormattedTextControl frmtdColTel = null;
		try {
			frmtdColTel = PIVTextControlFactory.criaFormattedTextControl( PIVMaskResources.mask.TEL );
		} catch (ParseException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
		frmtdColTel.setAttributeName("telefone");
		lblTelefone.setLabelFor(frmtdColTel);
		
		jpCentroPainel.add(frmtdColTel);
		
		super.add(jpCentroPainel, "dock center , grow");

	}

}