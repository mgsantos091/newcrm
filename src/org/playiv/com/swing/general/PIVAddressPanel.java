package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.function.PIVAddressFunction;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.webservice.PIVWebServiceCep;

public class PIVAddressPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private FormattedTextControl frmtdTxtCep;
	private ComboBoxControl cboBoxPaises;
	private ComboBoxControl cboBoxEstados;
	private TextControl txtCidade;
	private TextControl txtBairro;
	private TextControl txtLogradouro;
	private NumericControl txtNumero;
	private TextControl txtComplemento;
	private TextControl txtReferencia;

	public PIVAddressPanel ( ) throws ParseException {

		setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
	
		JLabel lblTituloPainel = new JLabel("ENDERECO");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);
		
		super.add(jpNomePainel,"h 25! , dock north , growx");
		
		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][left][right][grow][]", ""));

		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico.setIcon(new ImageIcon(PIVAddressPanel.class.getResource("/images/cadastros/endereco/endereco_64x64.png")));
		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , spany 3 , right");

		LabelControl lblCep = new LabelControl("Cep");
		jpCentroPainel.add(lblCep,"");
		
		frmtdTxtCep = PIVTextControlFactory.criaFormattedTextControl(PIVMaskResources.mask.CEP);
		frmtdTxtCep.setAttributeName("endereco.cep");
//		lblCep.setLabelFor(frmtdTxtCep);
		frmtdTxtCep.setLinkLabel(lblCep);
		jpCentroPainel.add(frmtdTxtCep,"grow");
		
		JButton btnAtualizaCep = new JButton();
		btnAtualizaCep.setIcon(new ImageIcon(PIVAddressPanel.class.getResource("/images/cadastros/endereco/encontra-cep_32x32.png")));
		btnAtualizaCep.setBackground(new Color(245, 245, 245));
		btnAtualizaCep.setBorder(BorderFactory.createEmptyBorder());
		btnAtualizaCep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String cep = (String) frmtdTxtCep.getValue();
				if(cep.equals(""))
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Por Favor, preencha o cep antes.");
				else{
					PIVAddressModel endereco = PIVAddressFunction.getEndereco(cep);
					if(endereco==null) {
						JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o foi poss�vel encontrar este CEP na base de dados, uma nova tentativa de busca sera realizada via internet");
						PIVWebServiceCep wscep = PIVWebServiceCep.searchCep(cep);
						if (wscep.wasSuccessful()) {
							String estado = wscep.getUf();
							String cidade = wscep.getCidade();
							String bairro = wscep.getBairro();
							String logradouro = wscep.getLogradouroFull();
							atualizaEndereco(estado,cidade,bairro,logradouro);
							return;
						}
					}
					if(endereco!=null) {
						String estado = endereco.getEstado();
						String cidade = endereco.getCidade();
						String bairro = endereco.getBairro();
						String logradouro = endereco.getLogradouro();
						atualizaEndereco(estado,cidade,bairro,logradouro);
					} else
						JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o foi poss�vel encontrar este CEP, favor verificar o cep ou digitar os dados manualmente.");
				}
			}
		});
		jpCentroPainel.add(btnAtualizaCep,"wrap");
		
		LabelControl lblPais = new LabelControl("Pais");
		jpCentroPainel.add(lblPais,"");
		
		cboBoxPaises = new ComboBoxControl();
		cboBoxPaises.setAttributeName("endereco.pais");
		cboBoxPaises.setDomainId("PAIS");
		cboBoxPaises.setCanCopy(true);
		cboBoxPaises.setLinkLabel(lblPais);
		/*cboBoxPaises.setRequired(true);*/
		cboBoxPaises.setSelectedIndex(0);
		cboBoxPaises.setEnabled(false);
//		lblPais.setLabelFor(cboBoxPaises);
		cboBoxPaises.setLinkLabel(lblPais);
		jpCentroPainel.add(cboBoxPaises,"grow");
		
		LabelControl lblEstado = new LabelControl("Estado");
		jpCentroPainel.add(lblEstado,"");
		
		cboBoxEstados = new ComboBoxControl();
		cboBoxEstados.setAttributeName("endereco.estado");
		cboBoxEstados.setDomainId("ESTADO");
		cboBoxEstados.setCanCopy(true);
		cboBoxEstados.setLinkLabel(lblPais);
		/*cboBoxEstados.setRequired(true);*/
		cboBoxEstados.setSelectedIndex(0);
		cboBoxEstados.setEnabled(false);
//		lblEstado.setLabelFor(cboBoxEstados);
		cboBoxEstados.setLinkLabel(lblEstado);
		jpCentroPainel.add(cboBoxEstados,"grow");
		
		jpCentroPainel.add(new JPanel(),"wrap");
		
		LabelControl lblCidade = new LabelControl("Cidade");
		jpCentroPainel.add(lblCidade,"right , top");
		
		txtCidade = new TextControl();
		txtCidade.setMaxCharacters(120);
		txtCidade.setTrimText(true);
		txtCidade.setAttributeName("endereco.cidade");
		txtCidade.setEnabled(false);
//		lblCidade.setLabelFor(txtCidade);
		txtCidade.setLinkLabel(lblCidade);
		jpCentroPainel.add(txtCidade,"growx , top , span , wrap");
		
		LabelControl lblBairro = new LabelControl("Bairro");
		jpCentroPainel.add(lblBairro);
		
		txtBairro = new TextControl();
		txtBairro.setMaxCharacters(60);
		txtBairro.setTrimText(true);
		txtBairro.setAttributeName("endereco.bairro");
		txtBairro.setEnabled(false);
//		lblBairro.setLabelFor(txtBairro);
		txtBairro.setLinkLabel(lblBairro);
		jpCentroPainel.add(txtBairro,"grow , span 2 , wrap");
		
		LabelControl lblLogradouro = new LabelControl("Logradouro:");
		jpCentroPainel.add(lblLogradouro);
		
		txtLogradouro = new TextControl();
		txtLogradouro.setMaxCharacters(60);
		txtLogradouro.setTrimText(true);
		txtLogradouro.setAttributeName("endereco.logradouro");
		txtLogradouro.setEnabled(false);
//		lblLogradouro.setLabelFor(txtLogradouro);
		txtLogradouro.setLinkLabel(lblLogradouro);
		jpCentroPainel.add(txtLogradouro,"grow , span 3");
		
		LabelControl lblNumero = new LabelControl("Num");
		jpCentroPainel.add(lblNumero,"right");
		
		txtNumero = new NumericControl();
		txtNumero.setColumns(5);
		txtNumero.setMaxCharacters(6);
		txtNumero.setAttributeName("endnumero");
		txtNumero.setLinkLabel(lblNumero);
//		lblNumero.setLabelFor(txtNumero);
		jpCentroPainel.add(txtNumero,"align left , wrap");
		
		LabelControl lblComplemento = new LabelControl("Complemento");
		jpCentroPainel.add(lblComplemento);
		
		txtComplemento = new TextControl();
		txtComplemento.setMaxCharacters(60);
		txtComplemento.setTrimText(true);
		txtComplemento.setAttributeName("endcomplemento");
//		lblComplemento.setLabelFor(txtComplemento);
		txtComplemento.setLinkLabel(lblComplemento);
		jpCentroPainel.add(txtComplemento,"grow , span , wrap");
		
		LabelControl lblReferencia = new LabelControl("Refer�ncia");
		jpCentroPainel.add(lblReferencia);
		
		txtReferencia = new TextControl();
		txtReferencia.setMaxCharacters(60);
		txtReferencia.setTrimText(true);
		txtReferencia.setAttributeName("endreferencia");
//		lblReferencia.setLabelFor(txtReferencia);
		txtReferencia.setLinkLabel(lblReferencia);
		jpCentroPainel.add(txtReferencia,"grow , span ");

		super.add(jpCentroPainel,"dock center , grow");
	
	}

	private void atualizaEndereco( String estado , String cidade , String bairro , String logradouro ) {
		try {
			this.cboBoxEstados.setValue(estado);
		} catch (Exception ignore) { }
		this.txtCidade.setText(cidade);
		this.txtBairro.setText(bairro);
		this.txtLogradouro.setText(logradouro);
	}

	public String getEstado() {
		return (String) this.cboBoxEstados.getValue();
	}
	
	public String getCidade( ) {
		return this.txtCidade.getText();
	}
	
	public String getBairro( ) {
		return this.txtBairro.getText();
	}
	
	public String getRua( ) {
		return this.txtLogradouro.getText();
	}

	public String getReferencia() {
		return txtReferencia.getText();
	}

	public String getComplemento() {
		return txtComplemento.getText();
	}

	public Integer getNumero() {
		return txtNumero.getValue() == null ? 0 :((java.math.BigDecimal) txtNumero.getValue()).intValue(); 
	}

	public String getCep() {
		return (String) frmtdTxtCep.getValue();
	}
	
	public String getPais( ) {
		return (String) this.cboBoxPaises.getValue(); 
	}

}