package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.mdi.client.InternalFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVSalesMainModel;

public class PIVSalesNotePanel extends JPanel /*extends InternalFrame*/ {

	private static final long serialVersionUID = 1L;

	public PIVSalesNotePanel( final PIVSalesMainModel vendasPai ) {

		/*setTitle("Observa��o a ser colocada na proposta comercial");
		setFrameIcon(new ImageIcon(PIVSalesNotePanel.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_16x16.png")));*/
		
		/*getContentPane().*/setLayout(new MigLayout());

		setPreferredSize(new Dimension(561, 332));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("OBSERVA��O");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][grow,fill]", "[grow]"));
		/*
		 * jpCentroPainel.add(lblImgTopico,
		 * "w 120! , h 120! , gaptop 5 , gapright 5 , top , east ");
		 */

		/*LabelControl lblAnotacao = new LabelControl("Anota��o:");
		jpCentroPainel.add(lblAnotacao, "");*/

		/*final */TextAreaControl txtAnotacao = new TextAreaControl();
		txtAnotacao.setMaxCharacters(10000);
		txtAnotacao.setAttributeName("anotacao");
		/*txtAnotacao.setRequired(true);*/

		/*lblAnotacao.setLabelFor(txtAnotacao);*/
		jpCentroPainel.add(txtAnotacao, "grow,span");

		/*getContentPane().*/add(jpCentroPainel, "dock center , grow , span");
		
		/*addInternalFrameListener(new InternalFrameAdapter() {
			
			public void internalFrameClosed(InternalFrameEvent e) {
				vendasPai.setAnotacao(txtAnotacao.getTextArea().getText());
				PIVDao.getInstance().update(vendasPai);
			}
			
			public void internalFrameOpened(InternalFrameEvent e) {
				txtAnotacao.getTextArea().setText(vendasPai.getAnotacao());
		    }
			
		});*/
		
		/*pack();*/
		
		/*setUniqueInstance(true);*/
		
	}

}