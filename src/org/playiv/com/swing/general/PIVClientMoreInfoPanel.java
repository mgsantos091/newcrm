package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.FormattedTextControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.general.PIVMaskResources;

public class PIVClientMoreInfoPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	public PIVClientMoreInfoPanel ( ) throws ParseException {

		setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));
	
		JLabel lblTituloPainel = new JLabel("INFORMA��ES ADICIONAIS");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);
		
		super.add(jpNomePainel,"h 25! , dock north , growx");
		
		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][left][right][grow][]", ""));

		JLabel lblImgTopico = new JLabel();
		lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		/*lblImgTopico.setIcon(new ImageIcon(PIVClientMoreInfoPanel.class.getResource("/images/cadastros/endereco/endereco_64x64.png")));*/
		jpCentroPainel.add(lblImgTopico, "w 120! , h 120! , spany 3 , right");

		LabelControl lblLeadFonte = new LabelControl("Fonte do lead:");
		jpCentroPainel.add(lblLeadFonte,"left");
		
		ComboBoxControl cboLeadFonte = new ComboBoxControl();
		cboLeadFonte.setAttributeName("fonte");
		cboLeadFonte.setDomainId("FONTE_LEAD");
		lblLeadFonte.setLabelFor(cboLeadFonte);
		jpCentroPainel.add(cboLeadFonte,"growx 2 , wrap");
		
		LabelControl lblLeadAtividade = new LabelControl("Tipo de neg�cio:");
		jpCentroPainel.add(lblLeadAtividade,"left");
		
		ComboBoxControl cboLeadNegocio = new ComboBoxControl();
		cboLeadNegocio.setAttributeName("atividade");
		cboLeadNegocio.setDomainId("ATIVIDADE_LEAD");
		lblLeadAtividade.setLabelFor(cboLeadNegocio);
		jpCentroPainel.add(cboLeadNegocio,"growx 2 , wrap");
		
		LabelControl lblEmpregados = new LabelControl("N�m. Empregados:");
		jpCentroPainel.add(lblEmpregados,"left");
		
		NumericControl numQuantEmpregados = new NumericControl();
		numQuantEmpregados.setAttributeName("empregados");
		lblEmpregados.setLabelFor(numQuantEmpregados);
		jpCentroPainel.add(numQuantEmpregados,"wrap");
		
		LabelControl lblSegundoEmail = new LabelControl("2. e-mail:");
		jpCentroPainel.add(lblSegundoEmail,"left");
		
		TextControl txtSegundoEmail = new TextControl();
		txtSegundoEmail.setAttributeName("segundo_email");
		lblSegundoEmail.setLabelFor(txtSegundoEmail);
		jpCentroPainel.add(txtSegundoEmail,"wrap");
		
		LabelControl lblSegundoTel = new LabelControl("2. telefone:");
		jpCentroPainel.add(lblSegundoTel,"left");
		
		FormattedTextControl frmtdSegundoTel = PIVTextControlFactory.criaFormattedTextControl(PIVMaskResources.mask.TEL);
		frmtdSegundoTel.setAttributeName("segundo_tel");
		lblSegundoTel.setLabelFor(frmtdSegundoTel);
		jpCentroPainel.add(frmtdSegundoTel);
		
		LabelControl lblTerceiroTel = new LabelControl("3. telefone:");
		jpCentroPainel.add(lblTerceiroTel,"left");
		
		FormattedTextControl frmtdTerceiroTel = PIVTextControlFactory.criaFormattedTextControl(PIVMaskResources.mask.TEL);
		frmtdTerceiroTel.setAttributeName("terceiro_tel");
		lblTerceiroTel.setLabelFor(frmtdTerceiroTel);
		jpCentroPainel.add(frmtdTerceiroTel,"wrap");
		
		LabelControl lblSegundoCel = new LabelControl("2. celular:");
		jpCentroPainel.add(lblSegundoCel,"left");
		
		FormattedTextControl frmtdSegundoCel = PIVTextControlFactory.criaFormattedTextControl(PIVMaskResources.mask.CEL);
		frmtdSegundoCel.setAttributeName("segundo_cel");
		lblSegundoCel.setLabelFor(frmtdSegundoCel);
		jpCentroPainel.add(frmtdSegundoCel);
		
		LabelControl lblTerceiroCel = new LabelControl("3. celular:");
		jpCentroPainel.add(lblTerceiroCel,"left");
		
		FormattedTextControl frmtdTerceiroCel = PIVTextControlFactory.criaFormattedTextControl(PIVMaskResources.mask.CEL);
		frmtdTerceiroCel.setAttributeName("terceiro_cel");
		lblTerceiroCel.setLabelFor(frmtdTerceiroCel);
		jpCentroPainel.add(frmtdTerceiroCel,"wrap");
		
		super.add(jpCentroPainel,"dock center , grow");
	
	}

}