package org.playiv.com.swing.general;
import java.awt.Component;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.playiv.com.library.general.PIVHistoryOfActionsElement;

public class PIVHistoryOfActionsCellRenderer extends DefaultTableCellRenderer
{
    private static final long serialVersionUID = 1L;

	@Override
    public Component getTableCellRendererComponent(
        JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        /*super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);*/
    	setIcon(null);
    	setToolTipText("");
    	setHorizontalAlignment(SwingConstants.CENTER);
        if(value instanceof String) {
        	/*setVerticalAlignment(SwingConstants.CENTER);*/
        	setText((String)value);
        }
        else if(value instanceof PIVHistoryOfActionsElement) {
        	PIVHistoryOfActionsElement elemento = (PIVHistoryOfActionsElement) value;
        	setIcon(elemento.getLblImgElem().getIcon());
        	if(elemento.getEvento()!=null) {
        		setText("");
        		String horario = new SimpleDateFormat("HH:mm:ss",new Locale("pt","BRA")).format(new Date(elemento.getEvento().getDataregistro().getTime())); 
        		setToolTipText("| " + horario + " | " + elemento.getEvento().getDescricaoevento());
//        		setVerticalAlignment(SwingConstants.BOTTOM);
//        		setText(horario);
        	}
        }
        repaint();
        return this;
    }
}