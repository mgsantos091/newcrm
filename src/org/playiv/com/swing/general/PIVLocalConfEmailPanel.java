package org.playiv.com.swing.general;

import java.awt.Color;
import java.text.ParseException;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CheckBoxControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextControl;

public class PIVLocalConfEmailPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public PIVLocalConfEmailPanel( ) throws ParseException {

		setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][grow][right][grow][grow]", "[][][][][][][]"));

		LabelControl lblHost = new LabelControl("SMPT Host:");
		jpCentroPainel.add(lblHost, "");
	
		TextControl txtHostServer = new TextControl();
		txtHostServer.setMaxCharacters(255);
		txtHostServer.setTrimText(true);
		txtHostServer.setAttributeName("email_server_smpt_host");
		lblHost.setLabelFor(txtHostServer);
		jpCentroPainel.add(txtHostServer, "growx, wrap");

		LabelControl lblPorta = new LabelControl("Porta:");
		jpCentroPainel.add(lblPorta, "");

		NumericControl txtPorta = new NumericControl();
		txtPorta.setMaxCharacters(6);
		txtPorta.setDecimals(0);
		txtPorta.setAttributeName("email_server_smpt_port");
		lblPorta.setLabelFor(txtPorta);
		jpCentroPainel.add(txtPorta, "wrap");
		
		LabelControl lblSSL = new LabelControl("SSL?");
		jpCentroPainel.add(lblSSL, "");
		
		CheckBoxControl chkSSL = new CheckBoxControl();
		chkSSL.setAttributeName("email_server_ssl");
		lblSSL.setLabelFor(chkSSL);
		jpCentroPainel.add(chkSSL, "");

		super.add(jpCentroPainel, "center , grow");

	}
	
}