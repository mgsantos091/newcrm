package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;

public class PIVK5nCalInternalFrameAdapter extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private PIVK5nCalInternalFrameAdapter( ) {
		this.setFrameIcon(new ImageIcon(PIVK5nCalInternalFrameAdapter.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_16x16.png")));
		new Form();
	}

	public static void incluirCalendario( ) {
		String[] args = new String[0];
		org.playiv.com.swing.component.calendario.Main main = org.playiv.com.swing.component.calendario.Main.initialize(args);
		PIVK5nCalInternalFrameAdapter adapter = new PIVK5nCalInternalFrameAdapter( );
		adapter.add(main);
		adapter.setPreferredSize(new Dimension(800,600));
		adapter.setTitle("PlayIV - Calendário");
		adapter.pack();
//		JPanel panel = new JPanel(new BorderLayout());
//		panel.add(fileExplorer,BorderLayout.CENTER);
		
		
//		adapter.innerComponent.add(panel);
//		adapter.add(adapter.innerComponent);
		MDIFrame.add(adapter);
		
	}
	
}