package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class PIVBusRelDefaultTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	private Map cores = new HashMap();

	private Point tempCel = new Point(0, 0);

	private Color defaultBG;

	public PIVBusRelDefaultTableCellRenderer() {
		this.defaultBG = getBackground();
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		Component c = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);

		this.tempCel.x = row;
		this.tempCel.y = column;
		if (this.cores.containsKey(this.tempCel))
			c.setBackground((Color) this.cores.get(this.tempCel));
		else
			c.setBackground(this.defaultBG);
		return c;
	}

	@SuppressWarnings("unchecked")
	public void colorirCelula(int row, int col, Color cor) {
		this.cores.put(new Point(row, col), cor);
	}

}