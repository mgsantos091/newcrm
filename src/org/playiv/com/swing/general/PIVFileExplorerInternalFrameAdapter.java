package org.playiv.com.swing.general;

import java.awt.Dimension;

import javax.swing.ImageIcon;

import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;

public class PIVFileExplorerInternalFrameAdapter extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private PIVFileExplorerInternalFrameAdapter( ) {
		this.setFrameIcon(new ImageIcon(PIVFileExplorerInternalFrameAdapter.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_16x16.png")));
		new Form();
	}

	public static void incluirFileExplorer( org.playiv.com.library.fileexplorer.FileExplorer fileExplorer ) {
		PIVFileExplorerInternalFrameAdapter adapter = new PIVFileExplorerInternalFrameAdapter( );
		adapter.add(fileExplorer);
		adapter.setPreferredSize(new Dimension(400,600));
		adapter.setTitle("PlayIV - Cubo do Conhecimento");
		adapter.setUniqueInstance(true);
		adapter.pack();
		/*JPanel panel = new JPanel(new BorderLayout());
		panel.add(fileExplorer,BorderLayout.CENTER);
		
		
		adapter.innerComponent.add(panel);
		adapter.add(adapter.innerComponent);*/
		MDIFrame.add(adapter);
	}
	
}