package org.playiv.com.swing.general;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.playiv.com.mvc.controller.PIVBusRelMngtGFController;
import org.playiv.com.mvc.controller.PIVBusRelGarbageMngtGFController;


public class PIVBusRelGarbageClickPopUpMenuListener extends MouseAdapter {

	private JPopupMenu menu;
	private PIVBusRelMngtGFController relComPrincipalController;

/*	public PIVBusRelGarbageClickPopUpMenuListener() {
		menu = new JPopupMenu();

		JMenuItem menuItem = new JMenuItem("Exibir clientes na lixeira");
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				new PIVBusRelGarbageMngtGFController();
			}
		});

		menu.add(menuItem);
	}
*/
	public PIVBusRelGarbageClickPopUpMenuListener(PIVBusRelMngtGFController relComPrincipalController) {
		this.relComPrincipalController = relComPrincipalController;
		menu = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Exibir clientes na lixeira");
		menuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				new PIVBusRelGarbageMngtGFController(PIVBusRelGarbageClickPopUpMenuListener.this.relComPrincipalController);
			}
		});

		menu.add(menuItem);
	}
	
	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger())
			doPop(e);
	}

	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger())
			doPop(e);
	}

	private void doPop(MouseEvent e) {
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
	
	public void mouseClicked(MouseEvent e) {
		if(e.getClickCount()>=2)
			new PIVBusRelGarbageMngtGFController(relComPrincipalController);
	}

}