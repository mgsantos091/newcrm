package org.playiv.com.swing.general;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.mvc.model.PIVEventRegisterModel;
import java.awt.Font;

public class PIVHistoryOfActionsTableFunc extends JTable {

	private PIVHistoryOfActionsTableModel historyOfActionsTableModel;
	private PIVHistoryOfActionsCellRenderer historyOfActionsCellRenderer;

	private static final long serialVersionUID = 1L;

	public PIVHistoryOfActionsTableFunc( ) {

		super();
		
		setFont(new Font("Arial", Font.PLAIN, 14));

		/*String sql = "from Evento in class org.playiv.com.mvc.model.PIVEventRegisterModel ";
		Session session = PIVDao.getInstance().getSession();
		@SuppressWarnings("unchecked")
		ArrayList<PIVEventRegisterModel> eventos = (ArrayList<PIVEventRegisterModel>) session.createQuery(sql).list();
		session.close();*/

		historyOfActionsTableModel = new PIVHistoryOfActionsTableModel( /*eventos*/ );
		setModel( historyOfActionsTableModel );

		historyOfActionsCellRenderer = new PIVHistoryOfActionsCellRenderer();		
		setDefaultRenderer( Object.class , historyOfActionsCellRenderer );

		for(int x=0;x<getColumnCount();x++) {
			TableColumn coluna = getColumnModel().getColumn(x);
			coluna.setWidth(40);
		}

		setTableHeader(null); // remove o cabe�alho das colunas
		setAutoscrolls(true);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		/*setShowGrid(false);*/
		setRowHeight(32);
		setColumnSelectionAllowed(false);
		setRowSelectionAllowed(false);
		setBorder(BorderFactory.createLineBorder(Color.WHITE));
		/*setBackground(Color.WHITE);*/

	}
	
	public void carregar( ArrayList<PIVEventRegisterModel> eventos ) {
		historyOfActionsTableModel.carregar(eventos);
	}

}