package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.TextAreaControl;

public class PIVClientDescPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public PIVClientDescPanel( )
			throws ParseException {

		super.setLayout(new MigLayout());

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("DESCRI��O DO CLIENTE");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("",
				"[grow,fill]", "[grow,fill]"));

		TextAreaControl txtDescricao = new TextAreaControl();
		txtDescricao.setAttributeName("descricao");
		jpCentroPainel.add(txtDescricao);

		super.add(jpCentroPainel, "dock center , grow");
	}

}