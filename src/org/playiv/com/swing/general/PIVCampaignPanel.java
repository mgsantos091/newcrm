package org.playiv.com.swing.general;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CodLookupControl;
import org.openswing.swing.client.ComboBoxControl;
import org.openswing.swing.client.CurrencyControl;
import org.openswing.swing.client.DateControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.TextAreaControl;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.mvc.controller.PIVCampaignCatLUFController;
import org.playiv.com.mvc.controller.PIVCampaignMngtDFController;

public class PIVCampaignPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private NumericControl txIdCampanha = new NumericControl( );
	
	private TextControl txNomeCampanha = new TextControl();
	private TextAreaControl txDescricaoCampanha = new TextAreaControl();
	private CodLookupControl codTipoCampanha = new CodLookupControl();
	
	private TextControl txPublicoAlvo = new TextControl();
	private TextControl txPatrocinador = new TextControl();
	private ComboBoxControl cboStatus = new ComboBoxControl();
	
	private CurrencyControl curValorOrcado = new CurrencyControl();
	private CurrencyControl curValorAtual = new CurrencyControl();
	private CurrencyControl curPrevisaoReceita = new CurrencyControl();
	
	private DateControl dtRegistro = new DateControl();

	private TextControl txtNomeCategoria;
	
	public PIVCampaignPanel(PIVCampaignMngtDFController controller)
			throws ParseException {

		setLayout(new MigLayout());

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("CAMPANHA");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");

		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new MigLayout("", "[right][113.00][right][grow]", "[][][][grow][][][][][]"));
		
		LabelControl lblIdEmp = new LabelControl("Id.");
		jpCentroPainel.add(lblIdEmp, "flowx,cell 0 0");
		lblIdEmp.setLabelFor(txIdCampanha);
		
		txIdCampanha.setColumns(5);
		txIdCampanha.setEnabledOnEdit(false);
		txIdCampanha.setEnabledOnInsert(false);
		txIdCampanha.setAttributeName("id");
		jpCentroPainel.add(txIdCampanha,"cell 1 0");
		
		JLabel lblImgTopico = new JLabel();
		/*lblImgTopico.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		lblImgTopico.setHorizontalAlignment(SwingConstants.CENTER);
		lblImgTopico.setIcon(new ImageIcon(PIVCampaignPanel.class.getResource("/images/cadastros/campanha/campanha_64x64.png")));

		// lblImgTopico.setIcon(new ImageIcon(PIVClientePanel.class
		// .getResource("/resources/agency.png")));
		jpCentroPainel.add(lblImgTopico, "spanx ,width 120!,alignx right,height 120!");

/*		private NumericControl txIdCampanha = new NumericControl( );
		
		private TextControl txNomeCampanha = new TextControl();
		private TextAreaControl txDescricaoCampanha = new TextAreaControl();
		private CodLookupControl codTipoCampanha = new CodLookupControl();
		
		private TextControl txPublicoAlvo = new TextControl();
		private TextControl txPatrocinador = new TextControl();
		private ComboBoxControl cboStatus = new ComboBoxControl();
		
		private CurrencyControl curValorOrcado = new CurrencyControl();
		private CurrencyControl curValorAtual = new CurrencyControl();
		private CurrencyControl curPrevisaoReceita = new CurrencyControl();*/
		
		LabelControl lbNomeCampanha = new LabelControl("Nome da campanha");
		jpCentroPainel.add(lbNomeCampanha, "cell 0 1");
		
		txNomeCampanha.setEnabledOnEdit(true);
		txNomeCampanha.setEnabledOnInsert(true);
		txNomeCampanha.setRequired(true);
		txNomeCampanha.setAttributeName("nome_campanha");
		lbNomeCampanha.setLabelFor(txNomeCampanha);
		jpCentroPainel.add(txNomeCampanha,"cell 1 1 2 1,growx");
		
		LabelControl lbDescricaoCampanha = new LabelControl("Descri��o");
		jpCentroPainel.add(lbDescricaoCampanha, "cell 0 2");
		
		txDescricaoCampanha.setEnabledOnEdit(true);
		txDescricaoCampanha.setEnabledOnInsert(true);
		txDescricaoCampanha.setRequired(false);
		txDescricaoCampanha.setAttributeName("descricao_campanha");
		lbDescricaoCampanha.setLabelFor(txDescricaoCampanha);
		jpCentroPainel.add(txDescricaoCampanha,"cell 1 2 3 2,grow");
		
		LabelControl lbTipoCampanha = new LabelControl("Tipo");
		jpCentroPainel.add(lbTipoCampanha, "cell 0 4");
		
		PIVCampaignCatLUFController catController = new PIVCampaignCatLUFController();
		
		codTipoCampanha.setLookupController(catController);
		codTipoCampanha.setLookupButtonVisible(true);
		codTipoCampanha.setRequired(true);
		codTipoCampanha.setAttributeName("tipo.id");
		codTipoCampanha.setCanCopy(true);
		codTipoCampanha.setMaxCharacters(5);
		codTipoCampanha.setRequired(true);
		jpCentroPainel.add(codTipoCampanha,"cell 1 4");
		
		txtNomeCategoria = new TextControl();
		txtNomeCategoria.setEnabledOnEdit(false);
		txtNomeCategoria.setEnabledOnInsert(false);
		txtNomeCategoria.setAttributeName("tipo.nome");
		txtNomeCategoria.setMaxCharacters(255);
		txtNomeCategoria.setTrimText(true);
		jpCentroPainel.add(txtNomeCategoria,"cell 2 4 2 1,grow");
		
		LabelControl lbPublicoAlvo = new LabelControl("P�blico alvo");
		jpCentroPainel.add(lbPublicoAlvo, "cell 0 5");
		
		txPublicoAlvo.setEnabledOnEdit(true);
		txPublicoAlvo.setEnabledOnInsert(true);
		txPublicoAlvo.setRequired(false);
		txPublicoAlvo.setAttributeName("publico_alvo");
		lbPublicoAlvo.setLabelFor(txPublicoAlvo);
		jpCentroPainel.add(txPublicoAlvo,"cell 1 5 2 1,grow");
		
		LabelControl lbPatrocinador = new LabelControl("Patrocinador");
		jpCentroPainel.add(lbPatrocinador, "cell 0 6");
		
		txPatrocinador.setEnabledOnEdit(true);
		txPatrocinador.setEnabledOnInsert(true);
		txPatrocinador.setRequired(false);
		txPatrocinador.setAttributeName("patrocinador");
		lbPatrocinador.setLabelFor(txPatrocinador);
		jpCentroPainel.add(txPatrocinador,"cell 1 6 2 1,grow");
		
		LabelControl lbStatus = new LabelControl("Status");
		jpCentroPainel.add(lbStatus, "cell 0 7");
		GridBagLayout gridBagLayout = (GridBagLayout) cboStatus.getLayout();
		gridBagLayout.columnWidths = new int[]{165};
		
		cboStatus.setDomainId("STATUS_CAMPANHA");
		cboStatus.setEnabledOnEdit(true);
		cboStatus.setEnabledOnInsert(true);
		cboStatus.setRequired(true);
		cboStatus.setAttributeName("status");
		lbStatus.setLabelFor(cboStatus);
		jpCentroPainel.add(cboStatus,"cell 1 7,growx");

		LabelControl lbDataRegistro = new LabelControl("Data de registro");
		jpCentroPainel.add(lbDataRegistro, "cell 0 8");
		
		dtRegistro.setEnabledOnEdit(false);
		dtRegistro.setEnabledOnInsert(false);
		dtRegistro.setAttributeName("data_registro");
		dtRegistro.setDateType(Consts.TYPE_DATE_TIME);
		lbDataRegistro.setLabelFor(dtRegistro);
		jpCentroPainel.add(dtRegistro, "cell 1 8");
	
		super.add(jpCentroPainel, "dock center , grow");
	}

}