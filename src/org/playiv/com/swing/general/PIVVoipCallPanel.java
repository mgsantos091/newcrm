package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.playiv.com.mvc.controller.PIVVoipRecordDFController;
import org.playiv.com.peers.gui.PIVSipVoipFrame;

public class PIVVoipCallPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public PIVVoipCallPanel(PIVVoipRecordDFController controller)
			throws ParseException {

		super.setLayout( new MigLayout( ) );

		JPanel jpNomePainel = new JPanel();
		jpNomePainel.setBackground(new Color(112, 128, 144));
		jpNomePainel.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblTituloPainel = new JLabel("INTEGRAÇÃO VOIP");
		lblTituloPainel.setForeground(Color.WHITE);
		lblTituloPainel.setFont(new Font("Arial Bold", Font.PLAIN, 16));
		jpNomePainel.add(lblTituloPainel);

		super.add(jpNomePainel, "h 25! , dock north , growx");
		
		PIVSipVoipFrame sipVoipFrame = PIVSipVoipFrame.getInstance();
//		sipVoipFrame.setBorder(new LineBorder(new Color(0, 0, 0)));
//		sipVoipFrame.setBackground(new Color(245, 245, 245));
		
		String numero = controller.getNumero();
		String domain = controller.getDomain();
		
		sipVoipFrame.setUriText("sip:" + numero + "@" + domain);
		JPanel pnlCall = sipVoipFrame.ligar();
		
		JPanel jpCentroPainel = new JPanel();
		jpCentroPainel.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentroPainel.setBackground(new Color(245, 245, 245));
		jpCentroPainel.setLayout(new BorderLayout());
		jpCentroPainel.add(pnlCall,BorderLayout.CENTER);

		super.add(jpCentroPainel, "dock center , grow");
		
	}

}