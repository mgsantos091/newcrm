package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.hibernate.Session;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LayeredBarRenderer;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.data.time.Month;
import org.jfree.ui.TextAnchor;
import org.jfree.util.SortOrder;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVSalesTargetModel;

public class PIVSalesTargetIndividualSellerGadget extends PIVMainGadget {

	private static final long serialVersionUID = 1L;
	
	private static final Locale LOCAL = new Locale("pt","BR");  

/*	public static void main( String args[ ] ) {
		Conteudo c;
		c = new Conteudo( );
	}*/
	
	public PIVSalesTargetIndividualSellerGadget() {
		setGadgetName("Metas de Vendas (por m�s)");
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
		setSize(new Dimension(400,200));
	}

	/*static */class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;

		private JFreeChart createChart( CategoryDataset categorydataset ) {

			JFreeChart jfreechart = ChartFactory.createBarChart("", "M�s", "Valor", categorydataset, PlotOrientation.VERTICAL, true, true, false);
			
			CategoryPlot categoryplot = (CategoryPlot)jfreechart.getPlot();
			categoryplot.setDomainGridlinesVisible(true);
			categoryplot.setRangePannable(true);
			categoryplot.setRangeZeroBaselineVisible(true);
			
			NumberAxis eixoDeDinheiro = (NumberAxis)categoryplot.getRangeAxis();
			eixoDeDinheiro.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			eixoDeDinheiro.setUpperMargin(0.10000000000000001D);
						
			LayeredBarRenderer layeredbarrenderer = new LayeredBarRenderer();
			layeredbarrenderer.setDrawBarOutline(false);
			
			categoryplot.setRenderer(layeredbarrenderer);
			categoryplot.setRowRenderingOrder(SortOrder.DESCENDING);
			
			GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
			GradientPaint gradientpaint1 = new GradientPaint(0.0F, 0.0F, Color.green, 0.0F, 0.0F, new Color(0, 64, 0));
			GradientPaint gradientpaint2 = new GradientPaint(0.0F, 0.0F, Color.red, 0.0F, 0.0F, new Color(64, 0, 0));
			
			layeredbarrenderer.setSeriesPaint(0, gradientpaint);
			layeredbarrenderer.setSeriesPaint(1, gradientpaint1);
			layeredbarrenderer.setSeriesPaint(2, gradientpaint2);
			
			return jfreechart;
			
			/*JFreeChart jfreechart = ChartFactory.createStackedBarChart3D("",
					"M�s", "Valor", categorydataset, PlotOrientation.VERTICAL,
					true, true, false);
			
			CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
			
			NumberAxis eixoDeDinheiro = (NumberAxis)categoryplot.getRangeAxis();
			eixoDeDinheiro.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
			eixoDeDinheiro.setUpperMargin(0.10000000000000001D);
			
			BarRenderer barrenderer = (BarRenderer) categoryplot.getRenderer();
			barrenderer.setDrawBarOutline(false);
			barrenderer
					.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
			barrenderer.setBaseItemLabelsVisible(true);
			barrenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
					ItemLabelAnchor.CENTER, TextAnchor.CENTER));
			barrenderer.setBaseNegativeItemLabelPosition(new ItemLabelPosition(
					ItemLabelAnchor.CENTER, TextAnchor.CENTER));
			barrenderer.setIncludeBaseInRange(false);
			barrenderer.setBaseItemLabelsVisible(true);
			barrenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
					ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));
			barrenderer
					.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
							"{0} : {1} = {2}", new SimpleDateFormat("MM")));
      
		    DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(LOCAL));   
		    df.setNegativePrefix("(");
			df.setNegativeSuffix(")");
			barrenderer.setBaseItemLabelsVisible(true);
			
			GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.green, 0.0F, 0.0F, new Color(0, 0, 64));
			GradientPaint gradientpaint1 = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 64, 0));
			GradientPaint gradientpaint2 = new GradientPaint(0.0F, 0.0F, Color.yellow, 0.0F, 0.0F, new Color(64, 0, 0));
			
			barrenderer.setSeriesPaint(0, gradientpaint);
			barrenderer.setSeriesPaint(1, gradientpaint1);
			barrenderer.setSeriesPaint(2, gradientpaint2);
			
			return jfreechart;*/
			
		}
		
		public Conteudo( )
		{
			/*JFrame frame = new JFrame();
			frame.setPreferredSize(new Dimension(500,300));
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setLayout(new BorderLayout());*/
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			JFreeChart jfreechart = createChart(createDataset());
			
			ChartPanel chartpanel = new ChartPanel(jfreechart);
			add(chartpanel,BorderLayout.CENTER);
			
			/*frame.add(chartpanel,BorderLayout.CENTER);
			frame.pack();
			frame.setVisible(true);*/
			
		}
		
		private CategoryDataset createDataset() {

			int idvendedor = PIVUserSession.getInstance().getVendedorSessao().getId();

			Calendar cal = Calendar.getInstance();
			int anoAtual = cal.get(Calendar.YEAR);

			double valormeta = 0d;
			
			/*DefaultStatisticalCategoryDataset categoryDataSet = new DefaultStatisticalCategoryDataset();*/
			DefaultCategoryDataset categoryDataSet = new DefaultCategoryDataset();

			String baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaVenda where MetaVenda.vendedor.id = " + idvendedor + " and idmeta = 6";
			Session s = PIVDao.getInstance().getSession();
			PIVSalesTargetModel metaDeVendas = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
			s.close();
			
			if(metaDeVendas!=null)
				valormeta = metaDeVendas.getValormeta();
			
			baseSQL = "" +
					"SELECT " +
					"	mes " +
					"	, ano " +
					"	, SUM(valortotal) valortotal " +
					"	, " +
					"( " +
					"	SELECT COALESCE(SUM(valortotal),0) valorforecast " +
					"	FROM \"VendasPai\" X INNER JOIN ( " +
					"		SELECT idvendedor , idcliente FROM \"NivelComercialRelacionamento\" WHERE completado = false AND idnivelcomercial = 3 AND ativo = true AND idvendedor = " + idvendedor +
					"	) Y ON X.idvendedor = Y.idvendedor AND X.idcliente = Y.idcliente " +
					"	WHERE EXTRACT(MONTH FROM X.dataregistro) = A.mes AND EXTRACT(YEAR FROM dataregistro) = a.ano and vendaefetivada = false " +
					") valorforecast " +
					"FROM " +
					"( " +
					"		SELECT " +
					"		valortotal " +
					"		, EXTRACT(MONTH FROM dataregistro) mes " +
					"		, EXTRACT(YEAR FROM dataregistro) ano " +
					"		FROM \"VendasPai\" " +
					"		WHERE idvendedor = " + idvendedor + " AND EXTRACT(YEAR FROM dataregistro) = " + anoAtual + " and vendaefetivada = true " +
					") A " +
					"GROUP BY mes , ano ";
			
			s = PIVDao.getInstance().getSession();
			Iterator iter = s.createSQLQuery(baseSQL).list().iterator();
			
			while ( iter.hasNext() ) {
				Object[] obj = (Object[]) iter.next();
				int mes = ((Double) obj[0]).intValue();
				int ano = ( (Double) obj[1] ).intValue();
				double valortotal = ((BigDecimal) obj[2]).doubleValue();
				double valorforecast = ((BigDecimal) obj[3]).doubleValue();
				categoryDataSet.addValue(valortotal, "Realizado", new Month(mes, ano));
				categoryDataSet.addValue(valorforecast, "Forecast", new Month(mes, ano));
				categoryDataSet.addValue(valormeta, "Meta", new Month(mes, ano));
				/*if(valormeta>0) {
					Double totalMaisForecast = valortotal + valorforecast;
					if(totalMaisForecast<valormeta) {
						categoryDataSet.addValue(valormeta - totalMaisForecast, "Meta", new Month(mes, ano));
					}
				}*/
			}
			
			s.close();
					
			/*if(metaDeVendas!=null)
				for(int x=1;x<=mesAtual;x++) {
					categoryDataSet.add(metaDeVendas.getValormeta().doubleValue(),0d,"Meta",new Month(x, anoAtual));
				}*/
					
			return categoryDataSet;
			
		}
		
		/*private CategoryDataset createDataset()
		{

			int idvendedor = PIVUserSession.getInstance().getVendedorSessao().getId();

			Calendar cal = Calendar.getInstance();
			int mesAtual = cal.get(Calendar.MONTH) + 1;
			int anoAtual = cal.get(Calendar.YEAR);

			DefaultStatisticalCategoryDataset categoryDataSet = new DefaultStatisticalCategoryDataset();

			TimeTableXYDataset timetablexydataset = new TimeTableXYDataset();

			String baseSQL = "" +
					"SELECT " +
					"	mes " +
					"	, ano " +
					"	, SUM(valortotal) valortotal " +
					"	, " +
					"( " +
					"	SELECT COALESCE(SUM(valortotal),0) valorforecast " +
					"	FROM \"VendasPai\" X INNER JOIN ( " +
					"		SELECT idvendedor , idcliente FROM \"NivelComercialRelacionamento\" WHERE completado = false AND idnivelcomercial = 3 AND ativo = true AND idvendedor = " + idvendedor +
					"	) Y ON X.idvendedor = Y.idvendedor AND X.idcliente = Y.idcliente " +
					"	WHERE EXTRACT(MONTH FROM X.dataregistro) = A.mes AND EXTRACT(YEAR FROM dataregistro) = a.ano and vendaefetivada = false " +
					") valorforecast " +
					"FROM " +
					"( " +
					"		SELECT " +
					"		valortotal " +
					"		, EXTRACT(MONTH FROM dataregistro) mes " +
					"		, EXTRACT(YEAR FROM dataregistro) ano " +
					"		FROM \"VendasPai\" " +
					"		WHERE idvendedor = " + idvendedor + " AND EXTRACT(YEAR FROM dataregistro) = " + anoAtual + " and vendaefetivada = true " +
					") A " +
					"GROUP BY mes , ano ";
			
			Session s = PIVDao.getInstance().getSession();
			Iterator iter = s.createSQLQuery(baseSQL).list().iterator();
			
			while ( iter.hasNext() ) {
				Object[] obj = (Object[]) iter.next();
				int mes = ((Double) obj[0]).intValue();
				int ano = ( (Double) obj[1] ).intValue();
				double valortotal = ((BigDecimal) obj[2]).doubleValue();
				double valorforecast = ((BigDecimal) obj[3]).doubleValue();
				timetablexydataset.add(new Month(mes, ano),valortotal,"Realizado");
				categoryDataSet.add(valortotal,valorforecast,"Realizado",new Month(mes, ano));
			}
			
			s.close();
			
			baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaVenda where MetaVenda.vendedor.id = " + idvendedor + " and idmeta = 6";
			s = PIVDao.getInstance().getSession();
			PIVSalesTargetModel metaDeVendas = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
			s.close();
			
			if(metaDeVendas!=null)
				for(int x=1;x<=mesAtual;x++) {
					categoryDataSet.add(metaDeVendas.getValormeta().doubleValue(),0d,"Meta",new Month(x, anoAtual));
				}
					
			return categoryDataSet;
			
		}*/
		
	}

}