package org.playiv.com.swing.general;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import net.miginfocom.swing.MigLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;

public abstract class PIVMainGadget extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private Dimension size = new Dimension( 350 , 200 );
	
	private JPanel jpConteudo;

	private JButton btnEdita;
	private JButton btnAtualiza;
	private JButton btnOculta;

	private JLabel lbGadgetName;

	public PIVMainGadget() {
		
		setBackground(Color.WHITE);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		/*setPreferredSize();*/
		setLayout(new MigLayout("", "[grow][]", "[][grow]"));
		
		lbGadgetName = new JLabel("");
		add(lbGadgetName, "cell 0 0");
		
		JPanel jpBotoes = new JPanel();
		jpBotoes.setBackground(Color.WHITE);
		add(jpBotoes, "cell 1 0,grow");
		
		btnEdita = new JButton(/*"1"*/);
		btnEdita.setBorder(BorderFactory.createEmptyBorder());
		btnEdita.setBackground(Color.WHITE);
		btnEdita.setIcon(new ImageIcon(PIVMainGadget.class.getResource("/images/gerenciamento_relcomercial/outros/edit_16x16.png")));
		btnEdita.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				edita( );
			}
		});
		jpBotoes.add(btnEdita);
		
		btnAtualiza = new JButton(/*"2"*/);
		btnAtualiza.setBorder(BorderFactory.createEmptyBorder());
		btnAtualiza.setBackground(Color.WHITE);
		btnAtualiza.setIcon(new ImageIcon(PIVMainGadget.class.getResource("/images/gerenciamento_relcomercial/outros/refresh_16x16.png")));
		btnAtualiza.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refresh( );
			}
		});
		jpBotoes.add(btnAtualiza);
		
		btnOculta = new JButton(/*"3"*/);
		btnOculta.setBorder(BorderFactory.createEmptyBorder());
		btnOculta.setBackground(Color.WHITE);
		btnOculta.setIcon(new ImageIcon(PIVMainGadget.class.getResource("/images/gerenciamento_relcomercial/outros/configure_16x15.png")));
		btnOculta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				oculta( );
			}
		});
		jpBotoes.add(btnOculta);
		
		jpConteudo = new JPanel();
		jpConteudo.setLayout(new BorderLayout());
		jpConteudo.setBackground(Color.WHITE);
		add(jpConteudo, "cell 0 1 2 1,grow");

	}
	
	public Component addConteudo( Component comp ) {
		if(jpConteudo.getComponents().length==0)
			jpConteudo.add(comp,BorderLayout.CENTER);
		return null;
	}

	private void refresh( ) {
		
	}
	
	private void oculta( ) {
		
	}
	
	private void edita( ) {
		
	}
	
	public void setGadgetName( String name ) {
		this.lbGadgetName.setText(name);
	}
	
	@Override
	public Dimension getSize( ) {
		return size;
	}
	
	@Override
	public Dimension getMinimumSize( ) {
		return size;
	}
	
	@Override
	public Dimension getMaximumSize( ) {
		return size;
	}
	
	public void setSize(Dimension size) {
		this.size = size;
		super.setSize(size);
		super.setMinimumSize(size);
		super.setMaximumSize(size);
	}

}