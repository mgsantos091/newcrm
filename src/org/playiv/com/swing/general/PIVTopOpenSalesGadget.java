package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.openswing.swing.client.GridControl;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.mvc.controller.PIVTopSalesGadgetGFController;

public class PIVTopOpenSalesGadget extends PIVMainGadget {

	private static final long serialVersionUID = 1L;

	public PIVTopOpenSalesGadget() {
		setGadgetName("PRINCIPAIS PROPOSTAS (abertas)");
		Conteudo conteudo = new Conteudo( );
		addConteudo(conteudo);
		/*setSize(new Dimension(350,300));*/
	}
	
	class Conteudo extends JPanel {

		private static final long serialVersionUID = 1L;

		public Conteudo( ) {
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			setBackground(Color.WHITE);
			
			IntegerColumn colId = new IntegerColumn();
			TextColumn colNomeCliente = new TextColumn( );
			CurrencyColumn colValorTotal = new CurrencyColumn();
			DateColumn colDataRegistro = new DateColumn();
			
			GridControl grid = new GridControl();

			grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSalesMainModel");

			colId.setColumnName("id");
			colId.setHeaderColumnName("Cod");
			colId.setMaxCharacters(5);
			colId.setPreferredWidth(25);
			grid.getColumnContainer().add(colId);
			
			colNomeCliente.setColumnName("cliente.nomefantasia");
			colNomeCliente.setPreferredWidth(150);
			grid.getColumnContainer().add(colNomeCliente);
			
			colValorTotal.setColumnName("valortotal");
			colValorTotal.setPreferredWidth(100);
			colValorTotal.setDecimals(4);
			grid.getColumnContainer().add(colValorTotal);

			colDataRegistro.setColumnName("dataregistro");
			colDataRegistro.setPreferredWidth(100);
			grid.getColumnContainer().add(colDataRegistro);
			
			PIVTopSalesGadgetGFController controller = new PIVTopSalesGadgetGFController( );
			
			grid.setGridDataLocator(controller);
			grid.setController(controller);
			
			JScrollPane scrollpane = new JScrollPane();
			scrollpane.getViewport().setBackground(Color.WHITE);
			scrollpane.setViewportView(grid);
			
			add(scrollpane,BorderLayout.CENTER);
			
			grid.reloadData();
			
		}
		
	}

}