package org.playiv.com.swing.general;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.swing.component.PIVCustomFlowLayout;

public class PIVBusRelElementPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private PIVBusRelMainCFController organizaClienteRelacionado;

	public PIVBusRelElementPanel( ) {
		inicializaListeners( );
		setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
	}

	public PIVBusRelMainCFController getOrganizaClienteRelacionado() {
		return this.organizaClienteRelacionado;
	}

	public void setOrganizaClienteRelacionado( PIVBusRelMainCFController organizaCliente ) {
		this.organizaClienteRelacionado = organizaCliente;
	}

	private void inicializaListeners() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(organizaClienteRelacionado!=null)
					organizaClienteRelacionado.setElementoSelecionado(null);
			}
		});
	}

}