package org.playiv.com.swing.general;

import java.awt.BorderLayout;
import java.awt.Dimension;

import org.jcalc.calculadora.JCalcStandardFrame;
import org.openswing.swing.mdi.client.InternalFrame;

public class PIVJCalcInternalFrameAdapter extends InternalFrame {

	private static final long serialVersionUID = 1L;

	public PIVJCalcInternalFrameAdapter( ) {
		
		JCalcStandardFrame calculadora = new JCalcStandardFrame();
		
		setTitle("PlayIV - Calculadora");

		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(200,230));
		setResizable(false);
		setMaximizable(false);
		
		add(calculadora.getPane(),BorderLayout.CENTER);

		pack();

	}
	
}