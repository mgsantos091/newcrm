package org.playiv.com.start;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.function.DateUtils;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.view.PIVMainMenuCFView;

public class Launcher {

	public static void main(String[] args) {

//		if(isExpirado()) {
//			System.exit(-1);
//		}
		
		PIVSplashScreen frameSplash = new PIVSplashScreen();
		frameSplash.setBarraDeProgressoVal(0);

		/*PIVDefaultConfiguration.initiate();*/
		
		frameSplash
				.addText("Verificando a exist�ncia de outras inst�ncias do aplicativo em execu��o...");
		
		try {
			PIVFileLockMngt.tryAcquireLock();
			frameSplash.addText("Verifica��o conclu�da...");
		} catch (Exception e) {
			frameSplash.addText( e.getMessage( ) );
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		frameSplash.setBarraDeProgressoVal(10);

		if (PIVFileLockMngt.getLock() == null) {
			frameSplash
					.addText("J� existe uma inst�ncia do aplicativo em execu��o...");
			try {
				Thread.sleep(5000);
				frameSplash.dispose();
				System.exit(0);
			} catch (InterruptedException e) {
				PIVLogSettings.getInstance().error(e.getMessage(), e);
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		} else {
			frameSplash
					.addText("Lock adquirido com sucesso, o aplicativo esta sendo iniciado...");
			/*EventQueue.invokeLater(new Runnable() {
				public void run() {*/
					try {
						
//						NativeInterface.open();
						
						UIManager.setLookAndFeel(UIManager
								.getSystemLookAndFeelClassName());
						
						frameSplash.setBarraDeProgressoVal(20);
						
						// UIManager.setLookAndFeel(UIManager.getLookAndFeel());
						new PIVMainMenuCFView( frameSplash );
						
						frameSplash.dispose();
						
					} catch (Exception e) {
						frameSplash.addText("Foi encontrado um erro: " + e.getMessage() + ", favor entrar em contato com o administrador do sistema");
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, e1.getMessage());
						}
						frameSplash.dispose();
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
						JOptionPane.showMessageDialog(null, e.getMessage());
					} finally {
//						NativeInterface.runEventPump();
					}
				/*}
			});*/
		}
	}
	
//	private static Boolean isExpirado( ) {
//		GregorianCalendar dataExpiracao = new GregorianCalendar();
//		dataExpiracao.set(Calendar.YEAR, 2017);
//		dataExpiracao.set(Calendar.MONTH, 4);
//		dataExpiracao.set(Calendar.DATE, 15);
//		try {
//			Calendar dataAtual = DateUtils.getAtomicTime();
//			if(dataAtual.before(dataExpiracao)) return false;
//			else {
//				JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Esta vers�o de demonstra��o do PlayIV expirou.\n"
//						+ "Favor entrar em contato para obter uma vers�o atualizada para continuar trabalhando.", "Software Expirado", JOptionPane.ERROR_MESSAGE);
//				return true;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Houve um problema ao recuperar dados via internet. Por favor, verifique sua conex�o com a internet.\n"
//					+ "Para utilizar esta vers�o de demonstra��o, � necess�rio uma conex�o ativa com a internet.");
//			return true;
//		}
//	}

}