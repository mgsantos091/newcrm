package org.playiv.com.start;

import javax.swing.UIManager;

import org.playiv.com.library.general.PIVMenuPrincipalFacade;
import org.playiv.com.mvc.view.PIVMainMenuCFView;

//import chrriis.dj.nativeswing.swtimpl.NativeInterface;

public class Debug {
	public static void main(String args[]) {
		try {
			PIVSplashScreen frameSplash = new PIVSplashScreen();
//			NativeInterface.open();
			UIManager.setLookAndFeel(UIManager
					.getSystemLookAndFeelClassName());
			PIVMainMenuCFView view = new PIVMainMenuCFView( frameSplash );
			PIVMenuPrincipalFacade facade = (PIVMenuPrincipalFacade) view.getClientFacade();
			facade.gerRelCom();
//			facade.cadCliente();
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
