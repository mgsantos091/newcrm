package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name="\"RegistroLigacao\"")
public class PIVVoipRecordModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;
	private Integer status;
	private String numero;
	private Timestamp inicio;
	private Timestamp fim;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private boolean ativo;
	private Timestamp duracao;
	private String anotacao;
	
	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Id
	@SequenceGenerator(name = "\"RegistroLigacao_id_seq\"", sequenceName = "\"RegistroLigacao_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"RegistroLigacao_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne
	@JoinColumn(name = "idvendedor", referencedColumnName = "id")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}

	public Integer getStatus() {
		return status;
	}

	public Timestamp getInicio() {
		return inicio;
	}

	public Timestamp getFim() {
		return fim;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}

	public void setFim(Timestamp fim) {
		this.fim = fim;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public Timestamp getDuracao() {
		if(this.inicio != null && this.fim != null) {
			this.duracao = new Timestamp( this.fim.getTime() - this.inicio.getTime() );
			return this.duracao;
		} else return null;
	}

	public void setDuracao(Timestamp duracao) {
		this.duracao = duracao;
	}
	
	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}
	
}