package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name = "\"CalendarioItem\"")
public class PIVCalendarItemModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;
	private String assunto;
	private boolean dia_inteiro;
	private Timestamp datar_inicio = PIVUtil.getCurrentTime();
	private int hora;
	private int minuto;
	private String categorias;
	private String url;
	private String local;
	private int status;
	private int repeticao;
	private String descricao;
	private Timestamp data_registro = PIVUtil.getCurrentTime();
	
	public PIVCalendarItemModel() {
	}
	
	@Id
	@SequenceGenerator(name = "\"CalendarioItem_id_seq\"", sequenceName = "\"CalendarioItem_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"CalendarioItem_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAssunto() {
		return assunto;
	}

	public boolean isDia_inteiro() {
		return dia_inteiro;
	}

	public Timestamp getDatar_inicio() {
		return datar_inicio;
	}

	public int getHora() {
		return hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public String getCategorias() {
		return categorias;
	}

	public String getUrl() {
		return url;
	}

	public String getLocal() {
		return local;
	}

	public int getStatus() {
		return status;
	}

	public int getRepeticao() {
		return repeticao;
	}

	public String getDescricao() {
		return descricao;
	}

	public Timestamp getData_registro() {
		return data_registro;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public void setDia_inteiro(boolean dia_inteiro) {
		this.dia_inteiro = dia_inteiro;
	}

	public void setDatar_inicio(Timestamp datar_inicio) {
		this.datar_inicio = datar_inicio;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}

	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

	public void setCategorias(String categorias) {
		this.categorias = categorias;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setData_registro(Timestamp data_registro) {
		this.data_registro = data_registro;
	}
	
	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne
	@JoinColumn(name = "idvendedor", referencedColumnName = "id")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}
	
}