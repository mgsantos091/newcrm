package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.openswing.swing.server.QueryUtil;

/**
 * The persistent class for the "Endereco" database table.
 * 
 */
@Entity
@Table(name = "\"Endereco\"")
public class PIVAddressModel extends ValueObjectImpl implements Serializable {
	private static final long serialVersionUID = 1L;
	private String cep;
	private String bairro;
	private String cidade;
	private String estado;
	private String pais;
	private String logradouro;

	public PIVAddressModel() {
	}

	@Id
//	@Column(unique = true, nullable = false, length = 8)
	@Column(nullable = false, length = 8)
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Column(length = 255)
	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Column(length = 255)
	public String getLogradouro() {
		return this.logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@Column(length = 255)
	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Column(length = 2)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(length = 255)
	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}