package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name = "\"FormaPagamento\"")
public class PIVPaymentMethodModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String forma;
	private String descricao;
	private boolean ativo;
	private Timestamp data_registro = PIVUtil.getCurrentTime();

	public PIVPaymentMethodModel() {
	}

	@Id
	@SequenceGenerator(name = "\"FormaPagamento_id_seq\"", sequenceName = "\"FormaPagamento_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"FormaPagamento_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(length = 255)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Column(unique = true, nullable = false)
	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public Timestamp getData_registro() {
		return data_registro;
	}

	public void setData_registro(Timestamp data_registro) {
		this.data_registro = data_registro;
	}

	
}