package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name = "\"VendasCondPagamento\"")
public class PIVPaymentConditionModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVSalesMainModel vendaPai;
	private PIVPaymentMethodModel formaPagamento;
	private Integer numparcela;
	private Timestamp dataparcela;
	
	public PIVPaymentConditionModel() {
	}

	@Id
	@SequenceGenerator(name = "\"VendasCondPagamento_item_seq\"", sequenceName = "\"VendasCondPagamento_item_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"VendasCondPagamento_item_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer item) {
		this.id = item;
	}
	
	// uni-directional many-to-one association to VendaPai
	@ManyToOne
	@JoinColumn(name="idvendapai")
	public PIVSalesMainModel getVendaPai() {
		return this.vendaPai;
	}

	public void setVendaPai(PIVSalesMainModel vendaPai) {
		this.vendaPai = vendaPai;
	}

	@ManyToOne
	@JoinColumn(name="idformapagamento")
	public PIVPaymentMethodModel getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(PIVPaymentMethodModel formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Integer getNumparcela() {
		return numparcela;
	}

	public Timestamp getDataparcela() {
		return dataparcela;
	}

	public void setNumparcela(Integer numparcela) {
		this.numparcela = numparcela;
	}

	public void setDataparcela(Timestamp dataparcela) {
		this.dataparcela = dataparcela;
	}
	
}