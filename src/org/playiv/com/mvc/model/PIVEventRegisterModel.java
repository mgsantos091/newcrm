package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name="\"RegistroEvento\"")
public class PIVEventRegisterModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer idnivelcomercial;
	private Integer idevento;
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;
	private String nomeevento;
	private String descricaoevento;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	
	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Id
	@SequenceGenerator(name = "\"RegistroEvento_id_seq\"", sequenceName = "\"RegistroEvento_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"RegistroEvento_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@Column(nullable=false)
	public Integer getIdnivelcomercial() {
		return this.idnivelcomercial;
	}

	public void setIdnivelcomercial(Integer idnivelcomercial) {
		this.idnivelcomercial = idnivelcomercial;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne
	@JoinColumn(name = "idvendedor", referencedColumnName = "id")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}

	@Column(length = 255)
	public String getNomeevento() {
		return nomeevento;
	}

	public void setNomeevento(String nomeevento) {
		this.nomeevento = nomeevento;
	}

	public String getDescricaoevento() {
		return descricaoevento;
	}

	public void setDescricaoevento(String descricaoevento) {
		this.descricaoevento = descricaoevento;
	}

	@Column(nullable=false)
	public Integer getIdevento() {
		return idevento;
	}

	public void setIdevento(Integer idevento) {
		this.idevento = idevento;
	}

	public static Comparator<PIVEventRegisterModel> ORDERNA_POR_DATA = new Comparator<PIVEventRegisterModel>() {
        public int compare(PIVEventRegisterModel primeiro , PIVEventRegisterModel segundo) {
            return primeiro.dataregistro.compareTo(segundo.dataregistro);
        }
    };

}