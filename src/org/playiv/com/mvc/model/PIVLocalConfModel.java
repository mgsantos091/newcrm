package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "ConfiguracoesLocais" database table.
 * 
 */
@Entity
@Table(name = "\"ConfiguracoesLocais\"")
public class PIVLocalConfModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String razaosocial;
	private String nomefantasia;
	private String inscricao_estadual;
	private String email;
	private String website;
	private String cnpj;
	private String nomeimagem = "";
	private Integer endnumero;
	private String endreferencia;
	private String endcomplemento;
	private String telefone;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private double latitude;
	private double longitude;
	private String email_server_smpt_host;
	private Integer  email_server_smpt_port;
	private boolean  email_server_ssl;
	
	private String voip_proxy;
	private String voip_dominio;
	private int voip_sipport;
	
	private Integer limite_parcelas = 0;
	
	private PIVAddressModel endereco;

	public PIVLocalConfModel() {
	}

	@Id
	@SequenceGenerator(name = "\"ConfiguracoesLocais_id_seq\"", sequenceName = "\"ConfiguracoesLocais_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"ConfiguracoesLocais_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 14)
	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = PIVUtil.getCurrentTime();
	}

	@Column(length = 255)
	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public Integer getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(Integer endnumero) {
		this.endnumero = endnumero;
	}

	@Column(length = 255)
	public String getNomefantasia() {
		return this.nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	@Column(length = 255)
	public String getRazaosocial() {
		return this.razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	@Column(length = 255)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	// uni-directional many-to-one association to Endereco
	@ManyToOne
	@JoinColumn(name = "cep")
	public PIVAddressModel getEndereco() {
		return this.endereco;
	}

	public void setEndereco(PIVAddressModel endereco) {
		this.endereco = endereco;
	}

	@Column(length = 255)
	public String getNomeimagem() {
		return nomeimagem;
	}

	public void setNomeimagem(String nomeimagem) {
		this.nomeimagem = nomeimagem;
	}

	@Column(length = 255)
	public String getEndreferencia() {
		return endreferencia;
	}

	public void setEndreferencia(String endreferencia) {
		this.endreferencia = endreferencia;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PIVLocalConfModel) {
			PIVLocalConfModel cliente = (PIVLocalConfModel) obj;
			if(this.id == cliente.getId())
				return true;
		}
		return false;
	}

	@Column(length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public String getEmail_server_smpt_host() {
		return email_server_smpt_host;
	}

	public void setEmail_server_smpt_host(String email_server_smpt_host) {
		this.email_server_smpt_host = email_server_smpt_host;
	}

	public Integer getEmail_server_smpt_port() {
		return email_server_smpt_port;
	}

	public void setEmail_server_smpt_port(Integer email_server_smpt_port) {
		this.email_server_smpt_port = email_server_smpt_port;
	}

	public boolean isEmail_server_ssl() {
		return email_server_ssl;
	}

	public void setEmail_server_ssl(boolean email_server_ssl) {
		this.email_server_ssl = email_server_ssl;
	}
	
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getInscricao_estadual() {
		return inscricao_estadual;
	}

	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}

	public Integer getLimite_parcelas() {
		return limite_parcelas;
	}

	public void setLimite_parcelas(Integer limite_parcelas) {
		this.limite_parcelas = limite_parcelas;
	}

	public String getVoip_proxy() {
		return voip_proxy;
	}

	public String getVoip_dominio() {
		return voip_dominio;
	}

	public void setVoip_proxy(String voip_proxy) {
		this.voip_proxy = voip_proxy;
	}

	public void setVoip_dominio(String voip_dominio) {
		this.voip_dominio = voip_dominio;
	}

	public int getVoip_sipport() {
		return voip_sipport;
	}

	public void setVoip_sipport(int voip_sipport) {
		this.voip_sipport = voip_sipport;
	}
	
}