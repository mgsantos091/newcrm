package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.*;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;



import java.sql.Timestamp;


/**
 * The persistent class for the "Usuario" database table.
 * 
 */
@Entity
@Table(name="\"Usuario\"")
public class PIVUserModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private String nome;
	
	private String senha;
	private String login;
	
	private String usuario_email;
	private String senha_email;
	
	private String usuario_voip;
	private String senha_voip;
	
	private boolean usuariovendedor;
	
	private boolean usuariogerente;

	public PIVUserModel() {
    }

	@Id
	@SequenceGenerator(name = "\"Usuario_id_seq\"", sequenceName = "\"Usuario_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Usuario_id_seq\"")
//	@Column(unique=true, nullable=false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}


	@Column(length=255)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	@Column(length=35)
	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}


	@Column(length=25)
	public String getlogin() {
		return this.login;
	}

	public void setlogin(String login) {
		this.login = login;
	}

    public boolean isUsuariovendedor() {
		return usuariovendedor;
	}

	public void setUsuariovendedor(boolean usuariovendedor) {
		this.usuariovendedor = usuariovendedor;
	}
	
	public String getUsuario_email() {
		return usuario_email;
	}

	public void setUsuario_email(String usuario_email) {
		this.usuario_email = usuario_email;
	}

	public String getSenha_email() {
		return senha_email;
	}

	public void setSenha_email(String senha_email) {
		this.senha_email = senha_email;
	}

	public String getUsuario_voip() {
		return usuario_voip;
	}

	public String getSenha_voip() {
		return senha_voip;
	}

	public void setUsuario_voip(String usuario_voip) {
		this.usuario_voip = usuario_voip;
	}

	public void setSenha_voip(String senha_voip) {
		this.senha_voip = senha_voip;
	}
	
	public boolean isUsuariogerente() {
		return usuariogerente;
	}

	public void setUsuariogerente(boolean usuariogerente) {
		this.usuariogerente = usuariogerente;
	}

}