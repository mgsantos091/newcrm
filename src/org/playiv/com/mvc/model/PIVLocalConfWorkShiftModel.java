package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name = "\"HorarioFuncionario\"")
public class PIVLocalConfWorkShiftModel extends ValueObjectImpl implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer diadasemana;

	private Timestamp horariomanhainicio;
	private Timestamp horariomanhafim;

	private Timestamp horariotardeinicio;
	private Timestamp horariotardefim;

	private Timestamp horarionoiteinicio;
	private Timestamp horarionoitefim;

	@Id
	@SequenceGenerator(name = "\"HorarioFuncionario_id_seq\"", sequenceName = "\"HorarioFuncionario_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "\"HorarioFuncionario_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDiadasemana() {
		return diadasemana;
	}

	public void setDiadasemana(Integer diadasemana) {
		this.diadasemana = diadasemana;
	}

	public Timestamp getHorariomanhainicio() {
		return horariomanhainicio;
	}

	public void setHorariomanhainicio(Timestamp horariomanhainicio) {
		this.horariomanhainicio = horariomanhainicio;
	}

	public Timestamp getHorariomanhafim() {
		return horariomanhafim;
	}

	public void setHorariomanhafim(Timestamp horariomanhafim) {
		this.horariomanhafim = horariomanhafim;
	}

	public Timestamp getHorariotardeinicio() {
		return horariotardeinicio;
	}

	public void setHorariotardeinicio(Timestamp horariotardeinicio) {
		this.horariotardeinicio = horariotardeinicio;
	}

	public Timestamp getHorariotardefim() {
		return horariotardefim;
	}

	public void setHorariotardefim(Timestamp horariotardefim) {
		this.horariotardefim = horariotardefim;
	}

	public Timestamp getHorarionoiteinicio() {
		return horarionoiteinicio;
	}

	public void setHorarionoiteinicio(Timestamp horarionoiteinicio) {
		this.horarionoiteinicio = horarionoiteinicio;
	}

	public Timestamp getHorarionoitefim() {
		return horarionoitefim;
	}

	public void setHorarionoitefim(Timestamp horarionoitefim) {
		this.horarionoitefim = horarionoitefim;
	}

}