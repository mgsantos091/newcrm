package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

@Entity
@Table(name="\"Tag\"")
public class PIVTagModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer codtag;
	private String nome;
	private PIVSalesMainModel vendaPai;
	
	@Id
	@SequenceGenerator(name = "\"Tag_id_seq\"", sequenceName = "\"Tag_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Tag_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	// uni-directional many-to-one association to VendaPai
	@ManyToOne
	@JoinColumn(name="idvendapai")
	public PIVSalesMainModel getVendaPai() {
		return this.vendaPai;
	}

	public void setVendaPai(PIVSalesMainModel vendaPai) {
		this.vendaPai = vendaPai;
	}
	
	@Column(nullable = false)
	public Integer getCodtag() {
		return codtag;
	}

	public void setCodtag(Integer codtag) {
		this.codtag = codtag;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}