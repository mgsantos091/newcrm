package org.playiv.com.mvc.model;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class PIVCubeFileTreeNodeModel extends ValueObjectImpl {
	
	private static final long serialVersionUID = 1L;
	
	public String nome;
	public String pathCompleto;
	public boolean isFile;
	
	public PIVCubeFileTreeNodeModel( String nome ) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getPathCompleto() {
		return pathCompleto;
	}

	public void setPathCompleto(String pathCompleto) {
		this.pathCompleto = pathCompleto;
	}

	public boolean isFile() {
		return isFile;
	}

	public void setFile(boolean isFile) {
		this.isFile = isFile;
	}

}
