package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "Produto" database table.
 * 
 */
@Entity
@Table(name = "\"Produto\"")
public class PIVProductModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nome;
	private String descricao;
	private String unidade;
	private Float valcompra;
	private Float valvenda;
	private String nomeimagem;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private boolean ativo;

	private PIVCategoryModel categoria;
	
	public PIVProductModel() {
	}

	@Id
	@SequenceGenerator(name = "\"Produto_id_seq\"", sequenceName = "\"Produto_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Produto_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = PIVUtil.getCurrentTime();
	}

	// uni-directional many-to-one association to Categoria
	@ManyToOne
	@JoinColumn(name="idcategoria")
	public PIVCategoryModel getCategoria() {
		return this.categoria;
	}

	public void setCategoria(PIVCategoryModel categoria) {
		this.categoria = categoria;
	}

	public void setEndereco(PIVCategoryModel categoria) {
		this.categoria = categoria;
	}

	@Column(length = 255)
	public String getNomeimagem() {
		return nomeimagem;
	}

	public void setNomeimagem(String nomeimagem) {
		this.nomeimagem = nomeimagem;
	}
	
	@Column(length = 255)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(length = 255)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Float getValcompra() {
		return valcompra;
	}

	public void setValcompra(Float valcompra) {
		this.valcompra = valcompra;
	}

	public Float getValvenda() {
		return valvenda;
	}

	public void setValvenda(Float valvenda) {
		this.valvenda = valvenda;
	}
	
	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	
}