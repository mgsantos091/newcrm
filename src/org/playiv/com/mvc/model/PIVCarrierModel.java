package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.openswing.swing.server.QueryUtil;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "Cliente" database table.
 * 
 */
@Entity
@Table(name = "\"Transportadora\"")
public class PIVCarrierModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private boolean ativo;
	private String cnpj;
	private String endreferencia;
	private String endcomplemento;
	private Integer endnumero;
	private String nomefantasia;
	private String razaosocial;
	private String website;
	private String email;
	private PIVAddressModel endereco;
	/*private PIVAddressModel endereco_entrega;
	private Integer endnumero_entrega;
	private String endreferencia_entrega;
	private String endcomplemento_entrega;*/
	
	private Timestamp data_registro = PIVUtil.getCurrentTime();

	public PIVCarrierModel() {
	}

	@Id
	@SequenceGenerator(name = "\"Transportadora_id_seq\"", sequenceName = "\"Transportadora_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Transportadora_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Column(length = 255)
	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public Integer getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(Integer endnumero) {
		this.endnumero = endnumero;
	}

	@Column(length = 255)
	public String getNomefantasia() {
		return this.nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	@Column(length = 255)
	public String getRazaosocial() {
		return this.razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	@Column(length = 255)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	// uni-directional many-to-one association to Endereco
	@ManyToOne
	@JoinColumn(name = "cep")
	public PIVAddressModel getEndereco() {
		return this.endereco;
	}

	public void setEndereco(PIVAddressModel endereco) {
		this.endereco = endereco;
	}

	@Column(length = 255)
	public String getEndreferencia() {
		return endreferencia;
	}

	public void setEndreferencia(String endreferencia) {
		this.endreferencia = endreferencia;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PIVCarrierModel) {
			PIVCarrierModel transportadora = (PIVCarrierModel) obj;
			if(this.id == transportadora.getId())
				return true;
		}
		return false;
	}

	@Column(length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/*@ManyToOne
	@JoinColumn(name = "cep_entrega")
	public PIVAddressModel getEndereco_entrega() {
		return endereco_entrega;
	}

	public void setEndereco_entrega(PIVAddressModel endereco_entrega) {
		this.endereco_entrega = endereco_entrega;
	}
	
	public String getEndreferencia_entrega() {
		return endreferencia_entrega;
	}

	public void setEndreferencia_entrega(String endreferencia_entrega) {
		this.endreferencia_entrega = endreferencia_entrega;
	}

	public String getEndcomplemento_entrega() {
		return endcomplemento_entrega;
	}

	public void setEndcomplemento_entrega(String endcomplemento_entrega) {
		this.endcomplemento_entrega = endcomplemento_entrega;
	}
	
	public Integer getEndnumero_entrega() {
		return endnumero_entrega;
	}

	public void setEndnumero_entrega(Integer endnumero_entrega) {
		this.endnumero_entrega = endnumero_entrega;
	}*/

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Timestamp getData_registro() {
		return data_registro;
	}

	public void setData_registro(Timestamp data_registro) {
		this.data_registro = data_registro;
	}
	
}