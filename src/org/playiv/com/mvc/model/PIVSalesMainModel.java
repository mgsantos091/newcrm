package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name="\"VendasPai\"")
public class PIVSalesMainModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;
	private boolean vendaefetivada;
	private Double valorsubtotal = 0d;
	private Double valorfrete = 0d;
	private Double valordespesa = 0d;
	private Double porcdesconto = 0d;
	private Double valordesconto = 0d;
	private Double valortotal = 0d;
	
	private PIVSalesLevelModel estagio;
	
	private PIVCampaignModel campanha;
	
	private String anotacao;
	
	private String comprador;
	
	private Boolean ativo;
	
	public String getComprador() {
		return comprador;
	}

	public void setComprador(String comprador) {
		this.comprador = comprador;
	}

	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	
	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Id
	@SequenceGenerator(name = "\"VendasPai_id_seq\"", sequenceName = "\"VendasPai_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"VendasPai_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne
	@JoinColumn(name = "idvendedor", referencedColumnName = "id")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}
	
	public boolean isVendaefetivada() {
		return vendaefetivada;
	}

	public void setVendaefetivada(boolean vendaefetivada) {
		this.vendaefetivada = vendaefetivada;
	}

	@Column(precision = 4)
	public Double getValortotal() {
		return valortotal;
	}

	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	
	public Double getValorsubtotal() {
		return valorsubtotal;
	}

	public void setValorsubtotal(Double valorsubtotal) {
		this.valorsubtotal = valorsubtotal;
	}
	
	public Double getValordesconto() {
		return valordesconto;
	}

	public void setValordesconto(Double valordesconto) {
		this.valordesconto = valordesconto;
	}
	
	public Double getValorfrete() {
		return valorfrete;
	}

	public void setValorfrete(Double valorfrete) {
		this.valorfrete = valorfrete;
	}

	public Double getPorcdesconto() {
		return porcdesconto;
	}

	public void setPorcdesconto(Double porcdesconto) {
		this.porcdesconto = porcdesconto;
	}

	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}
	
	public Double getValordespesa() {
		return valordespesa;
	}

	public void setValordespesa(Double valordespesa) {
		this.valordespesa = valordespesa;
	}

	@ManyToOne
	@JoinColumn(name = "idestagio", referencedColumnName = "id")
	public PIVSalesLevelModel getEstagio() {
		return estagio;
	}

	public void setEstagio(PIVSalesLevelModel estagio) {
		this.estagio = estagio;
	}

	@ManyToOne
	@JoinColumn(name = "idcampanha", referencedColumnName = "id")
	public PIVCampaignModel getCampanha() {
		return campanha;
	}

	public void setCampanha(PIVCampaignModel campanha) {
		this.campanha = campanha;
	}
	
	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}