package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.*;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

import java.sql.Timestamp;

@Entity
@Table(name="\"MetasComerciais\"")
public class PIVSalesTargetModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer idmeta;
	private Double valormeta;
	private PIVSellerModel vendedor;
	
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	
	public PIVSalesTargetModel() {
    }

	@Id
	@SequenceGenerator(name = "\"MetasComerciais_id_seq\"", sequenceName = "\"MetasComerciais_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"MetasComerciais_id_seq\"")
//	@Column(unique=true, nullable=false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	public Integer getIdmeta() {
		return idmeta;
	}

	public Double getValormeta() {
		return valormeta;
	}

	@ManyToOne
	@JoinColumn(name = "idvendedor")
	public PIVSellerModel getVendedor() {
		return vendedor;
	}

	public void setIdmeta(Integer idmeta) {
		this.idmeta = idmeta;
	}

	public void setValormeta(Double valormeta) {
		this.valormeta = valormeta;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}

}