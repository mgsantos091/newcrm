package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;


/**
 * The persistent class for the "InformacoesAdicionaisCliente" database table.
 * 
 */
@Entity
@Table(name = "\"InformacoesAdicionaisCliente\"")
public class PIVClientMoreInfoModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVClientModel cliente;
	private Integer fonte;
	private Integer atividade;
	private Integer empregados;
	private String segundo_email;
	private String segundo_tel;
	private String terceiro_tel;
	private String segundo_cel;
	private String terceiro_cel;
	private PIVAddressModel endereco_entrega;
	private Integer endnumero_entrega;
	private String endreferencia_entrega;
	private String endcomplemento_entrega;
	private String descricao;
	
	public PIVClientMoreInfoModel() {
	}

	@Id
	@SequenceGenerator(name = "\"InformacoesAdicionaisCliente_id_seq\"", sequenceName = "\"InformacoesAdicionaisCliente_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"InformacoesAdicionaisCliente_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@OneToOne
	@JoinColumn(name = "idcliente")
	public PIVClientModel getCliente() {
		return cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}

	public Integer getFonte() {
		return fonte;
	}

	public void setFonte(Integer fonte) {
		this.fonte = fonte;
	}

	public Integer getAtividade() {
		return atividade;
	}

	public void setAtividade(Integer atividade) {
		this.atividade = atividade;
	}

	public Integer getEmpregados() {
		return empregados;
	}

	public void setEmpregados(Integer empregados) {
		this.empregados = empregados;
	}

	public String getSegundo_email() {
		return segundo_email;
	}

	public void setSegundo_email(String segundo_email) {
		this.segundo_email = segundo_email;
	}

	public String getSegundo_tel() {
		return segundo_tel;
	}

	public void setSegundo_tel(String segundo_tel) {
		this.segundo_tel = segundo_tel;
	}

	public String getTerceiro_tel() {
		return terceiro_tel;
	}

	public void setTerceiro_tel(String terceiro_tel) {
		this.terceiro_tel = terceiro_tel;
	}

	public String getSegundo_cel() {
		return segundo_cel;
	}

	public void setSegundo_cel(String segundo_cel) {
		this.segundo_cel = segundo_cel;
	}

	public String getTerceiro_cel() {
		return terceiro_cel;
	}

	public void setTerceiro_cel(String terceiro_cel) {
		this.terceiro_cel = terceiro_cel;
	}

	@ManyToOne
	@JoinColumn(name = "cep_entrega")
	public PIVAddressModel getEndereco_entrega() {
		return endereco_entrega;
	}

	public void setEndereco_entrega(PIVAddressModel endereco_entrega) {
		this.endereco_entrega = endereco_entrega;
	}

	public Integer getEndnumero_entrega() {
		return endnumero_entrega;
	}

	public void setEndnumero_entrega(Integer endnumero_entrega) {
		this.endnumero_entrega = endnumero_entrega;
	}

	public String getEndreferencia_entrega() {
		return endreferencia_entrega;
	}

	public void setEndreferencia_entrega(String endreferencia_entrega) {
		this.endreferencia_entrega = endreferencia_entrega;
	}

	public String getEndcomplemento_entrega() {
		return endcomplemento_entrega;
	}

	public void setEndcomplemento_entrega(String endcomplemento_entrega) {
		this.endcomplemento_entrega = endcomplemento_entrega;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}