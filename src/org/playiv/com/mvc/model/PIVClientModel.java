package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "Cliente" database table.
 * 
 */
@Entity
@Table(name = "\"Cliente\"")
public class PIVClientModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private boolean ativo;
	private boolean pjuridica;
	private String cnpjcpf;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private String endreferencia;
	private String endcomplemento;
	private Integer endnumero;
	private String nomefantasia;
	private String razaosocial;
	private String website;
	private String email;
	private String nomeimagem = "";
	private Integer fonte;
	private Integer atividade;
	private PIVAddressModel endereco;
	private PIVAddressModel endereco_entrega;
	private Integer endnumero_entrega;
	private String endreferencia_entrega;
	private String endcomplemento_entrega;
	private String telefone;
	/*private String descricao;*/
	/*private List<PIVContactModel> contatos;*/

	public PIVClientModel() {
	}

	@Id
	@SequenceGenerator(name = "\"Cliente_id_seq\"", sequenceName = "\"Cliente_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Cliente_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Column(length = 14)
	public String getCnpjcpf() {
		return this.cnpjcpf;
	}

	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = PIVUtil.getCurrentTime();
	}

	@Column(length = 255)
	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public Integer getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(Integer endnumero) {
		this.endnumero = endnumero;
	}

	@Column(length = 255)
	public String getNomefantasia() {
		return this.nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	@Column(length = 255)
	public String getRazaosocial() {
		return this.razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	@Column(length = 255)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	// uni-directional many-to-one association to Endereco
	@ManyToOne
	@JoinColumn(name = "cep")
	public PIVAddressModel getEndereco() {
		return this.endereco;
	}

	public void setEndereco(PIVAddressModel endereco) {
		this.endereco = endereco;
	}

/*	// bi-directional many-to-one association to Contato
	@OneToMany(mappedBy = "cliente")
	public List<PIVContactModel> getContatos() {
		return this.contatos;
	}*/

/*	public void setContatos(List<PIVContactModel> contatos) {
		this.contatos = contatos;
	}*/

	public boolean isPjuridica() {
		return pjuridica;
	}

	public void setPjuridica(boolean pjuridica) {
		this.pjuridica = pjuridica;
	}

	@Column(length = 255)
	public String getNomeimagem() {
		return nomeimagem;
	}

	public void setNomeimagem(String nomeimagem) {
		this.nomeimagem = nomeimagem;
	}

	@Column(length = 255)
	public String getEndreferencia() {
		return endreferencia;
	}

	public void setEndreferencia(String endreferencia) {
		this.endreferencia = endreferencia;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PIVClientModel) {
			PIVClientModel cliente = (PIVClientModel) obj;
			if(this.id == cliente.getId())
				return true;
		}
		return false;
	}

	@Column(length = 255)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@ManyToOne
	@JoinColumn(name = "cep_entrega")
	public PIVAddressModel getEndereco_entrega() {
		return endereco_entrega;
	}

	public void setEndereco_entrega(PIVAddressModel endereco_entrega) {
		this.endereco_entrega = endereco_entrega;
	}
	
	/*public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}*/
	
	public Integer getFonte() {
		return fonte;
	}
	
	public void setFonte(Integer fonte) {
		this.fonte = fonte;
	}

	public Integer getAtividade() {
		return atividade;
	}

	public void setAtividade(Integer atividade) {
		this.atividade = atividade;
	}
	
	public String getEndreferencia_entrega() {
		return endreferencia_entrega;
	}

	public void setEndreferencia_entrega(String endreferencia_entrega) {
		this.endreferencia_entrega = endreferencia_entrega;
	}

	public String getEndcomplemento_entrega() {
		return endcomplemento_entrega;
	}

	public void setEndcomplemento_entrega(String endcomplemento_entrega) {
		this.endcomplemento_entrega = endcomplemento_entrega;
	}
	
	public Integer getEndnumero_entrega() {
		return endnumero_entrega;
	}

	public void setEndnumero_entrega(Integer endnumero_entrega) {
		this.endnumero_entrega = endnumero_entrega;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}