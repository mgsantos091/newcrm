package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;


/**
 * The persistent class for the "GeoPosicionamentoCliente" database table.
 * 
 */
@Entity
@Table(name="\"GeoPosicionamentoCliente\"")
public class PIVClientGeoPosModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private PIVClientModel cliente;
	private double latitude;
	private double longitude;

    public PIVClientGeoPosModel() {
    }

	@Id
	@SequenceGenerator(name = "\"GeoPosicionamentoCliente_id_seq\"", sequenceName = "\"GeoPosicionamentoCliente_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"GeoPosicionamentoCliente_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}

	@Column(nullable = false)
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Column(nullable = false)
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}