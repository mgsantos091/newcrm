package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "Contato" database table.
 * 
 */
@Entity
@Table(name = "\"Contato\"")
public class PIVContactModel extends ValueObjectImpl implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Boolean ativo;
	private String celular;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private String email;
	private String funcao;
	private String nome;
	private String site;
	private String telefone;
	private boolean responsavel;

	private PIVClientModel cliente;
	
	public PIVContactModel() {
	}

	@Id
	@SequenceGenerator(name = "\"Contato_id_seq\"", sequenceName = "\"Contato_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Contato_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAtivo() {
		return this.ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Column(length = 25)
	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Column(length = 255)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 255)
	public String getFuncao() {
		return this.funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	@Column(length = 255)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(length = 255)
	public String getSite() {
		return this.site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	@Column(length = 25)
	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	// bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name = "idcliente")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	public boolean isResponsavel() {
		return responsavel;
	}

	public void setResponsavel(boolean responsavel) {
		this.responsavel = responsavel;
	}
	
	@Override
	public boolean equals(Object value) {
		if( value instanceof PIVContactModel ) {
			PIVContactModel contatoPValidar = (PIVContactModel) value;
			if(contatoPValidar.getId().equals(this.getId())) return true;
		}
		return false;
	}

}