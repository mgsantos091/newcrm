package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.*;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;



import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the "Vendedor" database table.
 * 
 */
@Entity
@Table(name = "\"Vendedor\"")
public class PIVSellerModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String cep;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private String endcomplemento;
	private Integer endnumero;
	private String nome;
	private PIVUserModel usuario;

	private String arquivo_calendario;
	
	// construtor sobrescrito para aceitar objeto usuario, desta maneira, ao incluir um usu�rio com op��o de 'vendedor', o sistema ir� automaticamente
	// criar um vendedor associado ao usu�rio criado
	public PIVSellerModel(PIVUserModel usuario) {
		this.nome = usuario.getNome();
		this.usuario = usuario;
	}

	public PIVSellerModel() {
	}

	@Id
	@SequenceGenerator(name = "\"Vendedor_id_seq\"", sequenceName = "\"Vendedor_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"Vendedor_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 8)
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Column(length = 255)
	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public Integer getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(Integer endnumero) {
		this.endnumero = endnumero;
	}

	@Column(length = 255)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	// uni-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "idusuario")
	public PIVUserModel getUsuario() {
		return this.usuario;
	}

	public void setUsuario(PIVUserModel usuario) {
		this.usuario = usuario;
	}

	public String getArquivo_calendario() {
		return arquivo_calendario;
	}

	public void setArquivo_calendario(String arquivo_calendario) {
		this.arquivo_calendario = arquivo_calendario;
	}

}