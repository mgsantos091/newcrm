package org.playiv.com.mvc.model;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class PIVTelToCallModel  extends ValueObjectImpl {

	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String tipo;
	private String numero;
	private String funcao;
	
	public String getNome() {
		return nome;
	}
	public String getNumero() {
		return numero;
	}
	public String getFuncao() {
		return funcao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
