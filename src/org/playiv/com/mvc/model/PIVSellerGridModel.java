package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

public class PIVSellerGridModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int nivelComercial;
	
	private int idVendedor;
	private String nomeVendedor;
	
	private int id_cliente;
	private String razaosocial;
	private String nomefantasia;
	
	private int qtdLigacoes;
	private int qtdEmails;
	private int qtdObs;
	private int qtdPropostas;
	
	private int diasNesteNivel;
	
	private Timestamp filtro_dtinicio;
	private Timestamp filtro_dtfim;
	
	public PIVSellerGridModel( ) {
		
	}

	public int getNivelComercial() {
		return nivelComercial;
	}

	public int getIdVendedor() {
		return idVendedor;
	}

	public String getNomeVendedor() {
		return nomeVendedor;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public String getRazaosocial() {
		return razaosocial;
	}

	public String getNomefantasia() {
		return nomefantasia;
	}

	public int getDiasNesteNivel() {
		return diasNesteNivel;
	}

	public int getQtdLigacoes() {
		return qtdLigacoes;
	}

	public int getQtdEmails() {
		return qtdEmails;
	}

	public int getQtdObs() {
		return qtdObs;
	}

	public int getQtdPropostas() {
		return qtdPropostas;
	}

	public void setNivelComercial(int nivelComercial) {
		this.nivelComercial = nivelComercial;
	}

	public void setIdVendedor(int idVendedor) {
		this.idVendedor = idVendedor;
	}

	public void setNomeVendedor(String nomeVendedor) {
		this.nomeVendedor = nomeVendedor;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public void setDiasNesteNivel(int diasNesteNivel) {
		this.diasNesteNivel = diasNesteNivel;
	}

	public void setQtdLigacoes(int qtdLigacoes) {
		this.qtdLigacoes = qtdLigacoes;
	}

	public void setQtdEmails(int qtdEmails) {
		this.qtdEmails = qtdEmails;
	}

	public void setQtdObs(int qtdObs) {
		this.qtdObs = qtdObs;
	}

	public void setQtdPropostas(int qtdPropostas) {
		this.qtdPropostas = qtdPropostas;
	}

	public Timestamp getFiltro_dtinicio() {
		return filtro_dtinicio;
	}

	public Timestamp getFiltro_dtfim() {
		return filtro_dtfim;
	}

	public void setFiltro_dtinicio(Timestamp filtro_dtinicio) {
		this.filtro_dtinicio = filtro_dtinicio;
	}

	public void setFiltro_dtfim(Timestamp filtro_dtfim) {
		this.filtro_dtfim = filtro_dtfim;
	}

}