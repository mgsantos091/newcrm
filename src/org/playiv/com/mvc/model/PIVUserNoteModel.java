package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;


/**
 * The persistent class for the "Contato" database table.
 * 
 */
@Entity
@Table(name = "\"AnotacaoDeDadosCliente\"")
public class PIVUserNoteModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;
	private String anotacao;
	private String nome_arquivo_voz;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();

	public PIVUserNoteModel() {
	}

	@Id
	@SequenceGenerator(name = "\"AnotacaoDeDadosCliente_id_seq\"", sequenceName = "\"AnotacaoDeDadosCliente_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"AnotacaoDeDadosCliente_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	// bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name = "idcliente")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	// bi-directional many-to-one association to Vendedor
	@ManyToOne
	@JoinColumn(name = "idvendedor")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}
	
	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	@Column(length = 255)
	public String getNome_arquivo_voz() {
		return nome_arquivo_voz;
	}

	public void setNome_arquivo_voz(String nome_arquivo_voz) {
		this.nome_arquivo_voz = nome_arquivo_voz;
	}

	public Timestamp getDataregistro() {
		return dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}
	
}