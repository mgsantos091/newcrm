package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

@Entity
@Table(name = "\"CampanhaPaiXVendasPai\"")

public class PIVSalesXCampaignModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private PIVCampaignModel campanha;
	private PIVSalesMainModel venda;
	private Timestamp data_registro = PIVUtil.getCurrentTime();
	
	@Id
	@SequenceGenerator(name = "\"CampanhaPaiXVendasPai_id_seq\"", sequenceName = "\"CampanhaPaiXVendasPai_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"CampanhaPaiXVendasPai_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Timestamp getData_registro() {
		return data_registro;
	}
	
	public void setData_registro(Timestamp dataregistro) {
		this.data_registro = dataregistro;
	}
	
	@ManyToOne
	@JoinColumn(name="id_campanha")
	public PIVCampaignModel getCampanha() {
		return campanha;
	}

	public void setCampanha(PIVCampaignModel campanha) {
		this.campanha = campanha;
	}
	
	@ManyToOne
	@JoinColumn(name="id_venda")
	public PIVSalesMainModel getVenda() {
		return venda;
	}
	
	public void setVenda(PIVSalesMainModel venda) {
		this.venda = venda;
	}

	
}