package org.playiv.com.mvc.model;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.playiv.com.library.general.PIVUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "\"CampanhaPai\"")
public class PIVCampaignModel extends ValueObjectImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;

	private String nome_campanha;
	private String descricao_campanha;
	private PIVCampaignCatModel tipo;
	private String publico_alvo;
	private String patrocinador;
	private Integer status;
	private Double valor_orcado;
	private Double valor_atual;
	private Double previsao_receita;
	
	private Timestamp data_registro = PIVUtil.getCurrentTime();
	
	@Id
	@SequenceGenerator(name = "\"CampanhaPai_id_seq\"", sequenceName = "\"CampanhaPai_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"CampanhaPai_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(length = 255)
	public String getNome_campanha() {
		return nome_campanha;
	}
	
	public void setNome_campanha(String nome_campanha) {
		this.nome_campanha = nome_campanha;
	}
	
	public String getDescricao_campanha() {
		return descricao_campanha;
	}
	
	public void setDescricao_campanha(String descricao_campanha) {
		this.descricao_campanha = descricao_campanha;
	}
	
	@Column(length = 255)
	public String getPublico_alvo() {
		return publico_alvo;
	}
	
	public void setPublico_alvo(String publico_alvo) {
		this.publico_alvo = publico_alvo;
	}
	
	@Column(length = 255)
	public String getPatrocinador() {
		return patrocinador;
	}
	
	public void setPatrocinador(String patrocinador) {
		this.patrocinador = patrocinador;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Double getValor_orcado() {
		return valor_orcado;
	}
	
	public void setValor_orcado(Double valor_orcado) {
		this.valor_orcado = valor_orcado;
	}
	
	public Double getValor_atual() {
		return valor_atual;
	}
	
	public void setValor_atual(Double valor_atual) {
		this.valor_atual = valor_atual;
	}
	
	public Double getPrevisao_receita() {
		return previsao_receita;
	}
	
	public void setPrevisao_receita(Double previsao_receita) {
		this.previsao_receita = previsao_receita;
	}
	
	public Timestamp getData_registro() {
		return data_registro;
	}
	
	public void setData_registro(Timestamp dataregistro) {
		this.data_registro = dataregistro;
	}

	@ManyToOne
	@JoinColumn(name="id_tipo_campanha")
	public PIVCampaignCatModel getTipo() {
		return tipo;
	}

	public void setTipo(PIVCampaignCatModel tipo) {
		this.tipo = tipo;
	}
	
}