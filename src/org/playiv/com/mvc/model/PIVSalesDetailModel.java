package org.playiv.com.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.openswing.swing.message.receive.java.ValueObjectImpl;

/**
 * The persistent class for the "VendasFilho" database table.
 * 
 */
@Entity
@Table(name = "\"VendasFilho\"")
public class PIVSalesDetailModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer item;
	private PIVSalesMainModel vendaPai;
	private PIVProductModel produto;
	private Double valvenda;
	private Double valdesconto;
	private Double porcdesconto;
	private Double qtditens;
	private Double valsubtotal;
	private Double valtotal;
	private boolean ativo;
	
	public PIVSalesDetailModel() {
	}

	@Id
	@SequenceGenerator(name = "\"VendasFilho_item_seq\"", sequenceName = "\"VendasFilho_item_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"VendasFilho_item_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}
	
	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	// uni-directional many-to-one association to VendaPai
	@ManyToOne
	@JoinColumn(name="idvendapai")
	public PIVSalesMainModel getVendaPai() {
		return this.vendaPai;
	}

	public void setVendaPai(PIVSalesMainModel vendaPai) {
		this.vendaPai = vendaPai;
	}
	
	// uni-directional many-to-one association to Produto
	@ManyToOne
	@JoinColumn(name="idproduto")
	public PIVProductModel getProduto() {
		return this.produto;
	}

	public void setProduto(PIVProductModel produto) {
		this.produto = produto;
	}

	public Double getValvenda() {
		return valvenda;
	}

	public void setValvenda(Double valvenda) {
		this.valvenda = valvenda;
	}

	public Double getValdesconto() {
		return valdesconto;
	}

	public void setValdesconto(Double valdesconto) {
		this.valdesconto = valdesconto;
	}

	public Double getQtditens() {
		return qtditens;
	}

	public void setQtditens(Double qtditens) {
		this.qtditens = qtditens;
	}

	public Double getValtotal() {
		return valtotal;
	}

	public void setValtotal(Double valtotal) {
		this.valtotal = valtotal;
	}

	public Double getValsubtotal() {
		return valsubtotal;
	}

	public void setValsubtotal(Double valsubtotal) {
		this.valsubtotal = valsubtotal;
	}

	public Double getPorcdesconto() {
		return porcdesconto;
	}

	public void setPorcdesconto(Double porcdesconto) {
		this.porcdesconto = porcdesconto;
	}	

}