package org.playiv.com.mvc.model;

import java.io.Serializable;
import javax.persistence.*;

import org.openswing.swing.message.receive.java.ValueObjectImpl;
import org.openswing.swing.util.client.ClientUtils;
import org.playiv.com.library.general.PIVUtil;



import java.sql.Timestamp;
import java.util.List;
import static javax.persistence.FetchType.LAZY;


/**
 * The persistent class for the "NivelComercialRelacionamento" database table.
 * 
 */
@Entity
@Table(name="\"NivelComercialRelacionamento\"")
public class PIVBusRelModel extends ValueObjectImpl implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Boolean completado = false;
	private Boolean ativo = true;
	private Timestamp dataregistro = PIVUtil.getCurrentTime();
	private Integer id;
	private Integer idnivelcomercial;
	/*private Integer nivel_classificao;*/
	private PIVClientModel cliente;
	private PIVSellerModel vendedor;

    public PIVBusRelModel() {
    	
    }

	@Column(nullable=false)
	public Boolean getCompletado() {
		return this.completado;
	}

	public void setCompletado(Boolean completado) {
		this.completado = completado;
	}

	@Column(nullable=false)
	public Boolean getAtivo() {
		return this.ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Timestamp getDataregistro() {
		return this.dataregistro;
	}

	public void setDataregistro(Timestamp dataregistro) {
		this.dataregistro = dataregistro;
	}

	@Id
	@SequenceGenerator(name = "\"NivelComercialRelacionamento_id_seq\"", sequenceName = "\"NivelComercialRelacionamento_id_seq\"", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="\"NivelComercialRelacionamento_id_seq\"")
//	@Column(unique = true, nullable = false)
	@Column(nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@Column(nullable=false)
	public Integer getIdnivelcomercial() {
		return this.idnivelcomercial;
	}

	public void setIdnivelcomercial(Integer idnivelcomercial) {
		this.idnivelcomercial = idnivelcomercial;
	}

	@OneToOne
	@JoinColumn(name = "idcliente", referencedColumnName = "id")
	public PIVClientModel getCliente() {
		return this.cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne
	@JoinColumn(name = "idvendedor", referencedColumnName = "id")
	public PIVSellerModel getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(PIVSellerModel vendedor) {
		this.vendedor = vendedor;
	}
	
	@Override
	public boolean equals(Object value) {
		if(value instanceof PIVBusRelModel) {
			PIVBusRelModel PIVBusRelModelPValidar = (PIVBusRelModel) value;
			if(PIVBusRelModelPValidar.getId().equals(this.getId())) return true;
		}
		return false;
	}

	/*public Integer getNivel_classificao() {
		return nivel_classificao;
	}

	public void setNivel_classificao(Integer nivel_classificao) {
		this.nivel_classificao = nivel_classificao;
	}*/

}