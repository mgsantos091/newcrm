package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.util.HashSet;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVCarrierMngtDFController;
import org.playiv.com.swing.general.PIVAddressPanel;
import org.playiv.com.swing.general.PIVCarrierPanel;
import org.playiv.com.swing.general.PIVDeliveryAddressPanel;

public class PIVCarrierMngtDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private PIVCarrierPanel jpTransportePanel;
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	
	private PIVCarrierMngtGFView gridFrame;
	private PIVAddressPanel jpEndereco;
/*	private PIVDeliveryAddressPanel jpEnderecoEntrega;*/
	
	/*private JTabbedPane tbEndereco = new JTabbedPane(JTabbedPane.TOP);*/
	
	public PIVCarrierMngtDFView(PIVCarrierMngtDFController controller, Integer pk) {

		setTitle("Transportadoras - Vis�o Detalhada");
		/*setFrameIcon(new ImageIcon(PIVCarrierMngtDFView.class.getResource("/images/cadastros/cliente/agencia_16x16.png")));*/
		
		this.gridFrame = controller.getGridFrame();

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					PIVCarrierMngtDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);

		/*super.setPreferredSize(new Dimension(800, 600));*/
		getContentPane().setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		if (gridFrame != null) {

			// link the parent grid to the current Form...
			HashSet hashpk = new HashSet();
			hashpk.add("id"); // pk for Form is based on one only attribute...
			jpPrincipal.linkGrid(gridFrame.getGrid(), hashpk, true, true, true,
					null);

		}

		jpPrincipal
				.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill][grow,fill]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVCarrierModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		jpTransportePanel = new PIVCarrierPanel( );

		jpPrincipal.add(jpTransportePanel, "grow");
		
		jpEndereco = null;
		try {
			jpEndereco = new PIVAddressPanel();
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		
		/*jpEnderecoEntrega = null;
		jpEnderecoEntrega = new PIVDeliveryAddressPanel( jpEndereco );*/
		
		/*tbEndereco.addTab("Endereco comercial", jpEndereco);*/
		/*tbEndereco.addTab("Endereco de entrega", jpEnderecoEntrega);*/

		/*jpPrincipal.add(tbEndereco, "grow , wrap");*/
		jpPrincipal.add(jpEndereco, "grow , wrap");
		
		getContentPane().add(jpPrincipal, "grow , span");

		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

	public Form getForm( ){
		return this.jpPrincipal;
	}
	
}