package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

import org.hibernate.Session;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.openswing.swing.mdi.client.InternalFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVTextControlFactory;
import org.playiv.com.library.general.PIVMaskResources;
import org.playiv.com.library.webservice.PIVMapFrame;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVClientGeoPosModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;


public class PIVMapCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	private JTextField txEstado;
	private JTextField txCidade;
	private JTextField txBairro;
	private JTextField txRua;
	private JTextField txNumero;
	private JTextField txComplemento;
	private JTextField txReferencia;

	private PIVMapFrame mapa;

	private String cep = "";
	private Integer numero = 0;
	private String rua = "";
	private String bairro = "";
	private String cidade = "";
	private String estado = "";
	private String referencia;
	private String complemento;

	private JButton confirmaPosicao;

	private PIVClientModel cliente;

	private PIVLocalConfModel confLocal;

	/**
	 * Create the application.
	 * 
	 * @throws ParseException
	 */
	public PIVMapCFView(String cep, String estado, String cidade, String bairro,
			String rua, int numero, String referencia, String complemento)
			throws ParseException {

		super.setTitle("Vis�o de Mapa");
		super.setFrameIcon(new ImageIcon(PIVMapCFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/mapa_16x16.png")));
		
		mapa = new PIVMapFrame();

		this.cep = cep;
		this.estado = estado;
		this.cidade = cidade;
		this.bairro = bairro;
		this.rua = rua;
		this.numero = numero;
		this.referencia = referencia;
		this.complemento = complemento;

		setPreferredSize(new Dimension(800, 650));

		initialize(true);

		atualizaMapa();

	}

	public PIVMapCFView(PIVLocalConfModel confLocal)
			throws ParseException {

		this.confLocal = confLocal;
		
		mapa = new PIVMapFrame();

		PIVAddressModel endereco = confLocal.getEndereco();

		this.cep = endereco.getCep();
		this.estado = endereco.getEstado();
		this.cidade = endereco.getCidade();
		this.bairro = endereco.getBairro();
		this.rua = endereco.getLogradouro();
		this.numero = confLocal.getEndnumero();
		this.referencia = confLocal.getEndreferencia();
		this.complemento = confLocal.getEndcomplemento();

		setPreferredSize(new Dimension(800, 650));

		initialize(true);

		atualizaMapa();
	}

	public PIVMapCFView(PIVClientModel cliente) throws ParseException {
		
		this.cliente = cliente;
		
		mapa = new PIVMapFrame();

		this.cep = cliente.getEndereco().getCep();
		this.estado = cliente.getEndereco().getEstado();
		this.cidade = cliente.getEndereco().getCidade();
		this.bairro = cliente.getEndereco().getBairro();
		this.rua = cliente.getEndereco().getLogradouro();
		this.numero = cliente.getEndnumero();
		this.referencia = cliente.getEndreferencia();
		this.complemento = cliente.getEndcomplemento();

		setPreferredSize(new Dimension(800, 650));

		initialize(true);

		atualizaMapa();
		
	}

	public PIVMapCFView(
			ArrayList<PIVClientGeoPosModel> posicionamentos)
			throws ParseException {
		mapa = new PIVMapFrame();
		setPreferredSize(new Dimension(800, 650));
		initialize(false);
		atualizaMapa(posicionamentos);
	}
	
	public PIVMapCFView(PIVClientGeoPosModel posicionamento)
			throws ParseException {
		mapa = new PIVMapFrame();
		setPreferredSize(new Dimension(800, 650));
		initialize(false);
		atualizaMapa(posicionamento);
	}

	private void atualizaMapa(
			ArrayList<PIVClientGeoPosModel> posicionamentos) {
		mapa.buscaEndereco(posicionamentos);
	}

	private void atualizaMapa(PIVClientGeoPosModel posicionamento) {
		mapa.buscaEndereco(posicionamento);
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws ParseException
	 */
	private void initialize(boolean cabecalho) throws ParseException {
		setLayout(new BorderLayout(25, 25));
		setTitle("Visualiza��o de Mapa");

		if (cabecalho) {

			JPanel panel = new JPanel();
			panel.setLayout(new MigLayout("", "[right][][right][]"));

			/* panel.setPreferredSize(new Dimension(0, 100)); */
			add(panel, BorderLayout.NORTH);

			JLabel lbCep = new JLabel("Cep:");
			panel.add(lbCep);

			JTextField txCep = PIVTextControlFactory
					.criaFormattedTextField(PIVMaskResources.mask.CEP);
			txCep.setColumns(10);
			txCep.setText(cep);
			panel.add(txCep, "wrap");

			JLabel lbCidade = new JLabel("Cidade:");
			panel.add(lbCidade);

			txCidade = new JTextField();
			txCidade.setColumns(20);
			txCidade.setText(cidade);
			panel.add(txCidade);

			JLabel lbEstado = new JLabel("Estado:");
			panel.add(lbEstado);

			txEstado = new JTextField();
			txEstado.setText(estado);
			txEstado.setColumns(10);
			panel.add(txEstado, "wrap");

			JLabel lbBairro = new JLabel("Bairro:");
			panel.add(lbBairro);

			txBairro = new JTextField();
			txBairro.setText(bairro);
			txBairro.setColumns(20);
			panel.add(txBairro);

			JLabel lbRua = new JLabel("Rua:");
			panel.add(lbRua);

			txRua = new JTextField();
			txRua.setText(rua);
			txRua.setColumns(20);
			panel.add(txRua);

			JLabel lbNumero = new JLabel("Num.:");
			panel.add(lbNumero);

			txNumero = new JTextField();
			txNumero.setText(String.valueOf(numero));
			txNumero.setColumns(10);
			panel.add(txNumero, "wrap");

			JLabel lbComplemento = new JLabel("Complemento:");
			panel.add(lbComplemento);

			txComplemento = new JTextField();
			txComplemento.setText(complemento);
			txComplemento.setColumns(30);
			panel.add(txComplemento, "wrap");

			JLabel lbReferencia = new JLabel("Refer�ncia:");
			panel.add(lbReferencia);

			txReferencia = new JTextField();
			txReferencia.setText(referencia);
			txReferencia.setColumns(30);
			panel.add(txReferencia, "wrap");

			if (this.cliente != null || this.confLocal != null) {
				confirmaPosicao = new JButton("Gravar localiza��o");
				if (this.cliente != null) {
					confirmaPosicao.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							GeoPosition posicao = PIVMapCFView.this.mapa
									.getPosicao();
							if (posicao != null) {
								double lat = posicao.getLatitude();
								double lon = posicao.getLongitude();
								if (cliente != null) {
									PIVDao pdao = PIVDao.getInstance();
									Session session = pdao.getSession();
									PIVClientGeoPosModel posicionamento = (PIVClientGeoPosModel) session
											.createQuery(
													"from org.playiv.com.mvc.model.PIVClientGeoPosModel as Pos where Pos.cliente.id = "
															+ cliente.getId())
											.uniqueResult();
									session.close();
									if (posicionamento != null) {
										posicionamento.setLatitude(lat);
										posicionamento.setLongitude(lon);
										pdao.update(posicionamento);
									} else {
										posicionamento = new PIVClientGeoPosModel();
										posicionamento.setCliente(cliente);
										posicionamento.setLatitude(lat);
										posicionamento.setLongitude(lon);
										pdao.save(posicionamento);
									}
									JOptionPane
											.showMessageDialog(
													null,
													"Localiza��o gravada com sucesso",
													"Localiza��o do cliente atualizado",
													JOptionPane.INFORMATION_MESSAGE);
								} else
									JOptionPane
											.showMessageDialog(
													null,
													"� necess�rio o cadastro do cliente antes de gravar a localiza��o do cliente, favor salvar os dados.",
													"Cadastro de cliente n�o encontrado",
													JOptionPane.ERROR_MESSAGE);
							} /*else
								JOptionPane
										.showMessageDialog(
												null,
												"O endere�o descrito n�o foi encontrado, por favor verificar e tentar novamente.",
												"Endere�o n�o encontrado",
												JOptionPane.INFORMATION_MESSAGE);*/
						}
					});
				} else if (this.confLocal != null) {
					confirmaPosicao.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							GeoPosition posicao = PIVMapCFView.this.mapa
									.getPosicao();
							if (posicao != null) {
								double lat = posicao.getLatitude();
								double lon = posicao.getLongitude();
								PIVDao pdao = PIVDao.getInstance();
								confLocal.setLatitude(lat);
								confLocal.setLongitude(lon);
								pdao.update(confLocal);
								JOptionPane
								.showMessageDialog(
										null,
										"Localiza��o gravada com sucesso",
										"Localiza��o da empresa atualizado",
										JOptionPane.INFORMATION_MESSAGE);
							} else
								JOptionPane
										.showMessageDialog(
												null,
												"O endere�o descrito n�o foi encontrado, por favor verificar e tentar novamente.",
												"Endere�o n�o encontrado",
												JOptionPane.INFORMATION_MESSAGE);
						}
					});
				}
				panel.add(confirmaPosicao);
			}

		}

		mapa.setZoom(4);
		add(mapa, BorderLayout.CENTER);

		// Image img = mapa.createImage(500, 500);

		pack();
		
		setUniqueInstance(true);
	}

	private void atualizaMapa() {
		mapa.buscaEndereco(this.estado, this.cidade, this.bairro, this.rua);
		// atualizaTextoCValorVariaveis();
	}

	/*
	 * private void atualizaTextoCValorVariaveis( ) {
	 * this.txEstado.setText(this.estado); this.txCidade.setText(this.cidade);
	 * this.txBairro.setText(this.bairro); this.txRua.setText(this.rua); }
	 */

	/*
	 * private boolean checaValoresPrincipais( ) { boolean chkVal = true;
	 * if(this.estado.equals("")) chkVal = false; else
	 * if(this.cidade.equals("")) chkVal = false; else if(this.numero == 0)
	 * chkVal = false; else if(this.rua.equals("")) chkVal = false;
	 * if(chkVal==false) JOptionPane.showMessageDialog(null,
	 * "Algum dos campos principais (Estado, Cidade, Rua ou N�mero esta incompleto."
	 * ); return chkVal; }
	 */

	public PIVClientModel getCliente() {
		return cliente;
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}

}