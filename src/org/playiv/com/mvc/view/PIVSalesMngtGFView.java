package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVBulsRelMainDetailCFController;
import org.playiv.com.mvc.controller.PIVSalesMngtGFController;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVSalesMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	/* private InsertButton insertButton = new InsertButton(); */

	private InsertButton insertButton = new InsertButton();
//	private JButton fecharPedidoButton = new JButton();
	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton = new ExportButton();

	private JButton docButton = new JButton("gerar doc");
	
	private IntegerColumn colId = new IntegerColumn();
	private ComboColumn colTipoOrcamento = new ComboColumn();
	private CurrencyColumn colValOrcamento = new CurrencyColumn();
	private DateColumn colDataRegistro = new DateColumn();

	private PIVSalesMngtGFController controller;

	public PIVSalesMngtGFView(PIVSalesMngtGFController controller) {
		this.controller = controller;
		try {
			super.setTitle("Propostas - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(
					PIVClientMngtGFView.class
							.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_16x16.png")));
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		
//		grid.setAnchorLastColumn(true);
		
//		fecharPedidoButton.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				int row = grid.getSelectedRow();
//				PIVSalesMainModel venda = (PIVSalesMainModel) grid.getVOListTableModel().getObjectForRow(row);
//				boolean vendaEfetivada = venda.isVendaefetivada();
//				if(vendaEfetivada)
//					JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Aten��o, este pedido (num. " + venda.getId() + ") j� foi fechado, favor selecionar outro pedido","Venda fechada",JOptionPane.ERROR_MESSAGE);
//				else
//				{
//					venda.setVendaefetivada(true);
//					PIVDao.getInstance().update(venda);
//					JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Pedido (num. " + venda.getId() + ") fechado com sucesso.","Venda fechada",JOptionPane.ERROR_MESSAGE);
//				}
//			}
//		});
		
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		/* grid.setInsertButton(insertButton); */
		grid.setReloadButton(reloadButton);
		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSalesMainModel");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		
		docButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.gerarArquivo( );
				controller.abrirArquivoRelacionado( );
			}
		});
		
		buttonsPanel.add(insertButton);
//		buttonsPanel.add(fecharPedidoButton);
		buttonsPanel.add(deleteButton);
		buttonsPanel.add(reloadButton);
		buttonsPanel.add(exportButton);
		buttonsPanel.add(docButton);
		
		colId.setColumnName("id");
		colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(25);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);
		
		colTipoOrcamento.setColumnName("vendaefetivada");
		colTipoOrcamento.setDomainId("TIPO_ORCAMENTO");
		colTipoOrcamento.setPreferredWidth(80);
		colTipoOrcamento.setColumnFilterable(true);
		colTipoOrcamento.setColumnSortable(true);
		grid.getColumnContainer().add(colTipoOrcamento);

		colValOrcamento.setColumnName("valortotal");
		colValOrcamento.setPreferredWidth(100);
		colValOrcamento.setColumnSortable(true);
		colValOrcamento.setDecimals(4);
		grid.getColumnContainer().add(colValOrcamento);

		colDataRegistro.setColumnName("dataregistro");
		colDataRegistro.setPreferredWidth(100);
		grid.getColumnContainer().add(colDataRegistro);

		setSize(500, 300);

		setUniqueInstance(true);

	}

	void insertButton_actionPerformed(ActionEvent e) {
		PIVClientModel cliente = PIVSalesMngtGFView.this.controller
				.getCliente();
		
		new PIVBulsRelMainDetailCFController( controller.getOrganizacliente() , cliente , null );
		
		/*PIVBusRelMainDetailCFView vendaFrame = new PIVBusRelMainDetailCFView(cliente, null);
		MDIFrame.add(vendaFrame);*/
		/*
		 * new
		 * PIVBusRelMainDetailCFView(PIVSalesMngtGFView.this.controller.get);
		 */
		/* new PIVClientMngtDFController(this, null); */
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVSalesMngtGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(
				PIVSalesMngtGFView pivSalesMngtGFView) {
			this.adaptee = pivSalesMngtGFView;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

}