package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.controller.PIVBusRelGarbageMngtGFController;
import org.playiv.com.mvc.model.PIVUserModel;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVBusRelGarbageMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	GridControl grid = new GridControl();

	FlowLayout flowLayout1 = new FlowLayout();

	JPanel buttonsPanel = new JPanel();

	JButton recuperaButton = new JButton("Recuperar cliente(s)");
	ReloadButton reloadButton = new ReloadButton();

	private IntegerColumn coldIdCliente = new IntegerColumn();
	/*private ComboColumn colNivelCliente = new ComboColumn();*/
	private TextColumn colNomeFantasiaCliente = new TextColumn();
	/*private IntegerColumn colNivelComercial = new IntegerColumn();*/
	/*private CheckBoxColumn colAtivoCliente = new CheckBoxColumn();*/
	private TextColumn colMotivoNaovenda = new TextColumn();
	private DateColumn colDataEncerramento = new DateColumn();

	private PIVBusRelGarbageMngtGFController controller;
	
/*	private final IntegerColumn colId = new IntegerColumn();
	private final TextColumn colNome = new TextColumn();
	private final TextColumn colUsuario = new TextColumn();*/

	public PIVBusRelGarbageMngtGFView(PIVBusRelGarbageMngtGFController controller) {
		super.setTitle("Relacionamento Comerciais Exclu�dos - Recuperar");
		super.setFrameIcon(new ImageIcon(PIVBusRelGarbageMngtGFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/lixo_16x16.png")));
		try {
			jbInit();
			
			setSize(new Dimension(500, 400));
			
			this.controller = controller;
			
			grid.setController(controller);			
			grid.setGridDataLocator(controller);
			
			MDIFrame.add(this);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		/*grid.setAnchorLastColumn(true);*/
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
/*		grid.setDeleteButton(null);
		grid.setExportButton(null);
		grid.setFilterButton(null);
		grid.setInsertButton(null);*/
		grid.setReloadButton(reloadButton);
		grid.setEditOnSingleRow(false);

		/*grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVBusRelModel");*/
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVGarbageClientModel");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		
		recuperaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVBusRelGarbageMngtGFView.this.controller.recuperarClientes();
				try {
					closeFrame();
				} catch (PropertyVetoException error) {
					// TODO Auto-generated catch block
					error.printStackTrace();
					PIVLogSettings.getInstance().error(error.getMessage(), error);					
				}
			}
		});
		
		buttonsPanel.add(recuperaButton);
		buttonsPanel.add(reloadButton, null);
		
		PIVUserModel usuario = PIVUserSession.getInstance().getUsuarioSessao();
		
		if(usuario.isUsuariogerente()) {
			
			IntegerColumn colIdVendedor = new IntegerColumn();
			colIdVendedor.setColumnName("vendedor.id");
			colIdVendedor.setColumnSortable(true);
			colIdVendedor.setSortVersus(Consts.DESC_SORTED);
			colIdVendedor.setSortingOrder(1);
			grid.getColumnContainer().add(colIdVendedor);
			
			TextColumn colNomeVendedor = new TextColumn();
			colNomeVendedor.setColumnName("vendedor.nome");
			grid.getColumnContainer().add(colNomeVendedor);
			
		}
		
		/*colNivelCliente.setDomainId("NIVEL_COMERCIAL");
		colNivelCliente.setColumnName("idnivelcomercial");
		colNivelCliente.setColumnSortable(false);
		colNivelCliente.setSortVersus(Consts.ASC_SORTED);
		colNivelCliente.setSortingOrder(usuario.isUsuariogerente()?2:1);
		grid.getColumnContainer().add(colNivelCliente);*/
		
		coldIdCliente.setColumnName("cliente.id");
		coldIdCliente.setMaxCharacters(5);
		coldIdCliente.setEditableOnEdit(false);
		grid.getColumnContainer().add(coldIdCliente);

		colNomeFantasiaCliente.setColumnName("cliente.nomefantasia");
		colNomeFantasiaCliente.setMaxCharacters(120);
		colNomeFantasiaCliente.setEditableOnEdit(false);
		grid.getColumnContainer().add(colNomeFantasiaCliente);
		
		colMotivoNaovenda.setColumnName("motivo");
		grid.getColumnContainer().add(colMotivoNaovenda);

/*		colNivelComercial.setColumnName("idnivelcomercial");
		colNivelComercial.setMaxCharacters(1);
		colNivelComercial.setEditableOnEdit(false);
		grid.getColumnContainer().add(colNivelComercial);*/

/*		colAtivoCliente.setColumnName("ativo");
		colAtivoCliente.setEditableOnEdit(true);
		grid.getColumnContainer().add(colAtivoCliente);*/

		colDataEncerramento.setColumnName("dataregistro");
		grid.getColumnContainer().add(colDataEncerramento);

		setUniqueInstance(true);
		
	}

	public GridControl getGrid() {
		return grid;
	}

}
