package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;
import org.playiv.com.mvc.controller.PIVClientContactGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.swing.general.PIVClientDescPanel;
import org.playiv.com.swing.general.PIVClientPanel;
import org.playiv.com.swing.general.PIVContactPanel;
import org.playiv.com.swing.general.PIVAddressPanel;
import org.playiv.com.swing.general.PIVDeliveryAddressPanel;

public class PIVClientMngtDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private PIVClientPanel jpCliente_1;
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	private PIVClientContactGFController contatoController;
	private PIVClientMngtDFController clienteDFController;
	
//	private PIVClientMngtGFView gridFrame;
	private PIVAddressPanel jpEndereco;
	private PIVDeliveryAddressPanel jpEnderecoEntrega;

	private PIVContactPanel jpContato;
	/*private PIVClientDescPanel jpDescricao_1;*/
	
	private JTabbedPane tbEndereco = new JTabbedPane(JTabbedPane.TOP);

	public PIVClientMngtDFView(PIVClientMngtDFController controller, Integer pk,
			PIVDao pdao) {

		setTitle("Clientes - Vis�o Detalhada");
		setFrameIcon(new ImageIcon(PIVClientMngtDFView.class.getResource("/images/cadastros/cliente/agencia_16x16.png")));
		
		this.clienteDFController = controller;

//		this.gridFrame = controller.getGridFrame();

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					PIVClientMngtDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);

		/*super.setPreferredSize(new Dimension(800, 600));*/
		getContentPane().setLayout(new MigLayout("", "[grow,fill][]", "[grow,fill][grow,fill][]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		JButton btnMapa = new JButton("");
		btnMapa.setIcon(new ImageIcon(PIVClientMngtDFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/mapa_32x32.png")));
		btnMapa.setBorder(BorderFactory.createEmptyBorder());
		btnMapa.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evento) {

				String cep = jpEndereco.getCep();
				
				if((cep==null||cep.equals(""))) { JOptionPane.showMessageDialog(null,"� necess�rio a informa��o do CEP ao menos","Preencher o CEP",JOptionPane.ERROR_MESSAGE); return; }
				
				/*String estado = jpEndereco.getEstado();
				String cidade = jpEndereco.getCidade();
				String bairro = jpEndereco.getBairro();
				String rua = jpEndereco.getRua();
				Integer numero = jpEndereco.getNumero();
				String complemento = jpEndereco.getComplemento();
				String referencia = jpEndereco.getReferencia();*/

				PIVMapCFView mapaView = null;
				try {

					PIVClientModel cliente = (PIVClientModel) PIVClientMngtDFView.this.jpPrincipal.getVOModel().getValueObject();
					if(cliente.getId()!=null)
						mapaView = new PIVMapCFView(cliente);
					else
						JOptionPane
						.showMessageDialog(
								null,
								"� necess�rio o cadastro do cliente antes de gravar a localiza��o do cliente, favor salvar os dados.",
								"Cadastro de cliente n�o encontrado",
								JOptionPane.ERROR_MESSAGE);

					if(cliente.getId()!=null) // se tiver Id significa que o objeto j� foi persistido
						mapaView.setCliente(cliente);

					MDIFrame.add(mapaView);

				} catch (ParseException e) {
					JOptionPane.showMessageDialog(
							MDIFrame.getInstance(),
							"Houve um erro ao processar o mapa. Favor tentar novamente mais tarde ou contate o administrador");
					e.printStackTrace();
					PIVLogSettings.getInstance().error(e.getMessage(), e);
				}
			}
		});
		jpBotoes.add(btnMapa);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

/*		if (gridFrame != null) {

			// link the parent grid to the current Form...
			HashSet hashpk = new HashSet();
			hashpk.add("id"); // pk for Form is based on one only attribute...
			jpPrincipal.linkGrid(gridFrame.getGrid(), hashpk, true, true, true,
					null);

		}*/

		jpPrincipal
				.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVClientModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		try {
			jpCliente_1 = new PIVClientPanel(this.clienteDFController);
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

		jpPrincipal.add(jpCliente_1, "");
		
		jpEndereco = null;
		try {
			jpEndereco = new PIVAddressPanel();
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		
		jpEnderecoEntrega = null;
		jpEnderecoEntrega = new PIVDeliveryAddressPanel( jpEndereco );
		
		tbEndereco.addTab("Endereco comercial", jpEndereco);
		tbEndereco.addTab("Endereco de entrega", jpEnderecoEntrega);

		jpPrincipal.add(tbEndereco, "");

		jpContato = null;
		contatoController = new PIVClientContactGFController(pk,false);
		jpContato = contatoController.getContatoPanel();

//		jpPrincipal.add(jpContato, "grow,span");
		
		/*try {
			jpDescricao_1 = new PIVClientDescPanel();
			jpDescricao_1.setMinimumSize(new Dimension(10, 10));
			jpDescricao_1.setPreferredSize(new Dimension(10, 10));
		} catch (ParseException e1) {
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
			e1.printStackTrace();
		}

		jpPrincipal.add(jpDescricao_1, "grow");*/
		
		getContentPane().add(jpPrincipal, "dock center , grow");
		getContentPane().add(jpContato,"dock south, grow");
	

		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public PIVClientPanel getClientePanel() {
		return this.jpCliente_1;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
		this.contatoController.getContatoPanel().setEnabledGridButtons(mode);
	}

	public ArrayList<PIVContactModel> getContatos() {
		return this.contatoController.getContatos();
	}

	public void setPk(Integer pk) {
		this.contatoController.setPk(pk);
	}
	
	public Form getForm( ){
		return this.jpPrincipal;
	}
	
	public PIVContactPanel getJpContato() {
		return jpContato;
	}

}