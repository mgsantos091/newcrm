package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVVoipRecordDFController;
import org.playiv.com.swing.general.PIVVoipCallPanel;
import org.playiv.com.swing.general.PIVVoipRecordPanel;
import org.playiv.com.swing.general.PIVVoipTimerPanel;

public class PIVVoipRecordDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private PIVVoipRecordPanel jpRecordPanel;
	private PIVVoipTimerPanel jpWatchPanel;
	private PIVVoipCallPanel jpVoipPanel;
	
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	
	private PIVVoipRecordDFController controller;

	public PIVVoipRecordDFView(PIVVoipRecordDFController controller) {

		setTitle("Liga��es - Vis�o Detalhada");
		setFrameIcon(new ImageIcon(PIVVoipRecordDFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
		
		this.controller = controller;

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					PIVVoipRecordDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);

		getContentPane().setLayout(new MigLayout("", "", ""));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		jpPrincipal
//				.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill][grow,fill]"));
				.setLayout(new MigLayout("", "[fill,grow][fill,grow]", "[fill,grow]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVVoipRecordModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setReloadButton(reloadButton);

		try {
			jpRecordPanel = new PIVVoipRecordPanel(this.controller);
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

//		jpPrincipal.add(jpRecordPanel, "grow , span , wrap");
		
//		if(controller.getPk()==null)
//			jpPrincipal.add(jpRecordPanel, "grow");
//		else
			jpPrincipal.add(jpRecordPanel, "grow,span");
		
//		if(controller.getPk()==null) { // novo registro
//			try {
//				jpVoipPanel = new PIVVoipCallPanel(this.controller);
//				jpPrincipal.add(jpVoipPanel, "");
//			} catch (ParseException e) {
//				e.printStackTrace();
//				PIVLogSettings.getInstance().error(e.getMessage(), e);
//			}
//		}
		
		getContentPane().add(jpPrincipal, "dock center, grow");
		
		try {
			jpWatchPanel = new PIVVoipTimerPanel(this.controller);
//			if(controller.getPk() == null) jpWatchPanel.startTimer();
		} catch (ParseException e1) {
			e1.printStackTrace();
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
		}
		
//		jpPrincipal.add(jpWatchPanel, "grow,span");

		getContentPane().add(jpWatchPanel, "dock south , grow");
		
		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

	public Form getForm( ){
		return this.jpPrincipal;
	}

	public PIVVoipTimerPanel getJpWatchPanel() {
		return jpWatchPanel;
	}

}