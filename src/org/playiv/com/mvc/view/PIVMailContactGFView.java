package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;





import org.openswing.swing.client.GenericButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDesktopFunc;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVMailContactGFController;
import org.playiv.com.mvc.model.PIVContactModel;


/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVMailContactGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	/*SaveButton saveButton = new SaveButton();
	EditButton editButton = new EditButton();*/
	private ReloadButton reloadButton = new ReloadButton();
	private GenericButton enviaEmailButton = new GenericButton();

	private IntegerColumn colIdContato = new IntegerColumn();
	private TextColumn colNomeContato = new TextColumn();
	private TextColumn colFuncaoContato = new TextColumn();
	private TextColumn colEmailContato = new TextColumn();

	private PIVMailContactGFController controller;
	
/*	private final IntegerColumn colId = new IntegerColumn();
	private final TextColumn colNome = new TextColumn();
	private final TextColumn colUsuario = new TextColumn();*/

	public PIVMailContactGFView(PIVMailContactGFController controller) {
		super.setTitle("Envio de e-mail - selecione os contatos (clique uma vez para selecionar)");
		super.setFrameIcon(new ImageIcon(PIVMailContactGFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/email_16x16.png")));
		try {
			jbInit();
			setSize(500,300);
			grid.setController(controller);
			grid.setGridDataLocator(controller);
			this.controller = controller;
			MDIFrame.add(this);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
/*		grid.setDeleteButton(null);
		grid.setExportButton(null);
		grid.setFilterButton(null);
		grid.setInsertButton(null);
		grid.setEditButton(editButton);
		grid.setSaveButton(saveButton);*/
		grid.setReloadButton(reloadButton);
		grid.setEditOnSingleRow(false);

		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVContactModel");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		
		enviaEmailButton.setIcon(new ImageIcon(PIVMailContactGFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/email_32x32.png")));
		enviaEmailButton.setPreferredSize(new Dimension(32,32));
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(enviaEmailButton, null);
		
		enviaEmailButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				PIVMailContactGFView.this.controller.enviarEmailAoCliente();
			}
		});
		
		/*
		buttonsPanel.add(saveButton,null);
		buttonsPanel.add(editButton,null);*/
		
		/*private IntegerColumn colIdContato = new IntegerColumn();
		private TextColumn colNomeContato = new TextColumn();
		private TextColumn colFuncaoContato = new TextColumn();
		private TextColumn colEmailContato = new TextColumn();*/
		
		colIdContato.setColumnName("id");
		grid.getColumnContainer().add(colIdContato);
		
		colNomeContato.setColumnName("nome");
		grid.getColumnContainer().add(colNomeContato);
		
		colFuncaoContato.setColumnName("funcao");
		grid.getColumnContainer().add(colFuncaoContato);
		
		colEmailContato.setColumnName("email");
		grid.getColumnContainer().add(colEmailContato);

}

	public GridControl getGrid() {
		return grid;
	}

}
