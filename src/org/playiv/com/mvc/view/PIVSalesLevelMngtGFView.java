package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.MultiLineTextColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.mvc.controller.PIVCatMngtGFController;
import org.playiv.com.mvc.controller.PIVSalesLevelMngtGFController;

public class PIVSalesLevelMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();
	
	private JPanel buttonsPanel = new JPanel();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private DeleteButton deleteButton = new DeleteButton();
	private SaveButton saveButton = new SaveButton();
	private CopyButton copyButton = new CopyButton();
	private ReloadButton reloadButton = new ReloadButton();
	private ExportButton exportButton = new ExportButton();
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colEstagio = new TextColumn();
	private DateColumn colDataRegistro = new DateColumn();

	private PIVSalesLevelMngtGFController controller = null;

	public PIVSalesLevelMngtGFView(PIVSalesLevelMngtGFController controller) {
		super.setTitle("Est�gios de Venda - Vis�o Geral");
		/*super.setFrameIcon(new ImageIcon(PIVSalesLevelMngtGFView.class.getResource("/images/cadastros/categoria/categoria_16x16.png")));*/
		this.controller = controller;
		initialize();
		super.setSize(600,400);
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		setUniqueInstance(true);
	}

	private void initialize() {

		grid.setCopyButton(copyButton);
	    grid.setDeleteButton(deleteButton);
	    grid.setEditButton(editButton);
	    grid.setExportButton(exportButton);
	    grid.setInsertButton(insertButton);
	    grid.setReloadButton(reloadButton);
	    grid.setSaveButton(saveButton);
	    grid.setRowHeight(50);
		
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSalesLevelModel");
		
		colId.setColumnFilterable(true);
		colId.setColumnName("id");
		colId.setColumnSortable(true);
		colId.setEditableOnEdit(false);
		colId.setEditableOnInsert(false);
		colId.setColumnRequired(false);
		
		colEstagio.setColumnSortable(true);
		colEstagio.setColumnFilterable(true);
		colEstagio.setColumnRequired(true);
		colEstagio.setColumnName("estagio");
		colEstagio.setEditableOnEdit(true);
		colEstagio.setEditableOnInsert(true);
		/*colEstagio.setLocale(new Locale("pt", "BR"));*/
		/*colEstagio.get*/

		colDataRegistro.setEditableOnEdit(false);
		colDataRegistro.setEditableOnInsert(false);
		colDataRegistro.setColumnRequired(false);
		colDataRegistro.setColumnName("dataregistro");
		
		this.getContentPane().add(grid, BorderLayout.CENTER);
	    this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);	    
	    buttonsPanel.add(insertButton, null);
	    buttonsPanel.add(copyButton, null);
	    buttonsPanel.add(editButton, null);
	    buttonsPanel.add(reloadButton, null);
	    buttonsPanel.add(saveButton, null);
	    buttonsPanel.add(exportButton, null);
	    buttonsPanel.add(deleteButton, null);
	    
	    grid.getColumnContainer().add(colId, null);
	    grid.getColumnContainer().add(colEstagio, null);
	    grid.getColumnContainer().add(colDataRegistro, null);
	    
	}

	public GridControl getGrid() {
		return this.grid;
	}

	public void reloadData() {
		this.grid.reloadData();
	}

}