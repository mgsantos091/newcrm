package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVCampaignMngtDFController;
import org.playiv.com.mvc.controller.PIVCampaignMngtGFController;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVCampaignMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private ExportButton exportButton1 = new ExportButton();
	private FilterButton filterButton1 = new FilterButton();
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colNome = new TextColumn();
	private TextColumn colDesc = new TextColumn();
	private TextColumn colTipoCampanha = new TextColumn();
	private TextColumn colPublicoAlvo = new TextColumn();
	private TextColumn colPatrocinador = new TextColumn();
	private ComboColumn colStatusCampanha = new ComboColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();

	public PIVCampaignMngtGFView(PIVCampaignMngtGFController controller) {
		super.setTitle("Campanhas - Vis�o Geral");
		super.setFrameIcon(new ImageIcon(PIVCampaignMngtGFView.class.getResource("/images/cadastros/campanha/campanha_16x16.png")));
		try {
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {

		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		
		grid.setExportButton(exportButton1);
		grid.setFilterButton(filterButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVCampaignModel");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(exportButton1, null);
		buttonsPanel.add(filterButton1, null);

		/*private IntegerColumn colId = new IntegerColumn();
		private TextColumn colNome = new TextColumn();
		private TextColumn colDesc = new TextColumn();
		private TextColumn colTipoCampanha = new TextColumn();
		private TextColumn colPublicoAlvo = new TextColumn();
		private TextColumn colPatrocinador = new TextColumn();
		private TextColumn colStatusCampanha = new TextColumn();
		private DateTimeColumn colDataRegistro = new DateTimeColumn();*/
				
		colId.setColumnName("id");
		grid.getColumnContainer().add(colId);
		
		colNome.setColumnName("nome_campanha");
		colNome.setColumnFilterable(true);
		colNome.setColumnSortable(true);
		grid.getColumnContainer().add(colNome);
		
		colDesc.setColumnName("descricao_campanha");
		colDesc.setColumnFilterable(true);
		colDesc.setColumnSortable(false);
		grid.getColumnContainer().add(colDesc);
		
		colTipoCampanha.setColumnName("tipo.nome");
		colTipoCampanha.setColumnFilterable(true);
		colTipoCampanha.setColumnSortable(true);
		grid.getColumnContainer().add(colTipoCampanha);
		
		colPublicoAlvo.setColumnName("publico_alvo");
		colPublicoAlvo.setColumnFilterable(true);
		colPublicoAlvo.setColumnSortable(true);
		grid.getColumnContainer().add(colPublicoAlvo);
		
		colPatrocinador.setColumnName("patrocinador");
		colPatrocinador.setColumnFilterable(true);
		colPatrocinador.setColumnSortable(true);
		grid.getColumnContainer().add(colPatrocinador);
		
		colStatusCampanha.setDomainId("STATUS_CAMPANHA");
		colStatusCampanha.setColumnName("status");
		colStatusCampanha.setColumnFilterable(true);
		colStatusCampanha.setColumnSortable(true);
		grid.getColumnContainer().add(colStatusCampanha);
		
		colDataRegistro.setColumnName("data_registro");
		colDataRegistro.setColumnFilterable(false);
		colDataRegistro.setColumnSortable(true);
		grid.getColumnContainer().add(colDataRegistro);

		setSize(800, 600);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVCampaignMngtDFController(this, null);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVCampaignMngtGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVCampaignMngtGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}
}
