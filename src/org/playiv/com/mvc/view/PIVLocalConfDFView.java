package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.TimeColumn;
import org.openswing.swing.util.java.Consts;

import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVLocalConfDFController;
import org.playiv.com.mvc.controller.PIVLocalConfWorkShiftController;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.swing.general.PIVAddressPanel;
import org.playiv.com.swing.general.PIVLocalConfEmailPanel;
import org.playiv.com.swing.general.PIVLocalConfPanel;

public class PIVLocalConfDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;

//	private JPanel jpWorkingShift = new JPanel();

	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private ReloadButton reloadButton;

	private PIVLocalConfPanel jpConfLocal;
	private PIVAddressPanel jpEndereco;

//	private GridControl gridWorkShift = new GridControl();

//	private JSplitPane splitPane = new JSplitPane();

//	private EditButton btEditWorkShift;
//	private SaveButton btSaveWorkShift;
//	private ReloadButton btReloadWorkShift;

	private JTabbedPane tb1 = new JTabbedPane(JTabbedPane.TOP);

	private PIVLocalConfEmailPanel jpConfEmail;
	
	private JPanel jpVoipConf;
	
	private JPanel jpOutrasConf;
	
	public PIVLocalConfDFView(PIVLocalConfDFController controller) {

		super.setTitle("Configura��es Locais");
		super.setFrameIcon(new ImageIcon(PIVLocalConfDFView.class.getResource("/images/configuracao/configuracoes_locais/configuracao_16x16.png")));

//		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
//		splitPane.setDividerLocation(300);

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};

		super.getInputMap().put(esc, "esc");
		super.getActionMap().put("esc", actionESC);

		getContentPane().setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		JButton btnMapa = new JButton("MAPA");
		btnMapa.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evento) {

				String cep = jpEndereco.getCep();

				if ((cep == null || cep.equals(""))) {
					JOptionPane.showMessageDialog(null,
							"� necess�rio a informa��o do CEP ao menos",
							"Preencher o CEP", JOptionPane.ERROR_MESSAGE);
					return;
				}

				PIVMapCFView mapaView = null;
				try {
					PIVLocalConfModel confLocal = (PIVLocalConfModel) PIVLocalConfDFView.this.jpPrincipal
							.getVOModel().getValueObject();
					mapaView = new PIVMapCFView(confLocal);
					MDIFrame.add(mapaView);

				} catch (ParseException e) {
					JOptionPane.showMessageDialog(
							MDIFrame.getInstance(),
							"Houve um erro ao processar o mapa. Favor tentar novamente mais tarde ou contate o administrador");
					e.printStackTrace();
					PIVLogSettings.getInstance().error(e.getMessage(), e);
				}
			}
		});
		jpBotoes.add(btnMapa);

		/*
		 * ExportButton exportButton = new ExportButton();
		 * jpBotoes.add(exportButton);
		 */

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		// link the parent grid to the current Form...
		/*
		 * HashSet hashpk = new HashSet(); hashpk.add("id"); // pk for Form is
		 * based on one only attribute...
		 * jpPrincipal.linkGrid(gridFrame.getGrid(),hashpk,true,true,true,null);
		 */

		jpPrincipal.setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow,fill]"));

		jpPrincipal
				.setVOClassName("org.playiv.com.mvc.model.PIVLocalConfModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		jpConfLocal = null;
		try {
			jpConfLocal = new PIVLocalConfPanel(controller);
		} catch (ParseException e1) {
			e1.printStackTrace();
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
		}

		jpConfEmail = null;
		try {
			jpConfEmail = new PIVLocalConfEmailPanel( );
		} catch (ParseException e2) {
			e2.printStackTrace();
			PIVLogSettings.getInstance().error(e2.getMessage(), e2);
		}
		
		jpVoipConf = new JPanel( );
		
		jpVoipConf.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpVoipConf.setBackground(new Color(245, 245, 245));
		jpVoipConf.setLayout(new MigLayout("", "[right][grow][right][grow][grow]", ""));
		
		LabelControl lblVoipProxy = new LabelControl("Proxy:");
		jpVoipConf.add(lblVoipProxy, "");
	
		TextControl txtVoipProxy = new TextControl();
		txtVoipProxy.setAttributeName("voip_proxy");
		lblVoipProxy.setLabelFor(txtVoipProxy);
		jpVoipConf.add(txtVoipProxy, "growx, wrap");
		
		LabelControl lblVoipDominio = new LabelControl("Dominio:");
		jpVoipConf.add(lblVoipDominio, "");
	
		TextControl txtVoipDominio = new TextControl();
		txtVoipDominio.setAttributeName("voip_dominio");
		lblVoipDominio.setLabelFor(txtVoipDominio);
		jpVoipConf.add(txtVoipDominio, "growx, wrap");
		
		LabelControl lblVoipPorta = new LabelControl("Porta:");
		jpVoipConf.add(lblVoipPorta, "");
	
		NumericControl txtVoipPorta = new NumericControl();
		txtVoipPorta.setAttributeName("voip_sipport");
		lblVoipPorta.setLabelFor(txtVoipPorta);
		jpVoipConf.add(txtVoipPorta, "growx, wrap");		
		
		jpOutrasConf = new JPanel( );
		
		jpOutrasConf.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpOutrasConf.setBackground(new Color(245, 245, 245));
		jpOutrasConf.setLayout(new MigLayout("", "[right][grow][right][grow][grow]", ""));
		
		LabelControl lblNumParcelasLimite = new LabelControl("Num. m�x de parcelas:");
		jpOutrasConf.add(lblNumParcelasLimite, "");
	
		NumericControl txtNumParcelasLimite = new NumericControl();
		txtNumParcelasLimite.setAttributeName("limite_parcelas");
		lblNumParcelasLimite.setLabelFor(txtNumParcelasLimite);
		jpOutrasConf.add(txtNumParcelasLimite, "growx, wrap");
		
		tb1.addTab("Dados da empresa", jpConfLocal);
		tb1.addTab("Configura��o de e-mail", jpConfEmail);
		tb1.addTab("Voip", jpVoipConf);
		tb1.addTab("Outras conf.", jpOutrasConf);
		
		jpPrincipal.add(tb1, "growx");

		jpEndereco = null;
		try {
			jpEndereco = new PIVAddressPanel();
		} catch (ParseException e1) {
			e1.printStackTrace();
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
		}

		jpPrincipal.add(jpEndereco, "growx");

//		splitPane.add(jpPrincipal, JSplitPane.TOP);
//		splitPane.add(jpWorkingShift, JSplitPane.BOTTOM);

//		TitledBorder titledBorder3;
//		titledBorder3 = new TitledBorder("");
//		titledBorder3.setTitleColor(Color.blue);
//		titledBorder3.setTitle("Hor�rio de Trabalho");

//		jpWorkingShift.setLayout(new MigLayout("", "[grow,fill]", "[grow]"));
//		jpWorkingShift.setBorder(titledBorder3);

//		JLabel lblImgTopico = new JLabel();
//		lblImgTopico.setHorizontalAlignment(SwingConstants.LEFT);
//		lblImgTopico.setIcon(new ImageIcon(PIVLocalConfDFView.class.getResource("/images/configuracao/configuracoes_locais/turno_64x64.png")));
//		jpWorkingShift.add(lblImgTopico, "w 120! , h 120! , spany 3 , gaptop 5 , gapright 5 , top , west ");
		
//		PIVLocalConfWorkShiftController gridController = new PIVLocalConfWorkShiftController(gridWorkShift);
//		gridWorkShift.setGridDataLocator(gridController);
//		gridWorkShift.setController(gridController);

//		btEditWorkShift = new EditButton();
//		btSaveWorkShift = new SaveButton();
//		btReloadWorkShift = new ReloadButton();
		
//		JPanel jpBotoesWorkShift = new JPanel(new FlowLayout());
//		jpBotoesWorkShift.add(btEditWorkShift);
//		jpBotoesWorkShift.add(btSaveWorkShift);
//		jpBotoesWorkShift.add(btReloadWorkShift);

//		gridWorkShift.setEditButton(btEditWorkShift);
//		gridWorkShift.setReloadButton(btReloadWorkShift);
//		gridWorkShift.setSaveButton(btSaveWorkShift);
//		gridWorkShift.setValueObjectClassName("org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel");
//		gridWorkShift.setAutoLoadData(true);
//		gridWorkShift.setVisibleStatusPanel(false);

//		ComboColumn colDiaDaSemana = new ComboColumn();
//		colDiaDaSemana.setDomainId("DIAS_DA_SEMANA");
//		colDiaDaSemana.setColumnName("diadasemana");
//		colDiaDaSemana.setColumnSortable(false);
//		colDiaDaSemana.setSortVersus(org.openswing.swing.util.java.Consts.ASC_SORTED);
//		colDiaDaSemana.setSortingOrder(1);

//		TimeColumn colHorarioManhaInicio = new TimeColumn();
//		TimeColumn colHorarioManhaFim = new TimeColumn();
//		TimeColumn colHorarioTardeInicio = new TimeColumn();
//		TimeColumn colHorarioTardeFim = new TimeColumn();
//		TimeColumn colHorarioNoiteInicio = new TimeColumn();
//		TimeColumn colHorarioNoiteFim = new TimeColumn();

//		colHorarioManhaInicio.setColumnName("horariomanhainicio");
//		colHorarioManhaInicio.setColumnRequired(false);
//		colHorarioManhaInicio.setEditableOnEdit(true);
//
//		colHorarioManhaFim.setColumnName("horariomanhafim");
//		colHorarioManhaFim.setColumnRequired(false);
//		colHorarioManhaFim.setEditableOnEdit(true);
//
//		colHorarioTardeInicio.setColumnName("horariotardeinicio");
//		colHorarioTardeInicio.setColumnRequired(false);
//		colHorarioTardeInicio.setEditableOnEdit(true);
//
//		colHorarioTardeFim.setColumnName("horariotardefim");
//		colHorarioTardeFim.setColumnRequired(false);
//		colHorarioTardeFim.setEditableOnEdit(true);
//
//		colHorarioNoiteInicio.setColumnName("horarionoiteinicio");
//		colHorarioNoiteInicio.setColumnRequired(false);
//		colHorarioNoiteInicio.setEditableOnEdit(true);
//
//		colHorarioNoiteFim.setColumnName("horarionoitefim");
//		colHorarioNoiteFim.setColumnRequired(false);
//		colHorarioNoiteFim.setEditableOnEdit(true);

//		gridWorkShift.getColumnContainer().add(colDiaDaSemana);
//		gridWorkShift.getColumnContainer().add(colHorarioManhaInicio);
//		gridWorkShift.getColumnContainer().add(colHorarioManhaFim);
//		gridWorkShift.getColumnContainer().add(colHorarioTardeInicio);
//		gridWorkShift.getColumnContainer().add(colHorarioTardeFim);
//		gridWorkShift.getColumnContainer().add(colHorarioNoiteInicio);
//		gridWorkShift.getColumnContainer().add(colHorarioNoiteFim);
//
//		gridWorkShift.setShowFilterPanelOnGrid(false);
		
//		jpWorkingShift.add(jpBotoesWorkShift, "north , growx");
//		jpWorkingShift.add(gridWorkShift, "grow");

//		super.getContentPane().add(splitPane, "grow , span");

		 super.add(jpPrincipal, "dock center , grow , span");

		super.pack();
		
		setUniqueInstance(true);
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

	public PIVLocalConfPanel getJpConfLocal() {
		return jpConfLocal;
	}

//	public void setEnableGridButtonsWorkShift(boolean enabled) {
//		if (enabled) {
//			btEditWorkShift.setEnabled(enabled);
//			btReloadWorkShift.setEnabled(enabled);
//		} else {
//			btEditWorkShift.setEnabled(enabled);
//			btSaveWorkShift.setEnabled(enabled);
//			btReloadWorkShift.setEnabled(enabled);
//		}
//	}
	
//	public GridControl getGrid( ) {
//		return this.gridWorkShift;
//	}

}