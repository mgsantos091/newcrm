package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.HashSet;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.mvc.controller.PIVProdMngtDFController;
import org.playiv.com.swing.general.PIVProductPanel;

public class PIVProdMngtDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private PIVProductPanel jpProduto;
	private SaveButton saveButton;
	private DeleteButton deleteButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;

	private PIVProdMngtGFView gridFrame;
	
	public PIVProdMngtDFView( PIVProdMngtDFController controller , Integer pk ) {

		super.setTitle("Produtos - Vis�o Detalhada");
		super.setFrameIcon(new ImageIcon(PIVProdMngtDFView.class.getResource("/images/cadastros/produto/produto_16x16.png")));
		
		this.gridFrame = controller.getGridFrame();

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeFrame();
				}
				catch (PropertyVetoException ex) { }
			}
		};
		super.getInputMap().put(esc,"esc");
		super.getActionMap().put("esc",actionESC);

		super.setLayout(new MigLayout());
		
		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		/*ExportButton exportButton = new ExportButton();
		jpBotoes.add(exportButton);*/

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		// link the parent grid to the current Form...
		HashSet hashpk = new HashSet();
		hashpk.add("id"); // pk for Form is based on one only attribute...
		jpPrincipal.linkGrid(gridFrame.getGrid(),hashpk,true,true,true,null);

		jpPrincipal
				.setLayout(new MigLayout());
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVProductModel");
		jpPrincipal.setFormController(controller);
		
		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setDeleteButton(deleteButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);
		
		
		jpProduto = new PIVProductPanel();

		jpPrincipal.add(jpProduto, "grow");

		super.add(jpPrincipal, "dock center , grow , span");

		super.pack();
		
		setUniqueInstance(true);
	}

	public Form getPanel(){
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}
	
}