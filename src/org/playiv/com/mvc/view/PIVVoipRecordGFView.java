package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.internationalization.java.Resources;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.table.columns.client.TimeColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVTelContactGFController;
import org.playiv.com.mvc.controller.PIVVoipRecordDFController;
import org.playiv.com.mvc.controller.PIVVoipRecordGFController;
import org.playiv.com.mvc.model.PIVVoipRecordModel;

public class PIVVoipRecordGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
//	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton1 = new ExportButton();

	private IntegerColumn colId = new IntegerColumn();
	private ComboColumn colStatusLigacao = new ComboColumn();
	private TimeColumn colDuracao = new TimeColumn(TimeZone.getTimeZone("GMT"));
	private TextColumn colNumero = new TextColumn();
	private TextColumn colAnotacao = new TextColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();

	private PIVVoipRecordGFController controller;
	
	public PIVVoipRecordGFView(PIVVoipRecordGFController controller) {
		try {
			super.setTitle("Liga��es - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(PIVVoipRecordGFView.class
					.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
			this.controller = controller;
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
//		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
//		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVVoipRecordModel");
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
				this));
		exportButton1.setText("exportButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(editButton, null);
		buttonsPanel.add(reloadButton, null);
//		buttonsPanel.add(deleteButton, null);
		buttonsPanel.add(exportButton1, null);

		colId.setColumnName("id");
		// colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(35);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		colStatusLigacao.setColumnName("status");
		colStatusLigacao.setPreferredWidth(125);
		colStatusLigacao.setColumnSortable(true);
		colStatusLigacao.setColumnFilterable(true);
		colStatusLigacao.setDomainId("STATUS_LIGACAO");
		grid.getColumnContainer().add(colStatusLigacao);

		colNumero.setColumnName("numero");
		// colNumero.setHeaderColumnName("Site");
		colNumero.setMaxCharacters(30);
		colNumero.setPreferredWidth(90);
		grid.getColumnContainer().add(colNumero);

		colDuracao.setColumnName("duracao");
		colDuracao.setPreferredWidth(120);
		colDuracao.setTimeFormat(Resources.HH_MM_SS);
		grid.getColumnContainer().add(colDuracao);
		
		colAnotacao.setColumnName("anotacao");
		colAnotacao.setMaxCharacters(255);
		colAnotacao.setPreferredWidth(220);
		grid.getColumnContainer().add(colAnotacao);
		
		colDataRegistro.setColumnName("dataregistro");
		colDataRegistro.setPreferredWidth(100);
		grid.getColumnContainer().add(colDataRegistro);
		
		setSize(800, 600);
		
		setUniqueInstance(true);
	}

	void insertButton_actionPerformed(ActionEvent e) {
//		new PIVVoipRecordDFController(this,controller.getCliente());
		new PIVTelContactGFController(this,controller.getCliente());
	}

	void editButton_actionPerformed(ActionEvent e) {
		PIVVoipRecordModel voipRecord = (PIVVoipRecordModel) grid
				.getVOListTableModel().getObjectForRow(grid.getSelectedRow());
		int pk = voipRecord.getId();
		new PIVVoipRecordDFController(this, pk);
	}

	public GridControl getGrid() {
		return grid;
	}
	
	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVVoipRecordGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVVoipRecordGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

	class EmpGridFrame_editButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVVoipRecordGFView adaptee;

		EmpGridFrame_editButton_actionAdapter(PIVVoipRecordGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.editButton_actionPerformed(e);
		}
	}

}