package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVSalesTargetGFController;
import org.playiv.com.mvc.controller.PIVUserMngtDFController;
import org.playiv.com.mvc.controller.PIVUserMngtGFController;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVUserMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton1 = new ExportButton();
	private FilterButton filterButton1 = new FilterButton();
	
	private final IntegerColumn colId = new IntegerColumn();
	private final TextColumn colNome = new TextColumn();
	private final TextColumn colLogin = new TextColumn();

	public PIVUserMngtGFView(PIVUserMngtGFController controller) {
		super.setTitle("Usu�rios - Vis�o Geral");
		super.setFrameIcon(new ImageIcon(PIVProdMngtDFView.class.getResource("/images/cadastros/usuario/usuario_16x16.png")));
		try {
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {

		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton1);
		grid.setFilterButton(filterButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVUserModel");
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		exportButton1.setText("exportButton1");
		filterButton1.setText("filterButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(deleteButton, null);
		buttonsPanel.add(exportButton1, null);
		buttonsPanel.add(filterButton1, null);

		colId.setColumnName("id");
		colId.setHeaderColumnName("Cod");
		colId.setColumnSortable(true);
		colId.setColumnFilterable(true);
		/*colId.setMaxCharacters(5);*/
		colId.setPreferredWidth(25);
		grid.getColumnContainer().add(colId);

		colLogin.setColumnName("login");
		colLogin.setHeaderColumnName("Login");
		colLogin.setColumnSortable(true);
		colLogin.setColumnFilterable(true);
		/*colLogin.setMaxCharacters(80);*/
		colLogin.setPreferredWidth(150);
		grid.getColumnContainer().add(colLogin);

		colNome.setColumnName("nome");
		colNome.setHeaderColumnName("Nome");
		colNome.setColumnFilterable(true);
		/*colNome.setMaxCharacters(80);*/
		colNome.setPreferredWidth(250);
		grid.getColumnContainer().add(colNome);
		
		setSize(800, 600);

	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVUserMngtDFController(this, null);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVUserMngtGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVUserMngtGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}
}
