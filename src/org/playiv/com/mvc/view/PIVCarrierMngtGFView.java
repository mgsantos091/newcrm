package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVCarrierMngtDFController;
import org.playiv.com.mvc.controller.PIVCarrierMngtGFController;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVCarrierMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton1 = new ExportButton();

	private final IntegerColumn colId = new IntegerColumn();

	private final TextColumn colNomeRed = new TextColumn();
	private final TextColumn colNomeComp = new TextColumn();
	private final TextColumn colSite = new TextColumn();
	private final TextColumn colEstado = new TextColumn();
	private final TextColumn colCidade = new TextColumn();

	public PIVCarrierMngtGFView(PIVCarrierMngtGFController controller) {
		try {
			super.setTitle("Transportadoras - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(PIVCarrierMngtGFView.class
					.getResource("/images/cadastros/transportadora/transport_16x16.png")));

			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVCarrierModel");
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		exportButton1.setText("exportButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(deleteButton, null);
		buttonsPanel.add(exportButton1, null);

		colId.setColumnName("id");
		colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(25);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		colNomeRed.setColumnName("nomefantasia");
		colNomeRed.setHeaderColumnName("Nome Reduzido");
		colNomeRed.setMaxCharacters(80);
		colNomeRed.setPreferredWidth(150);
		colNomeRed.setColumnFilterable(true);
		colNomeRed.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeRed);

		colNomeComp.setColumnName("razaosocial");
		colNomeComp.setHeaderColumnName("Nome Completo");
		colNomeComp.setMaxCharacters(80);
		colNomeComp.setPreferredWidth(225);
		colNomeComp.setColumnFilterable(true);
		grid.getColumnContainer().add(colNomeComp);

		colSite.setColumnName("website");
		colSite.setHeaderColumnName("Site");
		colSite.setMaxCharacters(80);
		colSite.setPreferredWidth(100);
		grid.getColumnContainer().add(colSite);

		colEstado.setColumnName("endereco.estado");
		colEstado.setHeaderColumnName("Estado");
		colEstado.setMaxCharacters(80);
		colEstado.setPreferredWidth(25);
		grid.getColumnContainer().add(colEstado);

		colCidade.setColumnName("endereco.cidade");
		colCidade.setHeaderColumnName("Cidade");
		colCidade.setMaxCharacters(80);
		colCidade.setPreferredWidth(80);
		grid.getColumnContainer().add(colCidade);

		setSize(800, 600);

		setUniqueInstance(true);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVCarrierMngtDFController(this, null);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVCarrierMngtGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVCarrierMngtGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}

}