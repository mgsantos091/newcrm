package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.function.IPIVCommandFunc;
import org.playiv.com.library.function.PIVAddSellerCommandFunc;
import org.playiv.com.library.function.PIVBusRelCommandSubjectFunc;
import org.playiv.com.library.function.PIVNoCommandFunc;
import org.playiv.com.library.function.PIVSalesBudgetCommandFunc;
import org.playiv.com.library.function.PIVSalesCalendarCommandFunc;
import org.playiv.com.library.function.PIVSalesCubeCommandFunc;
import org.playiv.com.library.function.PIVSalesStatsCommandFunc;
import org.playiv.com.library.function.PIVSalesTargetCommandFunc;
import org.playiv.com.library.function.PIVSellerInfoFunc;
import org.playiv.com.mvc.controller.PIVSellerMngtBCGMatrixController;
import org.playiv.com.mvc.controller.PIVSellerMngtCFController;
import org.playiv.com.swing.component.PIVCustomFlowLayout;
import org.playiv.com.swing.general.PIVBusRelBeginFrame;
import org.playiv.com.swing.general.PIVSalesElementPanel;

public class PIVSellerMngtCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private PIVBusRelBeginFrame jpInicio = new PIVBusRelBeginFrame();
	private PIVSalesElementPanel jpVendedores = new PIVSalesElementPanel();
	private JPanel jpMatrizBCG = new JPanel();
	private JPanel jpClientesEstrela = new JPanel();
	private JPanel jpClientesQuest = new JPanel();
	private JPanel jpClientesVacaLeit = new JPanel();
	private JPanel jpClientesAbacaxi = new JPanel();

	private JTabbedPane tpCentral = new JTabbedPane(JTabbedPane.TOP);

	private JLabel lblLixo;

	private PIVSellerMngtCFController controller;

	private JLabel lblTemp;

	private JButton btnAddVend;
	private JButton btnVisualizaPedidos;
	private JButton btnStatusDoVendedor;
	private JButton btnMetasComerciais;
	private JButton btnCalendarioVendedores;
	private JButton btnCuboVendedor;

	private PIVBusRelCommandSubjectFunc subjCommand = new PIVBusRelCommandSubjectFunc();
	private IPIVCommandFunc addCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc orcamentoCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc statusDasVendasCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc metasComerciaisCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc calendarioCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc cuboCommand = new PIVNoCommandFunc();
	
	/**
	 * Create the application.
	 */
	public PIVSellerMngtCFView() {

		super.setTitle("Gerenciamento de Vendedores");
		super.setFrameIcon(new ImageIcon(PIVSellerMngtCFView.class.getResource("/images/gerenciamento_relcomercial/logo/gerrelcomercial-icone_16x16.png")));

		controller = new PIVSellerMngtCFController( jpVendedores , subjCommand );
		
		super.setPreferredSize(new Dimension(800, 600));
		super.getContentPane().setLayout(new BorderLayout());

		JScrollPane scrollPaneVendedores = new JScrollPane(/*JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER*/);
		scrollPaneVendedores.getViewport().setBackground(Color.WHITE);
		scrollPaneVendedores.setViewportView(jpVendedores);
		
		JPanel jpClientesPNivel = new JPanel();
		jpClientesPNivel.setLayout(new BorderLayout(0, 0));

		JPanel jpOpEDadosCli = new JPanel();
		jpOpEDadosCli.setPreferredSize(new Dimension(0, 175));
		jpOpEDadosCli.setLayout(new BorderLayout(0, 0));

		jpClientesPNivel.add(jpOpEDadosCli, BorderLayout.SOUTH);

		JPanel jpAcoes = new JPanel();
		jpAcoes.setPreferredSize(new Dimension(225, 10));
		jpAcoes.setBackground(Color.WHITE);
		jpAcoes.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Painel de A��es",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(47, 79,
						79)));
		jpAcoes.setLayout(new GridLayout(0, 4, 5, 5));
		
		
		addCommand = new PIVAddSellerCommandFunc( controller );
		btnAddVend = new JButton(); // ADD
		btnAddVend.setIcon(new ImageIcon(
				PIVSellerMngtCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/addcliente_32x32.png")));
		btnAddVend.setBorder(null);
		btnAddVend.setBackground(Color.WHITE);
		btnAddVend.setForeground(Color.WHITE);
		/*btnAddVend.setMnemonic(KeyEvent.VK_U);*/
		btnAddVend.setToolTipText("Adiciona um novo vendedor");
		btnAddVend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				addCommand.execute();
			}
		});
		subjCommand.registerCommand(addCommand);
		jpAcoes.add(btnAddVend);

		orcamentoCommand = new PIVSalesBudgetCommandFunc();
		btnVisualizaPedidos = new JButton();
		btnVisualizaPedidos.setIcon(new ImageIcon(
				PIVSellerMngtCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_32x32.png")));
		btnVisualizaPedidos.setBorder(null);
		btnVisualizaPedidos.setBackground(Color.WHITE);
		btnVisualizaPedidos.setForeground(Color.WHITE);
		btnVisualizaPedidos.setToolTipText("Visualiza pedidos abertos e fechados do vendedor");
		subjCommand.registerCommand(orcamentoCommand);
		btnVisualizaPedidos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				orcamentoCommand.execute();
			}
		});
		jpAcoes.add(btnVisualizaPedidos);
		
		statusDasVendasCommand = new PIVSalesStatsCommandFunc( );
		btnStatusDoVendedor = new JButton("X");
		btnStatusDoVendedor.setBorder(null);
		btnStatusDoVendedor.setBackground(Color.WHITE);
		/*btnStatusDoVendedor.setForeground(Color.WHITE);*/
		btnStatusDoVendedor.setToolTipText("Visualiza o status geral do vendedor");
		subjCommand.registerCommand(statusDasVendasCommand);
		btnStatusDoVendedor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				statusDasVendasCommand.execute();
			}
		});
		jpAcoes.add(btnStatusDoVendedor);
		
		metasComerciaisCommand = new PIVSalesTargetCommandFunc();
		btnMetasComerciais = new JButton("Y");
		btnMetasComerciais.setBorder(null);
		btnMetasComerciais.setBackground(Color.WHITE);
		btnMetasComerciais.setToolTipText("Determine as metas comerciais da empresa");
		subjCommand.registerCommand(metasComerciaisCommand);
		btnMetasComerciais.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				metasComerciaisCommand.execute();
			}
		});
		jpAcoes.add(btnMetasComerciais);
		
		calendarioCommand = new PIVSalesCalendarCommandFunc();
		btnCalendarioVendedores = new JButton(); // CALEND
		btnCalendarioVendedores.setBorder(null);
		btnCalendarioVendedores.setBackground(Color.WHITE);
		btnCalendarioVendedores.setForeground(Color.WHITE);
		btnCalendarioVendedores
				.setIcon(new ImageIcon(
						PIVSellerMngtCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_32x32.png")));
		btnCalendarioVendedores.setToolTipText("Exibe o calend�rio dos vendedores, utilizado para marcar reuni�es e outros eventos");
		btnCalendarioVendedores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				calendarioCommand.execute();
			}
		});
		subjCommand.registerCommand(calendarioCommand);
		jpAcoes.add(btnCalendarioVendedores);
		
		cuboCommand = new PIVSalesCubeCommandFunc();
		btnCuboVendedor = new JButton(); // CUBO
		btnCuboVendedor.setBorder(null);
		btnCuboVendedor.setBackground(Color.WHITE);
		btnCuboVendedor.setForeground(Color.WHITE);
		btnCuboVendedor.setToolTipText("Abre o Cubo de Conhecimento do vendedor"); 
		btnCuboVendedor.setIcon(new ImageIcon(
				PIVSellerMngtCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_32x32.png")));
		btnCuboVendedor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				cuboCommand.execute();
			}
		});
		subjCommand.registerCommand(cuboCommand);
		jpAcoes.add(btnCuboVendedor);
		
		/*metasCommand = new PIVAddCommandFunc( organizaClienteSuspect );*/

		// Registra os itens que n�o possuem bot�es relacionados no painel de a��es
		/*subjCommand.registerCommand(detalheCommand);*/
		
		jpOpEDadosCli.add(jpAcoes, BorderLayout.WEST);

		JPanel jpDadosVend = new JPanel();
		jpDadosVend.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Informa��es do Vendedor",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(47, 79,
						79)));
		jpDadosVend.setBackground(Color.WHITE);
		jpOpEDadosCli.add(jpDadosVend, BorderLayout.CENTER);
		jpDadosVend
				.setLayout(new MigLayout("", "[right][]", "[][][][][][][]"));

		JLabel lblClienteSelecionado = new JLabel("Vendedor selecionado:");
		jpDadosVend.add(lblClienteSelecionado);

		JLabel lblClienteSelecionadoValor = new JLabel("--");
		jpDadosVend.add(lblClienteSelecionadoValor, "cell 1 0,wrap");

		PIVSellerInfoFunc getInfoVend = new PIVSellerInfoFunc( lblClienteSelecionadoValor );
		controller.setgerInfoVendedor(getInfoVend);
		
		JPanel jpPrincCentro_centro = new JPanel();
		jpPrincCentro_centro.setBackground(Color.WHITE);
		jpPrincCentro_centro.setLayout(new BorderLayout(0, 0));

		jpPrincCentro_centro.add(tpCentral, BorderLayout.CENTER);

		// objeto para representar o menu dentro dos n�veis
		/*PIVBusRelPopUpMenu popMenuPainel = new PIVBusRelPopUpMenu(null,
				this.metasCommand, null, null, this.calcCommand,
				this.internetCommand, this.calendCommand, null,
				this.mapaCommand, null , null , this.telCommand , null);*/

		/*jpInicio.setLayout(new FlowLayout(FlowLayout.LEADING));*/
		
		JScrollPane scrollPaneInicio = new JScrollPane();
		scrollPaneInicio.getViewport().setBackground(Color.WHITE);
		
		jpInicio.setLayout(new MigLayout("","",""));
		
		jpInicio.setBackground(new Color(255, 255, 255));
		jpInicio.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		scrollPaneInicio.setViewportView(jpInicio);
		
		tpCentral.addTab("Inicio", new ImageIcon(
				PIVSellerMngtCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/home_16x16.png")), scrollPaneInicio, null);
		tpCentral.getComponentAt(0).addMouseListener(new MouseAdapter() {
			
		});

		jpVendedores.setController(controller);
		/*jpSuspect.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));*/
		jpVendedores.setBackground(new Color(255, 255, 255));
		jpVendedores.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		/*jpVendedores.addMouseListener(new PIVBusRelClickPopUpMenuListener(
				popMenuPainel));*/
				
		tpCentral.addTab( "Vendedores" , null /*new ImageIcon(
				PIVSellerMngtCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-0_16x16.png"))*/, scrollPaneVendedores, null);

		JPanel jpDireita = new JPanel();
		jpDireita.setLayout(new MigLayout("", "[]", "[grow]"));
		jpDireita.setBackground(Color.WHITE);
		/*jpDireita.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		// jpDireita.setPreferredSize(new Dimension(50, 0));

		jpPrincCentro_centro.add(jpDireita, BorderLayout.EAST);

		lblTemp = new JLabel(/* "Temp" */);
		lblTemp.setHorizontalAlignment(SwingConstants.CENTER);
		lblTemp.setIcon(new ImageIcon(PIVSellerMngtCFView.class
				.getResource("/images/menu_principal/relogio0_64x64.png")));
		lblTemp.setAlignmentX(Component.CENTER_ALIGNMENT);
		jpDireita.add(lblTemp,"spany ,north,growy");

		jpClientesEstrela.setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
		jpClientesEstrela.setBackground(Color.WHITE);
		jpClientesEstrela.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jpMatrizBCG.add(jpClientesEstrela);
		
		jpClientesQuest.setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
		jpClientesQuest.setBackground(Color.WHITE);
		jpClientesQuest.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jpMatrizBCG.add(jpClientesQuest);
				
		jpClientesVacaLeit.setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
		jpClientesVacaLeit.setBackground(Color.WHITE);
		jpClientesVacaLeit.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jpMatrizBCG.add(jpClientesVacaLeit);
		
		jpClientesAbacaxi.setLayout(new PIVCustomFlowLayout(PIVCustomFlowLayout.LEFT));
		jpClientesAbacaxi.setBackground(Color.WHITE);
		jpClientesAbacaxi.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jpMatrizBCG.add(jpClientesAbacaxi);
		
		PIVSellerMngtBCGMatrixController matrizBcgController = new PIVSellerMngtBCGMatrixController(
				subjCommand, jpClientesEstrela, jpClientesQuest,
				jpClientesVacaLeit, jpClientesAbacaxi);
		
		jpMatrizBCG.setBackground(new Color(255, 255, 255));
		jpMatrizBCG.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		
		jpMatrizBCG.setLayout( new GridLayout( 2 , 2 , 25 , 25 ) );
		
		tpCentral.addTab( "Matriz BCG" , null /*new ImageIcon(
				PIVSellerMngtCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-0_16x16.png"))*/, jpMatrizBCG, null);
		
		/*
		 * JLabel lblTemp_1 = new JLabel("Temp");
		 * lblTemp_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		 * jpTempo.add(lblTemp_1);
		 * 
		 * JLabel lblTemp_2 = new JLabel("Temp");
		 * lblTemp_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		 * jpTempo.add(lblTemp_2);
		 */

		JButton btRecarrega = new JButton("Recarregar");
		btRecarrega.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.carregar();
			}
		});
		jpDireita.add(btRecarrega, "dock south");
		
		jpClientesPNivel.add(jpPrincCentro_centro, BorderLayout.CENTER);

		super.getContentPane().add(jpClientesPNivel, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/*setSize(800, 600);*/
		
		pack( );

		controller.carregar();
		matrizBcgController.carregar();

		inicializarTemporizador();
		
		setUniqueInstance(true);
		
		MDIFrame.add(this);

	}

	public JLabel getLblTemp() {
		return lblTemp;
	}

	public Integer getNivelSelecionado() {
		return this.tpCentral.getSelectedIndex();
	}

	public JLabel getLblLixo() {
		return this.lblLixo;
	}

	private void inicializarTemporizador() {
		Timer timer = new Timer(60000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.carregar();
			}
		});
		timer.start();
	}

}