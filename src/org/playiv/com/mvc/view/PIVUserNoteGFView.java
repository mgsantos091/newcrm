package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVUserNoteDFController;
import org.playiv.com.mvc.controller.PIVUserNoteGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.swing.general.PIVUserNotePanel;


/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVUserNoteGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private PIVClientModel cliente;
	private PIVUserNoteGFController controller;
	
	private PIVBusRelMainCFController organizaCliente;
	
	private PIVUserNotePanel userNotePanel;

	public PIVUserNoteGFView(PIVUserNoteGFController controller, PIVClientModel cliente) {
		super.setTitle("Registro de Observa��es - Vis�o Geral");
		super.setFrameIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_16x16.png")));
		try {
			this.cliente = cliente;
			this.controller = controller;
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public PIVUserNoteGFView(PIVUserNoteGFController pivUserNoteGFController,
			PIVClientModel cliente2, PIVBusRelMainCFController organizaCliente) {
		this(pivUserNoteGFController,cliente2);
		this.organizaCliente = organizaCliente;
	}

	public void reloadData() {
		this.userNotePanel.getGrid().reloadData();
	}

	private void jbInit() throws Exception {
		this.userNotePanel = new PIVUserNotePanel( controller , false );
		setLayout(new BorderLayout());
		this.add(this.userNotePanel,BorderLayout.CENTER);
		setSize(500, 300);
	}

	public GridControl getGrid() {
		return this.userNotePanel.getGrid();
	}

}
