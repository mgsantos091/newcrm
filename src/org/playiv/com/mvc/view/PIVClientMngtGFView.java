package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.message.send.java.GridParams;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.util.client.ClientUtils;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVClientMngtDFController;
import org.playiv.com.mvc.controller.PIVClientMngtGFController;
import org.playiv.com.mvc.model.PIVClientModel;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVClientMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton1 = new ExportButton();

	private final IntegerColumn colId = new IntegerColumn();

	private final TextColumn colNomeRed = new TextColumn();
	private final TextColumn colNomeComp = new TextColumn();
	private final TextColumn colSite = new TextColumn();
	private final TextColumn colEstado = new TextColumn();
	private final TextColumn colCidade = new TextColumn();

	public PIVClientMngtGFView(PIVClientMngtGFController controller) {
		try {
			super.setTitle("Clientes - Vis�o Geral");
			super.setFrameIcon(new ImageIcon(PIVClientMngtGFView.class
					.getResource("/images/cadastros/cliente/agencia_16x16.png")));
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVClientModel");
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		editButton.addActionListener(new EmpGridFrame_editButton_actionAdapter(
				this));
		exportButton1.setText("exportButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(editButton, null);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(deleteButton, null);
		buttonsPanel.add(exportButton1, null);

		colId.setColumnName("id");
		colId.setHeaderColumnName("Cod");
		colId.setMaxCharacters(5);
		colId.setPreferredWidth(25);
		colId.setColumnSortable(true);
		grid.getColumnContainer().add(colId);

		colNomeRed.setColumnName("nomefantasia");
		colNomeRed.setHeaderColumnName("Nome Reduzido");
		colNomeRed.setMaxCharacters(80);
		colNomeRed.setPreferredWidth(150);
		colNomeRed.setColumnFilterable(true);
		colNomeRed.setColumnSortable(true);
		grid.getColumnContainer().add(colNomeRed);

		colNomeComp.setColumnName("razaosocial");
		colNomeComp.setHeaderColumnName("Nome Completo");
		colNomeComp.setMaxCharacters(80);
		colNomeComp.setPreferredWidth(225);
		colNomeComp.setColumnFilterable(true);
		grid.getColumnContainer().add(colNomeComp);

		colSite.setColumnName("website");
		colSite.setHeaderColumnName("Site");
		colSite.setMaxCharacters(80);
		colSite.setPreferredWidth(100);
		grid.getColumnContainer().add(colSite);

		colEstado.setColumnName("endereco.estado");
		colEstado.setHeaderColumnName("Estado");
		colEstado.setMaxCharacters(80);
		colEstado.setPreferredWidth(25);
		grid.getColumnContainer().add(colEstado);

		colCidade.setColumnName("endereco.cidade");
		colCidade.setHeaderColumnName("Cidade");
		colCidade.setMaxCharacters(80);
		colCidade.setPreferredWidth(80);
		grid.getColumnContainer().add(colCidade);

		setSize(800, 600);

		setUniqueInstance(true);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVClientMngtDFController(this, null);
	}

	void editButton_actionPerformed(ActionEvent e) {
		PIVClientModel client = (PIVClientModel) grid.getVOListTableModel()
				.getObjectForRow(grid.getSelectedRow());
		int pk = client.getId();
		new PIVClientMngtDFController(this, pk);
	}

	public GridControl getGrid() {
		return grid;
	}

}

class EmpGridFrame_insertButton_actionAdapter implements
		java.awt.event.ActionListener {
	PIVClientMngtGFView adaptee;

	EmpGridFrame_insertButton_actionAdapter(PIVClientMngtGFView adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.insertButton_actionPerformed(e);
	}
}

class EmpGridFrame_editButton_actionAdapter implements
		java.awt.event.ActionListener {
	PIVClientMngtGFView adaptee;

	EmpGridFrame_editButton_actionAdapter(PIVClientMngtGFView adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.editButton_actionPerformed(e);
	}
}