package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CodLookupControl;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.LabelControl;
import org.openswing.swing.client.NumericControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.client.TextControl;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.mvc.controller.PIVCampaignLUFController;
import org.playiv.com.mvc.controller.PIVSalesLevelLUFController;
import org.playiv.com.mvc.controller.PIVSalesMoreInfoDFController;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.swing.general.PIVPayCondPanel;
import org.playiv.com.swing.general.PIVSalesNotePanel;

public class PIVSalesMoreInfoDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	
	private SaveButton saveButton;
	private EditButton editButton;
	private ReloadButton reloadButton;
	
	/*private JTabbedPane tbEndereco = new JTabbedPane(JTabbedPane.TOP);*/

	/*private JSplitPane splitPane = new JSplitPane();*/
	
/*	public static void main(String args[]) {
		JFrame frame = new JFrame();
		frame.setSize(new Dimension(400,400));
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new PIVSalesMoreInfoDFView(null,null));
		frame.pack();
		frame.setVisible(true);
	}
*/	
	public PIVSalesMoreInfoDFView( PIVSalesMoreInfoDFController controller , PIVSalesMainModel vendasPai ) {

		setTitle("Proposta - Vis�o Detalhada");
		setFrameIcon(new ImageIcon(PIVSalesMoreInfoDFView.class.getResource("/images/gerenciamento_relcomercial/logo/gerrelcomercial-icone_16x16.png")));
		
		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					PIVSalesMoreInfoDFView.this.closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		getInputMap().put(esc, "esc");
		getActionMap().put("esc", actionESC);
		
		/*splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(150);*/

		/*super.setPreferredSize(new Dimension(800, 600));*/
		getContentPane().setLayout(new MigLayout("", "[grow,fill][grow,fill]", "[grow]"));

		jpPrincipal = new Form( );
		
		/*splitPane.add(jpPrincipal, JSplitPane.TOP);*/
		
		jpPrincipal
				.setLayout(new MigLayout("", "[right][fill][grow]", "[][][]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVSalesMainModel");
		jpPrincipal.setFormController(controller);
		/*codCampanha.setRequired(true);*/
		
		JPanel jpBotoes = new JPanel();
		FlowLayout fl_jpBotoes = (FlowLayout) jpBotoes.getLayout();
		fl_jpBotoes.setAlignment(FlowLayout.LEFT);
		jpBotoes.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		jpPrincipal.add(jpBotoes, "h 50! , growx , spanx , bottom , north");
		
		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);
		jpPrincipal.setReloadButton(reloadButton);
		
		editButton = new EditButton();
		jpBotoes.add(editButton);
				
		jpPrincipal.setEditButton(editButton);
				
		saveButton = new SaveButton();
		jpBotoes.add(saveButton);
		jpPrincipal.setSaveButton(saveButton);
		
		PIVPayCondPanel condicaoPagamentoPainel = new PIVPayCondPanel( );
		
		jpPrincipal.add(condicaoPagamentoPainel,"spanx , dock east , grow");
		
		LabelControl lblComprador = new LabelControl("Comprador:");
		jpPrincipal.add(lblComprador, "");
		
		TextControl txtComprador = new TextControl( );
		txtComprador.setAttributeName("comprador");
		jpPrincipal.add(txtComprador,"wrap");
		lblComprador.setLabelFor(txtComprador);
		
		LabelControl lblValFrete = new LabelControl("Valor de frete:");
		jpPrincipal.add(lblValFrete, "");
		
		NumericControl txtValorFrete = new NumericControl( );
		txtValorFrete.setAttributeName("valorfrete");
		txtValorFrete.setDecimals(2);
		jpPrincipal.add(txtValorFrete,"wrap");
		lblValFrete.setLabelFor(txtValorFrete);
		
		LabelControl lblValDespesas = new LabelControl("Valor de despesas:");
		jpPrincipal.add(lblValDespesas, "");
		
		NumericControl txtValorDespesas = new NumericControl( );
		txtValorDespesas.setAttributeName("valordespesa");
		txtValorDespesas.setDecimals(2);
		jpPrincipal.add(txtValorDespesas,"wrap");
		lblValDespesas.setLabelFor(txtValorDespesas);

		/*splitPane.add(anotacaoPainel, JSplitPane.BOTTOM);*/
		
		LabelControl lblCodCategoria = new LabelControl("Id Campanha:");
		jpPrincipal.add(lblCodCategoria, "");
		
		CodLookupControl codCampanha = new CodLookupControl();
		codCampanha.setLookupController(new PIVCampaignLUFController());
		codCampanha.setLookupButtonVisible(true);
		codCampanha.setAttributeName("campanha.id");
		codCampanha.setCanCopy(true);
		codCampanha.setMaxCharacters(5);
		jpPrincipal.add(codCampanha, "");
		
		TextControl txtCampanha = new TextControl();
		txtCampanha.setEnabledOnEdit(false);
		txtCampanha.setEnabledOnInsert(false);
		txtCampanha.setAttributeName("campanha.nome_campanha");
		txtCampanha.setMaxCharacters(255);
		txtCampanha.setTrimText(true);
		jpPrincipal.add(txtCampanha,"wrap");
		
		LabelControl lblCodEstagioVenda = new LabelControl("Est�gio de venda:");
		jpPrincipal.add(lblCodEstagioVenda);
		
		CodLookupControl codEstagioVenda = new CodLookupControl();
		codEstagioVenda.setLookupController(new PIVSalesLevelLUFController());
		codEstagioVenda.setLookupButtonVisible(true);
		codEstagioVenda.setAttributeName("estagio.id");
		codEstagioVenda.setCanCopy(true);
		codEstagioVenda.setMaxCharacters(5);
		jpPrincipal.add(codEstagioVenda, "");
		
		TextControl txtEstagio = new TextControl();
		txtEstagio.setEnabledOnEdit(false);
		txtEstagio.setEnabledOnInsert(false);
		txtEstagio.setAttributeName("estagio.estagio");
		txtEstagio.setMaxCharacters(255);
		txtEstagio.setTrimText(true);
		
		jpPrincipal.add(txtEstagio,"growx,wrap");
		
		PIVSalesNotePanel anotacaoPainel = new PIVSalesNotePanel( vendasPai );
		
		jpPrincipal.add(anotacaoPainel,"spanx ,aligny bottom,grow");
		
		/*getContentPane().add(splitPane, "grow , span");*/
		
		getContentPane().add(jpPrincipal, "grow , span");

		pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}

}