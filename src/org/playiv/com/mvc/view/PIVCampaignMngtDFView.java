package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVCampaignMngtDFController;
import org.playiv.com.swing.general.PIVCampaignPanel;

public class PIVCampaignMngtDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	private PIVCampaignPanel jpCampanha;
	private SaveButton saveButton;
	private EditButton editButton;
	private InsertButton insertButton;
	private ReloadButton reloadButton;
	private PIVCampaignMngtDFController campanhaController;
	
	public PIVCampaignMngtDFView(PIVCampaignMngtDFController controller, Integer pk,
			PIVDao pdao) {

		super.setTitle("Campanhas - Vis�o Detalhada");
		super.setFrameIcon(new ImageIcon(PIVCampaignMngtDFView.class.getResource("/images/cadastros/campanha/campanha_16x16.png")));
		
		this.campanhaController = controller;

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		super.getInputMap().put(esc, "esc");
		super.getActionMap().put("esc", actionESC);

		super.setPreferredSize(new Dimension(600, 400));
		getContentPane().setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");

		jpPrincipal = new Form();

		jpPrincipal
				.setLayout(new MigLayout("", "[grow]", "[grow,fill]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVCampaignModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setInsertButton(insertButton);
		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setCopyButton(copyButton);
		jpPrincipal.setReloadButton(reloadButton);

		jpCampanha = null;
		try {
			jpCampanha = new PIVCampaignPanel(this.campanhaController);
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}

		jpPrincipal.add(jpCampanha, "grow");

		super.add(jpPrincipal, "grow , span");

		super.pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}
	
	public Form getForm( ){
		return this.jpPrincipal;
	}

}