package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVTelContactGFController;


/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVTelContactGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	/*SaveButton saveButton = new SaveButton();
	EditButton editButton = new EditButton();*/
	private ReloadButton reloadButton = new ReloadButton();

//	private IntegerColumn colIdContato = new IntegerColumn();
	private TextColumn colNomeContato = new TextColumn();
	private TextColumn colTipo = new TextColumn();
	private TextColumn colFuncaoContato = new TextColumn();
	private TextColumn colNumeroContato = new TextColumn();
//	private TextColumn colCelContato = new TextColumn();

	private PIVTelContactGFController controller;
	
/*	private final IntegerColumn colId = new IntegerColumn();
	private final TextColumn colNome = new TextColumn();
	private final TextColumn colUsuario = new TextColumn();*/

	public PIVTelContactGFView(PIVTelContactGFController controller) {
		super.setTitle("Liga��o - selecione o contato (clique duas vezes para ligar)");
		super.setFrameIcon(new ImageIcon(PIVTelContactGFView.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_16x16.png")));
		try {
			jbInit();
			setSize(500,300);
			grid.setController(controller);
			grid.setGridDataLocator(controller);
			this.controller = controller;
			MDIFrame.add(this);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
/*		grid.setDeleteButton(null);
		grid.setExportButton(null);
		grid.setFilterButton(null);
		grid.setInsertButton(null);
		grid.setEditButton(editButton);
		grid.setSaveButton(saveButton);*/
		grid.setReloadButton(reloadButton);
		grid.setEditOnSingleRow(false);

//		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVContactModel");
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVTelToCallModel");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(reloadButton, null);
		
		/*
		buttonsPanel.add(saveButton,null);
		buttonsPanel.add(editButton,null);*/
		
		/*private IntegerColumn colIdContato = new IntegerColumn();
		private TextColumn colNomeContato = new TextColumn();
		private TextColumn colFuncaoContato = new TextColumn();
		private TextColumn colEmailContato = new TextColumn();*/
		
//		colIdContato.setColumnName("id");
//		grid.getColumnContainer().add(colIdContato);
		
		colNomeContato.setColumnName("nome");
		grid.getColumnContainer().add(colNomeContato);
		
		colTipo.setColumnName("tipo");
		grid.getColumnContainer().add(colTipo);
		
		colFuncaoContato.setColumnName("funcao");
		grid.getColumnContainer().add(colFuncaoContato);
		
		colNumeroContato.setColumnName("numero");
		grid.getColumnContainer().add(colNumeroContato);

//		colCelContato.setColumnName("celular");
//		grid.getColumnContainer().add(colCelContato);

}

	public GridControl getGrid() {
		return grid;
	}

}
