package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.FilterButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.CurrencyColumn;
import org.openswing.swing.table.columns.client.DateColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVProdMngtDFController;
import org.playiv.com.mvc.controller.PIVProdMngtGFController;

/**
 * Title: OpenSwing Framework Description: Grid Frame Copyright: Copyright (C)
 * 2006 Mauro Carniel
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVProdMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();

	private FlowLayout flowLayout1 = new FlowLayout();

	private JPanel buttonsPanel = new JPanel();

	private ReloadButton reloadButton = new ReloadButton();
	private InsertButton insertButton = new InsertButton();
	
	private DeleteButton deleteButton = new DeleteButton();
	private ExportButton exportButton1 = new ExportButton();
	private FilterButton filterButton1 = new FilterButton();
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colNome = new TextColumn();
	private TextColumn colDesc = new TextColumn();
	private CurrencyColumn colValCompra = new CurrencyColumn();
	private CurrencyColumn colValVenda = new CurrencyColumn();
	private TextColumn colCategoriaNome = new TextColumn();
	private DateColumn colDataRegistro = new DateColumn();

	public PIVProdMngtGFView(PIVProdMngtGFController controller) {
		super.setTitle("Produtos - Vis�o Geral");
		super.setFrameIcon(new ImageIcon(PIVProdMngtGFView.class.getResource("/images/cadastros/produto/produto_16x16.png")));
		try {
			jbInit();
			grid.setController(controller);
			grid.setGridDataLocator(controller);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
		}
		setUniqueInstance(true);
	}

	public void reloadData() {
		grid.reloadData();
	}

	private void jbInit() throws Exception {
		grid.setAnchorLastColumn(true);
		buttonsPanel.setLayout(flowLayout1);
		flowLayout1.setAlignment(FlowLayout.LEFT);
		grid.setDeleteButton(deleteButton);
		grid.setExportButton(exportButton1);
		grid.setFilterButton(filterButton1);
		grid.setInsertButton(null);
		grid.setReloadButton(reloadButton);
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVProductModel");
		insertButton.setText("insertButton1");
		insertButton
				.addActionListener(new EmpGridFrame_insertButton_actionAdapter(
						this));
		exportButton1.setText("exportButton1");
		filterButton1.setText("filterButton1");
		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.add(insertButton, null);
		buttonsPanel.add(reloadButton, null);
		buttonsPanel.add(deleteButton, null);
		buttonsPanel.add(exportButton1, null);
		buttonsPanel.add(filterButton1, null);

		/*private IntegerColumn colId = new IntegerColumn();
		private TextColumn colNome = new TextColumn();
		private TextColumn colDesc = new TextColumn();
		private NumericControl colValCompra = new NumericControl();
		private NumericControl colValVenda = new NumericControl();
		private Timestamp dataregistro = PIVUtil.getCurrentTime();*/

		colId.setColumnName("id");
		colId.setMaxCharacters(5);
		grid.getColumnContainer().add(colId);

		colNome.setColumnName("nome");
		colNome.setMaxCharacters(255);
		grid.getColumnContainer().add(colNome);
		
		colDesc.setColumnName("descricao");
		colDesc.setMaxCharacters(255);
		grid.getColumnContainer().add(colDesc);

		colValCompra.setColumnName("valcompra");
		colValCompra.setDecimals(2);
		grid.getColumnContainer().add(colValCompra);
		
		colValVenda.setColumnName("valvenda");
		colValVenda.setDecimals(2);
		grid.getColumnContainer().add(colValVenda);
		
		colCategoriaNome.setColumnName("categoria.nome");
		grid.getColumnContainer().add(colCategoriaNome);
		
		colDataRegistro.setColumnName("dataregistro");
		grid.getColumnContainer().add(colDataRegistro);
		
		setSize(800, 600);
	}

	void insertButton_actionPerformed(ActionEvent e) {
		new PIVProdMngtDFController(this, null);
	}

	public GridControl getGrid() {
		return grid;
	}

	class EmpGridFrame_insertButton_actionAdapter implements
			java.awt.event.ActionListener {
		PIVProdMngtGFView adaptee;

		EmpGridFrame_insertButton_actionAdapter(PIVProdMngtGFView adaptee) {
			this.adaptee = adaptee;
		}

		public void actionPerformed(ActionEvent e) {
			adaptee.insertButton_actionPerformed(e);
		}
	}
}
