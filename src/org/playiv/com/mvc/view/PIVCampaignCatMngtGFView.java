package org.playiv.com.mvc.view;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.DateTimeColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.playiv.com.mvc.controller.PIVCampaignCatMngtGFController;

public class PIVCampaignCatMngtGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private GridControl grid = new GridControl();
	
	private JPanel buttonsPanel = new JPanel();
	
	private InsertButton insertButton = new InsertButton();
	private EditButton editButton = new EditButton();
	private DeleteButton deleteButton = new DeleteButton();
	private SaveButton saveButton = new SaveButton();
	private CopyButton copyButton = new CopyButton();
	private ReloadButton reloadButton = new ReloadButton();
	private ExportButton exportButton = new ExportButton();
	
	private IntegerColumn colId = new IntegerColumn();
	private TextColumn colNome = new TextColumn();
	private DateTimeColumn colDataRegistro = new DateTimeColumn();

	public PIVCampaignCatMngtGFView(PIVCampaignCatMngtGFController controller) {
		super.setTitle("Categorias de Campanhas - Vis�o Geral");
		super.setFrameIcon(new ImageIcon(PIVCampaignCatMngtGFView.class.getResource("/images/cadastros/campanha/campanha_16x16.png")));
		initialize();
		super.setSize(600,400);
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		setUniqueInstance(true);
	}

	private void initialize() {

		grid.setCopyButton(copyButton);
	    grid.setDeleteButton(deleteButton);
	    grid.setEditButton(editButton);
	    grid.setExportButton(exportButton);
	    grid.setInsertButton(insertButton);
	    grid.setReloadButton(reloadButton);
	    grid.setSaveButton(saveButton);
	    /*grid.setRowHeight(50);*/
		
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVCampaignCatModel");
		
		colId.setColumnFilterable(true);
		colId.setColumnName("id");
		colId.setColumnSortable(true);
		colId.setEditableOnEdit(false);
		colId.setEditableOnInsert(false);
		colId.setColumnRequired(false);
		
		colNome.setColumnSortable(true);
		colNome.setColumnFilterable(true);
		colNome.setColumnRequired(true);
		colNome.setColumnName("nome");
		colNome.setEditableOnEdit(true);
		colNome.setEditableOnInsert(true);
		
		colDataRegistro.setColumnSortable(false);
		colDataRegistro.setColumnFilterable(true);
		colDataRegistro.setColumnRequired(false);
		colDataRegistro.setColumnName("data_registro");
		colDataRegistro.setEditableOnEdit(false);
		colDataRegistro.setEditableOnInsert(false);
		
		this.getContentPane().add(grid, BorderLayout.CENTER);
	    this.getContentPane().add(buttonsPanel, BorderLayout.NORTH);	    
	    buttonsPanel.add(insertButton, null);
	    buttonsPanel.add(copyButton, null);
	    buttonsPanel.add(editButton, null);
	    buttonsPanel.add(reloadButton, null);
	    buttonsPanel.add(saveButton, null);
	    buttonsPanel.add(exportButton, null);
	    buttonsPanel.add(deleteButton, null);
	    
	    grid.getColumnContainer().add(colId);
	    grid.getColumnContainer().add(colNome);
	    grid.getColumnContainer().add(colDataRegistro);
	    
	}

	public GridControl getGrid() {
		return this.grid;
	}

	public void reloadData() {
		this.grid.reloadData();
	}

}