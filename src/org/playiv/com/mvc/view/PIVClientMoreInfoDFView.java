package org.playiv.com.mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVClientMoreInfoDFController;
import org.playiv.com.swing.general.PIVClientDescPanel;
import org.playiv.com.swing.general.PIVClientMoreInfoPanel;
import org.playiv.com.swing.general.PIVDeliveryAddressPanel;

public class PIVClientMoreInfoDFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private Form jpPrincipal;
	
	private PIVClientMoreInfoPanel jpInfoAdicional;
	private PIVDeliveryAddressPanel jpEnderecoEntrega;
	private PIVClientDescPanel jpDescricao;
	
	private SaveButton saveButton;
	private EditButton editButton;
	private ReloadButton reloadButton;
	
	private PIVClientMoreInfoDFController controller;
	
	private JTabbedPane tbPrincipal = new JTabbedPane(JTabbedPane.TOP);
	
	public PIVClientMoreInfoDFView(PIVClientMoreInfoDFController controller) {

		super.setTitle("Informa��es adicionais (Cliente) - Vis�o Detalhada");
		/*super.setFrameIcon(new ImageIcon(PIVCampaignMngtDFView.class.getResource("/images/cadastros/cliente/agencia_16x16.png")));*/
		
		this.controller = controller;

		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeFrame();
				} catch (PropertyVetoException ex) {
				}
			}
		};
		
		super.getInputMap().put(esc, "esc");
		super.getActionMap().put("esc", actionESC);

		super.setPreferredSize(new Dimension(600, 400));
		getContentPane().setLayout(new MigLayout("", "[grow]", "[grow]"));

		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		super.add(jpBotoes, "h 50! , dock north , growx");
		
		jpPrincipal = new Form();

		jpPrincipal
				.setLayout(new MigLayout("", "[grow]", "[grow,fill]"));
		jpPrincipal.setVOClassName("org.playiv.com.mvc.model.PIVClientMoreInfoModel");
		jpPrincipal.setFormController(controller);

		jpPrincipal.setEditButton(editButton);
		jpPrincipal.setReloadButton(reloadButton);
		jpPrincipal.setSaveButton(saveButton);
		jpPrincipal.setReloadButton(reloadButton);

		jpPrincipal.add(tbPrincipal);
				
		jpInfoAdicional = null;
		try {
			jpInfoAdicional = new PIVClientMoreInfoPanel( );
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		
		jpEnderecoEntrega = null;
		jpEnderecoEntrega = new PIVDeliveryAddressPanel( this.controller.getCliente() );
		
		jpDescricao = null;
		try {
			jpDescricao = new PIVClientDescPanel( );
		} catch (ParseException e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		
		/*jpPrincipal.add(jpInfoAdicional, "grow");*/
		
		tbPrincipal.addTab("+ Informa��es", jpInfoAdicional);
		tbPrincipal.addTab("Endere�o de Entrega", jpEnderecoEntrega);
		tbPrincipal.addTab("Descri��o", jpDescricao);
		
		super.add(jpPrincipal, "grow , span");

		super.pack();

		setUniqueInstance(true);
		
	}

	public Form getPanel() {
		return this.jpPrincipal;
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}
	
	public Form getForm( ){
		return this.jpPrincipal;
	}

}