package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.controller.PIVBulsRelMainDetailCFController;
import org.playiv.com.mvc.controller.PIVClientContactGFController;
import org.playiv.com.mvc.controller.PIVMailContactGFController;
import org.playiv.com.mvc.controller.PIVProductSaleGFController;
import org.playiv.com.mvc.controller.PIVUserNoteDFController;
import org.playiv.com.mvc.controller.PIVUserNoteGFController;
import org.playiv.com.mvc.controller.PIVVoipRecordGFController;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.model.PIVUserNoteModel;
import org.playiv.com.swing.general.PIVContactPanel;
import org.playiv.com.swing.general.PIVFileExplorerInternalFrameAdapter;
import org.playiv.com.swing.general.PIVHistoryOfActionsTableFunc;
import org.playiv.com.swing.general.PIVProductSalesPanel;
import org.playiv.com.swing.general.PIVUserNotePanel;

import javax.swing.JCheckBox;

public class PIVBusRelMainDetailCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JTextField txTag1;
	private JTextField txTag2;
	private JTextField txTag3;
	private JTextField txTag4;

	private JTextField txVendasDesconto;
	private JTextField txValDescontoTotal;

	private JLabel lbNomeEmpresa;
	private JLabel lbCodEmpresa;
	private JLabel lbSiteEmpresa;

	private JLabel lbEndRua;
	private JLabel lbEndBairro;
	private JLabel lbEndCidade;
	private JLabel lbEndEstado;
	private JLabel lbEndCep;
	private JLabel lbEndPais;

	private PIVContactPanel jpContato;
	private PIVProductSalesPanel jpVendas;

	private PIVHistoryOfActionsTableFunc tbl_HistoryOfActions;

	private PIVClientModel cliente;
	private PIVSalesMainModel venda;
	
	private PIVBulsRelMainDetailCFController controller;
	
	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc.getInstance();

	private PIVUserNoteGFController anotacaoController;

	private PIVUserNotePanel anotacaoPainel;
	
	private PIVUserNoteDFController anotacaoControllerAtiva;
	private JLabel lbContatos;
	
	private Boolean playAuto = false;
	
	public PIVBusRelMainDetailCFView( PIVBulsRelMainDetailCFController controller ) {

		this.controller = controller;
		
		this.cliente = controller.getCliente();
		this.venda = controller.getVenda();
		
		super.setTitle("Propostas - Vis�o Detalhada - Proposta Comercial: N\u00B0 " + controller.getVenda().getId() );
		super.setFrameIcon(new ImageIcon(
				PIVBusRelMainDetailCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_16x16.png")));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 854, 717);
		contentPane = new JPanel();
		/*contentPane.setForeground(Color.LIGHT_GRAY);*/
		/*contentPane.setBackground(Color.LIGHT_GRAY);*/
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel jpCentro = new JPanel();
		/*jpCentro.setBackground(Color.WHITE);*/
		contentPane.add(jpCentro, BorderLayout.CENTER);
		jpCentro.setLayout(new MigLayout("", "[450px,grow,fill]", "[][175.00px][][150px,grow,fill][]"));
		
		JPanel jp_NomeEmpresa = new JPanel();
		jp_NomeEmpresa.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_NomeEmpresa.setBackground(Color.WHITE);
		jpCentro.add(jp_NomeEmpresa, "cell 0 0,grow");
		jp_NomeEmpresa.setLayout(new MigLayout("", "[50px][300px,grow,fill][60px]", "[][]"));
		
		JLabel lblNewLabel_1 = new JLabel();
		lblNewLabel_1.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/agencia_48x48.png")));
		jp_NomeEmpresa.add(lblNewLabel_1, "cell 0 0 1 2,grow");
		
		lbNomeEmpresa = new JLabel();
		lbNomeEmpresa.setName("");
		lbNomeEmpresa.setHorizontalAlignment(SwingConstants.CENTER);
		lbNomeEmpresa.setFont(new Font("Arial", Font.BOLD, 14));
		jp_NomeEmpresa.add(lbNomeEmpresa, "cell 1 0,growx,aligny center");
		
		lbCodEmpresa = new JLabel();
		lbCodEmpresa.setFont(new Font("Arial", Font.BOLD, 14));
		jp_NomeEmpresa.add(lbCodEmpresa, "cell 2 0 1 2,growx,aligny center");
		
		lbSiteEmpresa = new JLabel();
		jp_NomeEmpresa.add(lbSiteEmpresa, "cell 1 1");
		lbSiteEmpresa.setForeground(Color.BLUE);
		lbSiteEmpresa.setHorizontalAlignment(SwingConstants.CENTER);
		lbSiteEmpresa.setFont(new Font("Arial", Font.BOLD, 12));
		
		JPanel jp_Contato = new JPanel();
		jp_Contato.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Contato.setBackground(Color.WHITE);
		jpCentro.add(jp_Contato, "cell 0 1,grow");
		
		/*jp_Contato.setLayout(new MigLayout("", "[430px,grow,fill]", "[50px:50px:50px][100px:100px:100px]"));*/
		jp_Contato.setLayout(new MigLayout());
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		jp_Contato.add(panel, "north , growx");
		/*panel.setLayout(new MigLayout("", "[46px][87px][][173px][173px]", "[35px:35px:35px]"));*/
		panel.setLayout(new MigLayout("", "[][grow,fill][][][]", "[fill]"));
		
		JLabel lblNewLabel_8 = new JLabel();
		/*lblNewLabel_8.setHorizontalAlignment(SwingConstants.TRAILING);*/
		lblNewLabel_8.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/contato_32x32.png")));
		panel.add(lblNewLabel_8, "cell 0 0,grow");
		
		JPanel jpContatosTitulo = new JPanel( );
		jpContatosTitulo.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpContatosTitulo.setBackground(new Color(112, 128, 144));
		
		lbContatos = new JLabel("CONTATOS");
		lbContatos.setForeground(Color.WHITE);
		lbContatos.setHorizontalAlignment(SwingConstants.CENTER);
		lbContatos.setFont(new Font("Arial", Font.BOLD, 12));
		
		jpContatosTitulo.add(lbContatos,BorderLayout.CENTER);
		
		panel.add(jpContatosTitulo, "cell 1 0");
		
		JButton button_4 = new JButton("");
		button_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new PIVVoipRecordGFController( cliente );
			}
		});
		button_4.setBackground(Color.WHITE);
		button_4.setBorder(BorderFactory.createEmptyBorder());
		button_4.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/telefone_32x32.png")));
		panel.add(button_4, "cell 3 0,alignx right,growy");
		
		JButton button_6 = new JButton("");
		button_6.setPreferredSize(new Dimension(35, 5));
		button_6.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/orcamento_32x32.png")));
		button_6.setBackground(Color.WHITE);
		button_6.setBorder(BorderFactory.createEmptyBorder());
		panel.add(button_6, "cell 2 0");
		
		JButton btEmail = new JButton();
		btEmail.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new PIVMailContactGFController(cliente,PIVBusRelMainDetailCFView.this.controller.getOrganizacliente());
			}
		});
		btEmail.setBackground(Color.WHITE);
		btEmail.setBorder(BorderFactory.createEmptyBorder());
		btEmail.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/email_32x32.png")));
		panel.add(btEmail, "cell 4 0,alignx left,growy");
		
		PIVClientContactGFController contatoController = null;
		contatoController = new PIVClientContactGFController(cliente.getId(),true);
		
		/*GridControl gridContato = jpContato.getGrid();
		gridContato.setMode(Consts.READONLY);
		gridContato.setAutoLoadData(true);*/
		
		jpContato = contatoController.getContatoPanel();
		/*jpContato.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		jpContato.setBackground(Color.WHITE);
		jpContato.getGrid().setMode(Consts.READONLY);
		jpContato.getGrid().setAutoLoadData(true);
		jpContato.getGrid().reloadData();
	
		/*JScrollPane scrollPaneContato = new JScrollPane();*/
		/*scrollPaneContato.setLayout(new BorderLayout());*/
		/*scrollPaneContato.getViewport().setBackground(Color.WHITE);
		scrollPaneContato.setViewportView(jpContato);*/
		
		/*table = new JTable();
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));*/
		
		/*jp_Contato.add(table, "cell 0 1,growx,aligny top");*/
		
		/*jp_Contato.add(gridContato, "cell 0 1,alignx center,grow");*/
		/*jp_Contato.add(scrollPaneContato, "cell 0 1,alignx center,grow");*/
		jp_Contato.add(jpContato, "dock center , spanx , grow");
		
		JPanel jp_Produto = new JPanel();
		jp_Produto.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Produto.setBackground(Color.WHITE);
		jpCentro.add(jp_Produto, "cell 0 3,grow");
		/*jp_Produto.setLayout(new MigLayout("", "[430px]", "[39px][80px,grow]"));*/
		jp_Produto.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));
				
		/*table_1 = new JTable();
		table_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));*/
		
		/*jp_Produto.add(vendasPainel, "cell 0 1,grow,fill,aligny top");*/
		
		/*JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		jp_Produto.add(panel_1, "north , ,grow");
		panel_1.setLayout(new MigLayout("", "[45px][57px][45px][14px][57px][69px][14px][55px]", "[39px]"));
		
		JLabel lblDesconto = new JLabel("Desconto:");
		lblDesconto.setFont(new Font("Arial", Font.BOLD, 12));
		panel_1.add(lblDesconto, "cell 1 0,alignx left,aligny center");
		
		txVendasDesconto = new JTextField();
		panel_1.add(txVendasDesconto, "cell 2 0,growx,aligny center");
		txVendasDesconto.setColumns(10);
		
		JLabel label = new JLabel("%");
		label.setFont(new Font("Arial", Font.BOLD, 12));
		panel_1.add(label, "cell 3 0,growx,aligny center");
		
		JLabel lblCondio = new JLabel("Condi\u00E7\u00E3o:");
		lblCondio.setFont(new Font("Arial", Font.BOLD, 12));
		panel_1.add(lblCondio, "cell 4 0,growx,aligny center");
		
		JComboBox cbCondPagamento = new JComboBox();
		panel_1.add(cbCondPagamento, "cell 5 0,growx,aligny center");
		
		JLabel label_1 = new JLabel("=");
		label_1.setFont(new Font("Arial", Font.BOLD, 12));
		panel_1.add(label_1, "cell 6 0,growx,aligny center");
		
		txValDescontoTotal = new JTextField();
		panel_1.add(txValDescontoTotal, "cell 7 0,growx,aligny center");
		txValDescontoTotal.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/produto_48x48.png")));
		panel_1.add(lblNewLabel_7, "cell 0 0,grow");*/
		
		PIVProductSaleGFController vendasController = null;
		try {
			vendasController = new PIVProductSaleGFController( venda , cliente );
			jpVendas = new PIVProductSalesPanel(vendasController, true);
		} catch (ParseException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
		/*jpVendas = vendasController.getVendasPanel();*/
		jpVendas.setBackground(Color.WHITE);

		jp_Produto.add(jpVendas);
		
		JPanel jp_Entrega = new JPanel();
		jp_Entrega.setBackground(Color.WHITE);
		jp_Entrega.setBorder(new LineBorder(new Color(0, 0, 0)));
		jpCentro.add(jp_Entrega, "cell 0 4,grow");
		jp_Entrega.setLayout(new MigLayout("", "[25px][10px][15px][10px][42px][10px][173px,grow][17px][10px][][1px][9px][1px][10px][35px][2px][50px,right]", "[15px][10px][25px]"));
		
		JButton btMapa = new JButton();
		btMapa.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PIVMapCFView mapaView = null;
				try {
					mapaView = new PIVMapCFView(PIVBusRelMainDetailCFView.this.cliente);
					MDIFrame.add(mapaView);
				} catch (ParseException ex) {
					JOptionPane
					.showMessageDialog(
							null,
							"Ocorreu um erro ao processar o mapa, favor contatar o administrador do sistema",
							"Erro ao processar mapa",
							JOptionPane.ERROR_MESSAGE);
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
				}
			}
		});
		
		JLabel lblRua = new JLabel("Rua:");
		lblRua.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblRua, "cell 0 0,alignx left,aligny center");

		lbEndRua = new JLabel();
		/*lbEndRua.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndRua.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndRua, "cell 2 0 5 1,growx,aligny bottom");

		JLabel lblBairro = new JLabel("Bairro:");
		lblBairro.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblBairro, "cell 7 0 6 1,alignx left,aligny top");
		
		lbEndBairro = new JLabel();
		/*lbEndBairro.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndBairro.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndBairro, "cell 14 0 3 1,growx,aligny bottom");

		btMapa.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/mapa_32x32.png")));
		jp_Entrega.add(btMapa, "cell 0 1 3 2,grow");

		JButton btCuboConhecimento = new JButton();
		btCuboConhecimento.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				PIVClientModel cliente = PIVBusRelMainDetailCFView.this.cliente;
				String nomePastaUsuario = cliente.getId().toString()/* + "_"
						+ cliente.getNomefantasia().replace(" ", "_")*/;
				
				String pathArquivos = gerenciarDiretorio.getArquivosDeClientePath()
						+ nomePastaUsuario
						+ (PIVGlobalSettings.isWindows() ? "\\" : "//");

				if (gerenciarDiretorio.criaDiretorio(pathArquivos)) {
					org.playiv.com.library.fileexplorer.FileExplorer adaptee = new org.playiv.com.library.fileexplorer.FileExplorer(pathArquivos,new ImageIcon(PIVFileExplorerInternalFrameAdapter.class.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_16x16.png")));
					PIVFileExplorerInternalFrameAdapter.incluirFileExplorer(adaptee);
				}
				
			}
		});
		
		JLabel lblCidade = new JLabel("Cidade:");
		lblCidade.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblCidade, "cell 4 1,alignx left,aligny center");
		
		lbEndCidade = new JLabel();
		/*lbEndCidade.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndCidade.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndCidade, "cell 6 1,growx,aligny bottom");
		
		JLabel lblUf = new JLabel("UF:");
		lblUf.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblUf, "cell 7 1,alignx left,aligny top");
		
		lbEndEstado = new JLabel();
		/*lbEndEstado.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndEstado.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndEstado, "cell 9 1 6 1,growx,aligny bottom");

		btCuboConhecimento.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/cubo_32x32.png")));
		jp_Entrega.add(btCuboConhecimento, "cell 16 1 1 2,grow");
		
		JLabel lblCep = new JLabel("CEP:");
		lblCep.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblCep, "cell 4 2,growx,aligny top");
		
		lbEndCep = new JLabel();
		/*lbEndCep.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndCep.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndCep, "cell 6 2,alignx left,aligny top");
		
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Entrega.add(lblPais, "cell 7 2 4 1,alignx left,aligny top");
		
		lbEndPais = new JLabel();
		/*lbEndPais.setFont(new Font("Arial", Font.PLAIN, 10));*/
		lbEndPais.setFont(new Font("Arial", Font.PLAIN, 12));
		jp_Entrega.add(lbEndPais, "cell 12 2 3 1,growx,aligny top");
		
		JPanel jp_Direito = new JPanel();
		jp_Direito.setPreferredSize(new Dimension(350, 0));
		/*jp_Direito.setBackground(Color.WHITE);*/
		contentPane.add(jp_Direito, BorderLayout.EAST);
		jp_Direito.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][][50px]"));
		
		JPanel jp_Historico = new JPanel();
		jp_Historico.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Historico.setBackground(Color.WHITE);
		jp_Direito.add(jp_Historico, "wrap");
		jp_Historico.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill]"));
		
		JPanel jp_Titulo = new JPanel();
		jp_Titulo.setPreferredSize(new Dimension(10, 25));
		jp_Titulo.setBackground(new Color(112, 128, 144));
		jp_Titulo.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Historico.add(jp_Titulo, "spany 50,north,growx");
		
		JLabel lblNewLabel = new JLabel("HIST\u00D3RICO DE A\u00C7\u00D5ES");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Titulo.add(lblNewLabel);
		
		tbl_HistoryOfActions = new PIVHistoryOfActionsTableFunc();
		/*tbl_HistoryOfActions.carregar(controller.getEventos());*/

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getViewport().setBackground(Color.WHITE);
		scrollPane.setViewportView(tbl_HistoryOfActions);
		/*scrollPane.setBackground(Color.WHITE);*/
		/*scrollPane.getRootPane().setBackground(Color.WHITE);*/
		jp_Historico.add(scrollPane, "center , grow ");

		/*scrollPane.repaint();*/
		
		JPanel jp_QuebraCabeca = new JPanel();
		jp_QuebraCabeca.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_QuebraCabeca.setBackground(Color.WHITE);
		jp_QuebraCabeca.setLayout(new MigLayout("", "[150px][110px,grow,trailing][150px]", "[30px][30px]"));
		
		JPanel jp_Titulo3 = new JPanel();
		jp_Titulo3.setPreferredSize(new Dimension(10, 25));
		jp_Titulo3.setBackground(new Color(112, 128, 144));
		jp_Titulo3.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_QuebraCabeca.add(jp_Titulo3, "north,grow");
		jp_Titulo3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblFatorCritico = new JLabel("FATORES CR�TICOS DE SUCESSO");
		lblFatorCritico.setForeground(new Color(255, 255, 255));
		lblFatorCritico.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Titulo3.add(lblFatorCritico);
		
		txTag1 = new JTextField();
		txTag1.setFont(new Font("Arial", Font.BOLD, 14));
		jp_QuebraCabeca.add(txTag1, "cell 0 0,gapx 5,gapy 5,grow");
		txTag1.setColumns(10);
		
		txTag2 = new JTextField();
		txTag2.setFont(new Font("Arial", Font.BOLD, 14));
		txTag2.setColumns(10);
		jp_QuebraCabeca.add(txTag2, "cell 2 0,gapright 5,gapy 5,grow");
		
		txTag3 = new JTextField();
		txTag3.setFont(new Font("Arial", Font.BOLD, 14));
		txTag3.setColumns(10);
		jp_QuebraCabeca.add(txTag3, "cell 0 1,gapx 5,gapbottom 5,grow");
		
		txTag4 = new JTextField();
		txTag4.setFont(new Font("Arial", Font.BOLD, 14));
		txTag4.setColumns(10);
		jp_QuebraCabeca.add(txTag4, "cell 2 1, gapbottom 5 , gapright 5 , grow");
		
		JLabel lblLogo = new JLabel();
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/quebracabeca_64x64.png")));
		jp_QuebraCabeca.add(lblLogo, "cell 1 0 1 2,grow");
		
		jp_Direito.add(jp_QuebraCabeca, "wrap");
		
		JPanel jp_Gravacao = new JPanel();
		jp_Gravacao.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Gravacao.setBackground(Color.WHITE);
		jp_Direito.add(jp_Gravacao, "");
		jp_Gravacao.setLayout(new MigLayout("", "[50px][grow]", "[25px][40px][40px][]"));
		
		JPanel jp_Titulo2 = new JPanel();
		jp_Titulo2.setPreferredSize(new Dimension(10, 25));
		jp_Titulo2.setBackground(new Color(112, 128, 144));
		jp_Titulo2.setBorder(new LineBorder(new Color(0, 0, 0)));
		jp_Gravacao.add(jp_Titulo2, "north,grow");
		jp_Titulo2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblHistricoDePosio = new JLabel("HIST\u00D3RICO DE POSI\u00C7\u00C3O DO CLIENTE");
		lblHistricoDePosio.setForeground(new Color(255, 255, 255));
		lblHistricoDePosio.setFont(new Font("Arial", Font.BOLD, 12));
		jp_Titulo2.add(lblHistricoDePosio);
		
		anotacaoController = new PIVUserNoteGFController(this.cliente,this.controller.getOrganizacliente());
		
		anotacaoPainel = new PIVUserNotePanel( anotacaoController , true );
		anotacaoPainel.getGrid().setShowFilterPanelOnGrid(false);
		anotacaoPainel.getGrid().setShowPageNumber(false);
		anotacaoController.setGrid(anotacaoPainel.getGrid());
		
		anotacaoPainel.setBackground(Color.WHITE);
		
		/*JPanel jpAnotacao = new JPanel();
		jpAnotacao.setLayout(new BorderLayout());
		jpAnotacao.add(anotacaoPainel,BorderLayout.CENTER);
		jpAnotacao.setBackground(Color.WHITE);*/
		
		JScrollPane scrollPaneAnotacaoGrid = new JScrollPane();
		scrollPaneAnotacaoGrid.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		scrollPaneAnotacaoGrid.getViewport().setBackground(Color.WHITE);
		scrollPaneAnotacaoGrid.setViewportView(anotacaoPainel);
		
		JButton btNovaAnotacao = new JButton();
		btNovaAnotacao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				anotacaoControllerAtiva = new PIVUserNoteDFController(anotacaoPainel.getGrid(), null,cliente,PIVBusRelMainDetailCFView.this.controller.getOrganizacliente(),playAuto);
			}
		});
		btNovaAnotacao.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/mic_32x32.png")));
		/*jp_Gravacao.add(button, "cell 0 1,aligny center");*/
		jp_Gravacao.add(btNovaAnotacao, "cell 0 0,gapleft 5,gaptop 5");
		/*jp_Gravacao.add(button_1, "cell 0 2,aligny center");*/
		/*jp_Gravacao.add(button_2, "cell 0 3,aligny top");*/
				
		JButton btPlayPause = new JButton();
		btPlayPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int id_anotacao = ((PIVUserNoteModel) anotacaoPainel.getGrid().getVOListTableModel().getObjectForRow(anotacaoPainel.getGrid().getSelectedRow())).getId();
				anotacaoControllerAtiva = new PIVUserNoteDFController(anotacaoPainel.getGrid(), id_anotacao ,cliente,PIVBusRelMainDetailCFView.this.controller.getOrganizacliente(),playAuto);
				anotacaoControllerAtiva.executaPlayPause(id_anotacao);
			}
		});
		btPlayPause.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/playpause_32x32.png")));
		jp_Gravacao.add(btPlayPause, "cell 0 1,gapleft 5");
		
		JButton btStop = new JButton();
		btStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(anotacaoControllerAtiva!=null)
					anotacaoControllerAtiva.executaParar();
				else
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"N�o existe nenhuma grava��o sendo reproduzida atualmente.","",JOptionPane.ERROR_MESSAGE);
			}
		});
		btStop.setHorizontalTextPosition(SwingConstants.CENTER);
		btStop.setIcon(new ImageIcon(PIVUserNoteGFView.class.getResource("/images/gerenciamento_relcomercial/tela-2/stop_32x32.png")));
		jp_Gravacao.add(btStop, "cell 0 2,gapleft 5");
		
		jp_Gravacao.add(scrollPaneAnotacaoGrid, "cell 1 0 1 4");
		
		JCheckBox chckbxContinuaPlay = new JCheckBox("Auto");
		chckbxContinuaPlay.setBackground(Color.WHITE);
		jp_Gravacao.add(chckbxContinuaPlay, "cell 0 3 2 1");
				
		chckbxContinuaPlay.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				AbstractButton abstractButton = (AbstractButton)ce.getSource();
		        ButtonModel buttonModel = abstractButton.getModel();
		        playAuto = buttonModel.isSelected();
				/*if(((javax.swing.JCheckBox) ce.getSource()).isSelected())
					playAuto = true;
				else
					playAuto = false;*/
				System.out.println(playAuto);
			}
		});
				
		pack( );
		
		/*PIVUserNoteGFController anotacaoController = new PIVUserNoteGFController(this.cliente);
		PIVUserNoteGFView anotacaoGridView = anotacaoController.getGrid();
		GridControl anotacaoGrid = anotacaoGridView.getGrid();
		anotacaoGrid.setShowPageNumber(false);
		anotacaoGrid.setShowFilterPanelOnGrid(false);

		JPanel jpAnotacao = new JPanel();
		jpAnotacao.setLayout(new BorderLayout());
		jpAnotacao.add(anotacaoGrid,BorderLayout.CENTER);
		jpAnotacao.setBackground(Color.WHITE);
		
		JScrollPane scrollPaneAnotacaoGrid = new JScrollPane();
		scrollPaneAnotacaoGrid.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		scrollPaneAnotacaoGrid.getViewport().setBackground(Color.WHITE);
		scrollPaneAnotacaoGrid.setViewportView(jpAnotacao);*/
		
		/*JList list = new JList();
		list.setBorder(new LineBorder(new Color(0, 0, 0)));*/
		/*jp_Gravacao.add(anotacaoGridView, "center,grow");*/
			
		setUniqueInstance(true);
	}
	
	public JTextField getTxTag1() {
		return txTag1;
	}

	public JTextField getTxTag2() {
		return txTag2;
	}

	public JTextField getTxTag3() {
		return txTag3;
	}

	public JTextField getTxTag4() {
		return txTag4;
	}

	public JTextField getTxVendasDesconto() {
		return txVendasDesconto;
	}

	public JTextField getTxValDescontoTotal() {
		return txValDescontoTotal;
	}

	public JLabel getLbNomeEmpresa() {
		return lbNomeEmpresa;
	}
	
	public JLabel getLbCodEmpresa() {
		return lbCodEmpresa;
	}

	public JLabel getLbSiteEmpresa() {
		return lbSiteEmpresa;
	}

	public JLabel getLbEndRua() {
		return lbEndRua;
	}

	public JLabel getLbEndBairro() {
		return lbEndBairro;
	}

	public JLabel getLbEndCidade() {
		return lbEndCidade;
	}

	public JLabel getLbEndEstado() {
		return lbEndEstado;
	}

	public JLabel getLbEndCep() {
		return lbEndCep;
	}

	public JLabel getLbEndPais() {
		return lbEndPais;
	}
	
	public PIVContactPanel getJpContato() {
		return jpContato;
	}
	
	public PIVProductSalesPanel getJpVendas() {
		return jpVendas;
	}
	
	public PIVHistoryOfActionsTableFunc getTbl_HistoryOfActions() {
		return tbl_HistoryOfActions;
	}
		
}