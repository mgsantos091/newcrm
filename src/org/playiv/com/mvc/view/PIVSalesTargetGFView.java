package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.openswing.swing.client.CopyButton;
import org.openswing.swing.client.DeleteButton;
import org.openswing.swing.client.EditButton;
import org.openswing.swing.client.ExportButton;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.client.InsertButton;
import org.openswing.swing.client.ReloadButton;
import org.openswing.swing.client.SaveButton;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.table.columns.client.CodLookupColumn;
import org.openswing.swing.table.columns.client.ComboColumn;
import org.openswing.swing.table.columns.client.IntegerColumn;
import org.openswing.swing.table.columns.client.TextColumn;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.mvc.controller.PIVSalesTargetGFController;
import org.playiv.com.mvc.controller.PIVSellerLUFController;

public class PIVSalesTargetGFView extends InternalFrame {

	private static final long serialVersionUID = 1L;
	
	private SaveButton saveButton;
	private EditButton editButton;
	private ReloadButton reloadButton;
	private InsertButton insertButton;
	private ExportButton exportButton;
	private DeleteButton deleteButton;
	
	private GridControl grid = new GridControl();
	
	private IntegerColumn colIdMetaComercial = new IntegerColumn();
	private CodLookupColumn colIdVendedor = new CodLookupColumn();
	private TextColumn colNomeVendedor = new TextColumn();
	private ComboColumn colNomeMetaComercial = new ComboColumn();
	private IntegerColumn colValorMeta = new IntegerColumn();
	
	public PIVSalesTargetGFView( PIVSalesTargetGFController controller ) {

		super.setTitle("Vendedor - Metas Comerciais");
		/*super.setFrameIcon(new ImageIcon(PIVSalesTargetGFView.class.getResource("/images/cadastros/usuario/usuario_16x16.png")));*/
		
		KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		Action actionESC = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				try {
					closeFrame();
				}
				catch (PropertyVetoException ex) { }
			}
		};
		
		grid.setController(controller);
		grid.setGridDataLocator(controller);
		
		super.getInputMap().put(esc,"esc");
		super.getActionMap().put("esc",actionESC);
		
		this.getContentPane().setLayout(new BorderLayout());
		
		JPanel jpBotoes = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpBotoes.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		reloadButton = new ReloadButton();
		jpBotoes.add(reloadButton);

		CopyButton copyButton = new CopyButton();
		jpBotoes.add(copyButton);

		editButton = new EditButton();
		jpBotoes.add(editButton);

		saveButton = new SaveButton();
		jpBotoes.add(saveButton);

		insertButton = new InsertButton();
		jpBotoes.add(insertButton);
		
		exportButton = new ExportButton();
		jpBotoes.add(exportButton);
		
		deleteButton = new DeleteButton();
		jpBotoes.add(deleteButton);
		
		grid.setCopyButton(copyButton);
	    grid.setDeleteButton(deleteButton);
	    grid.setEditButton(editButton);
	    grid.setExportButton(exportButton);
	    grid.setInsertButton(insertButton);
	    grid.setReloadButton(reloadButton);
	    grid.setSaveButton(saveButton);
	    grid.setRowHeight(50);
		
	    colIdMetaComercial.setColumnName("id");
	    colIdMetaComercial.setColumnRequired(false);
	    colIdMetaComercial.setEditableOnEdit(false);
	    colIdMetaComercial.setEditableOnInsert(false);
	    colIdMetaComercial.setPreferredWidth(50);
	    grid.add(colIdMetaComercial);
	    
	    colNomeMetaComercial.setDomainId("META_COMERCIAL");
	    colNomeMetaComercial.setColumnName("idmeta");
	    colNomeMetaComercial.setEditableOnEdit(false);
	    colNomeMetaComercial.setEditableOnInsert(true);
	    colNomeMetaComercial.setPreferredWidth(300);
	    grid.add(colNomeMetaComercial);
	    
	    PIVSellerLUFController lookupController = new PIVSellerLUFController( );
	    lookupController.setVisibleStatusPanel(true);
	    colIdVendedor.setColumnRequired(false);
	    colIdVendedor.setLookupController(lookupController);
	    colIdVendedor.setColumnName("vendedor.id");
	    colIdVendedor.setEditableOnEdit(true);
	    colIdVendedor.setEditableOnInsert(true);
	    colIdVendedor.setPreferredWidth(100);
	    grid.add(colIdVendedor);
	    
	    colNomeVendedor.setColumnName("vendedor.nome");
	    colNomeVendedor.setEditableOnEdit(false);
	    colNomeVendedor.setEditableOnInsert(false);
	    colNomeVendedor.setColumnRequired(false);
	    colNomeVendedor.setPreferredWidth(300);
	    grid.add(colNomeVendedor);
	    
	    colValorMeta.setColumnName("valormeta");
	    colValorMeta.setEditableOnEdit(true);
	    colValorMeta.setEditableOnInsert(true);
	    colValorMeta.setMaxValue(999999999);
	    colValorMeta.setColumnRequired(true);
	    colValorMeta.setPreferredWidth(120);
	    grid.add(colValorMeta);
	    
		grid.setValueObjectClassName("org.playiv.com.mvc.model.PIVSalesTargetModel");
		
		this.getContentPane().add(grid, BorderLayout.CENTER);
	    this.getContentPane().add(jpBotoes, BorderLayout.NORTH);
		
		super.pack();
		
		setUniqueInstance(true);
	}

	public void setEnableGridButtons(int mode) {
		if (mode == Consts.INSERT) {
			saveButton.setEnabled(true);
		} else if (mode == Consts.EDIT) {
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		} else if (mode == Consts.READONLY) {
			editButton.setEnabled(true);
			saveButton.setEnabled(true);
			reloadButton.setEnabled(true);
		}
	}
	
}