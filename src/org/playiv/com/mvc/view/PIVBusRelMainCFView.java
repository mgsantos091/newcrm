package org.playiv.com.mvc.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.library.function.IPIVCommandFunc;
import org.playiv.com.library.function.PIVAddCommandFunc;
import org.playiv.com.library.function.PIVBudgetCommandFunc;
import org.playiv.com.library.function.PIVBusRelClientInfoFunc;
import org.playiv.com.library.function.PIVBusRelCommandSubjectFunc;
import org.playiv.com.library.function.PIVCalcCommandFunc;
import org.playiv.com.library.function.PIVCalendarCommandFunc;
import org.playiv.com.library.function.PIVCubeCommandFunc;
import org.playiv.com.library.function.PIVEditClientFunc;
import org.playiv.com.library.function.PIVElementUPCommandFunc;
import org.playiv.com.library.function.PIVGarbageCommandFunc;
import org.playiv.com.library.function.PIVHistOfActionsFunc;
import org.playiv.com.library.function.PIVMailCommandFunc;
import org.playiv.com.library.function.PIVMapCommandFunc;
import org.playiv.com.library.function.PIVNetCommandFunc;
import org.playiv.com.library.function.PIVNoCommandFunc;
import org.playiv.com.library.function.PIVNoteCommandFunc;
import org.playiv.com.library.function.PIVRefreshCommandFunc;
import org.playiv.com.library.function.PIVTelCommandFunc;
import org.playiv.com.mvc.controller.PIVBusRelGarbageMngtGFController;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVBusRelMngtGFController;
import org.playiv.com.swing.general.PIVBusRelBeginFrame;
import org.playiv.com.swing.general.PIVBusRelClickPopUpMenuListener;
import org.playiv.com.swing.general.PIVBusRelElementPanel;
import org.playiv.com.swing.general.PIVBusRelLevelLayerPanel;
import org.playiv.com.swing.general.PIVBusRelPopUpMenu;
//import org.playiv.com.swing.general.PIVCloseSalesPipelineGadget;
import org.playiv.com.swing.general.PIVGeneralGadget;
import org.playiv.com.swing.general.PIVOpenSalesPipelineGadget;
//import org.playiv.com.swing.general.PIVSalesTargetIndividualSellerGadget;
import org.playiv.com.swing.general.PIVTopOpenSalesGadget;

import net.miginfocom.swing.MigLayout;

public class PIVBusRelMainCFView extends InternalFrame {

	private static final long serialVersionUID = 1L;

	private final int LINHAS_TABELAS = 5;
	private final int COLUNAS_TABELAS = 10;

	private JButton btnAdd;
	private JButton btnRemove;
	private JButton btnUp;
	private JButton btnInternet;
	private JButton btnLixo;
	private JButton btnTelefone;
	private JButton btnCubo;
	private JButton btnEmail;
	private JButton btnLocalCli;
	private JButton btnCalend;
	private JButton btnGravaVoz;

	private PIVBusRelCommandSubjectFunc subjCommand = new PIVBusRelCommandSubjectFunc();
	private IPIVCommandFunc addCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc lixoCommand = new PIVGarbageCommandFunc();
	private IPIVCommandFunc upCommand = new PIVElementUPCommandFunc();
	private IPIVCommandFunc internetCommand = new PIVNetCommandFunc();
	private IPIVCommandFunc calcCommand = new PIVCalcCommandFunc();
	private IPIVCommandFunc telCommand = new PIVTelCommandFunc();
	private IPIVCommandFunc cuboCommand = new PIVCubeCommandFunc();
	private IPIVCommandFunc emailCommand = new PIVMailCommandFunc();
	private IPIVCommandFunc mapaCommand = new PIVMapCommandFunc(this);
	private IPIVCommandFunc calendCommand = new PIVCalendarCommandFunc();
	private IPIVCommandFunc anotacaoCommand = new PIVNoteCommandFunc();
	private IPIVCommandFunc orcamentoCommand = new PIVBudgetCommandFunc();
	private IPIVCommandFunc historicoEventosCommand = new PIVHistOfActionsFunc();
	private IPIVCommandFunc refreshCommand = new PIVNoCommandFunc();
	private IPIVCommandFunc editarCommand = new PIVEditClientFunc();
	/*private IPIVCommandFunc detalheCommand = new PIVClientMoreInfoCommandFunc();*/

	private PIVBusRelBeginFrame jpInicio = new PIVBusRelBeginFrame();
	private PIVBusRelElementPanel jpSuspect = new PIVBusRelElementPanel();
	private PIVBusRelElementPanel jpProspect = new PIVBusRelElementPanel();
	private PIVBusRelElementPanel jpForecast = new PIVBusRelElementPanel();
	private PIVBusRelElementPanel jpCustomer = new PIVBusRelElementPanel();

	private JTabbedPane tpCentral = new JTabbedPane(JTabbedPane.TOP);

	private PIVBusRelMainCFController organizaClienteSuspect;
	private PIVBusRelMainCFController organizaClienteProspect;
	private PIVBusRelMainCFController organizaClienteForecast;
	private PIVBusRelMainCFController organizaClienteCustomer;

//	private JLabel lblLixo;

	private PIVBusRelMngtGFController controller;

	private JLabel lblTemp;

	private JButton btnOrcamento;

	/**
	 * Create the application.
	 */
	public PIVBusRelMainCFView() {

		super.setTitle("Gerenciamento de Rela��o Comercial");
		super.setFrameIcon(new ImageIcon(PIVBusRelMainCFView.class.getResource("/images/gerenciamento_relcomercial/logo/gerrelcomercial-icone_16x16.png")));

		controller = new PIVBusRelMngtGFController(this);
		refreshCommand = new PIVRefreshCommandFunc(controller);

		super.setPreferredSize(new Dimension(800, 600));
		super.getContentPane().setLayout(new BorderLayout());

		JPanel jpNiveis = new JPanel();
		jpNiveis.setBackground(new Color(255, 255, 255));
		// jpNiveis.setPreferredSize(new Dimension(275, 0));
		super.getContentPane().add(jpNiveis, BorderLayout.WEST);
		jpNiveis.setLayout(new MigLayout("", "[275!,grow]", "[][][][][][][]"));

/*		JLabel lbKammiaCorporation = new JLabel("Kammia Software Inc.");
		lbKammiaCorporation.setFont(new Font("Arial", Font.BOLD, 17));
		lbKammiaCorporation.setForeground(new Color(47, 79, 79));

		jpNiveis.add(lbKammiaCorporation, "cell 0 0");*/

		JLabel lbRelacoesComerciais = new JLabel("Rela��es Comerciais");
		lbRelacoesComerciais.setFont(new Font("Arial", Font.BOLD, 12));

		jpNiveis.add(lbRelacoesComerciais, "cell 0 1");

		/*
		 * JLabel lblInstrucao = new JLabel();
		 * lblInstrucao.setVerticalTextPosition(SwingConstants.TOP);
		 * lblInstrucao.setVerticalAlignment(SwingConstants.TOP);
		 * lblInstrucao.setBackground(Color.WHITE);
		 * lblInstrucao.setForeground(Color.RED); lblInstrucao.setFont(new
		 * Font("Arial", Font.PLAIN, 9)); lblInstrucao.setText(
		 * "Clique na seta para cima no painel de opera��es para subir uma rela��o comercial de n�vel."
		 * ); lblInstrucao.setEnabled(false);
		 * 
		 * jpNiveis.add(lblInstrucao, "cell 0 2");
		 */

		JTextArea taInstrucao = new JTextArea();
		taInstrucao.setBackground(Color.WHITE);
		taInstrucao.setForeground(Color.RED);
		taInstrucao.setFont(new Font("Arial", Font.PLAIN, 9));
		taInstrucao.setEditable(false);
		taInstrucao.setEnabled(false);
		taInstrucao.setLineWrap(true);
		taInstrucao.setWrapStyleWord(false);
		taInstrucao
				.setText("Clique na seta para cima no painel de opera��es para subir uma rela��o comercial de n�vel.");
		jpNiveis.add(taInstrucao, "cell 0 2,growx,aligny top");

		JScrollPane scrollPaneSuspect = new JScrollPane(/*JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER*/);
		scrollPaneSuspect.getViewport().setBackground(Color.WHITE);
		scrollPaneSuspect.setViewportView(jpSuspect);
		
		JScrollPane scrollPaneProspect = new JScrollPane();
		scrollPaneProspect.getViewport().setBackground(Color.WHITE);
		scrollPaneProspect.setViewportView(jpProspect);
		
		JScrollPane scrollPaneForecast = new JScrollPane();
		scrollPaneForecast.getViewport().setBackground(Color.WHITE);
		scrollPaneForecast.setViewportView(jpForecast);
		
		JScrollPane scrollPaneCustomer = new JScrollPane();
		scrollPaneCustomer.getViewport().setBackground(Color.WHITE);
		scrollPaneCustomer.setViewportView(jpCustomer);
		
		PIVBusRelLevelLayerPanel gerRelComBoxSuspect = new PIVBusRelLevelLayerPanel(
				"Suspect", this.LINHAS_TABELAS, this.COLUNAS_TABELAS);
		organizaClienteSuspect = new PIVBusRelMainCFController(tpCentral,
				scrollPaneSuspect , jpSuspect, PIVBusRelMainCFController.nivel.SUSPECT,
				gerRelComBoxSuspect, subjCommand, controller);
		gerRelComBoxSuspect.setOrganizacliente(organizaClienteSuspect);
		gerRelComBoxSuspect.getLbNivelPrint().setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-0_16x16.png")));

		PIVBusRelLevelLayerPanel gerRelComBoxProspect = new PIVBusRelLevelLayerPanel(
				"Prospect", this.LINHAS_TABELAS, this.COLUNAS_TABELAS);
		organizaClienteProspect = new PIVBusRelMainCFController(tpCentral,
				scrollPaneProspect , jpProspect, PIVBusRelMainCFController.nivel.PROSPECT,
				gerRelComBoxProspect, subjCommand, controller);
		gerRelComBoxProspect.setOrganizacliente(organizaClienteProspect);
		gerRelComBoxProspect.getLbNivelPrint().setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-1_16x16.png")));

		PIVBusRelLevelLayerPanel gerRelComBoxForecast = new PIVBusRelLevelLayerPanel(
				"Forecast", this.LINHAS_TABELAS, this.COLUNAS_TABELAS);
		organizaClienteForecast = new PIVBusRelMainCFController(tpCentral,
				scrollPaneForecast, jpForecast , PIVBusRelMainCFController.nivel.FORECAST,
				gerRelComBoxForecast, subjCommand, controller);
		gerRelComBoxForecast.setOrganizacliente(organizaClienteForecast);
		gerRelComBoxForecast.getLbNivelPrint().setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-2_16x16.png")));

		PIVBusRelLevelLayerPanel gerRelComBoxCustomer = new PIVBusRelLevelLayerPanel(
				"Customer", this.LINHAS_TABELAS, this.COLUNAS_TABELAS);
		organizaClienteCustomer = new PIVBusRelMainCFController(tpCentral,
				scrollPaneCustomer, jpCustomer, PIVBusRelMainCFController.nivel.CUSTOMER,
				gerRelComBoxCustomer, subjCommand, controller);
		gerRelComBoxCustomer.setOrganizacliente(organizaClienteCustomer);
		gerRelComBoxCustomer.getLbNivelPrint().setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-3_16x16.png")));

		organizaClienteSuspect.setProxOrganizaCliente(organizaClienteProspect);
		gerRelComBoxSuspect.setBoxPosterior(gerRelComBoxProspect);

		organizaClienteProspect.setProxOrganizaCliente(organizaClienteForecast);
		gerRelComBoxProspect.setBoxPosterior(gerRelComBoxForecast);

		organizaClienteForecast.setProxOrganizaCliente(organizaClienteCustomer);
		gerRelComBoxProspect.setBoxPosterior(gerRelComBoxSuspect);

		jpNiveis.add(gerRelComBoxCustomer, "cell 0 3, gapright 5");
		jpNiveis.add(gerRelComBoxForecast, "cell 0 4, gapright 5");
		jpNiveis.add(gerRelComBoxProspect, "cell 0 5, gapright 5");
		jpNiveis.add(gerRelComBoxSuspect, "cell 0 6, gapright 5");

		JPanel jpClientesPNivel = new JPanel();
		jpClientesPNivel.setLayout(new BorderLayout(0, 0));

		JPanel jpOpEDadosCli = new JPanel();
		jpOpEDadosCli.setPreferredSize(new Dimension(0, 175));
		jpOpEDadosCli.setLayout(new BorderLayout(0, 0));

		jpClientesPNivel.add(jpOpEDadosCli, BorderLayout.SOUTH);

		JPanel jpAcoes = new JPanel();
		jpAcoes.setPreferredSize(new Dimension(225, 10));
		jpAcoes.setBackground(Color.WHITE);
		jpAcoes.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Painel de A��es",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(47, 79,
						79)));
		jpAcoes.setLayout(new GridLayout(0, 4, 5, 5));
		
		subjCommand.registerCommand(editarCommand);
		
		btnUp = new JButton(); // UP
		btnUp.setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/up_32x32.png")));
		btnUp.setBorder(null);
		btnUp.setBackground(Color.WHITE);
		btnUp.setForeground(Color.WHITE);
		btnUp.setMnemonic(KeyEvent.VK_U);
		btnUp.setToolTipText("Sobe o cliente para o pr�ximo n�vel no relacionamento comercial");
		btnUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				upCommand.execute();
			}
		});
		subjCommand.registerCommand(upCommand);
		jpAcoes.add(btnUp);

		addCommand = new PIVAddCommandFunc(organizaClienteSuspect);

		btnAdd = new JButton(); // ADD
		btnAdd.setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/addcliente_32x32.png")));
		btnAdd.setBorder(null);
		btnAdd.setBackground(Color.WHITE);
		btnAdd.setForeground(Color.WHITE);
		btnAdd.setMnemonic(KeyEvent.VK_PLUS);
		btnAdd.setToolTipText("Adiciona um novo cliente a camada 'Suspect'");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				addCommand.execute();
			}
		});
		subjCommand.registerCommand(addCommand);
		jpAcoes.add(btnAdd);

		btnRemove = new JButton(); // REMOVE
		btnRemove
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/removercliente_32x32.png")));
		btnRemove.setBorder(null);
		btnRemove.setBackground(Color.WHITE);
		btnRemove.setForeground(Color.WHITE);
		btnRemove.setMnemonic(KeyEvent.VK_MINUS);
		btnRemove.setToolTipText("Remove o cliente da camada atual e o leva para a lixeira, aonde ser� poss�vel recupera-lo mais tarde");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				lixoCommand.execute();
			}
		});
		/* ((PIVLixoCommand) lixoCommand).setGerRelComPrincip(this); */
		subjCommand.registerCommand(lixoCommand);
		jpAcoes.add(btnRemove);

		btnCubo = new JButton(); // CUBO
		btnCubo.setBorder(null);
		btnCubo.setBackground(Color.WHITE);
		btnCubo.setForeground(Color.WHITE);
		btnCubo.setMnemonic(KeyEvent.VK_O);
		btnCubo.setToolTipText("Abre o Cubo de Conhecimento, respons�vel pelo gerenciamento de arquivos relacionados ao cliente dentro do sistema"); 
		btnCubo.setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/cubo_32x32.png")));
		btnCubo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				cuboCommand.execute();
			}
		});
		subjCommand.registerCommand(cuboCommand);
		jpAcoes.add(btnCubo);

		btnEmail = new JButton(); // EMAIL
		btnEmail.setBorder(null);
		btnEmail.setBackground(Color.WHITE);
		btnEmail.setForeground(Color.WHITE);
		btnEmail.setMnemonic(KeyEvent.VK_E);
		btnEmail.setToolTipText("Envia um e-mail ao cliente. Os e-mails dispon�veis s�o os cadastrados nos contatos do cliente");
		btnEmail.setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/email_32x32.png")));
		btnEmail.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				emailCommand.execute();
			}
		});
		subjCommand.registerCommand(emailCommand);
		jpAcoes.add(btnEmail);

		btnTelefone = new JButton(); // TEL
		btnTelefone.setBorder(null);
		btnTelefone.setBackground(Color.WHITE);
		btnTelefone.setForeground(Color.WHITE);
		btnTelefone.setToolTipText("Realiza uma liga��o para o cliente");
		btnTelefone
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/telefone_32x32.png")));
		btnTelefone.setMnemonic(KeyEvent.VK_T);
		btnTelefone.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				telCommand.execute();
			}
		});
		subjCommand.registerCommand(telCommand);
		jpAcoes.add(btnTelefone);

		btnGravaVoz = new JButton(); // VOZ
		btnGravaVoz.setBorder(null);
		btnGravaVoz.setBackground(Color.WHITE);
		btnGravaVoz
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/anotacao_32x32.png")));
		btnGravaVoz.setMnemonic(KeyEvent.VK_R);
		btnGravaVoz.setToolTipText("Gostaria de adicionar uma observa��o ou uma grava��o de voz sobre o cliente em quest�o? Aqui � o lugar certo");
		btnGravaVoz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				anotacaoCommand.execute();
			}
		});
		subjCommand.registerCommand(anotacaoCommand);
		jpAcoes.add(btnGravaVoz);

		/*
		 * JPanel pnlEspaco = new JPanel(); // Espaco Vazio
		 * pnlEspaco.setBackground(Color.WHITE);
		 * pnlEspaco.setForeground(Color.WHITE); jpOperacoes.add(pnlEspaco);
		 */

		btnLocalCli = new JButton(); // MAPA
		btnLocalCli.setBorder(null);
		btnLocalCli.setBackground(Color.WHITE);
		btnLocalCli.setForeground(Color.WHITE);
		btnLocalCli
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/mapa_32x32.png")));
		btnLocalCli.setMnemonic(KeyEvent.VK_M);
		btnLocalCli.setToolTipText("Visualiza o endere�o (caso registrado) do cliente no mapa, � poss�vel visualizar todos os clientes da camada no mapa n�o selecionado nenhum cliente ou clicando com o bot�o direito do mouse em uma parte vazia dentro do painel acima e escolhendo a op��o referente ao mapa");
		btnLocalCli.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				mapaCommand.execute();
			}
		});
		subjCommand.registerCommand(mapaCommand);
		jpAcoes.add(btnLocalCli);

		btnInternet = new JButton(); // NET
		btnInternet.setBorder(null);
		btnInternet.setBackground(Color.WHITE);
		btnInternet.setForeground(Color.WHITE);
		btnInternet
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/internet_32x32.png")));
		btnInternet.setMnemonic(KeyEvent.VK_I);
		btnInternet.setToolTipText("Acessa o website registrado no cadastro do cliente");
		btnInternet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				internetCommand.execute();
			}
		});
		subjCommand.registerCommand(internetCommand);
		jpAcoes.add(btnInternet);

		btnLixo = new JButton(); // LIXEIRA
		btnLixo.setBorder(null);
		btnLixo.setBackground(Color.WHITE);
		btnLixo.setForeground(Color.WHITE);
		btnLixo.setIcon(new ImageIcon(
				PIVBusRelMainCFView.class
						.getResource("/images/gerenciamento_relcomercial/painel_operacao/lixo_32x32.png")));
		btnLixo.setMnemonic(KeyEvent.VK_A);
//		btnLixo.setToolTipText("Exibe a fun��o de calculadora do sistema, utilizado para realizar c�lculos matem�ticos simples");
		btnLixo.setToolTipText("Exibe os clientes que foram adicionados � lixeira.");
		btnLixo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
//				lixoCommand.execute();
				new PIVBusRelGarbageMngtGFController(PIVBusRelMainCFView.this.controller);
			}
		});
//		subjCommand.registerCommand(lixoCommand);
		jpAcoes.add(btnLixo);

		btnCalend = new JButton(); // CALEND
		btnCalend.setBorder(null);
		btnCalend.setBackground(Color.WHITE);
		btnCalend.setForeground(Color.WHITE);
		btnCalend
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/calendario_32x32.png")));
		btnCalend.setMnemonic(KeyEvent.VK_C);
		btnCalend.setToolTipText("Exibe o calend�rio do sistema, utilizado para marcar reuni�es e outros eventos");
		btnCalend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				calendCommand.execute();
			}
		});
		subjCommand.registerCommand(calendCommand);
		jpAcoes.add(btnCalend);

		btnOrcamento = new JButton(); // ORCAMENTO
		btnOrcamento.setBorder(null);
		btnOrcamento.setBackground(Color.WHITE);
		btnOrcamento.setForeground(Color.WHITE);
		btnOrcamento
				.setIcon(new ImageIcon(
						PIVBusRelMainCFView.class
								.getResource("/images/gerenciamento_relcomercial/painel_operacao/orcamento_32x32.png")));
		btnOrcamento.setMnemonic(KeyEvent.VK_C);
		btnOrcamento.setToolTipText("Visualize todos os or�amentos relacionados ao cliente"); 
		btnOrcamento.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				orcamentoCommand.execute();
			}
		});
		subjCommand.registerCommand(orcamentoCommand);
		jpAcoes.add(btnOrcamento);	

		// Registra os itens que n�o possuem bot�es relacionados no painel de a��es
		/*subjCommand.registerCommand(detalheCommand);*/
		subjCommand.registerCommand(historicoEventosCommand);
		
		jpOpEDadosCli.add(jpAcoes, BorderLayout.WEST);

		JPanel jpDadosCli = new JPanel();
		jpDadosCli.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Informa��es do Cliente",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(47, 79,
						79)));
		jpDadosCli.setBackground(Color.WHITE);
		jpOpEDadosCli.add(jpDadosCli, BorderLayout.CENTER);
		jpDadosCli
				.setLayout(new MigLayout("", "[right][]", "[][][][][][][]"));

		JLabel lblClienteSelecionado = new JLabel("Cliente selecionado:");
		jpDadosCli.add(lblClienteSelecionado);

		JLabel lblClienteSelecionadoValor = new JLabel("--");
		jpDadosCli.add(lblClienteSelecionadoValor, "cell 1 0,wrap");

		JLabel lblRelacaoComercial = new JLabel("Rela��o Comercial:");
		jpDadosCli.add(lblRelacaoComercial);

		JLabel lblRCValor = new JLabel("--");
		jpDadosCli.add(lblRCValor, "cell 1 1,growx,wrap");

		JLabel lblResponsvel = new JLabel("Respons�vel:");
		jpDadosCli.add(lblResponsvel, "cell 0 2");

		JLabel lblResponsavelValor = new JLabel("--");
		jpDadosCli.add(lblResponsavelValor, "cell 1 2,growx,wrap");

		JLabel lblOrigem = new JLabel("Origem:");
		jpDadosCli.add(lblOrigem, "cell 0 3");

		JLabel lblOrigemLeadValor = new JLabel("--");
		jpDadosCli.add(lblOrigemLeadValor, "cell 1 3,growx,wrap");

		JLabel lblEmail = new JLabel("E-mail:");
		jpDadosCli.add(lblEmail, "cell 0 4");

		JLabel lblEmailValor = new JLabel("--");
		jpDadosCli.add(lblEmailValor, "cell 1 4,growx,wrap");

		JLabel lblEndereco = new JLabel("Endere�o:");
		jpDadosCli.add(lblEndereco, "cell 0 5");

		JLabel lblEnderecoValor = new JLabel("--");
		jpDadosCli.add(lblEnderecoValor, "cell 1 5,growx,wrap");

		PIVBusRelClientInfoFunc gerInfoCli = new PIVBusRelClientInfoFunc(
				lblClienteSelecionadoValor, lblRCValor, lblResponsavelValor,
				lblOrigemLeadValor, lblEmailValor, lblEnderecoValor);
		organizaClienteCustomer.setGerInfoCli(gerInfoCli);
		organizaClienteForecast.setGerInfoCli(gerInfoCli);
		organizaClienteProspect.setGerInfoCli(gerInfoCli);
		organizaClienteSuspect.setGerInfoCli(gerInfoCli);

		JPanel jpPrincCentro_centro = new JPanel();
		jpPrincCentro_centro.setBackground(Color.WHITE);
		jpPrincCentro_centro.setLayout(new BorderLayout(0, 0));

		jpPrincCentro_centro.add(tpCentral, BorderLayout.CENTER);

		// objeto para representar o menu dentro dos n�veis
		PIVBusRelPopUpMenu popMenuPainel = new PIVBusRelPopUpMenu(null,
				this.addCommand, null, null, this.calcCommand,
				this.internetCommand, this.calendCommand, null,
				this.mapaCommand, null , null , null , this.telCommand , this.refreshCommand , null /*, null*/);

		/*jpInicio.setLayout(new FlowLayout(FlowLayout.LEADING));*/
		
		JScrollPane scrollPaneInicio = new JScrollPane();
		scrollPaneInicio.getViewport().setBackground(Color.WHITE);
		
		jpInicio.setLayout(new MigLayout("","",""));
		
		jpInicio.setBackground(new Color(255, 255, 255));
		jpInicio.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		scrollPaneInicio.setViewportView(jpInicio);
		
		PIVGeneralGadget gadgetDadosGerais = new PIVGeneralGadget();	
		jpInicio.add(gadgetDadosGerais,"");
		
		PIVOpenSalesPipelineGadget gadgetVendasAbertasPipeline = new PIVOpenSalesPipelineGadget();
		jpInicio.add(gadgetVendasAbertasPipeline,"wrap");
		
		/*PIVCloseSalesPipelineGadget gadgetVendasFechadasPipeline = new PIVCloseSalesPipelineGadget();*/
		/*jpInicio.add(gadgetVendasFechadasPipeline,"wrap");*/
		
//		PIVSalesTargetIndividualSellerGadget gadgetMetasDeVendas = new PIVSalesTargetIndividualSellerGadget( );
//		jpInicio.add(gadgetMetasDeVendas,"wrap,spanx,h 320!,w 750!");
		
		PIVTopOpenSalesGadget gadgetPrincipaisVendasAbertas = new PIVTopOpenSalesGadget( );
		jpInicio.add(gadgetPrincipaisVendasAbertas,"spanx,h 320!,w 550!");
		
		
		tpCentral.addTab("Inicio", new ImageIcon(
				PIVBusRelMainCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/home_16x16.png")), scrollPaneInicio, null);
		tpCentral.getComponentAt(0).addMouseListener(new MouseAdapter() {
			
		});

		jpSuspect.setOrganizaClienteRelacionado(organizaClienteSuspect);
		/*jpSuspect.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));*/
		jpSuspect.setBackground(new Color(255, 255, 255));
		jpSuspect.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		jpSuspect.addMouseListener(new PIVBusRelClickPopUpMenuListener(
				popMenuPainel));
				
		tpCentral.addTab("Suspect", new ImageIcon(
				PIVBusRelMainCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-0_16x16.png")), scrollPaneSuspect, null);

		jpProspect.setOrganizaClienteRelacionado(organizaClienteProspect);
		/*jpProspect.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));*/
		jpProspect.setBackground(new Color(255, 255, 255));
		jpProspect.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		jpProspect.addMouseListener(new PIVBusRelClickPopUpMenuListener(
				popMenuPainel));
				
		tpCentral.addTab("Prospect", new ImageIcon(
				PIVBusRelMainCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-1_16x16.png")), scrollPaneProspect, null);

		jpForecast.setOrganizaClienteRelacionado(organizaClienteForecast);
		/*jpForecast.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));*/
		jpForecast.setBackground(new Color(255, 255, 255));
		jpForecast.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		jpForecast.addMouseListener(new PIVBusRelClickPopUpMenuListener(
				popMenuPainel));
			
		tpCentral.addTab("Forecast", new ImageIcon(
				PIVBusRelMainCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-2_16x16.png")), scrollPaneForecast, null);

		jpCustomer.setOrganizaClienteRelacionado(organizaClienteCustomer);
		/*jpCustomer.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));*/
		jpCustomer.setBackground(new Color(255, 255, 255));
		jpCustomer.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		jpCustomer.addMouseListener(new PIVBusRelClickPopUpMenuListener(
				popMenuPainel));
				
		tpCentral.addTab("Customer", new ImageIcon(
				PIVBusRelMainCFView.class
				.getResource("/images/gerenciamento_relcomercial/nivel_comercial/circulo-3_16x16.png")), scrollPaneCustomer, null);

		PIVBusRelPopUpMenu popMenuElemento = new PIVBusRelPopUpMenu(
				this.upCommand, null, this.lixoCommand, this.cuboCommand, null,
				this.internetCommand, this.calendCommand, this.emailCommand,
				this.mapaCommand, this.anotacaoCommand , this.orcamentoCommand , this.historicoEventosCommand , this.telCommand, null , this.editarCommand /*, this.detalheCommand*/);

		organizaClienteCustomer.setPopMenu(popMenuElemento);
		organizaClienteForecast.setPopMenu(popMenuElemento);
		organizaClienteProspect.setPopMenu(popMenuElemento);
		organizaClienteSuspect.setPopMenu(popMenuElemento);

//		JPanel jpDireita = new JPanel();
//		jpDireita.setLayout(new MigLayout("", "[]", "[grow]"));
//		jpDireita.setBackground(Color.WHITE);
		/*jpDireita.setBorder(BorderFactory.createLineBorder(Color.BLACK));*/
		// jpDireita.setPreferredSize(new Dimension(50, 0));

//		jpPrincCentro_centro.add(jpDireita, BorderLayout.EAST);

//		lblTemp = new JLabel(/* "Temp" */);
//		lblTemp.setHorizontalAlignment(SwingConstants.CENTER);
//		lblTemp.setIcon(new ImageIcon(PIVBusRelMainCFView.class
//				.getResource("/images/menu_principal/relogio0_64x64.png")));
//		lblTemp.setAlignmentX(Component.CENTER_ALIGNMENT);
//		jpDireita.add(lblTemp,"spany ,north,growy");

		/*
		 * JLabel lblTemp_1 = new JLabel("Temp");
		 * lblTemp_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		 * jpTempo.add(lblTemp_1);
		 * 
		 * JLabel lblTemp_2 = new JLabel("Temp");
		 * lblTemp_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		 * jpTempo.add(lblTemp_2);
		 */

//		JButton btRecarrega = new JButton("Recarregar");
//		btRecarrega.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				controller.carregar();
//			}
//		});
//		jpDireita.add(btRecarrega, "dock south");
		
//		lblLixo = new JLabel();
//		lblLixo.setHorizontalAlignment(SwingConstants.CENTER);
//		lblLixo.setAlignmentX(Component.CENTER_ALIGNMENT);
//		lblLixo.addMouseListener(new PIVBusRelGarbageClickPopUpMenuListener(
//				controller));
//
//		jpDireita.add(lblLixo, "dock south");

		jpClientesPNivel.add(jpPrincCentro_centro, BorderLayout.CENTER);

		super.getContentPane().add(jpClientesPNivel, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/*setSize(800, 600);*/
		
		pack( );

		controller.carregar();

		inicializarTemporizador();
		
		setUniqueInstance(true);
		
		MDIFrame.add(this);

	}

	public JLabel getLblTemp() {
		return lblTemp;
	}

	public Integer getNivelSelecionado() {
		return this.tpCentral.getSelectedIndex();
	}

//	public JLabel getLblLixo() {
//		return this.lblLixo;
//	}

	public PIVBusRelMainCFController getOrganizaClienteSuspect() {
		return organizaClienteSuspect;
	}

	public PIVBusRelMainCFController getOrganizaClienteProspect() {
		return organizaClienteProspect;
	}

	public PIVBusRelMainCFController getOrganizaClienteForecast() {
		return organizaClienteForecast;
	}

	public PIVBusRelMainCFController getOrganizaClienteCustomer() {
		return organizaClienteCustomer;
	}

	private void inicializarTemporizador() {
		Timer timer = new Timer(60000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.carregar();
			}
		});
		timer.start();
	}

}