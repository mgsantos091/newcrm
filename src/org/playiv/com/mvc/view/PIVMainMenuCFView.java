package org.playiv.com.mvc.view;

import java.awt.Font;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.hibernate.Session;
import org.openswing.swing.domains.java.Domain;
import org.openswing.swing.internationalization.java.BrazilianPortugueseOnlyResourceFactory;
import org.openswing.swing.internationalization.java.Language;
import org.openswing.swing.internationalization.java.ResourcesFactory;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.mdi.client.ClientFacade;
import org.openswing.swing.mdi.client.Clock;
import org.openswing.swing.mdi.client.MDIController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.mdi.java.ApplicationFunction;
import org.openswing.swing.permissions.client.LoginController;
import org.openswing.swing.table.profiles.client.FileGridProfileManager;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.AvisoAgendaThread;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVMenuPrincipalFacade;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController;
import org.playiv.com.mvc.controller.PIVBusRelMainCFController.nivel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.start.PIVSplashScreen;
import org.playiv.com.swing.general.PIVLoginPanel;

public class PIVMainMenuCFView implements MDIController, LoginController {

	boolean isGerente = PIVUserSession.getInstance().getUsuarioSessao().isUsuariogerente();
	
	private PIVDao pdao = PIVDao.getInstance();
	private PIVMenuPrincipalFacade menufacade = new PIVMenuPrincipalFacade();

	private PIVGlobalSettings globalSettings = PIVGlobalSettings.getInstance();

	private PIVDirectoryMngtFunc gerenciarDiretorios = PIVDirectoryMngtFunc
			.getInstance();
	
	private PIVLogSettings logMngr;
	
	private PIVSplashScreen frameSplash;
	
	private AvisoAgendaThread avisoAgendaThread;

	public PIVMainMenuCFView( PIVSplashScreen frameSplash ) {
		this.frameSplash = frameSplash;
		this.frameSplash.setBarraDeProgressoVal(60);
		configClientSettings();
	}

	@Override
	public ArrayList<Language> getLanguages() {
		ArrayList list = new ArrayList();
		list.add(new Language("BR", "Brazilian Portuguese"));
		return list;
	}

	private void configClientSettings( ) {

		this.frameSplash.setBarraDeProgressoVal(70);
		
		frameSplash.addText("Configurando o PlayIV...");
		System.out.println("Configurando o PlayIV...");
		
		Hashtable domains = new Hashtable();

		Domain fonteLead = new Domain("FONTE_LEAD");
		fonteLead.addDomainPair(1, "Chamada Fria");
		fonteLead.addDomainPair(2, "Cliente Existente");
		fonteLead.addDomainPair(3, "Empregado");
		fonteLead.addDomainPair(4, "Parceiro");
		fonteLead.addDomainPair(5, "Rela��es P�blicas");
		fonteLead.addDomainPair(6, "Mala Direta");
		fonteLead.addDomainPair(7, "Confer�ncia");
		fonteLead.addDomainPair(8, "Feira de Neg�cios");
		fonteLead.addDomainPair(9, "Web Site");
		fonteLead.addDomainPair(10, "Boca-boca");
		fonteLead.addDomainPair(11, "Outro");
		domains.put(fonteLead.getDomainId(), fonteLead);
		
		Domain atividadeLead = new Domain("ATIVIDADE_LEAD");
		atividadeLead.addDomainPair(1, "Vestu�rio");
		atividadeLead.addDomainPair(2, "Bancos");
		atividadeLead.addDomainPair(3, "Biotecnologia");
		atividadeLead.addDomainPair(4, "Qu�mica");
		atividadeLead.addDomainPair(5, "Comunica��es");
		atividadeLead.addDomainPair(6, "Constru��o");
		atividadeLead.addDomainPair(7, "Consultoria");
		atividadeLead.addDomainPair(8, "Educa��o");
		atividadeLead.addDomainPair(9, "Eletr�nica");
		atividadeLead.addDomainPair(10, "Energia");
		atividadeLead.addDomainPair(11, "Engenharia");
		atividadeLead.addDomainPair(12, "Entretenimento");
		atividadeLead.addDomainPair(13, "Meio Ambiente");
		atividadeLead.addDomainPair(14, "Finan�as");
		atividadeLead.addDomainPair(15, "Alimentos & Bebidas");
		atividadeLead.addDomainPair(16, "Governo");
		atividadeLead.addDomainPair(17, "Sa�de");
		atividadeLead.addDomainPair(18, "Hotelaria");
		atividadeLead.addDomainPair(19, "Seguro");
		atividadeLead.addDomainPair(20, "M�quinas");
		atividadeLead.addDomainPair(21, "Ind�stria");
		atividadeLead.addDomainPair(22, "M�dia");
		atividadeLead.addDomainPair(23, "ONGs");
		atividadeLead.addDomainPair(24, "Recrea��o");
		atividadeLead.addDomainPair(25, "Com�rcio");
		atividadeLead.addDomainPair(26, "Transporte Mar�timo");
		atividadeLead.addDomainPair(27, "Tecnologia");
		atividadeLead.addDomainPair(28, "Telecomunica��es");
		atividadeLead.addDomainPair(29, "Transportes");
		atividadeLead.addDomainPair(30, "Servi�o P�blico");
		atividadeLead.addDomainPair(31, "Outro");
		domains.put(atividadeLead.getDomainId(), atividadeLead);
		
		Domain statusLead = new Domain("STATUS_LEAD");
		statusLead.addDomainPair(1,"Tentativa Contato");
		statusLead.addDomainPair(2,"Frio");
		statusLead.addDomainPair(3,"Contactar no Futuro");
		statusLead.addDomainPair(4,"Contactado");
		statusLead.addDomainPair(5,"Quente");
		statusLead.addDomainPair(6,"Descartado");
		statusLead.addDomainPair(7,"Perdido");
		statusLead.addDomainPair(8,"N�o Contactado");
		statusLead.addDomainPair(9,"Pr�-Qualificado");
		statusLead.addDomainPair(10,"Qualificado");
		statusLead.addDomainPair(11,"Morno");
		domains.put(statusLead.getDomainId(), statusLead);

		Domain statusCampanha = new Domain("STATUS_CAMPANHA");
		statusCampanha.addDomainPair(1,"Planejamento");
		statusCampanha.addDomainPair(2,"Ativo");
		statusCampanha.addDomainPair(3,"Inativo");
		statusCampanha.addDomainPair(4,"Conclu�da");
		statusCampanha.addDomainPair(5,"Cancelada");
		domains.put(statusCampanha.getDomainId(), statusCampanha);
		
		Domain tipoOrcamento = new Domain("TIPO_ORCAMENTO");
		tipoOrcamento.addDomainPair(true, "Efetivado");
		tipoOrcamento.addDomainPair(false, "Em aberto");
		domains.put(tipoOrcamento.getDomainId(), tipoOrcamento);
		
		Domain statusLigacao = new Domain("STATUS_LIGACAO");
		statusLigacao.addDomainPair(0, "Liga��o Conclu�da");
		statusLigacao.addDomainPair(1, "N�o Atende");
		statusLigacao.addDomainPair(2, "Ocupado");
		statusLigacao.addDomainPair(3, "N�o Est�");
		statusLigacao.addDomainPair(4, "Telefone n�o existe");
		statusLigacao.addDomainPair(5, "Caixa postal");
		statusLigacao.addDomainPair(6, "Fax");
		statusLigacao.addDomainPair(7, "Secret�ria Eletr�nica");
		statusLigacao.addDomainPair(8, "Telefone Incorreto");
		statusLigacao.addDomainPair(9, "Telefone Residencial");
		statusLigacao.addDomainPair(10, "Agendamento");
		statusLigacao.addDomainPair(11, "Liga��o Caiu");
		statusLigacao.addDomainPair(12, "Liga��o Muda");
		statusLigacao.addDomainPair(13, "Telefone Mudou");
		statusLigacao.addDomainPair(14, "Telefone Fora de Servi�o");
		statusLigacao.addDomainPair(15, "Telefone mudou de Ramo");
		domains.put(statusLigacao.getDomainId(), statusLigacao);
		
		Domain nivelComercial = new Domain("NIVEL_COMERCIAL");
		nivelComercial.addDomainPair(PIVBusRelMainCFController.getNivelCod(nivel.SUSPECT),PIVBusRelMainCFController.getNivelDesc(nivel.SUSPECT));
		nivelComercial.addDomainPair(PIVBusRelMainCFController.getNivelCod(nivel.PROSPECT),PIVBusRelMainCFController.getNivelDesc(nivel.PROSPECT));
		nivelComercial.addDomainPair(PIVBusRelMainCFController.getNivelCod(nivel.FORECAST),PIVBusRelMainCFController.getNivelDesc(nivel.FORECAST));
		nivelComercial.addDomainPair(PIVBusRelMainCFController.getNivelCod(nivel.CUSTOMER),PIVBusRelMainCFController.getNivelDesc(nivel.CUSTOMER));

		domains.put(nivelComercial.getDomainId(), nivelComercial);

		Domain metaComercial = new Domain("META_COMERCIAL");
		metaComercial.addDomainPair(1, "Matriz BCG / Cliente estrela / valor acumulado de propostas 30 dias atr�s");
		metaComercial.addDomainPair(2, "Matriz BCG / Cliente d�vida / valor acumulado de propostas 30 dias atr�s");
		metaComercial.addDomainPair(3, "Matriz BCG / Cliente vaca leiteira / valor acumulado de propostas 30 dias atr�s");
		/*metaComercial.addDomainPair(4, "Matriz BCG / Cliente abacaxi / valor acumulado de propostas 30 dias atr�s");*/
		metaComercial.addDomainPair(5, "Valor total de propostas abertas (m�s)");
		metaComercial.addDomainPair(6, "Valor total de propostas fechadas (m�s)");
		
		domains.put(metaComercial.getDomainId(), metaComercial);
		
		Domain diaDaSemanaDomain = new Domain("DIAS_DA_SEMANA");
		diaDaSemanaDomain.addDomainPair(Calendar.SUNDAY, "Domingo");
		diaDaSemanaDomain.addDomainPair(Calendar.MONDAY, "Segunda-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.TUESDAY, "Ter�a-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.WEDNESDAY, "Quarta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.THURSDAY, "Quinta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.FRIDAY, "Sexta-Feira");
		diaDaSemanaDomain.addDomainPair(Calendar.SATURDAY, "Sab�do");

		domains.put(diaDaSemanaDomain.getDomainId(), diaDaSemanaDomain);

		Domain paisDomain = new Domain("PAIS");
		paisDomain.addDomainPair("BRASIL", "BRASIL");

		Domain estadoDomain = new Domain("ESTADO");
		estadoDomain.addDomainPair("AC", "Acre");
		estadoDomain.addDomainPair("AL", "Alagoas");
		estadoDomain.addDomainPair("AP", "Amap�");
		estadoDomain.addDomainPair("AM", "Amazonas");
		estadoDomain.addDomainPair("BA", "Bahia");
		estadoDomain.addDomainPair("CE", "Cear�");
		estadoDomain.addDomainPair("DF", "Distrito Federal");
		estadoDomain.addDomainPair("ES", "Esp�rito Santo");
		estadoDomain.addDomainPair("GO", "Goi�s");
		estadoDomain.addDomainPair("MA", "Maranh�o");
		estadoDomain.addDomainPair("MT", "Mato Grosso");
		estadoDomain.addDomainPair("MS", "Mato Grosso do Sul");
		estadoDomain.addDomainPair("MG", "Minas Gerais");
		estadoDomain.addDomainPair("PA", "Par�");
		estadoDomain.addDomainPair("PB", "Para�ba");
		estadoDomain.addDomainPair("PR", "Paran�");
		estadoDomain.addDomainPair("PE", "Pernambuco");
		estadoDomain.addDomainPair("PI", "Piau�");
		estadoDomain.addDomainPair("RJ", "Rio de Janeiro");
		estadoDomain.addDomainPair("RN", "Rio Grande do Norte");
		estadoDomain.addDomainPair("RS", "Rio Grande do Sul");
		estadoDomain.addDomainPair("RO", "Rond�nia");
		estadoDomain.addDomainPair("RR", "Roraima");
		estadoDomain.addDomainPair("SC", "Santa Catarina");
		estadoDomain.addDomainPair("SP", "S�o Paulo");
		estadoDomain.addDomainPair("SE", "Sergipe");
		estadoDomain.addDomainPair("TO", "Tocantis");

		domains.put(paisDomain.getDomainId(), paisDomain);

		domains.put(estadoDomain.getDomainId(), estadoDomain);

		Properties props = new Properties();

		// 'Universal'
		props.setProperty("id", "C�d.");
		props.setProperty("dataregistro", "Data de inclus�o");
		props.setProperty("data_registro", "Data de inclus�o");
		props.setProperty("ativo", "Ativo ?");
		props.setProperty("cep", "Cep");
		props.setProperty("pais", "Pais");
		props.setProperty("cidade", "Cidade");
		props.setProperty("estado", "estado");
		props.setProperty("bairro", "Bairro");
		props.setProperty("logradouro", "Logradouro");
		props.setProperty("endnumero", "Numero");
		props.setProperty("endreferencia", "Refer�ncia");
		props.setProperty("endcomplemento", "Complemento");
		props.setProperty("cnpjcpf", "CPNJ/CPF");
		props.setProperty("pjuridica", "P. Juridica?");
		props.setProperty("razaosocial", "Raz�o social");
		props.setProperty("nomefantasia", "Nome fantasia");
		props.setProperty("email", "E-mail");
		props.setProperty("website", "Website");
		props.setProperty("nomeimagem", "Nome imagem");
		props.setProperty("nome", "Nome");
		props.setProperty("telefone", "Telefone");
		props.setProperty("celular", "Celular");
		props.setProperty("site", "Website");
		props.setProperty("email", "Email");
		props.setProperty("funcao", "Fun��o");
		props.setProperty("responsavel", "Respons�vel");
		props.setProperty("descricao", "Descri��o");
		props.setProperty("valcompra", "Valor de compra");
		props.setProperty("valvenda", "Valor de venda");
		props.setProperty("tag", "Tag");
		props.setProperty("login", "Login");
		props.setProperty("senha", "Senha");
		props.setProperty("usuariovendedor", "Vendedor?");
		props.setProperty("anotacao", "Anota��o");
		props.setProperty("nome_arquivo_voz", "Nome do arquivo de voz");
		props.setProperty("cnpj", "Cnpj");
		props.setProperty("diadasemana", "Dia da semana");
		props.setProperty("horariomanhainicio", "Turno da manh� in�cio");
		props.setProperty("horariomanhafim", "Turno da manh� fim");
		props.setProperty("horariotardeinicio", "Turno da tarde in�cio");
		props.setProperty("horariotardefim", "Turno da tarde fim");
		props.setProperty("horarionoiteinicio", "Turno da noite in�cio");
		props.setProperty("horarionoitefim", "Turno da noite fim");
		props.setProperty("idnivelcomercial", "Nivel comercial");
		props.setProperty("valvenda","Valor de venda");
		props.setProperty("valdesconto","(R$) Desconto");
		props.setProperty("porcdesconto","(%) Desconto");
		props.setProperty("qtditens","Qtd. de itens");
		props.setProperty("valtotal","Valor total");
		props.setProperty("valortotal","Valor total");
		props.setProperty("vendaefetivada","Situa��o");
		props.setProperty("publico_alvo","P�blico alvo");
		props.setProperty("patrocinador","Patrocinador");
		props.setProperty("status","Status");
		props.setProperty("nome_campanha","Nome da campanha");
		props.setProperty("descricao_campanha","Descri��o");
		props.setProperty("item","Item");
		props.setProperty("valsubtotal", "Valor sub-total");

		// Tabela - Endere�o
		props.setProperty("endereco.cep", "Cep");
		props.setProperty("endereco.pais", "Pais");
		props.setProperty("endereco.cidade", "Cidade");
		props.setProperty("endereco.estado", "estado");
		props.setProperty("endereco.bairro", "Bairro");
		props.setProperty("endereco.logradouro", "Logradouro");

		// Tabela - Cliente
		props.setProperty("cliente.id", "C�d. Cliente");
		props.setProperty("cliente.cep", "Cep");
		props.setProperty("cliente.endnumero", "Numero");
		props.setProperty("cliente.endreferencia", "Refer�ncia");
		props.setProperty("cliente.endcomplemento", "Complemento");
		props.setProperty("cliente.cnpjcpf", "CPNJ/CPF");
		props.setProperty("cliente.pjuridica", "P. juridica?");
		props.setProperty("cliente.razaosocial", "Raz�o social");
		props.setProperty("cliente.nomefantasia", "Nome fantasia");
		props.setProperty("cliente.email", "E-mail");
		props.setProperty("cliente.website", "Website");
		props.setProperty("cliente.nomeimagem", "Nome imagem");
		props.setProperty("cliente.dataregitro", "Data de registro");

		// Tabela - Contato
		props.setProperty("contato.id", "C�d. Contato");
		props.setProperty("contato.nome", "Nome");
		props.setProperty("contato.telefone", "Telefone");
		props.setProperty("contato.celular", "Celular");
		props.setProperty("contato.site", "Website");
		props.setProperty("contato.email", "Email");
		props.setProperty("contato.funcao", "Fun��o");
		props.setProperty("contato.responsavel", "Respons�vel?");
		props.setProperty("contato.dataregitro", "Data de registro");

		// Especial - Grid tempor�rio para Liga��o
		props.setProperty("tipo", "Tipo");
		props.setProperty("numero", "N�mero");
		props.setProperty("duracao", "Dura��o da Liga��o");
		
		// Tabela - Categoria
		props.setProperty("categoria.id", "C�d. categoria");
		props.setProperty("categoria.nome", "Nome");
		props.setProperty("categoria.descricao", "Descri��o");
		props.setProperty("categoria.dataregitro", "Data de registro");

		// Tabela - Produto
		props.setProperty("produto.id", "C�d. produto");
		props.setProperty("produto.nome", "Nome");
		props.setProperty("produto.descricao", "Descri��o");
		props.setProperty("produto.valcompra", "Valor de compra");
		props.setProperty("produto.valvenda", "Valor de venda");
		props.setProperty("produto.nomeimagem", "Nome imagem");
		props.setProperty("produto.dataregitro", "Data de registro");;

		// Tabela - Tag
		props.setProperty("tag.id", "C�d. Tag");
		props.setProperty("tag.nome", "Nome");
		props.setProperty("tag.descricao", "Descri��o");
		props.setProperty("tag.dataregistro", "Data de registro");

		// Tabela - Usuario
		props.setProperty("usuario.id", "C�d. usu�rio");
		props.setProperty("usuario.nome", "Nome");
		props.setProperty("usuario.login", "Login");
		props.setProperty("usuario.senha", "Senha");
		props.setProperty("usuario.usuariovendedor", "Vendedor?");
		props.setProperty("usuario.dataregistro", "Data de registro");

		// Tabela - Vendedor
		props.setProperty("vendedor.id", "C�d. Vendedor");
		props.setProperty("vendedor.nome", "Nome");
		props.setProperty("vendedor.cep", "Cep");
		props.setProperty("vendedor.endnumero", "N�mero");
		props.setProperty("vendedor.endcomplemento", "Complemento");
		props.setProperty("vendedor.dataregistro", "Data de registro");

		// Tabela - NivelComercialRelacionamento
		props.setProperty("relacionamento.idnivelcomercial", "Nivel comercial");
		props.setProperty("relacionamento.completado", "Completo?");
		props.setProperty("relacionamento.ativo", "Ativo?");
		props.setProperty("relacionamento.dataregistro", "Data de registro");

		// Tabela - GeoPosicionamentoCliente
		props.setProperty("geoposicionamento.id", "");
		props.setProperty("geoposicionamento.latitude", "Latitude");
		props.setProperty("geoposicionamento.longitude", "Longitude");

		// Tabela - AnotacaoDeDadosCliente
		props.setProperty("anotacao.id", "C�d. Anota��o");
		props.setProperty("anotacao.anotacao", "Anota��o");
		props.setProperty("anotacao.nome_arquivo_voz", "Nome do arquivo de voz");
		props.setProperty("anotacao.dataregistro", "Data de registro");

		// Tabela - Configura��es Locais
		props.setProperty("conflocal.id", "");
		props.setProperty("conflocal.razaosocial", "Raz�o Social");
		props.setProperty("conflocal.nomefantasia", "Nome Fantasia");
		props.setProperty("conflocal.email", "E-mail");
		props.setProperty("conflocal.website", "Website");
		props.setProperty("conflocal.cnpj", "Cnpj");
		props.setProperty("conflocal.nomeimagem", "Nome Imagem");
		props.setProperty("conflocal.cep", "Cep");
		props.setProperty("conflocal.endnumero", "Numero");
		props.setProperty("conflocal.endreferencia", "Refer�ncia");
		props.setProperty("conflocal.endcomplemento", "Complemento");
		props.setProperty("conflocal.cnpjcpf", "CPNJ/CPF");
		props.setProperty("conflocal.dataregitro", "Data de registro");
		props.setProperty("conflocal.latitude", "Latitude");
		props.setProperty("conflocal.longitude", "Longitude");

		// Tabela - HorarioFuncionario
		props.setProperty("turno.diadasemana", "Dia da semana");
		props.setProperty("turno.horariomanhainicio", "Turno da manh� in�cio");
		props.setProperty("turno.horariomanhafim", "Turno da manh� fim");
		props.setProperty("turno.horariotardeinicio", "Turno da tarde in�cio");
		props.setProperty("turno.horariotardefim", "Turno da tarde fim");
		props.setProperty("turno.horarionoiteinicio", "Turno da noite in�cio");
		props.setProperty("turno.horarionoitefim", "Turno da noite fim");

		// Tabela - VendasFilho
		props.setProperty("item.id","C�d. Item");
		props.setProperty("item.vendaPai","C�d. da venda");
		props.setProperty("item.produto.nome","Nome do produto");
		props.setProperty("item.valvenda","Valor de venda");
		props.setProperty("item.valdesconto","(%) desconto");
		props.setProperty("item.qtditens","Qtd. de itens");
		props.setProperty("item.valtotal","Valor total");

		// Tabela - TipoCampanha
		props.setProperty("tipo.id","C�d. Tipo de Campanha");
		props.setProperty("tipo.nome","Tipo");
		props.setProperty("tipo.ativo","Ativo?");
		props.setProperty("tipo.data_registro","Data de registro");
		
		// Tabela - CampanhaPai
		props.setProperty("campanha.id","C�d. Campanha");
		props.setProperty("campanha.nome_campanha","Nome");
		props.setProperty("campanha.descricao_campanha","Descri��o");
		props.setProperty("campanha.publico_alvo","P�blico alvo");
		props.setProperty("campanha.patrocinador","Patrocinador");
		props.setProperty("campanha.status","Status");
		props.setProperty("campanha.data_registro","Data de registro");
		
  		ResourcesFactory resource = new BrazilianPortugueseOnlyResourceFactory(props, false);
  		
		resource.getResources().getDictionary().setProperty(Consts.EQ,"Igual a");
		resource.getResources().getDictionary().setProperty(Consts.GE,"Maior ou igual que");
		resource.getResources().getDictionary().setProperty(Consts.GT,"Maior que");
		resource.getResources().getDictionary().setProperty(Consts.IS_NOT_NULL,"Esta preenchido");
		resource.getResources().getDictionary().setProperty(Consts.IS_NULL,"N�o esta preenchido");
		resource.getResources().getDictionary().setProperty(Consts.LE,"Menor ou igual que");
		resource.getResources().getDictionary().setProperty(Consts.LIKE,"Cont�m");
		resource.getResources().getDictionary().setProperty(Consts.LT,"Menor que");
		resource.getResources().getDictionary().setProperty(Consts.NEQ,"N�o igual �");
		resource.getResources().getDictionary().setProperty(Consts.IN,"Cont�m os valores");
		resource.getResources().getDictionary().setProperty(Consts.ASC_SORTED,"Crescente");
  		resource.getResources().getDictionary().setProperty(Consts.DESC_SORTED,"Decrescente");
  		resource.getResources().getDictionary().setProperty(Consts.NOT_IN,"N�o cont�m os valores");
  				
		ClientSettings clientSettings = new ClientSettings(
				resource,
				domains);

		ClientSettings.BACKGROUND = "fundo_rgb_1920x1080.jpg";
		ClientSettings.BACK_IMAGE_DISPOSITION = Consts.BACK_IMAGE_STRETCHED;
		ClientSettings.HEADER_FONT = new Font("Arial", Font.PLAIN, 14);
		ClientSettings.TREE_BACK = null;
		ClientSettings.VIEW_MANDATORY_SYMBOL = true;
		ClientSettings.FILTER_PANEL_ON_GRID = true;
		ClientSettings.SHOW_FILTER_SYMBOL = true;
		ClientSettings.ASK_BEFORE_CLOSE = true;
		ClientSettings.GRID_PROFILE_MANAGER = new FileGridProfileManager();
		ClientSettings.LOOKUP_FRAME_CONTENT = LookupController.GRID_AND_FILTER_FRAME;
		ClientSettings.STORE_INTERNAL_FRAME_PROFILE = true;
		ClientSettings.AUTO_EXPAND_TREE_MENU = true;
		ClientSettings.SEARCH_ADDITIONAL_ROWS = true;
		ClientSettings.SELECT_DATA_IN_EDITABLE_GRID = false;
		ClientSettings.SHOW_TREE_MENU_ROOT = false;
		ClientSettings.SHOW_EVENT_QUEUE_EXCEPTIONS = true;
		ClientSettings.RELOAD_LAST_VO_ON_FORM = true;
		ClientSettings.ALLOW_OR_OPERATOR = false;
//		ClientSettings.FILTER_PANEL_ON_GRID_POLICY = Consts.FILTER_PANEL_ON_GRID_USE_CLOSE_BUTTON;
		/*ClientSettings.AUTO_FIT_COLUMNS = true;*/
		
//		ClientSettings.BACKGROUND = "background-logoplayiv_2560x1600.jpg";
//		ClientSettings.BACK_IMAGE_DISPOSITION = Consts.BACK_IMAGE_STRETCHED;
//		/*ClientSettings.VIEW_BACKGROUND_SEL_COLOR = true;*/
//		ClientSettings.TREE_BACK = null;
//		ClientSettings.VIEW_MANDATORY_SYMBOL = true;
//		ClientSettings.FILTER_PANEL_ON_GRID = true;
//		ClientSettings.SHOW_FILTER_SYMBOL = true;
//		ClientSettings.ASK_BEFORE_CLOSE = true;
//		ClientSettings.GRID_PROFILE_MANAGER = new FileGridProfileManager();
//		ClientSettings.LOOKUP_FRAME_CONTENT = LookupController.GRID_AND_FILTER_FRAME;
//		ClientSettings.STORE_INTERNAL_FRAME_PROFILE = true;
//		ClientSettings.AUTO_EXPAND_TREE_MENU = true;
//		ClientSettings.SEARCH_ADDITIONAL_ROWS = true;
//		ClientSettings.SELECT_DATA_IN_EDITABLE_GRID = true;
		/*ClientSettings.AUTO_FIT_COLUMNS = true;*/

		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "net.infonode.gui.laf.InfoNodeLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.birosoft.liquid.LiquidLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "ch.randelshofer.quaqua.QuaquaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.l2fprod.gui.plaf.skin.SkinLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceMistSilverLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.jvnet.substance.skin.SubstanceModerateLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "net.sourceforge.mlf.metouia.MetouiaLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.fife.plaf.Office2003.Office2003LookAndFeel";
		// ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		// "org.fife.plaf.OfficeXP.OfficeXPLookAndFeel";

		/*ClientSettings.LOOK_AND_FEEL_CLASS_NAME =
		"org.fife.plaf.VisualStudio2005.VisualStudio2005LookAndFeel";*/

		// Realiza a checagem das pastas padr�o a serem utilizadas
		// nas funcionalidades presentes no PlayIV

		frameSplash.addText("Realizando a checagem de pastas internas do PlayIV...");
		System.out.println("Realizando a checagem de pastas internas do PlayIV...");

		gerenciarDiretorios.checkOrganizacaoPastas();

		// ativa objeto respons�vel por gerenciar os logs
		
		frameSplash.addText("Inicializando o sistema de logs do PlayIV...");
		System.out.println("Inicializando o sistema de logs do PlayIV...");
		
		logMngr = PIVLogSettings.getInstance();
		logMngr.iniciar();
		
		frameSplash.addText("Opera��o completada, seja bem vindo :)");
		System.out.println("Opera��o completada, seja bem vindo :)");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			PIVLogSettings.getInstance().error(e.getMessage(),e);
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}
		
		frameSplash.dispose();
			
		loginSuccessful(null);
		
		frameSplash.setBarraDeProgressoVal(100);
		frameSplash.dispose();
		
		// ---------------
		// AREA EDITADA PARA TESTES
		
//		PIVLoginPanel d = new PIVLoginPanel(this);
		/*MDIFrame mdi = new MDIFrame(this);
		PIVSalesStatsGFController controller = new PIVSalesStatsGFController(null);*/
		/*PIVSalesTargetGFController controller = new PIVSalesTargetGFController(null );*/
		
		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = pdao.getSession();

		PIVLocalConfModel vo = (PIVLocalConfModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();

		if (vo == null) {
			vo = new PIVLocalConfModel();
//			vo.setVoip_dominio("brvoz.net.br");
//			vo.setVoip_proxy("brvoz.net.br");
//			vo.setVoip_sipport(5060);
			vo.setEmail_server_smpt_host("smtp.gmail.com");
			vo.setEmail_server_smpt_port(465);
			vo.setEmail_server_ssl(true);
			pdao.save(vo);
		}
		
		// ENVIAR EMAIL DA CONEX�O
		
//        InetAddress ip;
//        String hostname;
//        try {
//            ip = InetAddress.getLocalHost();
//            hostname = ip.getHostName();           
//            
//            URL whatismyip = new URL("http://checkip.amazonaws.com");
//            BufferedReader in = new BufferedReader(new InputStreamReader(
//                            whatismyip.openStream()));
//
//            String ipExterno = in.readLine(); //you get the IP as a String
//            ipExterno = ipExterno == null ? "" : ipExterno;
//            
//            String userName = System.getProperty("user.name");
//            
//            final HtmlEmail email = new HtmlEmail();
//    		email.setHostName("smtp.gmail.com");
//    		email.setSmtpPort(465);
//    		email.setSSL(true);
//            email.setAuthentication("testeplayiv@gmail.com", "sltmmzzmremkkmah");
//            
//            email.setFrom("testeplayiv@gmail.com");
//            email.setSubject("ALGU�M CONECTOU NO PLAYIV: HOSTNAME [" + hostname + "], IP [" + ip + "], IP EXTERNO [" + ipExterno + "], USU�RIO [" + userName + "]");
//            email.setHtmlMsg("<p></p>");
//            email.addTo("mgsantos091@gmail.com");
//            
//            new Thread(new Runnable()
//	        {
//	          public void run()
//	          {
//	        	  try {
//						email.send();
//					} catch (EmailException e) {}
//	          }
//	        }).start();
//        } catch (UnknownHostException e) {
//        } catch (EmailException e) {
//        } catch (MalformedURLException e1) { 
//        } catch (IOException e1) { }
		
		// ----
		
		session = pdao.getSession();
		baseSQL = "from org.playiv.com.mvc.model.PIVClientModel as Cliente";
		PIVClientModel cliente = (PIVClientModel) session.createQuery(baseSQL).setMaxResults(1).uniqueResult();
		session.close();
		if(cliente == null) { // base zerada
			// carrega lista via csv
			File f = new File("load/bd.csv");
			BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ";";
			try {
				br = new BufferedReader(new FileReader(f));
				while ((line = br.readLine()) != null) {
					String[] columns = line.split(cvsSplitBy);
					//CNPJ;RazaoSocial;NomeFantasia;TipoEmpresa;DDD1;FIXO1;DDD2;FIXO2;DDD3;FIXO3;DDD_CEL;CELULAR;Nome_Contato;DDD_Contato;Telefone_Contato
					String cnpj = columns[0];
					String razaoSocial = columns[1];
					String nomeFantasia = columns[2];
					String telefone = columns[5].equals("") ? "" : "0" + columns[4] + columns[5];
					String contatoNome = columns[12];
					String contatoTel = columns[14].equals("") ? "" : "0" + columns[13] + columns[14];
					
					cliente = new PIVClientModel();
					cliente.setCnpjcpf(cnpj);
					cliente.setRazaosocial(razaoSocial);
					cliente.setNomefantasia(nomeFantasia);
					cliente.setPjuridica(true);
					cliente.setTelefone(telefone);
					cliente.setAtivo(true);
					
					pdao.save(cliente);
					
					if(contatoNome != null && !contatoNome.trim().equals("")) {
						PIVContactModel contato = new PIVContactModel();
						contato.setNome(contatoNome);
						contato.setTelefone(contatoTel);
						contato.setCliente(cliente);
						contato.setAtivo(true);
						pdao.save(contato);
					}
					
				}
			} catch (FileNotFoundException e) {
//				e.printStackTrace();
			} catch (IOException e) {
//				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
//						e.printStackTrace();
					}
				}
			}
		}
		
	}

	@Override
	public int getMaxAttempts() {
		System.out.println("getMaxAttempts");
		return 5;
	}

	@Override
	public boolean authenticateUser(Map loginInfo) throws Exception {
//		if ("rico".equalsIgnoreCase((String)loginInfo.get("username")) &&
//	        "rico".equalsIgnoreCase((String)loginInfo.get("password")))
	      return true;
//	    else
//	      return false;
	}

	@Override
	public void loginSuccessful(Map loginInfo) {
		MDIFrame mdi = new MDIFrame(this);
		mdi.setIconImage(Toolkit.getDefaultToolkit().getImage(
				PIVMainMenuCFView.class.getResource("/images/logo/logo.png")));
		
		this.frameSplash.setBarraDeProgressoVal(90);
		
		avisoAgendaThread = new AvisoAgendaThread();
		avisoAgendaThread.start();
		
		System.out.println("loginSuccessful");
	}

	@Override
	public void afterMDIcreation(MDIFrame frame) {
		MDIFrame.addStatusComponent(new Clock());
//		PIVSipVoipFrame voipFrame = PIVSipVoipFrame.getInstance();
//		MDIFrame.addStatusComponent(voipFrame);
	}

	@Override
	public String getMDIFrameTitle() {
		return "NewCRM - Um software do NewCode";
	}

	@Override
	public int getExtendedState() {
		return JFrame.MAXIMIZED_BOTH;
	}

	@Override
	public String getAboutText() {
		return "NewCRM - 2016: Vers�o de Demonstra��o - Dispon�vel at� 15 de Abril de 2016.";
	}

	@Override
	public String getAboutImage() {
		return null;
	}

	@Override
	public void stopApplication() {
		pdao.closeConnection();
		logMngr.encerrar();
		System.exit(0);
	}

	@Override
	public boolean viewChangeLanguageInMenuBar() {
		return false;
	}

	@Override
	public boolean viewLoginInMenuBar() {
		return true;
	}

	@Override
	public JDialog viewLoginDialog(JFrame parentFrame) {
		PIVLoginPanel d = new PIVLoginPanel(this);
		return d;
	}

	@Override
	public boolean viewFunctionsInTreePanel() {
		return true;
	}

	@Override
	public boolean viewFunctionsInMenuBar() {
		return true;
	}

	@Override
	public ClientFacade getClientFacade() {
		return this.menufacade;
	}

	@Override
	public DefaultTreeModel getApplicationFunctions() {
		
		DefaultMutableTreeNode root = new OpenSwingTreeNode();

		DefaultTreeModel model = new DefaultTreeModel(root);

		ApplicationFunction principal = new ApplicationFunction("Principal", null);
		ApplicationFunction cadastro = new ApplicationFunction("Cadastros",null);
//		ApplicationFunction relatorio = new ApplicationFunction("Relat�rios",null);		
		ApplicationFunction configuracao = new ApplicationFunction("Configura��es",null);
		
		ApplicationFunction gerVends = null;
		if(isGerente)
			gerVends = new ApplicationFunction("Gerenciamento de Vendedores" , "gerVends" , "gerenciamento_relcomercial/logo/gerrelcomercial-icone_16x16.png" , "gerVends" );
		
		ApplicationFunction principGerRelCom = new ApplicationFunction(
				"Relacionamento Comercial", "gerRelCom", "gerenciamento_relcomercial/logo/gerrelcomercial-icone_16x16.png", "gerRelCom");

		ApplicationFunction cadastroCliente = new ApplicationFunction(
				"Clientes", "cadCliente", "cadastros/cliente/agencia_16x16.png", "cadCliente");

		ApplicationFunction cadastroUsuario = new ApplicationFunction("Usu�rios", "cadUsuario", "cadastros/usuario/usuario_16x16.png", "cadUsuario");

		ApplicationFunction cadastroProduto = new ApplicationFunction("Produtos", "cadProduto", "cadastros/produto/produto_16x16.png", "cadProduto");

		ApplicationFunction cadastroCategoria = new ApplicationFunction("Categorias de Produtos", "cadCategoria", "cadastros/categoria/categoria_16x16.png", "cadCategoria");

		ApplicationFunction configuracaoConfLocal = new ApplicationFunction("Configura��es Locais", "gerConfLocal", "configuracao/configuracoes_locais/configuracao_16x16.png", "gerConfLocal");

		ApplicationFunction cadastroCampanha = new ApplicationFunction("Campanhas de Marketing", "cadCampanha", "cadastros/campanha/campanha_16x16.png", "cadCampanha");
		
		ApplicationFunction cadastroCategoriaCampanha = new ApplicationFunction("Categorias de Campanhas", "cadCategoriaCampanha", "cadastros/categoria/categoria_16x16.png", "cadCategoriaCampanha");
		
		ApplicationFunction cadastroTransportadora = new ApplicationFunction("Transportadoras", "cadTransportadora", "cadastros/transportadora/transport_16x16.png", "cadTransportadora");
		
		ApplicationFunction cadastroFormaPagamento = new ApplicationFunction("Forma de Pagamento","cadFormaPagamento","cadastros/pagamento/forma_16x16.png","cadFormaPagamento");
		
//		ApplicationFunction cadastroEstagioVenda = new ApplicationFunction("Est�gio de Venda","cadEstagioVenda","","cadEstagioVenda");
		
		ApplicationFunction gerenciaNaoVendas = null;		
		
//		ApplicationFunction relVendasCli = new ApplicationFunction("Vendas por Clientes","relVendasCli","relatorios/report_16x16.png","relVendasCli");
//		ApplicationFunction relVendasVend = new ApplicationFunction("Vendas por Vendedor","relVendasVend","relatorios/report_16x16.png","relVendasVend");
//		ApplicationFunction relCliVend = new ApplicationFunction("Top clientes do Vendedor","relCliVend","relatorios/report_16x16.png","relCliVend");
//		ApplicationFunction relVendasProd = new ApplicationFunction("Vendas por Produtos","relVendasProd","relatorios/report_16x16.png","relVendasProd");
		
		if(isGerente)
			gerenciaNaoVendas = new ApplicationFunction("Vendas n�o conclu�das","gerNaoVendas","","gerNaoVendas");
		
		principal.add(principGerRelCom);
		if(gerVends!=null)
			principal.add(gerVends);
		root.add(principal);
		
		cadastro.add(cadastroCliente);
		cadastro.add(cadastroProduto);
		cadastro.add(cadastroCategoria);
		cadastro.add(cadastroUsuario);
		cadastro.add(cadastroCampanha);
		cadastro.add(cadastroCategoriaCampanha);
		cadastro.add(cadastroTransportadora);
		cadastro.add(cadastroFormaPagamento);
//		cadastro.add(cadastroEstagioVenda);
		if(gerenciaNaoVendas!=null)
			cadastro.add(gerenciaNaoVendas);
		
		root.add(cadastro);

//		relatorio.add(relVendasCli);
//		relatorio.add(relVendasVend);
//		relatorio.add(relCliVend);
//		relatorio.add(relVendasProd);
		
//		root.add(relatorio);
		
		configuracao.add(configuracaoConfLocal);
		root.add(configuracao);

		return model;

	}

	@Override
	public boolean viewOpenedWindowIcons() {
		return true;
	}

	@Override
	public boolean viewFileMenu() {
		return true;
	}

}