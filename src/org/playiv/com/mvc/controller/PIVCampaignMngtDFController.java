package org.playiv.com.mvc.controller;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVCampaignModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.view.PIVCampaignMngtDFView;
import org.playiv.com.mvc.view.PIVCampaignMngtGFView;
import org.playiv.com.mvc.view.PIVClientMngtDFView;
import org.playiv.com.mvc.view.PIVClientMngtGFView;

import br.com.caelum.stella.MessageProducer;
import br.com.caelum.stella.ResourceBundleMessageProducer;
import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import br.com.caelum.stella.validation.Validator;

/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVCampaignMngtDFController extends FormController implements
		IPIVFormController {

	private PIVCampaignMngtGFView gridFrame = null;
	private PIVCampaignMngtDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;

	public PIVCampaignMngtDFController(PIVCampaignMngtGFView gridFrame, Integer pk) {
		this.gridFrame = gridFrame;
		this.pk = pk;
		frame = new PIVCampaignMngtDFView(this, pk, pdao);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.EDIT);
			frame.getPanel().reload();
			/*frame.getJpContato().getGrid().reloadData();*/
		} else {
			frame.getPanel().setMode(Consts.INSERT);
		}
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			/*
			 * int row = gridFrame.getGrid().getSelectedRow();
			 * 
			 * if (row != -1) { PIVClienteModel gridVO = (PIVClienteModel)
			 * gridFrame.getGrid() .getVOListTableModel().getObjectForRow(row);
			 * pk = gridVO.getId(); }
			 */

			String baseSQL = "from Campanha in class org.playiv.com.mvc.model.PIVCampaignModel where Campanha.id = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			PIVCampaignModel vo = (PIVCampaignModel) session.createQuery(baseSQL)
					.uniqueResult();

			session.close();

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}

	}

	@Override
	public boolean validateControl(String attributeName, Object oldValue,
			Object newValue) {
		return true;
	}

	/**
	 * Method called by the Form panel to insert new data.
	 * 
	 * @param newValueObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {
			PIVCampaignModel vo = (PIVCampaignModel) newPersistentObject;
			pdao.save(vo);
			this.gridFrame.reloadData();
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVCampaignModel vo = (PIVCampaignModel) persistentObject;
			pdao.update(vo);
			this.gridFrame.reloadData();
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}

}