package org.playiv.com.mvc.controller;

import java.io.File;
import java.util.ArrayList;
//import java.util.Calendar; 

import org.hibernate.Session;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel;
import org.playiv.com.mvc.view.PIVLocalConfDFView;

/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVLocalConfDFController extends FormController implements
		IPIVFormController {

	private PIVLocalConfDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;
	
	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc
			.getInstance();

	public PIVLocalConfDFController() {
		frame = new PIVLocalConfDFView(this);
		MDIFrame.add(frame);
		frame.getPanel().setMode(Consts.EDIT);
		frame.getPanel().reload();
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
			Session session = pdao.getSession();

			PIVLocalConfModel vo = (PIVLocalConfModel) session.createQuery(
					baseSQL).uniqueResult();

			session.close();

			if (vo == null) {
				vo = new PIVLocalConfModel();
				pdao.save(vo);
			} else {
				if(vo.getNomeimagem()!=null&&!vo.getNomeimagem().equals(""))
					frame.getJpConfLocal().setEmpIcon(vo.getNomeimagem());
			}
			
			baseSQL = "from WorkShift in class org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel";
			
			session = pdao.getSession(); // obtain a JDBC connection and
										// instantiate a new Session
			ArrayList<PIVLocalConfWorkShiftModel> workShift = (ArrayList<PIVLocalConfWorkShiftModel>) session.createQuery(baseSQL).list();
			
			session.close();
			
//			if(workShift.size()==0) {
//				
//				PIVLocalConfWorkShiftModel shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.SUNDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.MONDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.TUESDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.WEDNESDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.THURSDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.FRIDAY);
//		        pdao.save(shift);
//		        
//		        shift = new PIVLocalConfWorkShiftModel();
//		        shift.setDiadasemana(Calendar.SATURDAY);
//		        pdao.save(shift);
//
//			}

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {

			PIVLocalConfModel vo = (PIVLocalConfModel) persistentObject;
			/*vo.setNomeimagem(frame.getJpConfLocal().getFileNameIcon());*/
			
			PIVAddressModel endereco = vo.getEndereco();
			if (!(endereco == null))
				if (!(endereco.getCep() == null)) {
					PIVAddressModel findEndereco = getEndereco(endereco.getCep());
					if(findEndereco==null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				}
				else
					vo.setEndereco(null);
			
			String filename = this.frame.getJpConfLocal().getFileNameIcon();			

			if(filename!=null&&!filename.equals("")) {
			
				// copia o logo para o servidor
				
				String fullpathfile = this.frame.getJpConfLocal().getFullPathIcon();
				
				int lastFileSep = fullpathfile.lastIndexOf(File.separator) + 1;
				int lastDot = fullpathfile.lastIndexOf('.');
	
				// Set to end of file when no file extension exists
				if (lastDot == -1) {
					lastDot = fullpathfile.length();
				}
				
				filename = fullpathfile.substring(lastFileSep, lastDot);
				String fileExt = fullpathfile.substring(lastDot, fullpathfile.length());
	
				// Atualiza o nome do arquivo no bd
//				String fileNamePlusExt = filename + fileExt;
				
				gerenciarDiretorio.copiaLogoDirDefault(fullpathfile);
				
				
				
				vo.setNomeimagem("logo_empresa" + fileExt);
			
			}
			
			pdao.update(vo);
			
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
//		if (currentMode == Consts.INSERT) {
//			frame.getGrid().clearData();
//			frame.setEnableGridButtonsWorkShift(false);
//		} else {
//			frame.setEnableGridButtonsWorkShift(true);
//		}
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}
	
	private PIVAddressModel getEndereco(String cep) {
		String baseSQL = "from Endereco in class org.playiv.com.mvc.model.PIVAddressModel where Endereco.cep = '"
				+ cep + "'";
		Session session = pdao.getSession(); // obtain a JDBC connection and
												// instantiate a new Session
		PIVAddressModel vo = (PIVAddressModel) session.createQuery(baseSQL).uniqueResult();
		session.close();
		return vo;

	}

}