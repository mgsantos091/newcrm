package org.playiv.com.mvc.controller;

import java.sql.Timestamp;
import java.util.ArrayList;

import javax.swing.JTextField;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVClientFunc;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVBusRelModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.swing.general.PIVBusRelSalesPerformedFrame;

public class PIVBusRelSalesPerformedController {

	private PIVBusRelSalesPerformedFrame view;

	private JTextField txRazaoSocialReference;
	private JTextField txNomeFantasiaReference;
	private JTextField txNomeContatoReference;
	private JTextField txTempoFinalReference;
	private JTextField txTempoMedioReference;
	private JTextField txQtdReunioesReference;
	private JTextField txQtdEmailsReference;
	private JTextField txQtdTelefonemasReference;

	private PIVClientModel cliente;

	private PIVDao pdao = PIVDao.getInstance();
	private PIVUserSession sessao = PIVUserSession.getInstance();

	private String defaultValue = String.format(String.format("%%0%dd", 30), 0).replace("0", "-");
	
	public PIVBusRelSalesPerformedController( PIVClientModel cliente ) {

		PIVBusRelSalesPerformedFrame view = new PIVBusRelSalesPerformedFrame( );
		this.view = view;
	
		this.txRazaoSocialReference = view.getTxRazaoSocial();
		this.txNomeFantasiaReference = view.getTxNomeContato();
		this.txNomeContatoReference = view.getTxNomeContato();
		this.txTempoFinalReference = view.getTxTempoFinal();
		this.txTempoMedioReference = view.getTxTempoMedio();
		this.txQtdReunioesReference = view.getTxQtdReunioes();
		this.txQtdEmailsReference = view.getTxQtdEmails();
		this.txQtdTelefonemasReference = view.getTxQtdTelefonemas();

		this.cliente = cliente;
		
		this.loadData();

	}

	private void loadData() {

		long hora = 60 * 60 * 1000L;

		// Atribui raz�o social
		String razaoSocial = this.getValor(this.cliente.getRazaosocial());
		this.txRazaoSocialReference.setText(razaoSocial);

		// Atribui nome fantasia
		String nomeFantasia = this.getValor(this.cliente.getNomefantasia());
		this.txNomeFantasiaReference.setText(nomeFantasia);

		// Atribui nome do contato 
		PIVContactModel contato = PIVClientFunc.getResponsavel(this.cliente);
		String contatoNome = contato == null || contato.getNome().equals("") ? this.defaultValue : contato.getNome();
		this.txNomeContatoReference.setText(contatoNome);

		// Atribui tempo final de venda
		int idVendedorLogado = this.sessao.getVendedorSessao().getId();
		int idcliente = this.cliente.getId();

		Session session = pdao.getSession();
		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento "
				+ "where Relacionamento.vendedor.id = '"
				+ idVendedorLogado
				+ "' and "
				+ "Relacionamento.cliente.id = '" + idcliente + "' "
				+ "order by Relacionamento.dataregistro ASC";

		@SuppressWarnings("unchecked")
		ArrayList<PIVBusRelModel> relcomercial = (ArrayList<PIVBusRelModel>) session
				.createQuery(baseSQL).list();

		// realiza calculo de tempo final da venda
		if(relcomercial.size()>=2) {
			
			PIVBusRelModel primeiraRelacao = relcomercial.get(0);
			PIVBusRelModel ultimaRelacao = relcomercial.get(relcomercial.size()-1);
			
			Timestamp dtPrimeiraRelacao = primeiraRelacao.getDataregistro();
			Timestamp dtUltimaRelacao = ultimaRelacao.getDataregistro();
			
			String qtdDias = (new Integer((int) ((dtUltimaRelacao.getTime() - dtPrimeiraRelacao.getTime() + hora) / (hora * 24)))).toString();				  	
			
			this.txTempoFinalReference.setText("venda finalizada em " + qtdDias + " dia(s) ");

		}
		
		// realiza calculo da media de tempo por camada
		
		session.close();
		
	}
	
	private String getValor(String valor) {
		return valor == null || valor.equals("") ? this.defaultValue : valor;
	}
	
}