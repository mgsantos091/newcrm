package org.playiv.com.mvc.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel;

public class PIVLocalConfWorkShiftController extends GridController implements
		GridDataLocator {

	private PIVDao pdao = PIVDao.getInstance();

	private GridControl grid;
	
	public PIVLocalConfWorkShiftController(GridControl grid) {
		this.grid = grid;
	}
	
	@Override
	public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {
		
		PIVLocalConfWorkShiftModel workShiftDay = (PIVLocalConfWorkShiftModel) grid.getVOListTableModel().getObjectForRow(rowNumber);
		
		// Realiza a valida��o dos dias organizados no turno
		
		Timestamp horariomanhainicio = workShiftDay.getHorariomanhainicio();
		Timestamp horariomanhafim = workShiftDay.getHorariomanhafim();

		Timestamp horariotardeinicio = workShiftDay.getHorariotardeinicio();
		Timestamp horariotardefim = workShiftDay.getHorariotardefim();

		Timestamp horarionoiteinicio = workShiftDay.getHorarionoiteinicio();
		Timestamp horarionoitefim = workShiftDay.getHorarionoitefim();
		
		Timestamp horario = new Timestamp(((java.util.Date) newValue).getTime());
		
		if(attributeName.equals("horariomanhainicio"))
			horariomanhainicio = horario;
		else if(attributeName.equals("horariomanhafim"))
			horariomanhafim = horario;
		else if(attributeName.equals("horariotardeinicio"))
			horariotardeinicio = horario;
		else if(attributeName.equals("horaritardefim"))
			horariotardefim = horario;
		else if(attributeName.equals("horarinoiteinicio"))
			horarionoiteinicio = horario;
		else if(attributeName.equals("horarinoitefim"))
			horarionoitefim =horario;
		
		// considera hor�rio do turno da manh� v�lido
		if(horariomanhainicio!=null||horariomanhafim!=null) {
			if(horariomanhafim!=null&&horariomanhainicio==null) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno matutino inicial antes do final.","Ordem dos valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
				return false;
			} else if(horariomanhafim!=null&&horariomanhainicio!=null) { // hor�rio inicial e final preenchidos corretamente, � checado se o hor�rio do fim � maior do que o do inicio
				if(horariomanhainicio.after(horariomanhafim)) {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno matutino inicial menor do que o final.","Valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		
		// considera hor�rio do turno da tarde v�lido
		if(horariotardeinicio!=null||horariotardefim!=null) {
			if(horariotardefim!=null&&horariotardeinicio==null) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno vespertino inicial antes do final.","Ordem dos valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
				return false;
			} else if(horariotardefim!=null&&horariotardeinicio!=null) { // hor�rio inicial e final preenchidos corretamente, � checado se o hor�rio do fim � maior do que o do inicio
				if(horariotardeinicio.after(horariotardefim)) {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno vespertino inicial menor do que o final.","Valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		
		// considera hor�rio do turno da noite v�lido
		if(horarionoiteinicio!=null||horarionoitefim!=null) {
			if(horarionoitefim!=null&&horarionoiteinicio==null) {
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno noturno inicial antes do final.","Ordem dos valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
				return false;
			} else if(horarionoitefim!=null&&horarionoiteinicio!=null) { // hor�rio inicial e final preenchidos corretamente, � checado se o hor�rio do fim � maior do que o do inicio
				if(horarionoiteinicio.after(horarionoitefim)) {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Preencha o hor�rio do turno noturno inicial menor do que o final.","Valores digitados inv�lidos",JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		
		return true;
		
	}

	@SuppressWarnings("unchecked")
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {

			String baseSQL = "from WorkShift in class org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel";

			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			ArrayList<PIVLocalConfWorkShiftModel> workShift = (ArrayList<PIVLocalConfWorkShiftModel>) session
					.createQuery(baseSQL).list();

			Response res = HibernateUtils.getBlockFromQuery(
					action,
					startIndex,
					50, // block size...
					filteredColumns, currentSortedColumns,
					currentSortedVersusColumns, valueObjectType, baseSQL,
					new Object[0], new Type[0], "WorkShift",
					pdao.getSessionFactory(), session);

			session.close();
			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		try {

			for(PIVLocalConfWorkShiftModel vo : (ArrayList<PIVLocalConfWorkShiftModel>) persistentObjects) {
				pdao.update(vo);
			}
			
			return new VOListResponse(persistentObjects,false,persistentObjects.size());

		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

}