package org.playiv.com.mvc.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;


import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.java.Consts;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.swing.general.PIVContactPanel;

public class PIVClientContactGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;

	private PIVDao pdao = PIVDao.getInstance();

	private PIVContactPanel contatoPanel;
	private Integer pk;

	private GridControl grid;

	// V�riavel de controle para evitar 'multi-chamada' do m�todo validateCell
	private Object checkNewValue;
	
	// var. utilizada para realizar o controle dos contatos no caso da opera��o
	// de inclus�o de cliente, como o cliente ainda n�o existe no bd, esta
	// v�riavel
	// � utilizado para 'agrupar' os contatos na mem�ria, caso o usu�rio
	// finaliza a opera��o com grava��o do cliente, os dados s�o persistidos no
	// bd
	private ArrayList<PIVContactModel> contatos = new ArrayList<PIVContactModel>();

	public PIVClientContactGFController(Integer pk, boolean onlyGrid) {
		
		// inclus�o de registro
//		if (pk == null)
//			pk = 0;

		this.pk = pk;

		PIVContactPanel contatoPanel = null;
		try {
			contatoPanel = new PIVContactPanel(pk,this,onlyGrid);
		} catch (ParseException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
		this.contatoPanel = contatoPanel;
		this.grid = contatoPanel.getGrid();
		
		/*this.contatoPanel.setEnabledGridButtons(Consts.INSERT);*/
		
		/*this.grid.reloadData();*/
		/*this.grid.setMode(Consts.INSERT);
		try {
			this.createValueObject(new PIVContactModel());
		} catch (Exception e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}*/
	}

	@Override
	public boolean beforeSaveDataInEdit(GridControl grid,int[] rowNumbers,ArrayList oldPersistentObjects,ArrayList persistentObjects) {
		// Varre todos os objetos do grid
		boolean validaInsert = false;
		for(int x=0;x<grid.getVOListTableModel().getRowCount();x++) {
			PIVContactModel contato = (PIVContactModel) this.grid.getVOListTableModel().getObjectForRow(x);
			if(contato.isResponsavel()) { return true; }
		}
		JOptionPane.showMessageDialog(MDIFrame.getInstance(),"� necess�rio ao menos o cadastro de um contato respons�vel. Favor checar o cadastro novamente.");
		return validaInsert;
	}
	
	@Override
	public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {

		PIVContactModel gridContato = (PIVContactModel) this.grid.getVOListTableModel().getObjectForRow(rowNumber);

		// Realiza verifica��o personalizada do atributo 'responsavel'
		if(attributeName.equals("responsavel")) {
			if((boolean)newValue.equals(true)) {
				if(this.pk==null) {
					for(PIVContactModel contato : this.contatos) {
						// J� existe um respons�vel no grid
						if(!contato.equals(gridContato) && contato.isResponsavel()) {
							if(this.checkNewValue != newValue)
								JOptionPane.showMessageDialog(MDIFrame.getInstance(), "J� existe um outro contato marcado como respons�vel!");
							this.checkNewValue = newValue;
							return false;
						}
					}
				}
				else {
					PIVClientModel cliente;
					String baseSQL = "from Cliente in class org.playiv.com.mvc.model.PIVClientModel where Cliente.id = '"
							+ pk + "'";

					Session session = pdao.getSession();
					cliente = (PIVClientModel) session.createQuery(baseSQL).uniqueResult();
					session.close();
					if (cliente != null) {
						baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contatos where Contatos.cliente.id = '"
								+ pk + "'";
						
						session = pdao.getSession(); // obtain a JDBC connection and
														// instantiate a new Session

						@SuppressWarnings("unchecked")
						ArrayList<PIVContactModel> contatosValida = (ArrayList<PIVContactModel>) session.createQuery(baseSQL).list();
						
						for(PIVContactModel contato : contatosValida) {
							if(contato.isResponsavel() && !(contato.equals(gridContato))) {
								if(this.checkNewValue != newValue)
									JOptionPane.showMessageDialog(MDIFrame.getInstance(),"Aten��o, s� pode existir um contato marcado respons�vel");
								this.checkNewValue = newValue;
								return false;
							}
						}
					}
				}
			}
		}

		return true;

	}

	public PIVContactPanel getContatoPanel() {
		return this.contatoPanel;
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {

			if (pk == null)
				return new VOListResponse(this.contatos, false,
						this.contatos.size());

			String baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contatos where Contatos.cliente.id = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			this.contatos = (ArrayList<PIVContactModel>) session.createQuery(baseSQL).list();

			Response res = HibernateUtils.getBlockFromQuery(
					action,
					startIndex,
					50, // block size...
					filteredColumns, currentSortedColumns,
					currentSortedVersusColumns, valueObjectType, baseSQL,
					new Object[0], new Type[0], "Contatos",
					pdao.getSessionFactory(), session);

			session.close();
			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	* Method invoked when the user has clicked on save button and the grid is
	* in INSERT mode.
	* 
	* @param rowNumbers
	*            row indexes related to the new rows to save
	* @param newValueObjects
	*            list of new value objects to save
	* @return an ErrorResponse value object in case of errors, VOListResponse
	*         if the operation is successfully completed
	*/
	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {

		// Busca o cliente relacionado ao contato
		PIVClientModel cliente = null;
		
		if( this.pk != null ) {
			String baseSQL = "from Cliente in class org.playiv.com.mvc.model.PIVClientModel where Cliente.id = '"
					+ pk + "'";
	
			Session session = pdao.getSession();
			cliente = (PIVClientModel) session.createQuery(baseSQL).uniqueResult();
			session.close();
		}

		ArrayList<PIVContactModel> contatos2 = (ArrayList<PIVContactModel>) newValueObjects;
		for (int i = 0; i < rowNumbers.length; i++) {

			PIVContactModel contato = contatos2.get(i);
			contato.setAtivo(true);
			contato.setCliente(cliente);

			// Primeiro contato � setado 'responsavel' como default
			if(!possuiContatoResponsavel())
				contato.setResponsavel(true);

			if (cliente != null)
				pdao.save(contato);
			else
				this.contatos.add(contato);

		}

		return new VOListResponse(contatos2, false, contatos2.size());

	}

	private boolean possuiContatoResponsavel() {
		
		if(pk==null) {
			for(PIVContactModel contato : this.contatos)
				if(contato.isResponsavel()) return true;
		} else {
			String baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contatos where Contatos.cliente = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			ArrayList<PIVContactModel> contatosResponsavel = (ArrayList<PIVContactModel>) session
					.createQuery(baseSQL).list();
			
			for(PIVContactModel contato : contatosResponsavel)
				if(contato.isResponsavel()) return true;
		}
		
		return false;
	}
	
	/**
	 * Method invoked when the user has clicked on save button and the grid is
	 * in EDIT mode.
	 * 
	 * @param rowNumbers
	 *            row indexes related to the changed rows
	 * @param oldPersistentObjects
	 *            old value objects, previous the changes
	 * @param persistentObjects
	 *            value objects relatied to the changed rows
	 * @return an ErrorResponse value object in case of errors, VOListResponse
	 *         if the operation is successfully completed
	 */
	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {

		ArrayList<PIVContactModel> contatos2 = (ArrayList<PIVContactModel>) persistentObjects;
		
		if(pk != null) {
		
		// Busca o cliente relacionado ao contato
		PIVClientModel cliente;
		String baseSQL = "from Cliente in class org.playiv.com.mvc.model.PIVClientModel where Cliente.id = '"
				+ pk + "'";

		Session session = pdao.getSession();
		cliente = (PIVClientModel) session.createQuery(baseSQL).uniqueResult();
		session.close();

		// Se a var. cliente n�o for nulo, significa que o cadastro corrente
		// esta sendo editado, neste caso, as modifica��es na tabela de contato
		// pode ser efetuada diretamente pelo banco
//		if (cliente != null) {
			for (int i = 0; i < rowNumbers.length; i++) {
				PIVContactModel contato = contatos2.get(i);
				contato.setCliente(cliente);
				if (contato.getId() == null)
					pdao.save(contato);
				else
					pdao.update(contato);
			}
		} else { // Se a edi��o for realizada no momento da inclus�o do cliente,
					// edita os contatos da mem�ria para serem alterados ap�s a
					// finaliza��o da opera��o
			for (PIVContactModel contatoAtualizado : contatos2) {
				for (PIVContactModel contato2 : this.contatos) {
					if (contatoAtualizado == contato2) {
						contato2 = contatoAtualizado;
						break;
					}

				}
			}
		}

		return new VOListResponse(persistentObjects, false,
				persistentObjects.size());
	}

	/**
	 * Method invoked when the user has clicked on delete button and the grid is
	 * in READONLY mode.
	 * 
	 * @param persistentObjects
	 *            value objects to delete (related to the currently selected
	 *            rows)
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		return new VOResponse(new Boolean(true));
	}

	/**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecord(ValueObject persistentObject) throws Exception {

		try {

			Session sessionContato = pdao.getSession();
			sessionContato.beginTransaction();
			sessionContato.delete(persistentObject);
			sessionContato.flush();
			sessionContato.getTransaction().commit();

			sessionContato.close();

			return new VOResponse(new Boolean(true));

		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}

	}

	public ArrayList<PIVContactModel> getContatos() {
		return this.contatos;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}
	
	

}