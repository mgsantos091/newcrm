package org.playiv.com.mvc.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;


import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;


public class PIVContactMngtLUFController extends LookupController {

	private PIVDao pdao = PIVDao.getInstance();

	private PIVClientModel cliente;
	
	public PIVContactMngtLUFController( PIVClientModel cliente ) {

		this.cliente = cliente;
		
		this.setLookupDataLocator(new LookupDataLocator() {

			PIVClientModel clienteLocator = PIVContactMngtLUFController.this.cliente;
			
			/**
			 * Method called by lookup controller when validating code.
			 * 
			 * @param code
			 *            code to validate
			 * @return code validation response: VOListResponse if code
			 *         validation has success, ErrorResponse otherwise
			 */
			public Response validateCode(String code) {
				try {

					String baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contato where Contato.clienteid = '" + clienteLocator.getId() + "'";
					Session session = pdao.getSession(); // obtain a JDBC
															// connection and
					SessionFactory sessions = pdao.getSessionFactory();

					Response res = HibernateUtils.getAllFromQuery(
							new HashMap(), new ArrayList(), new ArrayList(),
							PIVClientModel.class, baseSQL, new Object[0],
							new org.hibernate.type.Type[0], "PIVClienteModel",
							sessions, session);

					session.close();
					return res;
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}

				/*
				 * // an alternative way: you can define your own business logic
				 * to retrieve data and adding filtering/sorting conditions at
				 * hand... Statement stmt = null; try { stmt =
				 * DeptLookupController.this.conn.createStatement(); ResultSet
				 * rset = stmt.executeQuery(
				 * "select DEPT.DEPT_CODE,DEPT.DESCRIPTION,DEPT.ADDRESS from DEPT where STATUS='E' and DEPT_CODE='"
				 * +code + "'"); ArrayList list = new ArrayList(); while
				 * (rset.next()) { DeptVO vo = new DeptVO();
				 * vo.setDeptCode(rset.getString(1));
				 * vo.setDescription(rset.getString(2));
				 * vo.setAddress(rset.getString(3)); list.add(vo); } if
				 * (list.size() > 0) return new VOListResponse(list, false,
				 * list.size()); else return new
				 * ErrorResponse("Code not valid"); } catch (SQLException ex) {
				 * ex.printStackTrace(); return new
				 * ErrorResponse(ex.getMessage()); } finally { try {
				 * stmt.close(); } catch (SQLException ex1) { } }
				 */
			}

			/**
			 * Method called by lookup controller when user clicks on lookup
			 * button.
			 * 
			 * @param action
			 *            fetching versus: PREVIOUS_BLOCK_ACTION,
			 *            NEXT_BLOCK_ACTION or LAST_BLOCK_ACTION
			 * @param startIndex
			 *            current index row on grid to use to start fetching
			 *            data
			 * @param filteredColumns
			 *            filtered columns
			 * @param currentSortedColumns
			 *            sorted columns
			 * @param currentSortedVersusColumns
			 *            ordering versus of sorted columns
			 * @param valueObjectType
			 *            type of value object associated to the lookup grid
			 * @return list of value objects to fill in the lookup grid:
			 *         VOListResponse if data fetching has success,
			 *         ErrorResponse otherwise
			 */
			public Response loadData(int action, int startIndex,
					Map filteredColumns, ArrayList currentSortedColumns,
					ArrayList currentSortedVersusColumns, Class valueObjectType) {
				try {

					String baseSQL = "from org.playiv.com.mvc.model.PIVClientModel as Cliente";
					Session session = pdao.getSession(); // obtain a JDBC
															// connection and

					Response res = HibernateUtils.getBlockFromClass(
							valueObjectType, filteredColumns,
							currentSortedColumns, currentSortedVersusColumns,
							action, startIndex, 50, // block size...
							FetchMode.JOIN, session);

					return res;

				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}

				/*
				 * // an alternative way: you can define your own business logic
				 * to retrieve data and adding filtering/sorting conditions at
				 * hand... Statement stmt = null; try { stmt =
				 * DeptLookupController.this.conn.createStatement(); ResultSet
				 * rset = stmt.executeQuery(
				 * "select DEPT.DEPT_CODE,DEPT.DESCRIPTION,DEPT.ADDRESS from DEPT where STATUS='E' "
				 * ); ArrayList list = new ArrayList(); while (rset.next()) {
				 * DeptVO vo = new DeptVO(); vo.setDeptCode(rset.getString(1));
				 * vo.setDescription(rset.getString(2));
				 * vo.setAddress(rset.getString(3)); list.add(vo); } return new
				 * VOListResponse(list, false, list.size()); } catch
				 * (SQLException ex) { ex.printStackTrace(); return new
				 * ErrorResponse(ex.getMessage()); } finally { try {
				 * stmt.close(); } catch (SQLException ex1) { } }
				 */
			}

			/**
			 * Method called by the TreePanel to fill the tree.
			 * 
			 * @return a VOReponse containing a DefaultTreeModel object
			 */
			public Response getTreeModel(JTree tree) {
				return new VOResponse(new DefaultTreeModel(
						new OpenSwingTreeNode()));
			}

		});

		this.setLookupValueObjectClassName("org.playiv.com.mvc.model.PIVClientModel");
		this.addLookup2ParentLink("id", "id");
		this.setAllColumnVisible(true);
		this.setVisibleColumn("razaosocial", false);
		this.setVisibleColumn("ativo", false);
		this.setVisibleColumn("pjuridica", false);
		this.setVisibleColumn("dataregistro", false);
		this.setVisibleColumn("nomeimagem", false);
		this.setVisibleColumn("endereco", false);
		this.setVisibleColumn("contatos", false);
		this.setPreferredWidthColumn("nomefantasia", 200);
		this.setFilterableColumn("id", true);
		this.setFilterableColumn("nomefantasia", true);
		this.setSortableColumn("id", true);
		this.setSortableColumn("nomefantasia", true);
		this.setFramePreferedSize(new Dimension(350, 500));
	}

}