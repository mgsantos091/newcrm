package org.playiv.com.mvc.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JTree;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVCampaignModel;
import org.playiv.com.mvc.model.PIVCategoryModel;

public class PIVCampaignLUFController extends LookupController {

	private PIVDao pdao = PIVDao.getInstance();

	public PIVCampaignLUFController() {
		
		this.setCodeSelectionWindow(this.GRID_AND_FILTER_FRAME);
		this.setLookupDataLocator(new LookupDataLocator() {

			public Response validateCode(String code) {
				try {
					Session session = pdao.getSession();
					PIVCampaignModel campanha = (PIVCampaignModel) session
							.createQuery(
									"from org.playiv.com.mvc.model.PIVCampaignModel as Campanha where Campanha.status = 2").uniqueResult();
					session.close();
					return new VOResponse(campanha);
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			public Response loadData(int action, int startIndex,
					Map filteredColumns, ArrayList currentSortedColumns,
					ArrayList currentSortedVersusColumns, Class valueObjectType) {
				// method not required...
				try {

					String baseSQL = "from org.playiv.com.mvc.model.PIVCampaignModel as Campanha where Campanha.status = 2";
					Session session = pdao.getSession();

					Response res = HibernateUtils.getBlockFromQuery(
							action,
							startIndex,
							50, // block size...
							filteredColumns, currentSortedColumns,
							currentSortedVersusColumns, valueObjectType,
							baseSQL, new Object[0], new Type[0], "Campanha",
							pdao.getSessionFactory(), session);

					session.close();

					return res;
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			@Override
			public Response getTreeModel(JTree tree) {
				/*return new VOResponse(new DefaultTreeModel(
						new OpenSwingTreeNode()));*/
				return null;
			}

		});

		this.setLookupValueObjectClassName("org.playiv.com.mvc.model.PIVCampaignModel");
		this.setAllColumnVisible(false);
		
		this.addLookup2ParentLink("id", "campanha.id");
		this.addLookup2ParentLink("nome_campanha", "campanha.nome_campanha");
		
		this.setVisibleColumn("id", true);
		this.setVisibleColumn("nome_campanha", true);
		this.setVisibleColumn("publico_alvo",true);
		this.setVisibleColumn("patrocinador",true);
		
		this.setPreferredWidthColumn("id",30);
		this.setPreferredWidthColumn("nome_campanha", 150);
		this.setPreferredWidthColumn("publico_alvo",100);
		this.setPreferredWidthColumn("patrocinador",100);
		
		this.setFilterableColumn("id", true);
		this.setFilterableColumn("nome_campanha", true);
		this.setFilterableColumn("publico_alvo", true);
		this.setFilterableColumn("patrocinador", true);
		
		this.setSortableColumn("id", true);
		this.setSortableColumn("nome_campanha", true);
		this.setSortableColumn("publico_alvo", true);
		this.setSortableColumn("patrocinador", true);
		
		this.setFramePreferedSize(new Dimension(450, 550));

	}

}
