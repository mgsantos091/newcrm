package org.playiv.com.mvc.controller;

import org.hibernate.Session;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVClientMoreInfoModel;
import org.playiv.com.mvc.view.PIVClientMoreInfoDFView;

/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVClientMoreInfoDFController extends FormController {

	private PIVClientMoreInfoDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private PIVClientModel cliente;

	public PIVClientModel getCliente() {
		return cliente;
	}

	public PIVClientMoreInfoDFController( PIVClientModel cliente ) {
		this.cliente = cliente;
		frame = new PIVClientMoreInfoDFView( this );
		MDIFrame.add(frame);
		frame.getPanel().setMode(Consts.EDIT);
		frame.getPanel().reload();
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			String baseSQL = "from InfoAdicional in class org.playiv.com.mvc.model.PIVClientMoreInfoModel where InfoAdicional.cliente.id = '"
					+ this.cliente.getId() + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			PIVClientMoreInfoModel vo = (PIVClientMoreInfoModel) session.createQuery(baseSQL)
					.uniqueResult();
			
			session.close();
			
			if(vo==null) {
				vo = new PIVClientMoreInfoModel();
				vo.setCliente(this.cliente);
				pdao.save(vo);
			}

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}

	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			
			PIVClientMoreInfoModel vo = (PIVClientMoreInfoModel) persistentObject;
			
			PIVAddressModel endereco = vo.getEndereco_entrega();
			if (!(endereco == null))
				if (!(endereco.getCep() == null)) {
					PIVAddressModel findEndereco = getEndereco(endereco
							.getCep());
					if (findEndereco == null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				} else
					vo.setEndereco_entrega(null);
			
			pdao.update(vo);
			
			return new VOResponse(vo);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}
	
	public PIVAddressModel getEndereco(String cep) {
		String baseSQL = "from Endereco in class org.playiv.com.mvc.model.PIVAddressModel where Endereco.cep = '"
				+ cep + "'";
		Session session = pdao.getSession(); // obtain a JDBC connection and
												// instantiate a new Session
		PIVAddressModel vo = (PIVAddressModel) session.createQuery(baseSQL)
				.uniqueResult();
		session.close();
		return vo;

	}

}