package org.playiv.com.mvc.controller;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTextField;

import org.hibernate.Session;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVEventRegisterModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.mvc.model.PIVTagModel;
import org.playiv.com.mvc.view.PIVBusRelMainDetailCFView;
import org.playiv.com.swing.general.PIVContactPanel;
import org.playiv.com.swing.general.PIVHistoryOfActionsTableFunc;
import org.playiv.com.swing.general.PIVProductSalesPanel;

public class PIVBulsRelMainDetailCFController {

	private PIVBusRelMainDetailCFView vendasPrincipalFrame;

	private PIVUserSession sessao = PIVUserSession.getInstance();

	private PIVDao pdao = PIVDao.getInstance();
	
	// dados do cliente
	private JLabel lbNomeEmpresa;
	private JLabel lbCodEmpresa;

	private JLabel lbSiteEmpresa;
	private JTextField txSiteEmpresa;

	// contatos do cliente
	private PIVContactPanel jpContato;
	private PIVProductSalesPanel jpVenda;

	// tags do orcamento
	private JTextField txTag1;
	private JTextField txTag2;
	private JTextField txTag3;
	private JTextField txTag4;

	private String txTag1ValorAntesFoco;
	private String txTag1ValorDepoisFoco;
	private String txTag2ValorAntesFoco;
	private String txTag2ValorDepoisFoco;
	private String txTag3ValorAntesFoco;
	private String txTag3ValorDepoisFoco;
	private String txTag4ValorAntesFoco;
	private String txTag4ValorDepoisFoco;
	
	// vendas
	private PIVClientModel cliente;
	private PIVSalesMainModel venda;
	private PIVBusRelMainCFController organizacliente;
	
	// endereco
	private JLabel lbEndCep;
	private JLabel lbEndPais;
	private JLabel lbEndEstado;
	private JLabel lbEndCidade;
	private JLabel lbEndBairro;
	private JLabel lbEndRua;

	private PIVHistoryOfActionsTableFunc tbl_HistoryOfActions;

	private PIVTagModel tag1;
	private PIVTagModel tag2;
	private PIVTagModel tag3;
	private PIVTagModel tag4;

	private PIVBulsRelMainDetailCFController( PIVClientModel cliente ) {
		this.cliente = cliente;
	}

	public PIVBulsRelMainDetailCFController( PIVBusRelMainCFController organizacliente , PIVClientModel cliente , PIVSalesMainModel venda ) {
		this(cliente);
		this.setVenda(venda);
		this.organizacliente = organizacliente;
		initialize();
	}
	
	public void setVenda(PIVSalesMainModel venda) {
		this.venda = venda;
	}

	private void initialize( ) {
		
		this.vendasPrincipalFrame = new PIVBusRelMainDetailCFView( this );
		
		this.lbNomeEmpresa = this.vendasPrincipalFrame.getLbNomeEmpresa();
		this.lbCodEmpresa = this.vendasPrincipalFrame.getLbCodEmpresa();
		
		this.lbSiteEmpresa = this.vendasPrincipalFrame.getLbSiteEmpresa();
		
		this.jpContato = this.vendasPrincipalFrame.getJpContato();
		
		this.txTag1 = this.vendasPrincipalFrame.getTxTag1();
		this.txTag2 = this.vendasPrincipalFrame.getTxTag2();
		this.txTag3 = this.vendasPrincipalFrame.getTxTag3();
		this.txTag4 = this.vendasPrincipalFrame.getTxTag4();

		this.jpVenda = this.vendasPrincipalFrame.getJpVendas();
		
		this.lbEndCep = this.vendasPrincipalFrame.getLbEndCep();
		this.lbEndPais = this.vendasPrincipalFrame.getLbEndPais();
		this.lbEndEstado = this.vendasPrincipalFrame.getLbEndEstado();
		this.lbEndCidade = this.vendasPrincipalFrame.getLbEndCidade();
		this.lbEndBairro = this.vendasPrincipalFrame.getLbEndBairro();
		this.lbEndRua = this.vendasPrincipalFrame.getLbEndRua();

		this.tbl_HistoryOfActions = this.vendasPrincipalFrame.getTbl_HistoryOfActions();

		carregar();
		
		inicializaListeners();

		MDIFrame.add(this.vendasPrincipalFrame);

	}
	
	public void carregar() {
		
		this.lbNomeEmpresa.setText(this.cliente.getRazaosocial());
		this.lbCodEmpresa.setText("N\u00B0 " + this.cliente.getId().toString());
		
		this.lbSiteEmpresa.setText(this.cliente.getWebsite());
		
		this.jpContato.getGrid().reloadData();
		
		this.tag1 = getTagById(this.venda,1);
		this.tag2 = getTagById(this.venda,2);
		this.tag3 = getTagById(this.venda,3);
		this.tag4 = getTagById(this.venda,4);
		
		this.txTag1.setText(this.tag1==null ? "" : this.tag1.getNome());
		this.txTag2.setText(this.tag2==null ? "" : this.tag2.getNome());
		this.txTag3.setText(this.tag3==null ? "" : this.tag3.getNome());
		this.txTag4.setText(this.tag4==null ? "" : this.tag4.getNome());
		
		this.jpVenda.getGrid().reloadData();
		
		PIVAddressModel endereco = this.cliente.getEndereco();
		this.lbEndCep.setText(endereco==null ? "" : endereco.getCep());
		this.lbEndPais.setText(endereco==null ? "" : endereco.getPais());
		this.lbEndEstado.setText(endereco==null ? "" : endereco.getEstado());
		this.lbEndCidade.setText(endereco==null ? "" : endereco.getCidade());
		this.lbEndBairro.setText(endereco==null ? "" : endereco.getBairro());
		this.lbEndRua.setText(endereco==null ? "" : endereco.getLogradouro());

		this.tbl_HistoryOfActions.carregar(this.getEventos());
		
	}

	public PIVClientModel getCliente() {
		return cliente;
	}
	
	public PIVSalesMainModel getVenda() {
		if(this.venda==null) {
			PIVSellerModel vendedor = PIVUserSession.getInstance().getVendedorSessao();
			this.venda = new PIVSalesMainModel();
			this.venda.setCliente(this.cliente);
			this.venda.setVendedor(vendedor);
			this.venda.setCampanha(null);
			this.venda.setEstagio(null);
			this.venda.setAtivo(true);
			PIVDao.getInstance().save(this.venda);
		}
		return venda;
	}
	
	public PIVBusRelMainCFController getOrganizacliente() {
		return organizacliente;
	}
	
	public ArrayList<PIVEventRegisterModel> getEventos( ) {
		String sql = "from Evento in class org.playiv.com.mvc.model.PIVEventRegisterModel where Evento.vendedor.id = " + sessao.getVendedorSessao().getId() + " and Evento.cliente.id = " + this.cliente.getId();
		Session session = pdao.getSession();
		@SuppressWarnings("unchecked")
		ArrayList<PIVEventRegisterModel> eventos = (ArrayList<PIVEventRegisterModel>) session.createQuery(sql).list();
		session.close();
		return eventos;
	}
	
	public PIVTagModel getTagById( PIVSalesMainModel vendapai , Integer codtag ) {
		if(vendapai==null)
			return null;
		String sql = "from Tag in class org.playiv.com.mvc.model.PIVTagModel where Tag.vendaPai.id = " + vendapai.getId() + " and Tag.codtag = " + codtag + "";
		Session session = pdao.getSession();
		@SuppressWarnings("unchecked")
		PIVTagModel tag = (PIVTagModel) session.createQuery(sql).uniqueResult();
		session.close();
		return tag;
	}
	
	private void inicializaListeners( ) {
		
		this.txTag1.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag1ValorDepoisFoco = txAtual.getText();
				if(!txTag1ValorDepoisFoco.equals(txTag1ValorAntesFoco)) {
					PIVTagModel tag = new PIVTagModel();
					tag.setCodtag(1);
					tag.setNome(txTag1ValorDepoisFoco);
					tag.setVendaPai(venda);
					if(tag1!=null)
						pdao.update(tag);
					else
						pdao.save(tag);
					tag1 = tag;
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag1ValorAntesFoco = txAtual.getText();
			}
			
		});
		
		this.txTag2.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag2ValorDepoisFoco = txAtual.getText();
				if(!txTag2ValorDepoisFoco.equals(txTag2ValorAntesFoco)) {
					PIVTagModel tag = new PIVTagModel();
					tag.setCodtag(2);
					tag.setNome(txTag2ValorDepoisFoco);
					tag.setVendaPai(venda);
					if(tag2!=null)
						pdao.update(tag);
					else
						pdao.save(tag);
					tag2 = tag;
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag2ValorAntesFoco = txAtual.getText();
			}
			
		});
		
		this.txTag3.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag3ValorDepoisFoco = txAtual.getText();
				if(!txTag3ValorDepoisFoco.equals(txTag3ValorAntesFoco)) {
					PIVTagModel tag = new PIVTagModel();
					tag.setCodtag(3);
					tag.setNome(txTag3ValorDepoisFoco);
					tag.setVendaPai(venda);
					if(tag3!=null)
						pdao.update(tag);
					else
						pdao.save(tag);
					tag3 = tag;
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag3ValorAntesFoco = txAtual.getText();
			}
			
		});
		
		this.txTag4.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag4ValorDepoisFoco = txAtual.getText();
				if(!txTag4ValorDepoisFoco.equals(txTag4ValorAntesFoco)) {
					PIVTagModel tag = new PIVTagModel();
					tag.setCodtag(4);
					tag.setNome(txTag4ValorDepoisFoco);
					tag.setVendaPai(venda);
					if(tag4!=null)
						pdao.update(tag);
					else
						pdao.save(tag);
					tag4 = tag;
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				JTextField txAtual = (JTextField) e.getSource();
				txTag4ValorAntesFoco = txAtual.getText();
			}
			
		});
		
	}

}