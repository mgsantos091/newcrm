package org.playiv.com.mvc.controller;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVBusRelModel;
import org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel;
import org.playiv.com.mvc.view.PIVBusRelMainCFView;

public class PIVBusRelMngtGFController {

	private PIVDao pdao = PIVDao.getInstance();
	
	private PIVBusRelMainCFView view;

	private PIVBusRelMainCFController organizaSuspect;
	private PIVBusRelMainCFController organizaProspect;
	private PIVBusRelMainCFController organizaForecast;
	private PIVBusRelMainCFController organizaCustomer;

	private PIVUserSession pSessaoAtual = PIVUserSession.getInstance();
	
	public PIVBusRelMngtGFController( PIVBusRelMainCFView view ) {
		this.view = view; 
	}

	public void carregar( ) {
		
		organizaSuspect = view.getOrganizaClienteSuspect();
		organizaProspect = view.getOrganizaClienteProspect();
		organizaForecast = view.getOrganizaClienteForecast();
		organizaCustomer = view.getOrganizaClienteCustomer();
		
		// reseta os objetos respons�veis por gerenciar os conte�dos dos
		// 'n�veis' comerciais
		organizaSuspect.carregar();
		organizaProspect.carregar();
		organizaForecast.carregar();
		organizaCustomer.carregar();

		// verifica se existem clientes na lixeira
//		if(possuiClientesNaLixeira())
//			this.encherLixo();
//		else
//			this.esvaziarLixo();
		
		// atualiza o hor�rio de turno do vendedor de acordo com o cadastro do sistema
//		this.atualizarRelogio();
		
	}
	
//	private void atualizarRelogio() {
//		view.getLblTemp().setIcon(new ImageIcon(PIVBusRelMngtGFController.class.getResource("/images/menu_principal/relogio" + getPeriodoTurnoAtivo() + "_64x64.png")));
//		view.getLblTemp().repaint();
//	}

	private boolean possuiClientesNaLixeira( ) {
		
		Integer idVendedorLogado = pSessaoAtual.getVendedorSessao().getId();
		
		String sql = "from org.playiv.com.mvc.model.PIVBusRelModel as RelCom where RelCom.ativo = false and RelCom.vendedor.id = " + idVendedorLogado  + "";
		
		Session session = pdao.getSession();
		
		@SuppressWarnings("unchecked")
		List<PIVBusRelModel> niveisNaLixeira = (List<PIVBusRelModel>) session.createQuery(sql).list();
		
		session.close();
		
		if(niveisNaLixeira.size()>0)
			return true;
		else
			return false;
	}
	
//	private void esvaziarLixo() {
//		view.getLblLixo().setIcon(new ImageIcon(PIVBusRelMngtGFController.class.getResource("/images/gerenciamento_relcomercial/lixo/lixo_vazio.png")));
//		view.getLblLixo().repaint();
//	}

//	private void encherLixo() {
//		view.getLblLixo().setIcon(new ImageIcon(PIVBusRelMngtGFController.class.getResource("/images/gerenciamento_relcomercial/lixo/lixo_cheio.png")));
//		view.getLblLixo().repaint();
//	}

/*	public PIVBusRelMngtGFController() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String args[]){
		PIVBusRelMngtGFController x = new PIVBusRelMngtGFController();
		x.getPeriodoTurnoAtivo();
	}*/
	
	private Integer getPeriodoTurnoAtivo( ){
		
		String sql = "from org.playiv.com.mvc.model.PIVLocalConfWorkShiftModel as WorkShift where WorkShift.diadasemana = " + Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		
		Session session = pdao.getSession();
		
		PIVLocalConfWorkShiftModel workShiftDay = (PIVLocalConfWorkShiftModel) session.createQuery(sql).uniqueResult();
		
		session.close();
		
		if(workShiftDay!=null) {
			
			long startTime = new Timestamp(10800000).getTime();

			Timestamp horariomanhainicio = workShiftDay.getHorariomanhainicio();
			Timestamp horariomanhafim = workShiftDay.getHorariomanhafim();

			Timestamp horariotardeinicio = workShiftDay.getHorariotardeinicio();
			Timestamp horariotardefim = workShiftDay.getHorariotardefim();

			Timestamp horarionoiteinicio = workShiftDay.getHorarionoiteinicio();
			Timestamp horarionoitefim = workShiftDay.getHorarionoitefim();
			
			Date date = new Date();   // given date
			Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
			calendar.setTime(date);
			
			long time = startTime + ((calendar.get(Calendar.HOUR_OF_DAY) * 60) + (calendar.get(Calendar.MINUTE))) * 60 * 1000;
			Timestamp horarioatual = new Timestamp(time);
			
			long cargaHorariaConsumida = 0;
			
/*			Timestamp primeiroHorarioTurnoAtivo = null; // utilizado para guardar o per�odo do primeiro hor�rio do primeiro turno
			// vai ser comparado mais para frente se o hor�rio atual � anterior ao primeiro turno do dia (se for, o rel�gio n�o inicia)
*/			
			long timeHorarioManha = 0;	
			if(horariomanhainicio!=null&&horariomanhafim!=null) { // turno da manh�
				timeHorarioManha = horariomanhafim.getTime() - horariomanhainicio.getTime(); // contabiliza tempo da manh�
				if(horarioatual.after(horariomanhafim)) // turno da manh� acabou
					cargaHorariaConsumida += timeHorarioManha;
				else if(horarioatual.after(horariomanhainicio))
					cargaHorariaConsumida += horarioatual.getTime() - horariomanhainicio.getTime();
			}
			
			long timeHorarioTarde = 0;
			if(horariotardeinicio!=null&&horariotardefim!=null) { // turno da tarde
				timeHorarioTarde = horariotardefim.getTime() - horariotardeinicio.getTime(); // contabiliza tempo da tarde
				if(horarioatual.after(horariotardefim)) // turno da manh� acabou
					cargaHorariaConsumida += timeHorarioTarde;
				else if(horarioatual.after(horariotardeinicio))
					cargaHorariaConsumida += horarioatual.getTime() - horariotardeinicio.getTime();
			}
				
			long timeHorarioNoite = 0;
			if(horarionoiteinicio!=null&&horarionoitefim!=null) {
				timeHorarioNoite = horarionoitefim.getTime() - horarionoiteinicio.getTime(); // contabiliza tempo da noite
				if(horarioatual.after(horarionoitefim)) // turno da manh� acabou
					cargaHorariaConsumida += timeHorarioNoite;
				else if(horarioatual.after(horarionoiteinicio))
					cargaHorariaConsumida += horarioatual.getTime() - horarionoiteinicio.getTime();
			}
			
			// nenhum turno cadastrado
			long cargaHorariaTotal = timeHorarioManha + timeHorarioTarde + timeHorarioNoite;
			if(cargaHorariaTotal==0) return 0;
			
			if(cargaHorariaTotal==cargaHorariaConsumida) return 8;
			
			double porc = (((double)cargaHorariaConsumida / cargaHorariaTotal) * 100);
			return (int) (porc * 8) / 100;
			
			/*long timeHorarioManha = 0;	
			if(horariomanhainicio!=null&&horariomanhafim!=null) { // turno da manh�
				totalDeTurnosAtivos++;
				if(horarioatual.after(horariomanhafim)) { // turno da manh� acabou
					turnosCompletados ++;
				} else primeiroHorarioTurnoAtivo = horariomanhafim;
				timeHorarioManha = horariomanhafim.getTime() - horariomanhainicio.getTime(); // contabiliza tempo da manh�
			}
			
			long timeHorarioTarde = 0;
			if(horariotardeinicio!=null&&horariotardefim!=null) { // turno da tarde
				totalDeTurnosAtivos++;
				if(horarioatual.after(horariotardefim)) { // turno da tarde acabou
					turnosCompletados ++;
				} else if(primeiroHorarioTurnoAtivo==null) primeiroHorarioTurnoAtivo = horariotardefim;
				timeHorarioTarde = horariotardefim.getTime() - horariotardeinicio.getTime(); // contabiliza tempo da tarde
			}
			
			long timeHorarioNoite = 0;
			if(horarionoiteinicio!=null&&horarionoitefim!=null) { // turno da noite
				totalDeTurnosAtivos++;
				if(horarioatual.after(horarionoitefim)) { // turno da noite acabou
					turnosCompletados ++;
				} else if(primeiroHorarioTurnoAtivo==null) primeiroHorarioTurnoAtivo = horarionoitefim;
				timeHorarioNoite = horarionoitefim.getTime() - horarionoiteinicio.getTime(); // contabiliza tempo da noite
			}
			
			// nenhum turno cadastrado
			long cargaHorarioTotal = timeHorarioManha + timeHorarioTarde + timeHorarioNoite;
			if(cargaHorarioTotal==0) return 0;
			
			// turno completo
			if(totalDeTurnosAtivos==turnosCompletados)
				return 8;
			
			if(turnosCompletados==0) {
				if(horarioatual.before(primeiroHorarioTurnoAtivo))
					return 0;
				long timedif = horarioatual.getTime() - primeiroHorarioTurnoAtivo.getTime();
				double porc = (((double)timedif / cargaHorarioTotal) * 100);
				return (int) (porc * (8 / totalDeTurnosAtivos) / 100);
			} else {
				if(turnosCompletados==1) {
					long timedif = horarioatual.getTime() - primeiroHorarioTurnoAtivo.getTime();
					double porc = (((double)timedif / cargaHorarioTotal) * 100);
					return (int) (porc * (8 / totalDeTurnosAtivos) / 100) + (8/totalDeTurnosAtivos); 
				} else {
					if(turnosCompletados==2) {
						long timedif = horarioatual.getTime() - primeiroHorarioTurnoAtivo.getTime();
						double porc = (((double)timedif / cargaHorarioTotal) * 100);
						return (int) (porc * (8 / totalDeTurnosAtivos) / 100) + 6;
					}
				}
			}*/
			
		}
		
		return 0;
		
	}

}