package org.playiv.com.mvc.controller;

import java.beans.PropertyVetoException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.hibernate.Session;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
//import org.openswing.swing.internationalization.java.Resources;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
//import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVVoipRecordModel;
import org.playiv.com.mvc.view.PIVVoipRecordDFView;
import org.playiv.com.mvc.view.PIVVoipRecordGFView;

public class PIVVoipRecordDFController extends FormController implements
		IPIVFormController {

	private PIVVoipRecordGFView gridFrame = null;
	private PIVVoipRecordDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;

	private PIVClientModel cliente;

	private String numero;

	public PIVVoipRecordDFController(PIVVoipRecordGFView gridFrame,
			PIVClientModel cliente, String numero) {
		this.pk = null;
		this.cliente = cliente;
		this.gridFrame = gridFrame;
		this.numero = numero;
		frame = new PIVVoipRecordDFView(this);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.EDIT);
			frame.getPanel().reload();
		} else {
			frame.getPanel().setMode(Consts.INSERT);
		}
	}

	public PIVVoipRecordDFController(Integer pk) {
		this.pk = pk;
		frame = new PIVVoipRecordDFView(this);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.EDIT);
			frame.getPanel().reload();
		} else {
			frame.getPanel().setMode(Consts.INSERT);
		}
	}

	public PIVVoipRecordDFController(PIVVoipRecordGFView gridFrame, Integer pk) {
		this(pk);
		this.gridFrame = gridFrame;
	}

	// @Override
	// public boolean beforeInsertData(Form form) {
	// PIVVoipRecordModel vo = (PIVVoipRecordModel) form.getVOModel()
	// .getValueObject();
	// vo.setCliente(cliente);
	// vo.setNumero("alo alo terezinha");
	// return true;
	// }

	@Override
	public void afterInsertData(Form form) {
		PIVVoipRecordModel vo = (PIVVoipRecordModel) form.getVOModel()
				.getValueObject();
		vo.setCliente(cliente);
		vo.setNumero(numero);
		// vo.setNumero("alo alo terezinha");
		form.pull();
	}

	public Response loadData(Class valueObjectClass) {
		try {

			String baseSQL = "from Voip in class org.playiv.com.mvc.model.PIVVoipRecordModel where Voip.id = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			PIVVoipRecordModel vo = (PIVVoipRecordModel) session.createQuery(
					baseSQL).uniqueResult();

			session.close();

			if(vo.getDuracao()!=null) {
				long t = vo.getDuracao().getTime();
				this.frame.getJpWatchPanel().setTime(t);
				this.frame.getJpWatchPanel().setInicioTempo(vo.getInicio());
				this.frame.getJpWatchPanel().setFimTempo(vo.getFim());
			}

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}

	}

	@Override
	public void afterInsertData() {
		try {
			if (this.gridFrame != null)
				this.gridFrame.reloadData();
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	@Override
	public void afterEditData() {
		try {
			if (this.gridFrame != null)
				this.gridFrame.reloadData();
			this.frame.closeFrame();
		} catch (PropertyVetoException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	// @Override
	// public boolean beforeEditData(Form form) {
	// frame.getJpWatchPanel().setEnabled(false);
	// return true;
	// }

	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {
			PIVVoipRecordModel vo = (PIVVoipRecordModel) newPersistentObject;
			vo.setAtivo(true);

			Timestamp inicio = this.frame.getJpWatchPanel().getInicioTempo();
			Timestamp fim = this.frame.getJpWatchPanel().getFimTempo();
			if (inicio != null && fim == null) {
				fim = new Timestamp((new Date()).getTime());
			}
			vo.setInicio(inicio);
			vo.setFim(fim);

			// vo.setCliente(cliente);
			pdao.save(vo);

			SimpleDateFormat sdf = new SimpleDateFormat(
					"m' minuto(s) e 's' segundos'");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

			PIVEventMngrFunc.getInstance().registerEvent(
					cliente,
					PIVEventMngrFunc
							.getCodEvento(PIVEventMngrFunc.evento.LIGACAO),
					null,
					PIVEventMngrFunc.evento.LIGACAO,
					"Cliente - "
							+ cliente.getId().toString()
							+ " "
							+ cliente.getRazaosocial()
							+ " - recebeu uma liga��o no n�mero "
							+ vo.getNumero()
							+ (vo.getDuracao() != null ? " com dura��o de "
									+ sdf.format(vo.getDuracao().getTime())
									+ "." : "."));

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVVoipRecordModel vo = (PIVVoipRecordModel) persistentObject;

			// Timestamp inicio = this.frame.getJpWatchPanel().getInicioTempo();
			// Timestamp fim = this.frame.getJpWatchPanel().getFimTempo();
			// if( ( inicio != null && fim != null )) {
			// vo.setInicio(inicio);
			// vo.setFim(fim);
			// } else if (inicio != null && fim == null) {
			// fim = new Timestamp((new Date()).getTime());
			// vo.setInicio(inicio);
			// vo.setFim(fim);
			// }

			pdao.update(vo);
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			PIVVoipRecordModel vo = (PIVVoipRecordModel) persistentObject;
			vo.setAtivo(false);
			pdao.update(vo);
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}

	public String getNumero() {
		return numero;
	}
	
	public String getDomain() {
		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
		Session session = PIVDao.getInstance().getSession();

		PIVLocalConfModel empresa = (PIVLocalConfModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();
		
		String domain = empresa.getVoip_dominio();
		return domain;
	}
	
}