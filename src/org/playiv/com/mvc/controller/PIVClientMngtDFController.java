
package org.playiv.com.mvc.controller;

import java.beans.PropertyVetoException;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.InternalFrame;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVAddressFunction;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.model.PIVAddressModel;
import org.playiv.com.mvc.view.PIVClientMngtDFView;
import org.playiv.com.mvc.view.PIVClientMngtGFView;

import br.com.caelum.stella.MessageProducer;
import br.com.caelum.stella.ResourceBundleMessageProducer;
import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.format.CNPJFormatter;
import br.com.caelum.stella.format.CPFFormatter;
import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import br.com.caelum.stella.validation.Validator;

/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVClientMngtDFController extends FormController implements
		IPIVFormController {

	private PIVClientMngtGFView gridFrame = null;
	private PIVClientMngtDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;

	private PIVBusRelMainCFController organizaCliente;

	private PIVDirectoryMngtFunc gerenciarDiretorio = PIVDirectoryMngtFunc
			.getInstance();

	public PIVClientMngtDFController(Integer pk) {
		this.pk = pk;
		frame = new PIVClientMngtDFView(this, pk, pdao);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.EDIT);
			frame.getPanel().reload();
			/* frame.getJpContato().getGrid().reloadData(); */
		} else {
			frame.getPanel().setMode(Consts.INSERT);
		}
	}
	
	public PIVClientMngtDFController(PIVClientMngtGFView gridFrame, Integer pk) {
		this (pk);
		this.gridFrame = gridFrame;
	}

	public PIVClientMngtDFController(PIVBusRelMainCFController organizaCliente) {
		this(null, null);
		this.organizaCliente = organizaCliente;
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			/*
			 * int row = gridFrame.getGrid().getSelectedRow();
			 * 
			 * if (row != -1) { PIVClienteModel gridVO = (PIVClienteModel)
			 * gridFrame.getGrid() .getVOListTableModel().getObjectForRow(row);
			 * pk = gridVO.getId(); }
			 */

			String baseSQL = "from Clientes in class org.playiv.com.mvc.model.PIVClientModel where Clientes.id = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			PIVClientModel vo = (PIVClientModel) session.createQuery(baseSQL)
					.uniqueResult();

			frame.getClientePanel().setPJuridica(vo.isPjuridica());
			frame.getClientePanel().setEmpIcon(vo.getNomeimagem());

			session.close();

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
		

	}

	@Override
	public boolean validateControl(String attributeName, Object oldValue,
			Object newValue) {
		if (attributeName.equals("cnpjcpf")) {
			if (((String) newValue).equals(""))
				return true; // s� valida se existir algum valor
			Validator<String> validator;
			boolean pJuridica = false;
			try {
				// Valida CNPJ
				ResourceBundle resourceBundle = ResourceBundle.getBundle(
						"PlayIVValidationMessages", new Locale("pt", "BR"));
				MessageProducer messageProducer = new ResourceBundleMessageProducer(
						resourceBundle);
				boolean isFormatted = false;
				pJuridica = this.frame.getClientePanel().isPJuridica();
				if (pJuridica) {
					String cnpj = (String) newValue;
					validator = new CNPJValidator(messageProducer, isFormatted);
					validator.assertValid(cnpj);
				} else { // Valida CPF
					String cpf = (String) newValue;
					validator = new CPFValidator(messageProducer, isFormatted);
					validator.assertValid(cpf);
				}
			} catch (InvalidStateException e) {
				StringBuilder msgError = new StringBuilder("");
				for (ValidationMessage message : e.getInvalidMessages()) {
					msgError.append(message.getMessage() + "\n");
				}
				JOptionPane.showMessageDialog(MDIFrame.getInstance(),
						"Houve um erro na valida��o do "
								+ (pJuridica ? "CNPJ" : "CPF") + " :" + "\n"
								+ msgError);
				PIVLogSettings.getInstance().error(e.getMessage(), e);
				return false;
			}

		}
		return true;
	}

	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		ArrayList<PIVContactModel> contatos;
		if ((contatos = frame.getContatos()) != null && contatos.size() >= 1)
			return true;
		JOptionPane
				.showMessageDialog(
						MDIFrame.getInstance(),
						"� necess�rio ao menos o cadastro de um contato. Favor checar o cadastro novamente.");
		return false;
	}

	@Override
	public void afterInsertData() {
		if (this.organizaCliente != null) {
			try {
				if(this.gridFrame!=null) this.gridFrame.reloadData();
				this.frame.closeFrame();
			} catch (PropertyVetoException e) {
				PIVLogSettings.getInstance().error(e.getMessage(), e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * Method called by the Form panel to insert new data.
	 * 
	 * @param newValueObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {

			PIVClientModel vo = (PIVClientModel) newPersistentObject;
			/* vo.setNomeimagem(this.frame.getClientePanel().getFileNameIcon()); */
			vo.setAtivo(true);
			vo.setPjuridica(this.frame.getClientePanel().isPJuridica());

			PIVAddressModel endereco = vo.getEndereco();
			if (!(endereco == null))
				if (!(endereco.getCep() == null)) {
					PIVAddressModel findEndereco = PIVAddressFunction.getEndereco(endereco
							.getCep());
					if (findEndereco == null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				} else
					vo.setEndereco(null);

			PIVAddressModel endereco_entrega = vo.getEndereco_entrega();
			if (!(endereco_entrega == null))
				if (!(endereco_entrega.getCep() == null)) {
					PIVAddressModel findEndereco = PIVAddressFunction.getEndereco(endereco
							.getCep());
					if (findEndereco == null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				} else
					vo.setEndereco_entrega(null);

			pdao.save(vo);

			// copia o logo para o servidor

			String fullpathfile = this.frame.getClientePanel().getFullPathIcon();
			
			String filename = this.frame.getClientePanel().getFileNameIcon();
			
			if(!filename.equals("")) {
			
				int lastFileSep = fullpathfile.lastIndexOf(File.separator) + 1;
				int lastDot = fullpathfile.lastIndexOf('.');
	
				// Set to end of file when no file extension exists
				if (lastDot == -1) {
					lastDot = fullpathfile.length();
				}
				
				filename = fullpathfile.substring(lastFileSep, lastDot);
				String fileExt = fullpathfile.substring(lastDot, fullpathfile.length());
	
				// Atualiza o nome do arquivo no bd
				/*String fileNamePlusExt = filename + fileExt;*/
				
				gerenciarDiretorio.copiaLogoDirDefault(
						vo, fullpathfile, fileExt);
				
				vo.setNomeimagem(vo.getId()+fileExt);
				
				pdao.update(vo);
			
			}

			ArrayList<PIVContactModel> contatos;
			if ((contatos = frame.getContatos()) != null)
				for (PIVContactModel contato : contatos) {
					contato.setCliente(vo);
					pdao.save(contato);
				}

			// atualiza pk no controller de contatos, caso o usu�rio salve os
			// dados do cliente primeiro para depois salvar os contatos
			this.frame.setPk(vo.getId());

			if (this.organizaCliente != null) { // se o objeto 'organizacliente'
												// n�o for nulo, significa que
												// foi acionado pela tela de
												// gerenciamento de rela��o
												// comercial
				organizaCliente.add(vo, true, false/*, 0*/);
				PIVEventMngrFunc
						.getInstance()
						.registerEvent(
								vo,
								PIVEventMngrFunc
										.getCodEvento(PIVEventMngrFunc.evento.NOVO_CLIENTE),
								organizaCliente.getNivelcomercial(),
								PIVEventMngrFunc.evento.NOVO_CLIENTE,
								"Cliente - "
										+ vo.getId().toString()
										+ "_"
										+ vo.getRazaosocial()
										+ " - adicionado no n�vel comercial "
										+ PIVBusRelMainCFController
												.getNivelDesc(organizaCliente
														.getNivelcomercial()));

			}

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVClientModel vo = (PIVClientModel) persistentObject;
			vo.setNomeimagem(this.frame.getClientePanel().getFileNameIcon());
			vo.setAtivo(true);
			vo.setPjuridica(this.frame.getClientePanel().isPJuridica());

			((PIVClientModel) persistentObject).setPjuridica(this.frame
					.getClientePanel().isPJuridica());

			// Atualiza o cadastro de contatos
			/*
			 * ArrayList<PIVContatoModel> contatos; if((contatos =
			 * frame.getContatos())!=null) for( PIVContatoModel contato :
			 * contatos ) { if(!(contato.getId() == null)) pdao.update(contato);
			 * else { contato.setCliente(vo); pdao.save(contato); } }
			 */

			PIVAddressModel endereco = vo.getEndereco();
			if (!(endereco == null))
				if (!(endereco.getCep() == null)) {
					PIVAddressModel findEndereco = PIVAddressFunction.getEndereco(endereco
							.getCep());
					if (findEndereco == null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				} else
					vo.setEndereco(null);

			PIVAddressModel endereco_entrega = vo.getEndereco_entrega();
			if (!(endereco_entrega == null))
				if (!(endereco_entrega.getCep() == null)) {
					PIVAddressModel findEndereco = PIVAddressFunction.getEndereco(endereco
							.getCep());
					if (findEndereco == null)
						pdao.save(endereco);
					else
						pdao.update(endereco);
				} else
					vo.setEndereco_entrega(null);

			// copia o logo para o servidor
			
			String fullpathfile = this.frame.getClientePanel().getFullPathIcon();
			
			String filename = this.frame.getClientePanel().getFileNameIcon();
			
			if(!filename.equals("")&&!(((PIVClientModel)oldPersistentObject).getNomeimagem()!=null&&((PIVClientModel)oldPersistentObject).getNomeimagem().equals(filename))) {
			
				int lastFileSep = fullpathfile.lastIndexOf(File.separator) + 1;
				int lastDot = fullpathfile.lastIndexOf('.');
	
				// Set to end of file when no file extension exists
				if (lastDot == -1) {
					lastDot = fullpathfile.length();
				}
				
				/*String filename = fullpathfile.substring(lastFileSep, lastDot);*/
				String fileExt = fullpathfile.substring(lastDot, fullpathfile.length());
	
				// Atualiza o nome do arquivo no bd
				String fileNamePlusExt = filename + fileExt;
				
				gerenciarDiretorio.copiaLogoDirDefault(
						vo, fullpathfile, fileExt);
				
				vo.setNomeimagem(vo.getId().toString()+fileExt);
				
				pdao.update(vo);
			
			}
			
			pdao.update(vo);

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			PIVClientModel vo = (PIVClientModel) persistentObject;
			vo.setAtivo(false);

			pdao.update(vo);

			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	public PIVClientMngtGFView getGridFrame() {
		return this.gridFrame;
	}

	public PIVClientMngtDFView getFrame() {
		return this.frame;
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}

}