package org.playiv.com.mvc.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JTree;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVClienteLookupController  extends LookupController {

	private PIVDao pdao = PIVDao.getInstance();
	
	public PIVClienteLookupController( ) {
		setCodeSelectionWindow(GRID_FRAME);
		setLookupDataLocator(new LookupDataLocator() {

			public Response validateCode(String code) {
				try {
					
					PIVClientModel cliente = (PIVClientModel) PIVDao.getInstance().getObjectByID(PIVClientModel.class, "id", code);
					
//					Session session = pdao.getSession();
//					PIVClientModel cliente = (PIVClientModel) session.createQuery("from org.callcenter.frontend.model.PIVClientModel where id = '" + code + "'").uniqueResult();
//					session.close();
					
					List<PIVClientModel> clientes = new ArrayList<PIVClientModel>();
					clientes.add(cliente);
					
					VOListResponse voResponse = new VOListResponse(clientes,false,clientes.size());
					
					return voResponse;
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			public Response loadData(int action, int startIndex,
					Map filteredColumns, ArrayList currentSortedColumns,
					ArrayList currentSortedVersusColumns, Class valueObjectType) {
				// method not required...
				try {

					String baseSQL = "from org.playiv.com.mvc.model.PIVClientModel as cliente";
					Session session = pdao.getSession();

					Response res = HibernateUtils.getBlockFromQuery(
							action,
							startIndex,
							50, // block size...
							filteredColumns, currentSortedColumns,
							currentSortedVersusColumns, valueObjectType,
							baseSQL, new Object[0], new Type[0], "cliente",
							pdao.getSessionFactory(), session);

					session.close();

					return res;
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			@Override
			public Response getTreeModel(JTree tree) {
				/*return new VOResponse(new DefaultTreeModel(
						new OpenSwingTreeNode()));*/
				return null;
			}

		});
		
		this.setLookupValueObjectClassName(PIVClientModel.class.getCanonicalName());
		this.addLookup2ParentLink("id", "id");
		this.addLookup2ParentLink("razaosocial", "razaosocial");
		this.addLookup2ParentLink("nomefantasia", "nomefantasia");
		this.setAllColumnVisible(true);
		this.setAutoFitColumns(true);
//		this.setVisibleColumn("id", true);
//		this.setVisibleColumn("razaosocial", true);
//		this.setVisibleColumn("nomefantasia", true);
//		this.setPreferredWidthColumn("codigo", 120);
//		this.setPreferredWidthColumn("nome", 180);
//		this.setFilterableColumn("codigo", true);
//		this.setFilterableColumn("nome", true);
//		this.setSortableColumn("codigo", true);
//		this.setSortableColumn("nome", true);
		this.setFramePreferedSize(new Dimension(450, 550));
		
	}
	
}
