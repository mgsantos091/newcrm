package org.playiv.com.mvc.controller;




import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVUserModel;
import org.playiv.com.mvc.view.PIVUserMngtGFView;


/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Grid controller for employees.
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */
public class PIVUserMngtGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	private PIVUserMngtGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVUserModel voref;

	public PIVUserMngtGFController( ) {
		grid = new PIVUserMngtGFView(this);
		MDIFrame.add(grid);
	}

	/**
	 * Callback method invoked when the user has double clicked on the selected
	 * row of the grid.
	 * 
	 * @param rowNumber
	 *            selected row index
	 * @param persistentObject
	 *            v.o. related to the selected row
	 */
	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVUserModel vo = (PIVUserModel) persistentObject;
		new PIVUserMngtDFController(grid, vo.getId());
	}

	/**
	 * Callback method invoked to load data on the grid.
	 * 
	 * @param action
	 *            fetching versus: PREVIOUS_BLOCK_ACTION, NEXT_BLOCK_ACTION or
	 *            LAST_BLOCK_ACTION
	 * @param startPos
	 *            start position of data fetching in result set
	 * @param filteredColumns
	 *            filtered columns
	 * @param currentSortedColumns
	 *            sorted columns
	 * @param currentSortedVersusColumns
	 *            ordering versus of sorted columns
	 * @param valueObjectType
	 *            v.o. type
	 * @param otherGridParams
	 *            other grid parameters
	 * @return response from the server: an object of type VOListResponse if
	 *         data loading was successfully completed, or an ErrorResponse
	 *         object if some error occours
	 */
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			String baseSQL = "from org.playiv.com.mvc.model.PIVUserModel as Usuario";
			Session session = pdao.getSession(); // obtain a JDBC connection and
			
			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "Usuario",
			        pdao.getSessionFactory()	,
			        session
			      );// instantiate a new Session

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method invoked when the user has clicked on delete button and the grid is
	 * in READONLY mode.
	 * 
	 * @param persistentObjects
	 *            value objects to delete (related to the currently selected
	 *            rows)
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVUserModel usuario : ((ArrayList<PIVUserModel>) persistentObjects)) {
				Session session = pdao.getSession();
				session.beginTransaction();
				session.delete(usuario);
				session.flush();
				session.getTransaction().commit();
				session.close();
				grid.reloadData();
			}
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public PIVUserModel getVORef() {
		return this.voref;
	}

	public PIVUserMngtGFView getGrid() {
		return this.grid;
	}

}