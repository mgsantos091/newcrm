package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDesktopFunc;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.library.rtf.PIVRTFTemplateSalesMerge;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVSalesDetailModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.mvc.view.PIVSalesMngtGFView;


/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Grid controller for employees.
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */
public class PIVSalesMngtGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVSalesMngtGFView grid;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVClientModel cliente;
	
	private PIVBusRelMainCFController organizacliente;
	
	private PIVSellerModel vendedor;
		
	public PIVBusRelMainCFController getOrganizacliente() {
		return organizacliente;
	}

	public PIVSalesMngtGFController( PIVBusRelMainCFController organizacliente , PIVClientModel cliente ) {
		this.cliente = cliente;
		this.organizacliente = organizacliente;
		grid = new PIVSalesMngtGFView(this);
		MDIFrame.add(grid);
	}
	
	public PIVSalesMngtGFController( PIVSellerModel vendedor ) {
		this(null,null);
		this.vendedor = vendedor;
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVSalesMainModel vo = (PIVSalesMainModel) persistentObject;
		new PIVBulsRelMainDetailCFController(this.organizacliente,vo.getCliente(), vo);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = null;
			if(this.cliente!=null&&this.organizacliente!=null)
				baseSQL = "from org.playiv.com.mvc.model.PIVSalesMainModel as Venda where Venda.cliente.id = " + cliente.getId() + " and Venda.vendedor.id = " + PIVUserSession.getInstance().getVendedorSessao().getId();
			else if(this.vendedor!=null)
				baseSQL = "from org.playiv.com.mvc.model.PIVSalesMainModel as Venda where Venda.vendedor.id = " + this.vendedor.getId();
				
			
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			/*Response res = HibernateUtils.getBlockFromClass(
					PIVClienteModel.class, filteredColumns,
					currentSortedColumns, currentSortedVersusColumns, action,
					startIndex, 50, FetchMode.DEFAULT, session);*/

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "Venda",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	public void gerarArquivo( ) {
		
		int rowNumber = this.grid.getGrid().getTable().getSelectedRow();
		PIVSalesMainModel venda_pai = (PIVSalesMainModel) this.grid.getGrid().getVOListTableModel().getObjectForRow(rowNumber);
		
		String baseSQL = "from VendaFilho in class org.playiv.com.mvc.model.PIVSalesDetailModel where VendaFilho.vendaPai.id = " + venda_pai.getId();
		Session session = PIVDao.getInstance().getSession();

		ArrayList<PIVSalesDetailModel> venda_filho = (ArrayList<PIVSalesDetailModel>) session.createQuery(
				baseSQL).list();

		session.close();
		
		PIVRTFTemplateSalesMerge rtfTemplateTest = new PIVRTFTemplateSalesMerge( venda_pai );
		rtfTemplateTest.setVenda_pai(venda_pai);
		rtfTemplateTest.setVenda_filho(venda_filho);
		try {
			rtfTemplateTest.gerarDocumento();
			rtfTemplateTest.copyFileToServer( );
		} catch (Exception e) {
			PIVLogSettings.getInstance().error(e.getMessage(),e);
			e.printStackTrace();
		}

	}
	
	public void abrirArquivoRelacionado( ) {
		int rowNumber = this.grid.getGrid().getTable().getSelectedRow();
		
		PIVSalesMainModel venda_pai = (PIVSalesMainModel) this.grid.getGrid().getVOListTableModel().getObjectForRow(rowNumber);
		
		String nomePastaUsuario = venda_pai.getCliente().getId().toString();
		
		String destino = PIVDirectoryMngtFunc.getInstance().getArquivosDeClientePath()
				+ nomePastaUsuario
				+ "\\propostas"
				+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
		
		/*String destino = PIVDirectoryMngtFunc.getInstance().getArquivosDeClientePath( ) + "propostas\\";*/
		String nomeArquivo = "proposta" + "_" + /*venda_pai.getCliente().getNomefantasia().trim() + "_" + */venda_pai.getId()+".rtf";
		
		/*destino = "\"" + destino + nomeArquivo + "\"";*/
		destino += nomeArquivo;
		File arquivo = new File(destino);
		if(arquivo.isFile())
			/*PIVDesktopFunc.getInstance().abrirPrograma(arquivo);*/
			PIVDesktopFunc.getInstance().abrirPrograma(destino);
		else
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"O documento n�o foi encontrado, caso o problema persista, entre em contato com o administrador do sistema",
							"Erro ao abrir proposta", JOptionPane.ERROR_MESSAGE);
	}
	
	/*@Override
	public boolean beforeInsertGrid(GridControl grid) {
		PIVSalesMainModel venda = (PIVSalesMainModel) grid.getVOListTableModel().getObjectForRow(grid.getSelectedRow());
		new PIVBulsRelMainDetailCFController( this.cliente , venda);
		return true;
	}*/
	
	/*@Override
	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects) throws Exception {
		return new VOResponse(null);
	}*/
	
	/*@Override
	public void createValueObject(ValueObject valueObject) throws Exception { }*/
	
	public PIVClientModel getCliente() {
		return cliente;
	}
	
	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVSalesMainModel venda : ((ArrayList<PIVSalesMainModel>) persistentObjects)) {
				Session session = pdao.getSession();
				session.beginTransaction();
				venda.setAtivo(false);
				session.update(venda);
				session.flush();
				session.getTransaction().commit();
				session.close();
				grid.reloadData();
			}
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVSalesMainModel venda = (PIVSalesMainModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!venda.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

}