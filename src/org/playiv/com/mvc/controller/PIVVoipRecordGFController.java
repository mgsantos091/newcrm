package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVVoipRecordModel;
import org.playiv.com.mvc.view.PIVVoipRecordGFView;

public class PIVVoipRecordGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVVoipRecordGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVClientModel cliente;
	
	public PIVVoipRecordGFController(PIVClientModel cliente) {
		grid = new PIVVoipRecordGFView(this);
		this.cliente = cliente;
		MDIFrame.add(grid);
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVVoipRecordModel vo = (PIVVoipRecordModel) persistentObject;
		new PIVVoipRecordDFController(grid, vo.getId());
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVVoipRecordModel as Voip where Voip.cliente.id = '" + this.cliente.getId() + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "Voip",
			        pdao.getSessionFactory(),
			        session
			      );
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVVoipRecordModel voipRecord = (PIVVoipRecordModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!voipRecord.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

	public PIVClientModel getCliente() {
		return cliente;
	}

}