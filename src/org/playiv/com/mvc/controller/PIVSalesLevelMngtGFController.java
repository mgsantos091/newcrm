package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVSalesLevelModel;
import org.playiv.com.mvc.view.PIVSalesLevelMngtGFView;

public class PIVSalesLevelMngtGFController extends GridController implements GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVSalesLevelMngtGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	public PIVSalesLevelMngtGFController() {
		grid = new PIVSalesLevelMngtGFView(this);
		MDIFrame.add(grid);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVSalesLevelModel as EstagioVenda";
			Session session = pdao.getSession();

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "EstagioVenda",
			        pdao.getSessionFactory()	,
			        session
			      );

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {
		try {
			for (PIVSalesLevelModel estagioVenda : ((ArrayList<PIVSalesLevelModel>) newValueObjects)) {
				estagioVenda.setAtivo(true);
				pdao.save(estagioVenda);
			}
			return new VOListResponse(newValueObjects, false,newValueObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		try {
			for (PIVSalesLevelModel estagioVenda : ((ArrayList<PIVSalesLevelModel>) persistentObjects)) { pdao.update(estagioVenda); }
			return new VOListResponse(persistentObjects, false,persistentObjects.size());
			/*return new VOListResponse(new Boolean(true));*/
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVSalesLevelModel estagioVenda : ((ArrayList<PIVSalesLevelModel>) persistentObjects)) {
				estagioVenda.setAtivo(false); pdao.update(estagioVenda);
			}
			/*return new VOListResponse(persistentObjects, false,persistentObjects.size());*/
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	@Override
	  public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {
		// realiza a valida��o do atributo 'nome'
	    if(attributeName=="estagio") {
	    	String valor = (String) newValue;
	    	Session session = pdao.getSession();
	    	PIVSalesLevelModel estagioVenda = (PIVSalesLevelModel) session.createQuery("from org.playiv.com.mvc.model.PIVSalesLevelModel as EstagioVenda where EstagioVenda.estagio = '" + valor + "'").uniqueResult();
	    	session.close();
	    	if(estagioVenda!=null) { // estagio com o mesmo nome encontrada
	    		if(estagioVenda.isAtivo())
	    			JOptionPane.showMessageDialog(null,"J� existe um est�gio de nome " + estagioVenda.getEstagio() + " ativo, por favor, escolha uma nome diferente.","Est�gio j� existe",JOptionPane.ERROR_MESSAGE);
	    		else {
	    			int opt = JOptionPane.showConfirmDialog(null, "Existe um est�gio de nome " + estagioVenda.getEstagio() + " desativado, deseja ativa-lo?", "Est�gio desativada", JOptionPane.YES_NO_OPTION);
	    			if(opt==JOptionPane.YES_OPTION) {
	    				estagioVenda.setAtivo(true);
	    				pdao.update(estagioVenda);
	    			}
	    		}
	    		return false;
	    	}	    	
	    }
	    return true;
	  }

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVSalesLevelModel estagioVenda = (PIVSalesLevelModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!estagioVenda.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}
	
}