package org.playiv.com.mvc.controller;

import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.view.PIVSalesMoreInfoDFView;

public class PIVSalesMoreInfoDFController extends FormController {

	private PIVSalesMoreInfoDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVSalesMainModel vendasPai;

	public PIVSalesMoreInfoDFController( PIVSalesMainModel vendasPai ) {
		this.vendasPai = vendasPai;
		frame = new PIVSalesMoreInfoDFView(this, vendasPai);
		MDIFrame.add(frame);
		frame.getPanel().setMode(Consts.EDIT);
		frame.getPanel().reload();
	}

	public Response loadData(Class valueObjectClass) {
		try {
			return new VOResponse(this.vendasPai);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}

	}

	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVSalesMainModel vo = (PIVSalesMainModel) persistentObject;
			if(vo.getCampanha()!=null && vo.getCampanha().getNome_campanha().equals("")) vo.setCampanha(null);
			if(vo.getEstagio() != null && vo.getEstagio().getEstagio().equals("")) vo.setEstagio(null);
			Double valtotal = vo.getValorfrete() + vo.getValordespesa() + vo.getValordesconto() + vo.getValorsubtotal() - vo.getValordesconto();
			vo.setValortotal(valtotal);
			pdao.update(vo);
			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

}