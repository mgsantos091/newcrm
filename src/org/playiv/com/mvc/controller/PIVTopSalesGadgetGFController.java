package org.playiv.com.mvc.controller;

import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVSalesMainModel;

public class PIVTopSalesGadgetGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVDao pdao = PIVDao.getInstance();

	public PIVTopSalesGadgetGFController( ) {
		
	}

	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVSalesMainModel vo = (PIVSalesMainModel) persistentObject;
		new PIVBulsRelMainDetailCFController(null,vo.getCliente(), vo);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVSalesMainModel as Venda where Venda.vendedor.id = " + PIVUserSession.getInstance().getVendedorSessao().getId() + " and Venda.vendaefetivada = false and Venda.ativo = true order by Venda.valortotal DESC";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			ArrayList<PIVSalesMainModel> topVendas = (ArrayList<PIVSalesMainModel>) session.createQuery(baseSQL).setMaxResults(10).list();
				
			session.close();

			return new VOListResponse(topVendas, false, topVendas.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

}