package org.playiv.com.mvc.controller;


import org.hibernate.Session;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVProductModel;
import org.playiv.com.mvc.view.PIVProdMngtDFView;
import org.playiv.com.mvc.view.PIVProdMngtGFView;


/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVProdMngtDFController extends FormController implements IPIVFormController {

	private PIVProdMngtGFView gridFrame = null;
	private PIVProdMngtDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;

	public PIVProdMngtDFController(PIVProdMngtGFView gridFrame, Integer pk) {
		this.gridFrame = gridFrame;
		this.pk = pk;
		frame = new PIVProdMngtDFView(this, pk);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.READONLY);
			frame.getPanel().reload();
		} else
			frame.getPanel().setMode(Consts.INSERT);
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			int row = gridFrame.getGrid().getSelectedRow();

			if (row != -1) {
				PIVProductModel gridVO = (PIVProductModel) gridFrame.getGrid()
						.getVOListTableModel().getObjectForRow(row);
				pk = gridVO.getId();
			}

			String baseSQL = "from Produto in class org.playiv.com.mvc.model.PIVProductModel where Produto.id = '"
					+ pk + "'";
			Session session = pdao.getSession();

			PIVProductModel vo = (PIVProductModel) session.createQuery(baseSQL)
					.uniqueResult();

			session.close();

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to insert new data.
	 * 
	 * @param newValueObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {
			PIVProductModel vo = (PIVProductModel) newPersistentObject;
			pdao.save(vo);

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVProductModel vo = (PIVProductModel) persistentObject;
			pdao.update(vo);
			
			return new VOResponse(persistentObject);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			PIVProductModel vo = (PIVProductModel) persistentObject;
			vo.setAtivo(false);
			pdao.update(vo);			
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons( currentMode );
	}


	public PIVProdMngtGFView getGridFrame( ) {
		return this.gridFrame;
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}
	
}