package org.playiv.com.mvc.controller;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.tree.client.TreeController;
import org.openswing.swing.tree.client.TreeDataLocator;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.general.PIVGlobalSettings;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVCubeFileTreeNodeModel;
import org.playiv.com.swing.general.PIVCubeTreeFrame;

public class PIVCubeTreeFrameController extends TreeDataLocator implements TreeController , TreeWillExpandListener {
	
	private PIVClientModel cliente;
	
	/*public static void main(String args[]) {
		PIVDao pdao = PIVDao.getInstance();
		
		String sql = "from org.playiv.com.mvc.model.PIVClientModel where id = 1";
		
		Session sessao = pdao.getSession();
		
		PIVClientModel cliente = (PIVClientModel) sessao.createQuery(sql).uniqueResult();
		
		sessao.close();
		
		PIVCubeTreeFrameController controller = new PIVCubeTreeFrameController( cliente );
		
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(controller.getTree(),BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		
	}*/
	
	public PIVCubeTreeFrameController( PIVClientModel cliente ) {
		this.cliente = cliente;
		setNodeNameAttribute("nome");
	}

	@Override
	public void treeWillCollapse(TreeExpansionEvent tee)
			throws ExpandVetoException {
	}

	@Override
	public void treeWillExpand(TreeExpansionEvent tee)
			throws ExpandVetoException {
	}

	@Override
	public void leftClick(DefaultMutableTreeNode node) {		
	}

	@Override
	public boolean rightClick(DefaultMutableTreeNode node) {
		return false;
	}

	@Override
	public void doubleClick(DefaultMutableTreeNode node) {
	}

	@Override
	public Response getTreeModel(JTree tree) {
		
		tree.addTreeWillExpandListener(this);

	    DefaultMutableTreeNode root = new OpenSwingTreeNode();
	    DefaultTreeModel model = new DefaultTreeModel(root);
		
		String nomePastaUsuario = this.cliente.getId().toString();
		
		Vector dirView = new Vector();

		PIVDirectoryMngtFunc.getInstance().checkOrganizacaoPastas();
		
		String pathArquivos = PIVDirectoryMngtFunc.getInstance().getArquivosDeClientePath()
				+ nomePastaUsuario
				+ (PIVGlobalSettings.isWindows() ? "\\" : "//");
		
		File dir = new File(pathArquivos);
		
		if (dir.isDirectory()) {
			File[] listOfFiles = dir.listFiles();
			if (listOfFiles == null)
				JOptionPane.showMessageDialog(
						null,
						"Sem permiss�o para acessar a pasta: "
								+ dir.getAbsolutePath(), "Sem acesso",
						JOptionPane.ERROR_MESSAGE);
			else {
				for (int i = 0; i < listOfFiles.length; i++) {
					dirView.add(listOfFiles[i]); // .getName())
				}
			}
		}
		
		PIVCubeFileTreeNodeModel arquivo;
		DefaultMutableTreeNode node;
		for ( int i=0; i < dirView.size(); i++ ) {
			File nextFile = (File) dirView.get(i);
			arquivo = new PIVCubeFileTreeNodeModel(nextFile.getName());
			arquivo.setPathCompleto(nextFile.getAbsolutePath());
			arquivo.setFile(nextFile.isFile());
			if (nextFile.isDirectory() == true) {
				DefaultMutableTreeNode subNodeDir = new OpenSwingTreeNode(arquivo);
				root.add((getNodeFromDir(subNodeDir,nextFile.listFiles())));
			} else {
				DefaultMutableTreeNode subNode = new OpenSwingTreeNode(arquivo);
				root.add(subNode);
			}
		}
		
		return new VOResponse(model);
		
	}
	
	public DefaultMutableTreeNode getNodeFromDir( DefaultMutableTreeNode node , File[] listOfFiles ) {
		PIVCubeFileTreeNodeModel arquivo;
		for ( int i=0; i < listOfFiles.length; i++ ) {
			File nextFile = (File) listOfFiles[i];
			arquivo = new PIVCubeFileTreeNodeModel(nextFile.getName());
			arquivo.setPathCompleto(nextFile.getAbsolutePath());
			arquivo.setFile(nextFile.isFile());
			if (nextFile.isDirectory() == true) {
				DefaultMutableTreeNode subNodeDir = new OpenSwingTreeNode(arquivo);
				node.add((getNodeFromDir(subNodeDir,nextFile.listFiles())));
			} else {
				DefaultMutableTreeNode subNodeDirFile = new OpenSwingTreeNode(arquivo);
				node.add(subNodeDirFile);
			}
		}
		return node;
	}
	
	@Override
	public void loadDataCompleted(boolean error) {
		/*Thread t = new Thread() {
			public void run() {
				synchronized (lock) {
					while (tree.isVisible())
						try {
							lock.wait();
						} catch (InterruptedException e) {
							PIVLogSettings.getInstance().error(e.getMessage(),
									e);
							e.printStackTrace();
						}
					System.out.println("Working now");
				}
			}
		};
		t.start();

		tree.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				synchronized (lock) {
					tree.setVisible(false);
					lock.notify();
				}
			}
		});

		try {
			t.join();
		} catch (InterruptedException e1) {
			PIVLogSettings.getInstance().error(e1.getMessage(), e1);
			e1.printStackTrace();
		}*/
	}

}