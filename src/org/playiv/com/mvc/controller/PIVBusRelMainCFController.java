package org.playiv.com.mvc.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.hibernate.Session;
import org.openswing.swing.mdi.client.MDIFrame;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVBusRelClientInfoFunc;
import org.playiv.com.library.function.PIVBusRelCommandSubjectFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.general.PIVBusRelClientList;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVBusRelModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVGarbageClientModel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVBusRelClickPopUpMenuListener;
import org.playiv.com.swing.general.PIVBusRelElementComponent;
import org.playiv.com.swing.general.PIVBusRelLevelLayerPanel;
import org.playiv.com.swing.general.PIVBusRelPopUpMenu;

public class PIVBusRelMainCFController {

	private PIVSellerModel vendedorlogado = PIVUserSession.getInstance()
			.getVendedorSessao();

	public enum nivel {
		SUSPECT, PROSPECT, FORECAST, CUSTOMER
	}

	private PIVBusRelMainCFController proxOrganizaCliente;

	private PIVBusRelClientInfoFunc gerInfoCli;

	// box de clientes relacionado ao painel gerenciado pela classe
	private PIVBusRelLevelLayerPanel boxCliRelPainel;

	private PIVDao pdao = PIVDao.getInstance();

	private nivel nivelcomercial;

	private JPanel painel;
	private JScrollPane scrollPanelSelecionado;

	private ArrayList<PIVBusRelClientList> lista_elementos = new ArrayList<PIVBusRelClientList>();

	private PIVBusRelCommandSubjectFunc commandsSubj;

	private PIVBusRelElementComponent elementoSelecionado;
	private PIVBusRelModel nivelRelacionamentoSelecionado;

	private JTabbedPane tab;

	private PIVBusRelPopUpMenu popMenu;

	// var de controle para adicionar o listener de mouse nos elementos
	// cadastrados
	private boolean addListenerElementos = false;

	private PIVBusRelMngtGFController gerRelComPrincipController;

	@SuppressWarnings("unchecked")
	public PIVBusRelMainCFController(JTabbedPane tab, JScrollPane scrollPanelSelecionado , JPanel painel,
			nivel nivelcomercial, PIVBusRelLevelLayerPanel boxCliRelPainel,
			PIVBusRelCommandSubjectFunc commandsSubj,
			PIVBusRelMngtGFController gerRelComPrincipController) {

		this.tab = tab;
		this.painel = painel;
		this.scrollPanelSelecionado = scrollPanelSelecionado;
		this.nivelcomercial = nivelcomercial;
		this.boxCliRelPainel = boxCliRelPainel;
		this.commandsSubj = commandsSubj;
		this.gerRelComPrincipController = gerRelComPrincipController;
	}

	public void add(PIVClientModel cliente, boolean setFocus, boolean chkInclusao/*, Integer nivelClassificao*/) {
		boolean podeIncluir = false;
		if(chkInclusao) {
			if(verificarInclusaoRelacionamento(cliente)) {
				podeIncluir = true;
			}
		} else podeIncluir = true;
		if(podeIncluir) {
			if (cliente != null) {
				if (lista_elementos.indexOf(cliente) >= 0) // cliente j� existe
				{
					JOptionPane
							.showMessageDialog(
									MDIFrame.getInstance(),
									"Cliente j� inserido, favor selecionar outro cliente...",
									"Cliente presente na atual n�vel comercial",
									JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(this.nivelcomercial == nivel.CUSTOMER) { // apresenta a tela de 'venda efetivada'
					
				}
				adicionaElementoPainel(cliente, setFocus/*, nivelClassificao*/);
				adicionaCliente(cliente/*, nivelClassificao*/);
			}
		}
	}

	private void adicionaCliente(PIVClientModel cliente/*, Integer nivelClassificacao*/) {
		PIVBusRelModel relacionamento = new PIVBusRelModel();
		relacionamento.setVendedor(vendedorlogado);
		relacionamento.setCompletado(false);
		relacionamento.setCliente(cliente);
		relacionamento.setIdnivelcomercial(getNivelCod(this.nivelcomercial));
		/*relacionamento.setNivel_classificao(nivelClassificacao);*/
		pdao.save(relacionamento);
	}

	private void adicionaElementoPainel(PIVClientModel cliente, boolean setFocus /*, Integer nivelClassificao*/) {

		PIVBusRelElementComponent elemento = new PIVBusRelElementComponent(this/*, nivelClassificao*/);
		elemento.setCliente(cliente);

		elemento.addMouseListener(new MouseAdapter() {

			private PIVBusRelElementComponent elemento;
			@Override
			public void mouseExited(MouseEvent e) {
				PIVBusRelMainCFController.this.gerInfoCli.resetaPainel();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				elemento = (PIVBusRelElementComponent) e.getComponent();
				PIVClientModel cliente = elemento.getCliente();
				PIVBusRelMainCFController.this.gerInfoCli.atualizaPainel(
						cliente, PIVBusRelMainCFController.this.nivelcomercial);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				
				elemento = (PIVBusRelElementComponent) e.getComponent();
				
				PIVClientModel cliente = elemento.getCliente();
				System.out.println(elemento + " clicado");
				
				PIVBusRelMainCFController.this.setElementoSelecionado(elemento);
				PIVBusRelMainCFController.this.gerInfoCli
						.setLblClienteSelecionadoValor(cliente.getId()
								.toString() + " - " + cliente.getRazaosocial());
				
				if(e.getClickCount()>=2)
					new PIVSalesMngtGFController( PIVBusRelMainCFController.this , elemento.getCliente() );
				
			}
		});
		boxCliRelPainel.addCelulaPintada();

		PIVBusRelClientList lista_elemento = new PIVBusRelClientList(cliente,
				elemento);

		this.painel.add(elemento);
		this.lista_elementos.add(lista_elemento);

		if (setFocus) {
			setElementoSelecionado(elemento);
			setFocusPainelRelacionado();
		}

		if (this.popMenu != null)
			elemento.addMouseListener(new PIVBusRelClickPopUpMenuListener(
					popMenu));

		this.painel.repaint();

	}

	public void remove(PIVClientModel cliente) {
		if (!(cliente == null)) {
			removeElementoPainel(cliente);
			removeCliente(cliente);
			this.painel.repaint();
		}
	}

	private void removeCliente(PIVClientModel cliente) {
		PIVBusRelModel relcomercial = buscaRelacionamento(cliente);
		Session session = pdao.getSession();
		session.delete(relcomercial);
		session.close();
	}

	private PIVBusRelModel buscaRelacionamento(PIVClientModel cliente) {
		Session session = pdao.getSession();
		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento "
				+ "where Relacionamento.vendedor.id = '"
				+ this.vendedorlogado.getId()
				+ "' and "
				+ "Relacionamento.idnivelcomercial = '"
				+ getNivelCod(this.nivelcomercial)
				+ "' and "
				+ "Relacionamento.completado = false and "
				+ "Relacionamento.cliente.id = '"
				+ cliente.getId()
				+ "' and "
				+ "Relacionamento.ativo = true ";

		PIVBusRelModel relcomercial = (PIVBusRelModel) session.createQuery(
				baseSQL).uniqueResult();

		session.close();

		return relcomercial;
	}

	private ArrayList<PIVBusRelModel> buscaRelacionamentos(
			PIVClientModel cliente) {
		Session session = pdao.getSession();
		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento "
				+ "where Relacionamento.vendedor.id = '"
				+ this.vendedorlogado.getId()
				+ "' and "
				+ "Relacionamento.idnivelcomercial = '"
				+ getNivelCod(this.nivelcomercial)
				+ "' and "
				+ "Relacionamento.cliente.id = '" + cliente.getId() + "'";

		@SuppressWarnings("unchecked")
		ArrayList<PIVBusRelModel> relcomercial = (ArrayList<PIVBusRelModel>) session
				.createQuery(baseSQL).list();

		session.close();

		return relcomercial;
	}

	private void removeElementoPainel(PIVClientModel cliente) {
		this.boxCliRelPainel.removeCelulaPintada();
		int index = getIndexListaElementoByCliente(cliente);
		if (index >= 0) {
			PIVBusRelClientList lista_elemento = this.lista_elementos
					.get(index);
			PIVBusRelElementComponent elemento = lista_elemento.getElemento();
			this.painel.remove(elemento);
			this.lista_elementos.remove(index);
			this.painel.repaint();
		}
	}
	
	private void removeElementoPainel(PIVBusRelElementComponent elemento) {
		
		this.boxCliRelPainel.removeCelulaPintada();
		this.painel.remove(elemento);
		
		PIVBusRelClientList listaASerRemovida = null;
		for(PIVBusRelClientList lista_elemento : this.lista_elementos) {
			if(lista_elemento.getElemento()==elemento) { listaASerRemovida = lista_elemento; break ; }
		}
		
		if(listaASerRemovida!=null)
			this.lista_elementos.remove(listaASerRemovida);
		
		this.painel.repaint();
		
	}

	public void sobeNivel(PIVClientModel cliente, Integer nivelClassificao) {
		int index = getIndexListaElementoByCliente(cliente);

		if (index >= 0) {
			// se houver uma classe de 'organiza cliente' linkada como pr�xima
			// efetua a opera��o de inclus�o nela, ap�s efetuar a opera��o de
			// exclus�o
			if (!(proxOrganizaCliente == null)) {
				
				removeElementoPainel(cliente);
	
				PIVBusRelModel relcomercial = buscaRelacionamento(cliente);
				relcomercial.setCompletado(true);
				pdao.update(relcomercial);
	
				/*
				 * Session session = pdao.getSession(); String baseSQL =
				 * "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento "
				 * + "where Relacionamento.vendedor.id = '" +
				 * this.vendedorlogado.getId() + "' and " +
				 * "Relacionamento.idnivelcomercial = '" + getNivelCod() + "' and "
				 * + "Relacionamento.completado = false and " +
				 * "Relacionamento.cliente.id = '" + cliente.getId() + "'";
				 * 
				 * PIVNivelComercialRelacionamento relcomercial =
				 * (PIVNivelComercialRelacionamento)
				 * session.createQuery(baseSQL).uniqueResult();
				 * relcomercial.setCompletado(true);
				 * 
				 * session.update(relcomercial);
				 * 
				 * session.close();
				 */

				// registra o evento da aumento de n�vel de relacionamento comercial do cliente
				PIVEventMngrFunc.getInstance().registerEvent(
						cliente,
						PIVEventMngrFunc.getCodEvento(PIVEventMngrFunc.evento.SUBIR_NIVEL_CLIENTE),
						this.getNivelcomercial(),
						PIVEventMngrFunc.evento.SUBIR_NIVEL_CLIENTE,
						"Cliente - "
								+ cliente.getId().toString()
								+ "_"
								+ cliente.getRazaosocial()
								+ " - foi posicionado na categoria, de: " + this.getNivelDesc() + " para " + proxOrganizaCliente.getNivelDesc() + ".");
				
				proxOrganizaCliente.add(cliente, true, false/*, nivelClassificao*/);

				if(proxOrganizaCliente.nivelcomercial == nivel.CUSTOMER)
					new PIVBusRelSalesPerformedController(cliente); // apresenta a tela de parab�ns pela venda

			}
				
		}
	}

	public static Integer getNivelCod(nivel nivelcomercial) {
		switch (nivelcomercial) {
		case SUSPECT:
			return 1;
		case PROSPECT:
			return 2;
		case FORECAST:
			return 3;
		case CUSTOMER:
			return 4;
		}
		new IllegalArgumentException();
		return null;
	}

	public static nivel getNivelEnum(Integer nivelcomercial) {
		switch (nivelcomercial) {
		case 1:
			return nivel.SUSPECT;
		case 2:
			return nivel.PROSPECT;
		case 3:
			return nivel.FORECAST;
		case 4:
			return nivel.CUSTOMER;
		}
		new IllegalArgumentException();
		return null;
	}

	public static String getNivelDesc(nivel nivelcomercial) {
		switch (nivelcomercial) {
		case SUSPECT:
			return "Suspect";
		case PROSPECT:
			return "Prospect";
		case FORECAST:
			return "Forecast";
		case CUSTOMER:
			return "Customer";
		}
		new IllegalArgumentException();
		return null;
	}

	public String getNivelDesc() {
		switch (this.nivelcomercial) {
		case SUSPECT:
			return "Suspect";
		case PROSPECT:
			return "Prospect";
		case FORECAST:
			return "Forecast";
		case CUSTOMER:
			return "Customer";
		}
		new IllegalArgumentException();
		return null;
	}

	public void setElementoSelecionado(
			PIVBusRelElementComponent elementoSelecionado) {
		
		this.elementoSelecionado = elementoSelecionado;
		String clienteTitulo = null;
		
		if (elementoSelecionado != null) {
			PIVClientModel cliente = this.elementoSelecionado.getCliente();
			clienteTitulo = cliente.getId().toString() + " - "
					+ cliente.getRazaosocial();
			this.nivelRelacionamentoSelecionado = buscaRelacionamento(elementoSelecionado.getCliente());
		}
		
		if (this.gerInfoCli != null)
			this.gerInfoCli.setLblClienteSelecionadoValor(clienteTitulo);

		this.commandsSubj.notifyObservers(elementoSelecionado);
		
	}

	public void setProxOrganizaCliente(
			PIVBusRelMainCFController proxOrganizaCliente) {
		this.proxOrganizaCliente = proxOrganizaCliente;
	}

	private int getIndexListaElementoByCliente(PIVClientModel cliente) {
		Integer id = cliente.getId();
		for (PIVBusRelClientList lista_elemento : this.lista_elementos) {
			PIVClientModel _cliente = lista_elemento.getCliente();
			if (_cliente.getId() == id)
				return this.lista_elementos.indexOf(lista_elemento);
		}
		return -1;
	}

	public void setGerInfoCli(PIVBusRelClientInfoFunc gerInfoCli) {
		this.gerInfoCli = gerInfoCli;
	}

	public void setPopMenu(PIVBusRelPopUpMenu popMenu) {
		this.popMenu = popMenu;
		if (!addListenerElementos) {
			for (PIVBusRelClientList lista_elemento : this.lista_elementos) {
				PIVBusRelElementComponent elemento = lista_elemento
						.getElemento();
				elemento.addMouseListener(new PIVBusRelClickPopUpMenuListener(
						popMenu));
			}
			addListenerElementos = true;
		}
	}

	public void addElemLixera(PIVClientModel cliente) {

		int index = getIndexListaElementoByCliente(cliente);

		if (index >= 0) {

			PIVBusRelModel relcomercial = buscaRelacionamento(cliente);
			relcomercial.setAtivo(false);
			pdao.update(relcomercial);

			removeElementoPainel(cliente);

			// Avisa os 'observers' que n�o existe cliente pendente selecionado
			setElementoSelecionado(null);

			// re-carrega a p�gina principal de gerenciamento de relacionamento
			// comercial
			this.gerRelComPrincipController.carregar();

			this.gerInfoCli.setLblClienteSelecionadoValor("");
			
			// registra evento da inclus�o do cliente na lixeira
			PIVEventMngrFunc.getInstance().registerEvent(
					cliente,
					PIVEventMngrFunc.getCodEvento(PIVEventMngrFunc.evento.LIXEIRA),
					this.getNivelcomercial(),
					PIVEventMngrFunc.evento.LIXEIRA,
					"Cliente - "
							+ cliente.getId().toString()
							+ "_"
							+ cliente.getRazaosocial()
							+ " - do n�vel comercial - " + this.getNivelDesc() + " - foi removido da carteira de clientes.");
			
			String motivo = JOptionPane.showInputDialog(MDIFrame.getInstance(),"Qual foi o motivo da remo��o do " + getNivelDesc() + "?","Motivo",JOptionPane.QUESTION_MESSAGE);
			
			PIVGarbageClientModel relacionamentoClienteLixeira = new PIVGarbageClientModel( );
			relacionamentoClienteLixeira.setCliente(cliente);
			relacionamentoClienteLixeira.setVendedor(vendedorlogado);
			relacionamentoClienteLixeira.setMotivo(motivo);
			
			pdao.save(relacionamentoClienteLixeira);			

		}

	}

	public void carregar() {

		setElementoSelecionado(null);

		Integer idvendedor = vendedorlogado.getId();
		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento where Relacionamento.vendedor.id = '"
				+ idvendedor
				+ "' and Relacionamento.idnivelcomercial = '"
				+ getNivelCod(this.nivelcomercial)
				+ "' and Relacionamento.completado = false "
				+ " and Relacionamento.ativo = true ";

		Session session = pdao.getSession(); // obtain a JDBC connection and
		ArrayList<PIVBusRelModel> relcomercial = (ArrayList<PIVBusRelModel>) session
				.createQuery(baseSQL).list();
		session.close();

		if(this.lista_elementos.size()>=1) { // existem elementos pr�vios
			// apaga os existentes
			ArrayList<PIVBusRelClientList> lista_elementos_clone = (ArrayList<PIVBusRelClientList>) this.lista_elementos.clone();
			for(PIVBusRelClientList elemento_lista : lista_elementos_clone) {
				PIVBusRelElementComponent elemento = elemento_lista.getElemento();
				removeElementoPainel(elemento);
			}
			this.lista_elementos = new ArrayList<PIVBusRelClientList>();
		}
		
		if (relcomercial != null && relcomercial.size() > 0) {
			for (PIVBusRelModel relacionamento : relcomercial) {
				PIVClientModel cliente = relacionamento.getCliente();
				/*Integer nivelClassificao = relacionamento.getNivel_classificao();*/
				adicionaElementoPainel(cliente, false/*, nivelClassificao*/);
			}
		}

	}

	private boolean verificarInclusaoRelacionamento(PIVClientModel cliente) {

		Session session = pdao.getSession();

		Integer idvendedor = vendedorlogado.getId();

		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento where Relacionamento.vendedor.id = '"
				+ idvendedor
				+ "' "
				+ "and Relacionamento.cliente.id = "
				+ cliente.getId();

		PIVBusRelModel relacionamento = (PIVBusRelModel) session.createQuery(
				baseSQL).uniqueResult();

		if (relacionamento != null) { // cliente j� pertence a carteira de
										// clientes do vendedor
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"O Cliente j� existe na sua carteira de clientes, caso ele n�o esteja vis�vel nos n�veis comerciais\n"
									+ "Cheque a lixeira, aonde � poss�vel recuperar o cliente para sua carteira",
							"Cliente j� presente na carteira de clientes.",
							JOptionPane.ERROR_MESSAGE);
			return false;
		} else {
			baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento where Relacionamento.vendedor.id = '"
					+ idvendedor
					+ "' and Relacionamento.idnivelcomercial = '"
					+ getNivelCod(this.nivelcomercial)
					+ "' and Relacionamento.completado = false "
					+ " and Relacionamento.ativo = true ";
			int nQtdeRel = session.createQuery(baseSQL).list().size();
			if( nQtdeRel >= 50 ) {
				JOptionPane
				.showMessageDialog(
						MDIFrame.getInstance(),
						"O n�mero limite de relacionamentos para este n�vel j� foi alcan�ado. Por favor, avance com algum " + getNivelDesc() + " ou descarte-o na lixeira para continuar.",
						"No. limite alcan�ado para esta carteira.",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}

		session.close();
		
		return true;
	}
	
	public nivel getNivelcomercial( ) {
		return this.nivelcomercial;
	}
	
	public void setScrollPanelSelecionado(JScrollPane scrollPanelSelecionado) {
		this.scrollPanelSelecionado = scrollPanelSelecionado;
	}
	
	public void setFocusPainelRelacionado( ) {
		tab.setSelectedComponent(this.scrollPanelSelecionado);
	}

	public PIVBusRelModel getNivelRelacionamentoSelecionado() {
		return nivelRelacionamentoSelecionado;
	}

}