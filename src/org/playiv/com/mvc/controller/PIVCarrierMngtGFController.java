package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;


import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVCarrierModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.view.PIVCarrierMngtGFView;
import org.playiv.com.mvc.view.PIVClientMngtGFView;


/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Grid controller for employees.
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */
public class PIVCarrierMngtGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVCarrierMngtGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	public PIVCarrierMngtGFController( ) {
		grid = new PIVCarrierMngtGFView(this);
		MDIFrame.add(grid);
	}

	/**
	 * Callback method invoked when the user has double clicked on the selected
	 * row of the grid.
	 * 
	 * @param rowNumber
	 *            selected row index
	 * @param persistentObject
	 *            v.o. related to the selected row
	 */
	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVCarrierModel vo = (PIVCarrierModel) persistentObject;
		new PIVCarrierMngtDFController(grid, vo.getId());
	}

	/**
	 * Callback method invoked to load data on the grid.
	 * 
	 * @param action
	 *            fetching versus: PREVIOUS_BLOCK_ACTION, NEXT_BLOCK_ACTION or
	 *            LAST_BLOCK_ACTION
	 * @param startPos
	 *            start position of data fetching in result set
	 * @param filteredColumns
	 *            filtered columns
	 * @param currentSortedColumns
	 *            sorted columns
	 * @param currentSortedVersusColumns
	 *            ordering versus of sorted columns
	 * @param valueObjectType
	 *            v.o. type
	 * @param otherGridParams
	 *            other grid parameters
	 * @return response from the server: an object of type VOListResponse if
	 *         data loading was successfully completed, or an ErrorResponse
	 *         object if some error occours
	 */
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVCarrierModel as Transportadora";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "Transportadora",
			        pdao.getSessionFactory()	,
			        session
			      );
			
			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method invoked when the user has clicked on delete button and the grid is
	 * in READONLY mode.
	 * 
	 * @param persistentObjects
	 *            value objects to delete (related to the currently selected
	 *            rows)
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVCarrierModel transportadora : ((ArrayList<PIVCarrierModel>) persistentObjects)) {
				transportadora.setAtivo(false);
				pdao.update(transportadora);
			}
			grid.reloadData();
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method used to define the background color for each cell of the grid.
	 * 
	 * @param rowNumber
	 *            selected row index
	 * @param attributedName
	 *            attribute name related to the column currently selected
	 * @param value
	 *            object contained in the selected cell
	 * @return background color of the selected cell
	 */
	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVCarrierModel transportadora = (PIVCarrierModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!transportadora.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}

	public PIVCarrierMngtGFView getGrid( ) {
		return this.grid;
	}

}