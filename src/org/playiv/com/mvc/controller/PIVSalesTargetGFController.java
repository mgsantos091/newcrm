package org.playiv.com.mvc.controller;

import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVSalesTargetModel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.mvc.view.PIVSalesTargetGFView;

public class PIVSalesTargetGFController extends GridController implements GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVSalesTargetGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVSellerModel vendedor;
	
	public PIVSalesTargetGFController( PIVSellerModel vendedor ) {
		
		this.vendedor = vendedor;
		
		grid = new PIVSalesTargetGFView(this);
		MDIFrame.add(grid);
		
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		
		try {
			
			Session s = pdao.getSession();
			/*String baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta <= 4";*/
			String baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta <= 3";
			ArrayList<PIVSalesTargetModel> metas_bcg = (ArrayList<PIVSalesTargetModel>) s.createQuery(baseSQL).list();
			s.close();
			/*if(metas_bcg.size()!=4) {*/
			if(metas_bcg.size()!=3) {
				/*for(int x=1;x<=4;x++) {*/
				for(int x=1;x<=3;x++) {
					PIVSalesTargetModel meta_bcg = new PIVSalesTargetModel( );
					meta_bcg.setIdmeta(x);
					meta_bcg.setValormeta(0d);
					pdao.save(meta_bcg);
				}
			}
			
			baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial" + (this.vendedor!=null?" where MetaComercial.vendedor.id = " + vendedor.getId():"");
			
			s = pdao.getSession();

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "MetaComercial",
			        pdao.getSessionFactory()	,
			        s
			      );

			s.close();

			return res;
			
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
		
	}

	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {
		try {
			for (PIVSalesTargetModel meta : ((ArrayList<PIVSalesTargetModel>) newValueObjects)) {
				pdao.save(meta);
			}
			return new VOListResponse(newValueObjects, false,newValueObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		try {
			for (PIVSalesTargetModel meta : ((ArrayList<PIVSalesTargetModel>) persistentObjects)) {
				pdao.update(meta);
			}
			return new VOListResponse(persistentObjects, false, persistentObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVSalesTargetModel meta : ((ArrayList<PIVSalesTargetModel>) persistentObjects)) {
				pdao.delete(meta);
			}
			return new VOListResponse(persistentObjects, false, persistentObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
}