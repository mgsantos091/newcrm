package org.playiv.com.mvc.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVSalesDetailModel;
import org.playiv.com.mvc.model.PIVSalesMainModel;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVProductSalesPanel;

public class PIVProductSaleGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;

	private PIVDao pdao = PIVDao.getInstance();

	private PIVProductSalesPanel vendasPainel;

	private GridControl grid;

	public void setGrid(GridControl grid) {
		this.grid = grid;
	}

	private PIVSalesMainModel vendasPai;

	private PIVClientModel cliente;
	
	private PIVProductSalesPanel painelExpandido;
	
	public void setPainelExpandido(PIVProductSalesPanel painelExpandido) {
		this.painelExpandido = painelExpandido;
	}

	public PIVClientModel getCliente() {
		return cliente;
	}

	private PIVUserSession pusersession = PIVUserSession.getInstance();
	
	public PIVProductSaleGFController(PIVSalesMainModel vendasPai , PIVClientModel cliente) throws ParseException {

		this.vendasPai = vendasPai;
		this.cliente = cliente;

		/*this.vendasPainel = new PIVProductSalesPanel(this,true);
		this.grid = vendasPainel.getGrid();*/
		
/*		PIVSalesDetailModel novoItem = new PIVSalesDetailModel( );
		
		try {
			this.createValueObject(novoItem);
		} catch (Exception e) {
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}*/
		
		/*this.grid.setMode(Consts.EDIT);*/
		
	}
	/*
	public PIVProductSaleGFController( PIVSalesMainModel vendasPai , PIVClientModel cliente , PIVProductSalesPanel painel ) throws ParseException {
		this.vendasPai = vendasPai;
		this.cliente = cliente;
		this.vendasPainel = painel;
		this.grid = vendasPainel.getGrid();		
	}*/
	
	
	@Override
	public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {

		PIVSalesDetailModel item = (PIVSalesDetailModel) this.grid.getVOListTableModel().getObjectForRow(rowNumber);
		
		if(item!=null) {
		
			Double valvenda = item.getValvenda();
			Double valdesconto = item.getValdesconto();
			Double porcdesconto = item.getPorcdesconto();
			Double qtditens = item.getQtditens();
			Double valsubtotal = item.getValsubtotal();
			Double valtotal = item.getValtotal();
			
			if(attributeName.equals("valdesconto")||attributeName.equals("porcdesconto")||attributeName.equals("qtditens")) {
				
				if(attributeName.equals("qtditens"))
					qtditens = (Double) newValue;
				
				valsubtotal = valvenda * qtditens;
				item.setValsubtotal(valsubtotal);
				
				if(attributeName.equals("valdesconto")) {
					valdesconto = (Double) newValue;
					porcdesconto = valdesconto / valsubtotal * 100;
				} else if(attributeName.equals("porcdesconto")) {
					porcdesconto = (Double) newValue;
					if(porcdesconto>=0&&porcdesconto<=100)
						valdesconto = valsubtotal / 100 * porcdesconto;
					else {
						JOptionPane.showMessageDialog(MDIFrame.getInstance(), "Aten��o, o valor informado: " + porcdesconto + " � inv�lido, por favor, digite-o novamente.","Valor inv�lido",JOptionPane.ERROR_MESSAGE);
						return false;
					}
				}
				else {
					porcdesconto = 0d;
					valdesconto = 0d;
					valsubtotal = valvenda * qtditens;
					item.setValsubtotal(valsubtotal);
				}
				
				item.setValdesconto(valdesconto);
				item.setPorcdesconto(porcdesconto);

				/*valtotal = ( valvenda - ( valvenda * (valdesconto==null?0:valdesconto) / 100 ) ) * qtditens;*/
				valtotal = (valvenda * qtditens) - (valdesconto==null?0:valdesconto);
				item.setValtotal(valtotal);
				
			}
		
		}

		return true;

	}

	/*public PIVProductSalesPanel getVendasPanel() {
		return this.vendasPainel;
	}*/

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	@SuppressWarnings("unchecked")
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVSalesDetailModel as VendasDetalhe where VendasDetalhe.vendaPai.id = " + this.vendasPai.getId() +  " and VendasDetalhe.ativo = true ";
			
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session
			
			/*ArrayList<PIVSalesDetailModel> itens = (ArrayList<PIVSalesDetailModel>) session.createQuery(baseSQL).list();

			if(itens.size()==0) {
				if(novoItem==null)
				{
					novoItem = new PIVSalesDetailModel( );
					novoItem.setVendaPai(this.vendasPai);
				}
				itens.add(novoItem);
			}*/
			
			Response res = HibernateUtils.getBlockFromQuery(
					action,
					startIndex,
					50, // block size...
					filteredColumns, currentSortedColumns,
					currentSortedVersusColumns, valueObjectType, baseSQL,
					new Object[0], new Type[0], "VendasDetalhe",
					pdao.getSessionFactory(), session);

			session.close();
			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	* Method invoked when the user has clicked on save button and the grid is
	* in INSERT mode.
	* 
	* @param rowNumbers
	*            row indexes related to the new rows to save
	* @param newValueObjects
	*            list of new value objects to save
	* @return an ErrorResponse value object in case of errors, VOListResponse
	*         if the operation is successfully completed
	*/
	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {

		/*if(this.vendasPai==null) {
			this.vendasPai = new PIVSalesMainModel();
			this.vendasPai.setCliente(this.cliente);
			this.vendasPai.setVendedor(PIVUserSession.getInstance().getVendedorSessao());
			pdao.save(vendasPai);
		}*/
		
		ArrayList<PIVSalesDetailModel> itens = (ArrayList<PIVSalesDetailModel>) newValueObjects;
		
		for(PIVSalesDetailModel item : itens) {
			item.setAtivo(true);
			item.setVendaPai(this.vendasPai);
			pdao.save(item);
		}
		
		grid.getBottomTable().reload();
		
		if(painelExpandido!=null) {
			painelExpandido.getGrid().reloadData();
		}

		return new VOListResponse(itens, false, itens.size());

	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		
		ArrayList<PIVSalesDetailModel> itens = (ArrayList<PIVSalesDetailModel>) persistentObjects;
		
		for(PIVSalesDetailModel item : itens)
			pdao.update(item);

		if(painelExpandido!=null)
			painelExpandido.getGrid().reloadData();
		
		grid.getBottomTable().reload();
		
		return new VOListResponse(persistentObjects, false,
				persistentObjects.size());
	}

	/**
	 * Method invoked when the user has clicked on delete button and the grid is
	 * in READONLY mode.
	 * 
	 * @param persistentObjects
	 *            value objects to delete (related to the currently selected
	 *            rows)
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		
		ArrayList<PIVSalesDetailModel> itens = (ArrayList<PIVSalesDetailModel>) persistentObjects;
		
		for(PIVSalesDetailModel item : itens) {
			item.setAtivo(false);
			pdao.update(item);
		}
		
		if(painelExpandido!=null) {
			painelExpandido.getGrid().reloadData();
		}
		
		grid.getBottomTable().reload();
		
		return new VOResponse(new Boolean(true));

	}

/*	*//**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 *//*
	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			
			PIVSalesDetailModel item = (PIVSalesDetailModel) persistentObject;
			item.setAtivo(false);
			pdao.update(item);

			return new VOResponse(new Boolean(true));

		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}*/
	
	public PIVSalesMainModel getVendasPai() {
		return vendasPai;
	}

}