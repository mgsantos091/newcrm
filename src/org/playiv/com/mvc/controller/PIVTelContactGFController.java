package org.playiv.com.mvc.controller;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.model.PIVLocalConfModel;
import org.playiv.com.mvc.model.PIVTelToCallModel;
import org.playiv.com.mvc.view.PIVTelContactGFView;
import org.playiv.com.mvc.view.PIVVoipRecordGFView;
//import org.playiv.com.peers.gui.PIVSipVoipFrame;

public class PIVTelContactGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	private PIVTelContactGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVClientModel cliente;
	
	private PIVVoipRecordGFView pivVoipRecordGFView;
	
	public PIVTelContactGFController(PIVClientModel cliente) {
		grid = new PIVTelContactGFView(this);
		this.cliente = cliente;
	}
	
//	public PIVTelContactGFController(PIVClientModel cliente, PIVBusRelMainCFController organizaCliente) {
//		this(cliente);
//		this.organizaCliente = organizaCliente;
//	}

	public PIVTelContactGFController(PIVVoipRecordGFView pivVoipRecordGFView,
			PIVClientModel cliente) {
		this.pivVoipRecordGFView = pivVoipRecordGFView;
		this.cliente = cliente;
		grid = new PIVTelContactGFView(this);
	}

	@Override
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		
		try {
			
			int idcliente = cliente.getId();
//			int idVendedor = psessao.getVendedorSessao().getId();
			String baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contato where Contato.cliente.id = " + idcliente + " and (Contato.telefone <> '' or Contato.celular <> '') and Contato.ativo = true";
			Session session = pdao.getSession();
			
			ArrayList<PIVContactModel> contatos = (ArrayList<PIVContactModel>) session.createQuery(baseSQL).list();
			ArrayList<PIVTelToCallModel> registros = new ArrayList<PIVTelToCallModel>();
			
			for( PIVContactModel contato : contatos ) {
				PIVTelToCallModel registro = new PIVTelToCallModel();
				registro.setNome(contato.getNome());
				registro.setTipo(contato.isResponsavel() ? "RESPONSÁVEL" : "CONTATO");
				registro.setFuncao(contato.getFuncao());
				if( contato.getTelefone() != null && !contato.getTelefone().equals("") && contato.getCelular() != null && !contato.getCelular().equals("") ) {
					registro.setNumero(contato.getTelefone());
					registros.add(registro);
					PIVTelToCallModel registro2 = (PIVTelToCallModel) registro.clone();
					registro2.setNumero(contato.getCelular());
					registros.add(registro2);				
				} else {
					registro.setNumero(contato.getTelefone() == null ? contato.getCelular() : contato.getTelefone());
					registros.add(registro);
				}
			}
			
			if(cliente.getTelefone() != null && !cliente.getTelefone().equals("")) {
				PIVTelToCallModel registro = new PIVTelToCallModel();
				registro.setNome("EMPRESA");
				registro.setTipo("PRINCIPAL");
				registro.setNumero(cliente.getTelefone());
				registros.add(registro);
			}
			
			session.close();
			
			Response res = new VOListResponse(registros,false,registros.size());

//			Response res = HibernateUtils.getBlockFromQuery(
//					action,
//					startIndex,
//					50, // block size...
//					filteredColumns, currentSortedColumns,
//					currentSortedVersusColumns, valueObjectType, baseSQL,
//					new Object[0], new Type[0],
//					"Contato",
//					pdao.getSessionFactory(), session);

//			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
		
	}
	
	@Override
	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		
//		String baseSQL = "from Conf in class org.playiv.com.mvc.model.PIVLocalConfModel";
//		Session session = PIVDao.getInstance().getSession();

//		PIVLocalConfModel empresa = (PIVLocalConfModel) session.createQuery(
//				baseSQL).uniqueResult();

//		session.close();
		
//		String domain = empresa.getVoip_dominio();
		
		PIVTelToCallModel vo = (PIVTelToCallModel) persistentObject;
		
		String numero = vo.getNumero();
		
		new PIVVoipRecordDFController(this.pivVoipRecordGFView,this.cliente,numero);
		
		try {
			grid.closeFrame();
		} catch (PropertyVetoException e) {
			PIVLogSettings.getInstance().error(e.getMessage(), e);
//			e.printStackTrace();
		}
		
//		PIVSipVoipFrame frame = PIVSipVoipFrame.getInstance();
//		frame.setUriText("sip:" + numero + "@" + domain);
		
//		frame.ligar();
				
	}
	
}