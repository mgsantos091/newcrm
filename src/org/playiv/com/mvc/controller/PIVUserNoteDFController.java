package org.playiv.com.mvc.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDirectoryMngtFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.function.PIVSoundMngtFunc;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVUserNoteModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVProductModel;
import org.playiv.com.mvc.view.PIVUserNoteDFView;
import org.playiv.com.mvc.view.PIVUserNoteGFView;
import org.playiv.com.mvc.view.PIVProdMngtDFView;
import org.playiv.com.mvc.view.PIVProdMngtGFView;
import org.playiv.com.swing.general.PIVClientNotePanel;

/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Detail frame controller for the employee
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */

public class PIVUserNoteDFController extends FormController implements
		IPIVFormController {

	private GridControl grid;
	
	private PIVUserNoteDFView frame = null;
	private PIVDao pdao = PIVDao.getInstance();
	private Integer pk = null;
	private PIVClientModel cliente;

	private PIVBusRelMainCFController organizaCliente;

	private PIVSoundMngtFunc gerGrvVoz;

	public PIVSoundMngtFunc getGerGrvVoz() {
		return gerGrvVoz;
	}

	private JLabel audioInfoLabel;

	private String nomeArquivoSomTemp;
	
	private Boolean playAuto = false;
	
	private int last_id_play = 0;

	private String nomeArquivoSom;

	private String pathNomeArquivoSom;
	
	public String getNomeArquivoSomTemp() {
		return nomeArquivoSomTemp;
	}

	public void setInfo(JLabel info) {
		this.audioInfoLabel = info;
	}

	public PIVUserNoteDFController(GridControl grid , Integer pk,
			PIVClientModel cliente) {
		this.cliente = cliente;
		this.grid = grid;
		this.pk = pk;
		frame = new PIVUserNoteDFView(this, pk, cliente);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.READONLY);
			frame.getPanel().reload();
		} else
			frame.getPanel().setMode(Consts.INSERT);
	}

	public PIVUserNoteDFController(GridControl grid, Integer pk,
			PIVClientModel cliente, PIVBusRelMainCFController organizaCliente) {
		this(grid, pk, cliente);
		this.organizaCliente = organizaCliente;
	}

	public PIVUserNoteDFController(GridControl grid2, Integer id,
			PIVClientModel cliente2,
			PIVBusRelMainCFController organizacliente2, Boolean playAuto) {
		this(grid2, id, cliente2, organizacliente2);
		this.playAuto = playAuto;
	}

	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			int row = grid.getSelectedRow();

			if (row != -1) {
				PIVUserNoteModel gridVO = (PIVUserNoteModel) grid.getVOListTableModel().getObjectForRow(row);
				pk = gridVO.getId();
			}

			String baseSQL = "from Anotacao in class org.playiv.com.mvc.model.PIVUserNoteModel where id = '"
					+ pk + "'";
			Session session = pdao.getSession();

			PIVUserNoteModel vo = (PIVUserNoteModel) session.createQuery(
					baseSQL).uniqueResult();

			session.close();

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to insert new data.
	 * 
	 * @param newValueObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {
			PIVUserNoteModel vo = (PIVUserNoteModel) newPersistentObject;
			vo.setCliente(cliente);
			vo.setVendedor(PIVUserSession.getInstance().getVendedorSessao());
			pdao.save(vo);
			
			PIVEventMngrFunc
					.getInstance()
					.registerEvent(
							cliente,
							PIVEventMngrFunc
									.getCodEvento(PIVEventMngrFunc.evento.REGISTRO_OBSERVACAO),
							(organizaCliente==null?null:organizaCliente.getNivelcomercial()),
							PIVEventMngrFunc.evento.REGISTRO_OBSERVACAO,
							"Foi realizado um registro de observa��o para o cliente - "
									+ cliente.getId().toString() + "_"
									+ cliente.getRazaosocial() + ".");
			
			this.grid.reloadData();
			
			if(this.pk==null||this.pk==0)
				this.pk = vo.getId();
			
			// som foi gravado antes do registro ser salvo
			if(nomeArquivoSomTemp!=null) {
				
				pathNomeArquivoSom = PIVDirectoryMngtFunc.getInstance()
						.getArquivosDeSomPath();
				
				int lastFileSep = nomeArquivoSomTemp.lastIndexOf(File.separator) + 1;
				int lastDot = nomeArquivoSomTemp.lastIndexOf('.');

				// Set to end of file when no file extension exists
				if (lastDot == -1) {
					lastDot = nomeArquivoSomTemp.length();
				}
				
				String filename = nomeArquivoSomTemp.substring(lastFileSep, lastDot);
				String fileExt = nomeArquivoSomTemp.substring(lastDot, nomeArquivoSomTemp.length());

				// Atualiza o nome do arquivo no bd
				String fileNamePlusExt = this.pk + "_" + filename + fileExt;
				
				PIVDirectoryMngtFunc.getInstance().copiaArquivosPServidor( nomeArquivoSomTemp  , pathNomeArquivoSom + fileNamePlusExt , fileNamePlusExt);
				
				vo.setNome_arquivo_voz(fileNamePlusExt);
			
				pdao.update(vo);

				nomeArquivoSomTemp = null;
			}		
						
			return new VOResponse(vo);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVUserNoteModel vo = (PIVUserNoteModel) persistentObject;
			pdao.update(vo);

			return new VOResponse(persistentObject);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			PIVUserNoteModel vo = (PIVUserNoteModel) persistentObject;
			pdao.delete(vo);
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	public GridControl getGridFrame() {
		return this.grid;
	}

	@Override
	public Integer getPk() {
		return this.pk;
	}

	public void executaPlayPause( ) {
		this.executaPlayPause(this.pk);
	}
	
	public void executaPlayPause(Integer id_anotacao) {
		
		if(id_anotacao!=null&&id_anotacao!=0) {
			PIVDao pdao = PIVDao.getInstance();
			Session session = pdao.getSession();
			
			PIVUserNoteModel anotacao = (PIVUserNoteModel) session
					.createQuery(
							"from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.id = "
									+ id_anotacao).uniqueResult();
			
			session.close();
			if (anotacao != null) {
				nomeArquivoSom = anotacao.getNome_arquivo_voz();
				pathNomeArquivoSom = PIVDirectoryMngtFunc.getInstance()
						.getArquivosDeSomPath();
				pathNomeArquivoSom += nomeArquivoSom;
				if (nomeArquivoSom != null && !nomeArquivoSom.equals("")) {
					if (gerGrvVoz == null) {
						gerGrvVoz = PIVSoundMngtFunc.getInstance();
						if(this.playAuto) {
							gerGrvVoz.setAuto(this);
						}
						gerGrvVoz.setAudioInfoLabel(this.audioInfoLabel);
					} else {
						if (gerGrvVoz.isSoundBeingUsed()) {
							gerGrvVoz.pararReproducao();
						}
					}
					
					try {
						gerGrvVoz.comecarReproducao(PIVUserNoteDFController.this.pathNomeArquivoSom);
						last_id_play = id_anotacao;
					} catch (UnsupportedAudioFileException e) {
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
					} catch (IOException e) {
						e.printStackTrace();
						PIVLogSettings.getInstance().error(e.getMessage(), e);
					}
					
				} else {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),
							"Nenhum arquivo encontrado...",
							"Grave uma nova observa��o",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		} else
		{
			if(nomeArquivoSomTemp!=null) {
				if (gerGrvVoz == null) {
					gerGrvVoz = PIVSoundMngtFunc.getInstance();
					gerGrvVoz.setAudioInfoLabel(this.audioInfoLabel);
				} else {
					if (gerGrvVoz.isSoundBeingUsed()) {
						gerGrvVoz.pararReproducao();
					}
				}
				try {
					gerGrvVoz.comecarReproducao(nomeArquivoSomTemp);
				} catch (UnsupportedAudioFileException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					PIVLogSettings.getInstance().error(e1.getMessage(), e1);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					PIVLogSettings.getInstance().error(e1.getMessage(), e1);
				}
			}
		}
	}

	public void executaGravar() {
		/*int id;*/
		/*if (((id = frame.getJpAnotacao().getPk())) != 0) {*/
		
		pathNomeArquivoSom = "";
		
		if(pk!=null&&pk!=0) {
			PIVDao pdao = PIVDao.getInstance();
			Session session = pdao.getSession();
			PIVUserNoteModel anotacao = (PIVUserNoteModel) session
					.createQuery(
							"from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.id = "
									+ pk).uniqueResult();
			session.close();
			if (anotacao != null) {
				nomeArquivoSom = anotacao.getNome_arquivo_voz();
				pathNomeArquivoSom = PIVDirectoryMngtFunc.getInstance()
						.getArquivosDeSomPath();
				if (nomeArquivoSom != null && !nomeArquivoSom.equals("")) {
					JOptionPane
							.showMessageDialog(
									MDIFrame.getInstance(),
									"J� existe um arquivo de som anexado ao registro, para criar um novo registro de som, favor criar um novo registro de observa��o",
									"Foi encontrado um arquivo anexado",
									JOptionPane.ERROR_MESSAGE);
				} else {
					/*JOptionPane.showMessageDialog(MDIFrame.getInstance(),
							"Nenhum arquivo encontrado, sera criado um novo.",
							"Criando novo arquivo...",
							JOptionPane.INFORMATION_MESSAGE);*/
					
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyyMMddHHmmss");
					Date date = new Date();
					nomeArquivoSom = pk + "_"
							/*+ frame.getJpAnotacao().getNomefantasia() + "_"*/
							+ dateFormat.format(date) + ".wmv";
					pathNomeArquivoSom += nomeArquivoSom;

					gerGrvVoz = PIVSoundMngtFunc.getInstance();
					gerGrvVoz.setAudioInfoLabel(this.audioInfoLabel);
					gerGrvVoz.comecarGravacao(pathNomeArquivoSom);
					/*this.audioInfoLabel.setText("Grava��o iniciada...");*/

					anotacao.setNome_arquivo_voz(nomeArquivoSom);
					pdao.update(anotacao);
				}
			}
		} else {
			pathNomeArquivoSom = PIVDirectoryMngtFunc.getInstance()
					.getDiretorioTemp();
			DateFormat dateFormat = new SimpleDateFormat(
					"yyyyMMddHHmmss");
			Date date = new Date();
			nomeArquivoSom = dateFormat.format(date) + ".wmv";
			pathNomeArquivoSom += nomeArquivoSom;
			nomeArquivoSomTemp = pathNomeArquivoSom;
			gerGrvVoz = PIVSoundMngtFunc.getInstance();
			gerGrvVoz.setAudioInfoLabel(this.audioInfoLabel);
			gerGrvVoz.comecarGravacao(pathNomeArquivoSom);
		}
		
		/*else {
			JOptionPane
					.showMessageDialog(
							MDIFrame.getInstance(),
							"� necess�rio salvar a observa��o antes de realizar a grava��o.",
							"Aten��o", JOptionPane.INFORMATION_MESSAGE);
		}*/
	}

	/*public void executaPausar() {
		int id;
		if (((id = frame.getJpAnotacao().getPk())) != 0) {
			PIVDao pdao = PIVDao.getInstance();
			Session session = pdao.getSession();
			PIVUserNoteModel anotacao = (PIVUserNoteModel) session
					.createQuery(
							"from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.id = "
									+ id).uniqueResult();
			session.close();
			if (anotacao != null) {
				String nomeArquivoSom = anotacao.getNome_arquivo_voz();
				String pathNomeArquivoSom = PIVDirectoryMngtFunc.getInstance()
						.getArquivosDeSomPath();
				if (nomeArquivoSom != null && !nomeArquivoSom.equals("")) {
					if (gerGrvVoz == null) {
						JOptionPane
								.showMessageDialog(
										MDIFrame.getInstance(),
										"N�o existe nenhuma reprodu��o sendo executada neste momento.",
										"Erro", JOptionPane.ERROR_MESSAGE);
						return;
					}
					gerGrvVoz.pausarReproducao();
					this.audioInfoLabel.setText("Mensagem pausada...");
				} else {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),
							"Nenhum arquivo encontrado...",
							"Grave uma nova observa��o",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}*/

	public void executaParar() {
		/*int id;*/
		/*if (((id = frame.getJpAnotacao().getPk())) != 0) {*/
		if(gerGrvVoz.isRecording()) {
			gerGrvVoz.stopRecording();
		}
		if(pk!=null&&pk!=0) {
			PIVDao pdao = PIVDao.getInstance();
			Session session = pdao.getSession();
			PIVUserNoteModel anotacao = (PIVUserNoteModel) session
					.createQuery(
							"from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.id = "
									+ pk).uniqueResult();
			session.close();
			if (anotacao != null) {
				nomeArquivoSom = anotacao.getNome_arquivo_voz();
				if (nomeArquivoSom != null && !nomeArquivoSom.equals("")) {
					if (gerGrvVoz == null) {
						JOptionPane
								.showMessageDialog(
										MDIFrame.getInstance(),
										"N�o existe nenhuma reprodu��o sendo executada neste momento.",
										"Erro", JOptionPane.ERROR_MESSAGE);
						return;
					}
					gerGrvVoz.pararReproducao();
				} else {
					JOptionPane.showMessageDialog(MDIFrame.getInstance(),
							"Nenhum arquivo encontrado...",
							"Grave uma nova observa��o",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
	
	@Override
	public void afterEditData() {
		this.frame.setButtonSoundEnable(true);
	}

	@Override
	public void afterInsertData() {
		this.frame.setButtonSoundEnable(true);
	}
	
	@Override
	public void loadDataCompleted(boolean error) {
		this.frame.setButtonSoundEnable(true);
	}

	public void executaPlayPauseProx() {
		if(this.playAuto&&this.last_id_play>0) {
			Session session = pdao.getSession();
			
			String sql = "from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.id > "
					+ this.last_id_play + " and Anotacao.vendedor.id = " + PIVUserSession.getInstance().getVendedorSessao().getId();
			
			PIVUserNoteModel anotacao = (PIVUserNoteModel) session
					.createQuery(sql).setMaxResults(1).uniqueResult();
			
			session.close();
			
			if(anotacao!=null)
				this.executaPlayPause(anotacao.getId());
		} else
			this.gerGrvVoz.setAuto(null);
	}

}