package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVCampaignCatModel;
import org.playiv.com.mvc.view.PIVCampaignCatMngtGFView;

public class PIVCampaignCatMngtGFController extends GridController implements GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVCampaignCatMngtGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	public PIVCampaignCatMngtGFController() {
		grid = new PIVCampaignCatMngtGFView(this);
		MDIFrame.add(grid);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVCampaignCatModel as CampanhaCategoria";
			Session session = pdao.getSession();

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "CampanhaCategoria",
			        pdao.getSessionFactory()	,
			        session
			      );

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {
		try {
			for (PIVCampaignCatModel categoria : ((ArrayList<PIVCampaignCatModel>) newValueObjects)) {
				categoria.setAtivo(true);
				pdao.save(categoria);
			}
			return new VOListResponse(newValueObjects, false,newValueObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		try {
			for (PIVCampaignCatModel categoria : ((ArrayList<PIVCampaignCatModel>) persistentObjects)) { pdao.update(categoria); }
			/*return new VOListResponse(persistentObjects, false,persistentObjects.size());*/
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVCampaignCatModel categoria : ((ArrayList<PIVCampaignCatModel>) persistentObjects)) {
				categoria.setAtivo(false); pdao.update(categoria);
			}
			/*return new VOListResponse(persistentObjects, false,persistentObjects.size());*/
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	@Override
	  public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {
		// realiza a valida��o do atributo 'nome'
	    if(attributeName=="nome") {
	    	String valor = (String) newValue;
	    	Session session = pdao.getSession();
	    	PIVCampaignCatModel categoria = (PIVCampaignCatModel) session.createQuery("from org.playiv.com.mvc.model.PIVCampaignCatModel CampanhaCategoria where CampanhaCategoria.nome = '" + valor + "'").uniqueResult();
	    	session.close();
	    	if(categoria!=null) { // categoria com o mesmo nome encontrada
	    		if(categoria.isAtivo())
	    			JOptionPane.showMessageDialog(null,"J� existe uma categoria de nome " + categoria.getNome() + " ativo, por favor, escolha uma nome diferente.","Categoria j� existe",JOptionPane.ERROR_MESSAGE);
	    		else {
	    			int opt = JOptionPane.showConfirmDialog(null, "Existe uma categoria de nome " + categoria.getNome() + " desativado, deseja ativa-lo?", "Categoria desativada", JOptionPane.YES_NO_OPTION);
	    			if(opt==JOptionPane.YES_OPTION) {
	    				categoria.setAtivo(true);
	    				pdao.update(categoria);
	    			}
	    		}
	    		return false;
	    	}
	    	
	    }
	    return true;
	  }

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVCampaignCatModel categoria = (PIVCampaignCatModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!categoria.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}
	
}