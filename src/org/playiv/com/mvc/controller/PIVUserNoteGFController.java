package org.playiv.com.mvc.controller;

import java.util.ArrayList;
import java.util.Map;


import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.client.GridControl;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVUserNoteModel;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVProductModel;
import org.playiv.com.mvc.view.PIVUserNoteGFView;
import org.playiv.com.mvc.view.PIVProdMngtGFView;


/**
 * <p>
 * Title: OpenSwing Framework
 * </p>
 * <p>
 * Description: Grid controller for employees.
 * </p>
 * <p>
 * Copyright: Copyright (C) 2006 Mauro Carniel
 * </p>
 * <p>
 * </p>
 * 
 * @author Mauro Carniel
 * @version 1.0
 */
public class PIVUserNoteGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVClientModel cliente;
	private GridControl grid;
	
	private PIVDao pdao = PIVDao.getInstance();
	
	private PIVBusRelMainCFController organizaCliente;
	
	public PIVUserNoteGFController( PIVClientModel cliente ) {
		this.cliente = cliente;
		/*grid = new PIVUserNoteGFView(this,cliente);*/
	}
	
	public void setGrid(GridControl grid) {
		this.grid = grid;
		/*MDIFrame.add(grid);*/
	}
	
	public PIVUserNoteGFController( PIVClientModel cliente , PIVBusRelMainCFController organizaCliente) {
		this.cliente = cliente;
		/*grid = new PIVUserNoteGFView(this,cliente,organizaCliente);*/
		this.organizaCliente = organizaCliente;
	}

	/**
	 * Callback method invoked when the user has double clicked on the selected
	 * row of the grid.
	 * 
	 * @param rowNumber
	 *            selected row index
	 * @param persistentObject
	 *            v.o. related to the selected row
	 */
	public void doubleClick(int rowNumber, ValueObject persistentObject) {
		PIVUserNoteModel vo = (PIVUserNoteModel) persistentObject;
		new PIVUserNoteDFController(this.grid, vo.getId(),cliente,organizaCliente);
	}

	/**
	 * Callback method invoked to load data on the grid.
	 * 
	 * @param action
	 *            fetching versus: PREVIOUS_BLOCK_ACTION, NEXT_BLOCK_ACTION or
	 *            LAST_BLOCK_ACTION
	 * @param startPos
	 *            start position of data fetching in result set
	 * @param filteredColumns
	 *            filtered columns
	 * @param currentSortedColumns
	 *            sorted columns
	 * @param currentSortedVersusColumns
	 *            ordering versus of sorted columns
	 * @param valueObjectType
	 *            v.o. type
	 * @param otherGridParams
	 *            other grid parameters
	 * @return response from the server: an object of type VOListResponse if
	 *         data loading was successfully completed, or an ErrorResponse
	 *  
	 *         object if some error occours
	 */
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			String baseSQL = "from org.playiv.com.mvc.model.PIVUserNoteModel as Anotacao where Anotacao.cliente.id = " + cliente.getId() + " and Anotacao.vendedor.id = " + PIVUserSession.getInstance().getVendedorSessao().getId();
			Session session = pdao.getSession(); // obtain a JDBC connection and
			
			
			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "Anotacao",
			        pdao.getSessionFactory()	,
			        session
			      );// instantiate a new Session

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}


	public GridControl getGrid() {
		return this.grid;
	}
	
	public PIVClientModel getCliente() {
		return cliente;
	}

}