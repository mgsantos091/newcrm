package org.playiv.com.mvc.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;


import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.lookup.client.LookupController;
import org.openswing.swing.lookup.client.LookupDataLocator;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.table.columns.client.DecimalColumnSettings;
import org.openswing.swing.tree.java.OpenSwingTreeNode;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVCategoryModel;
import org.playiv.com.mvc.model.PIVProductModel;


public class PIVProductLUFController extends LookupController {

	private PIVDao pdao = PIVDao.getInstance();

	public PIVProductLUFController() {
		this.setCodeSelectionWindow(this.GRID_AND_FILTER_FRAME);
		this.setLookupDataLocator(new LookupDataLocator() {

			public Response validateCode(String code) {
				try {
					Session session = pdao.getSession();
					PIVProductModel produto = (PIVProductModel) session
							.createQuery(
									"from org.playiv.com.mvc.model.PIVProductModel as Produto where Produto.id = "
											+ code).uniqueResult();
					session.close();
					return new VOResponse(produto);
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			public Response loadData(int action, int startIndex,
					Map filteredColumns, ArrayList currentSortedColumns,
					ArrayList currentSortedVersusColumns, Class valueObjectType) {
				// method not required...
				try {

					String baseSQL = "from org.playiv.com.mvc.model.PIVProductModel as Produto";
					Session session = pdao.getSession();

					Response res = HibernateUtils.getBlockFromQuery(
							action,
							startIndex,
							50, // block size...
							filteredColumns, currentSortedColumns,
							currentSortedVersusColumns, valueObjectType,
							baseSQL, new Object[0], new Type[0], "Produto",
							pdao.getSessionFactory(), session);

					session.close();

					return res;
				} catch (Exception ex) {
					ex.printStackTrace();
					PIVLogSettings.getInstance().error(ex.getMessage(), ex);
					return new ErrorResponse(ex.getMessage());
				}
			}

			@Override
			public Response getTreeModel(JTree tree) {
				/*return new VOResponse(new DefaultTreeModel(
						new OpenSwingTreeNode()));*/
				return null;
			}

		});

		this.setLookupValueObjectClassName("org.playiv.com.mvc.model.PIVProductModel");
		this.addLookup2ParentLink("id", "produto.id");
		this.addLookup2ParentLink("nome", "produto.nome");
		this.addLookup2ParentLink("valvenda", "valvenda");
		this.setAllColumnVisible(false);
		this.setVisibleColumn("id", true);
		this.setVisibleColumn("nome", true);
		this.setVisibleColumn("descricao", true);
		this.setVisibleColumn("valvenda", true);
		this.setColumnDynamicSettings("valvenda", new DecimalColumnSettings() {
			@Override
			public boolean isGrouping(int row) {
				return false;
			}
			
			@Override
			public double getMinValue(int row) {
				return 0;
			}
			
			@Override
			public double getMaxValue(int row) {
				return 99999999999999d;
			}
			
			@Override
			public int getDecimals(int row) {
				return 4;
			}
		});
		this.setPreferredWidthColumn("descricao", 300);
		this.setPreferredWidthColumn("nome", 80);
		this.setPreferredWidthColumn("valvenda", 80);
		this.setFilterableColumn("id", true);
		this.setFilterableColumn("nome", true);
		this.setFilterableColumn("descricao", true);
		this.setSortableColumn("id", true);
		this.setSortableColumn("nome", true);
		this.setFramePreferedSize(new Dimension(450, 550));

	}

}
