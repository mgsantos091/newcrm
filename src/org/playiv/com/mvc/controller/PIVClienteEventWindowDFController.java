package org.playiv.com.mvc.controller;

import org.openswing.swing.form.client.FormController;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVClientModel;

public class PIVClienteEventWindowDFController  extends FormController {

	private PIVClientModel cliente;

	public PIVClienteEventWindowDFController() {
	}

	public PIVClienteEventWindowDFController(PIVClientModel cliente) {
		this.cliente = cliente;
	}

	public Response loadData(Class valueObjectClass) {
		try {
			return new VOResponse(cliente);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public void setCliente(PIVClientModel cliente) {
		this.cliente = cliente;
	}

}