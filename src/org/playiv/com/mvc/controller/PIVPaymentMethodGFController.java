package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVPaymentMethodModel;
import org.playiv.com.mvc.view.PIVPaymentMethodGFView;

public class PIVPaymentMethodGFController extends GridController implements GridDataLocator {

	private static final long serialVersionUID = 1L;
	
	private PIVPaymentMethodGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	public PIVPaymentMethodGFController() {
		grid = new PIVPaymentMethodGFView(this);
		MDIFrame.add(grid);
	}

	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			String baseSQL = "from org.playiv.com.mvc.model.PIVPaymentMethodModel as FormaPagamento";
			Session session = pdao.getSession();

			Response res = HibernateUtils.getBlockFromQuery(
			        action,
			        startIndex,
			        50, // block size...
			        filteredColumns,
			        currentSortedColumns,
			        currentSortedVersusColumns,
			        valueObjectType,
			        baseSQL,
			        new Object[0],
			        new Type[0],
			        "FormaPagamento",
			        pdao.getSessionFactory()	,
			        session
			      );

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response insertRecords(int[] rowNumbers, ArrayList newValueObjects)
			throws Exception {
		try {
			for (PIVPaymentMethodModel forma_pagamento : ((ArrayList<PIVPaymentMethodModel>) newValueObjects)) {
				forma_pagamento.setAtivo(true);
				pdao.save(forma_pagamento);
			}
			return new VOListResponse(newValueObjects, false,newValueObjects.size());
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response updateRecords(int[] rowNumbers,
			ArrayList oldPersistentObjects, ArrayList persistentObjects)
			throws Exception {
		try {
			for (PIVPaymentMethodModel forma_pagamento : ((ArrayList<PIVPaymentMethodModel>) persistentObjects)) { pdao.update(forma_pagamento); }
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public Response deleteRecords(ArrayList persistentObjects) throws Exception {
		try {
			for (PIVPaymentMethodModel forma_pagamento : ((ArrayList<PIVPaymentMethodModel>) persistentObjects)) {
				forma_pagamento.setAtivo(false); pdao.update(forma_pagamento);
			}
			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}
	
	@Override
	  public boolean validateCell(int rowNumber,String attributeName,Object oldValue,Object newValue) {
	    if(attributeName=="forma") {
	    	String valor = (String) newValue;
	    	Session session = pdao.getSession();
	    	PIVPaymentMethodModel forma_pagamento = (PIVPaymentMethodModel) session.createQuery("from org.playiv.com.mvc.model.PIVPaymentMethodModel FormaPagamento where FormaPagamento.forma = '" + valor + "'").uniqueResult();
	    	session.close();
	    	if(forma_pagamento!=null) { // categoria com o mesmo nome encontrada
	    		if(forma_pagamento.isAtivo())
	    			JOptionPane.showMessageDialog(null,"J� existe uma forma de pagamento de nome " + forma_pagamento.getForma() + " ativo, por favor, escolha uma nome diferente.","Forma de pagamento j� existe",JOptionPane.ERROR_MESSAGE);
	    		else {
	    			int opt = JOptionPane.showConfirmDialog(null, "Existe uma forma de pagamento " + forma_pagamento.getForma() + " desativado, deseja ativa-lo?", "Forma de pagamento desativada", JOptionPane.YES_NO_OPTION);
	    			if(opt==JOptionPane.YES_OPTION) {
	    				forma_pagamento.setAtivo(true);
	    				pdao.update(forma_pagamento);
	    			}
	    		}
	    		return false;
	    	}	    	
	    }
	    return true;
	  }

	public Color getBackgroundColor(int row, String attributedName, Object value) {
		PIVPaymentMethodModel forma_pagamento = (PIVPaymentMethodModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (!forma_pagamento.isAtivo())
			return new Color(120, 100, 100);
		return super.getBackgroundColor(row, attributedName, value);
	}
	
}