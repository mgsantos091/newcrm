package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVDesktopFunc;
import org.playiv.com.library.function.PIVEventMngrFunc;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVContactModel;
import org.playiv.com.mvc.view.PIVMailContactGFView;

public class PIVMailContactGFController extends GridController implements
		GridDataLocator {

	private static final long serialVersionUID = 1L;
	private PIVMailContactGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVUserSession psessao = PIVUserSession.getInstance();

	private PIVClientModel cliente;
	
	private ArrayList<PIVContactModel> contatosSelecionados = new ArrayList<PIVContactModel>();
	
	private PIVBusRelMainCFController organizaCliente;

	public PIVMailContactGFController(PIVClientModel cliente) {
		grid = new PIVMailContactGFView(this);
		this.cliente = cliente;
		grid.getGrid().getTable().getGrid().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int rowNumber = ( (org.openswing.swing.table.client.Grid) e.getSource() ).getSelectedRow();
				PIVContactModel contato = (PIVContactModel) PIVMailContactGFController.this.grid.getGrid().getVOListTableModel().getObjectForRow(rowNumber);
				if(isContatoSelecionado(contato)) PIVMailContactGFController.this.contatosSelecionados.remove(contato);
				else PIVMailContactGFController.this.contatosSelecionados.add(contato);
				/*PIVContatoEmailGFController.this.grid.getGrid().reloadData();*/
			}
		});
	}
	
	public PIVMailContactGFController(PIVClientModel cliente, PIVBusRelMainCFController organizaCliente) {
		this(cliente);
		this.organizaCliente = organizaCliente;
	}

	@Override
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		
		try {
			
			int idcliente = cliente.getId();
			int idVendedor = psessao.getVendedorSessao().getId();
			String baseSQL = "from org.playiv.com.mvc.model.PIVContactModel as Contato where Contato.cliente.id = " + idcliente + "";
			Session session = pdao.getSession();

			Response res = HibernateUtils.getBlockFromQuery(
					action,
					startIndex,
					50, // block size...
					filteredColumns, currentSortedColumns,
					currentSortedVersusColumns, valueObjectType, baseSQL,
					new Object[0], new Type[0],
					"PIVContatoModel",
					pdao.getSessionFactory(), session);

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
		
	}
	
	@Override
	public void selectedCell(int rowNumber, int columnIndex, String attributedName, ValueObject persistentObject) {
		/*PIVContatoModel contato = (PIVContatoModel) this.grid.getGrid().getVOListTableModel().getObjectForRow(rowNumber);
		if(isContatoSelecionado(contato)) this.contatosSelecionados.remove(contato);
		else this.contatosSelecionados.add(contato);
		this.grid.getGrid().reloadData();*/
	}
	
	private boolean isContatoSelecionado(PIVContactModel contato) {
		for(PIVContactModel contatoAComprarar : this.contatosSelecionados)
			if(contatoAComprarar.equals(contato)) return true;
		return false;
	}
	
	@Override
	public Color getBackgroundColor(int row,String attributeName,Object value) {
		PIVContactModel contato = (PIVContactModel) this.grid.getGrid().getVOListTableModel().getObjectForRow(row);
		if(isContatoSelecionado(contato)) return new Color(14,14,225); /*ClientSettings.GRID_SELECTION_BACKGROUND;*/
		else return ClientSettings.GRID_CELL_BACKGROUND;
	}

	public ArrayList<PIVContactModel> getContatosSelecionados() {
		return contatosSelecionados;
	}
	
	public void enviarEmailAoCliente( ) {
		ArrayList<PIVContactModel> contatosSelecionados = getContatosSelecionados();
		
		String contatosEnviados = "";
		for(PIVContactModel contato : contatosSelecionados) {
			contatosEnviados += contato.getNome() + "[" + contato.getEmail() + "] , ";
		}
		
		if(contatosEnviados.length()>=0)
			contatosEnviados = contatosEnviados.substring(0, contatosEnviados.length()-3);
		
		PIVDesktopFunc.getInstance().enviarEmail(contatosSelecionados,cliente);
		try {
			grid.closeFrame();
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			PIVLogSettings.getInstance().error(e.getMessage(), e);
		}
		
		/*String nivelComercialDesc = ((organizaCliente != null) ? organizaCliente.getNivelDesc() : "N�O FOI POSS�VEL RECUPERAR");*/
		// registra evento do envio do e-mail
		PIVEventMngrFunc.getInstance().registerEvent(
				cliente,
				PIVEventMngrFunc.getCodEvento(PIVEventMngrFunc.evento.EMAIL),
				(organizaCliente==null?null:organizaCliente.getNivelcomercial()),
				PIVEventMngrFunc.evento.EMAIL,
				"Foi enviado um e-mail para os contatos - " + contatosEnviados + " - do cliente - " + cliente.getId().toString()
						+ "_"
						+ cliente.getRazaosocial()
						+ ".");
	}
	
}