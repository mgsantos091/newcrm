package org.playiv.com.mvc.controller;

import java.beans.PropertyVetoException;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.openswing.swing.form.client.Form;
import org.openswing.swing.form.client.FormController;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOResponse;
import org.openswing.swing.message.receive.java.ValueObject;
import org.openswing.swing.util.java.Consts;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.IPIVFormController;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.mvc.model.PIVUserModel;
import org.playiv.com.mvc.view.PIVUserMngtDFView;
import org.playiv.com.mvc.view.PIVUserMngtGFView;

public class PIVUserMngtDFController extends FormController implements IPIVFormController {

	private static final long serialVersionUID = 1L;

	private Integer pk;
	private PIVUserMngtGFView grid;
	private PIVUserMngtDFView frame;
	private PIVSellerMngtCFController organizaVendedor;
	private PIVDao pdao = PIVDao.getInstance();

	public PIVUserMngtDFController(PIVUserMngtGFView grid , Integer pk) {
		this.grid = grid;
		this.pk = pk;
		frame = new PIVUserMngtDFView(this, pk);
		MDIFrame.add(frame);
		if (pk != null) {
			frame.getPanel().setMode(Consts.EDIT);
			frame.getPanel().reload();
		} else {
			frame.getPanel().setMode(Consts.INSERT);
		}
	}
	
	public PIVUserMngtDFController( PIVSellerMngtCFController organizaVendedor ) {
		this(null,null);
		this.organizaVendedor = organizaVendedor;
	}
	
	/**
	 * This method must be overridden by the subclass to retrieve data and
	 * return the valorized value object.
	 * 
	 * @param valueObjectClass
	 *            value object class
	 * @return a VOResponse object if data loading is successfully completed, or
	 *         an ErrorResponse object if an error occours
	 */
	public Response loadData(Class valueObjectClass) {
		try {

			int row = grid.getGrid().getSelectedRow();

			if (row != -1) {
				PIVUserModel gridVO = (PIVUserModel) grid.getGrid()
						.getVOListTableModel().getObjectForRow(row);
				pk = gridVO.getId();
			}

			String baseSQL = "from Usuarios in class org.playiv.com.mvc.model.PIVUserModel where Usuarios.id = '"
					+ pk + "'";
			Session session = pdao.getSession(); // obtain a JDBC connection and
													// instantiate a new Session

			PIVUserModel vo = (PIVUserModel) session.createQuery(baseSQL)
					.uniqueResult();

			session.close();

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	@Override
	public boolean beforeSaveDataInInsert(Form form) {
		// realiza a checagem do nome do usu�rio
		PIVUserModel usuario = (PIVUserModel) form.getVOModel().getValueObject();
		String usuarioNome = usuario.getlogin();
		Session session = pdao.getSession();
		PIVUserModel usuarioCadastrado = (PIVUserModel) session.createQuery("from org.playiv.com.mvc.model.PIVUserModel as Usuario where Usuario.login = '" + usuarioNome + "'").uniqueResult();
		session.close();
		// usu�rio com login j� cadastrados
		if(usuarioCadastrado!=null) {
			JOptionPane.showMessageDialog(MDIFrame.getInstance(),"J� existe um usu�rio com este login cadastrado, por favor, digite um diferente.","Login j� cadastrado",JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Method called by the Form panel to insert new data.
	 * 
	 * @param newValueObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response insertRecord(ValueObject newPersistentObject)
			throws Exception {
		try {

			PIVUserModel vo = (PIVUserModel) newPersistentObject;
			
			pdao.save(vo);
			
			PIVSellerModel vendedor = null;
			if (vo.isUsuariovendedor()||this.organizaVendedor!=null) {
				vo.setUsuariovendedor(true);
				vendedor = new PIVSellerModel(vo);
				pdao.save(vendedor);
			}
			
			this.pk = vo.getId();

			/*if (vo.isUsuariovendedor()) {

				// relaciona o usu�rio com o vendedor ap�s fazer a inser��o do
				// mesmo
				vendedor.setUsuario(vo);
				pdao.update(vendedor);

			}*/

			return new VOResponse(vo);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to update existing data.
	 * 
	 * @param oldPersistentObject
	 *            original value object, previous to the changes
	 * @param persistentObject
	 *            value object to save
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response updateRecord(ValueObject oldPersistentObject,
			ValueObject persistentObject) throws Exception {
		try {
			PIVUserModel vo = (PIVUserModel) persistentObject;
			pdao.update(vo);

			return new VOResponse(persistentObject);
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Method called by the Form panel to delete existing data.
	 * 
	 * @param persistentObject
	 *            value object to delete
	 * @return an ErrorResponse value object in case of errors, VOResponse if
	 *         the operation is successfully completed
	 */
	public Response deleteRecord(ValueObject persistentObject) throws Exception {
		try {
			PIVUserModel vo = (PIVUserModel) persistentObject;

			Session session = pdao.getSession();
			session.beginTransaction();
			session.delete(vo);
			session.getTransaction().commit();
			session.close();

			return new VOResponse(new Boolean(true));
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	/**
	 * Callback method called when the Form mode is changed.
	 * 
	 * @param currentMode
	 *            current Form mode
	 */
	public void modeChanged(int currentMode) {
		frame.setEnableGridButtons(currentMode);
	}

	public PIVUserMngtGFView getGridFrame() {
		return this.grid;
	}

	public Integer getPk() {
		return pk;
	}
	
	@Override
	public void afterInsertData() {
		if (this.organizaVendedor != null) {
			organizaVendedor.carregar();
			try {
				this.frame.closeFrame();
			} catch (PropertyVetoException e) {
				PIVLogSettings.getInstance().error(e.getMessage(), e);
				e.printStackTrace();
			}
		}
	}

	public PIVUserMngtDFView getFrame() {
		return this.frame;
	}
	
}