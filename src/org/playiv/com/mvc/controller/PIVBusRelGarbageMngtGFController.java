package org.playiv.com.mvc.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;

import org.hibernate.Session;
import org.hibernate.type.Type;
import org.openswing.swing.mdi.client.MDIFrame;
import org.openswing.swing.message.receive.java.ErrorResponse;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;
import org.openswing.swing.util.client.ClientSettings;
import org.openswing.swing.util.server.HibernateUtils;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.general.PIVLogSettings;
import org.playiv.com.library.general.PIVUserSession;
import org.playiv.com.mvc.model.PIVBusRelModel;
import org.playiv.com.mvc.model.PIVGarbageClientModel;
import org.playiv.com.mvc.model.PIVUserModel;
import org.playiv.com.mvc.view.PIVBusRelGarbageMngtGFView;

public class PIVBusRelGarbageMngtGFController extends GridController implements GridDataLocator {

	private static final long serialVersionUID = 1L;

	private PIVBusRelGarbageMngtGFView grid = null;
	private PIVDao pdao = PIVDao.getInstance();

	private PIVUserSession psessao = PIVUserSession.getInstance();

	private PIVBusRelMngtGFController relComPrincipalController;

	/*private ArrayList<PIVBusRelModel> relacionamentoSelecionados = new ArrayList<PIVBusRelModel>();*/
	/*private ArrayList<PIVGarbageClientModel> relacionamentoSelecionados = new ArrayList<PIVGarbageClientModel>();*/

	public PIVBusRelGarbageMngtGFController() {
		grid = new PIVBusRelGarbageMngtGFView(this);
		/*grid.getGrid().getTable().getGrid()
				.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						int rowNumber = ((org.openswing.swing.table.client.Grid) e
								.getSource()).getSelectedRow();
						PIVBusRelModel relacionamento = (PIVBusRelModel) PIVBusRelGarbageMngtGFController.this.grid
								.getGrid().getVOListTableModel()
								.getObjectForRow(rowNumber);
						PIVGarbageClientModel relacionamento = (PIVGarbageClientModel) PIVBusRelGarbageMngtGFController.this.grid
								.getGrid().getVOListTableModel()
								.getObjectForRow(rowNumber);
						if (isRelacionamentoSelecionado(relacionamento))
							PIVBusRelGarbageMngtGFController.this.relacionamentoSelecionados
									.remove(relacionamento);
						else
							PIVBusRelGarbageMngtGFController.this.relacionamentoSelecionados
									.add(relacionamento);
						
						 * PIVContatoEmailGFController.this.grid.getGrid().
						 * reloadData();
						 
					}
				});*/
	}

	/*private boolean isRelacionamentoSelecionado(PIVBusRelModel relacionamento) {
		for (PIVBusRelModel relacionamentoAComprarar : this.relacionamentoSelecionados)
			if (relacionamentoAComprarar.equals(relacionamento))
				return true;
		return false;
	}*/

	public PIVBusRelGarbageMngtGFController(
			PIVBusRelMngtGFController relComPrincipalController) {
		this();
		this.relComPrincipalController = relComPrincipalController;
	}

	@Override
	public Response loadData(int action, int startIndex, Map filteredColumns,
			ArrayList currentSortedColumns,
			ArrayList currentSortedVersusColumns, Class valueObjectType,
			Map otherGridParams) {
		try {
			
			PIVUserModel usuario = psessao.getUsuarioSessao();
			
			String baseSQL;
			if(!usuario.isUsuariogerente()) {
			
			int idVendedor = psessao.getVendedorSessao().getId();
			/*baseSQL = "from RelCom in class org.playiv.com.mvc.model.PIVBusRelModel where RelCom.ativo = false and RelCom.vendedor.id = "
					+ idVendedor + "";*/
			baseSQL = "from RelCom in class org.playiv.com.mvc.model.PIVGarbageClientModel where RelCom.vendedor.id = "
					+ idVendedor + "";
			
			} else {
				
				/*baseSQL = "from RelCom in class org.playiv.com.mvc.model.PIVBusRelModel where RelCom.ativo = false";*/
				baseSQL = "from RelCom in class org.playiv.com.mvc.model.PIVGarbageClientModel";
				
			}
			
			Session session = pdao.getSession(); // obtain a JDBC connection and

			Response res = HibernateUtils.getBlockFromQuery(
					action,
					startIndex,
					50, // block size...
					filteredColumns, currentSortedColumns,
					currentSortedVersusColumns, valueObjectType, baseSQL,
					new Object[0], new Type[0],
					"RelCom",
					pdao.getSessionFactory(), session);// instantiate a new
														// Session

			session.close();

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();
			PIVLogSettings.getInstance().error(ex.getMessage(), ex);
			return new ErrorResponse(ex.getMessage());
		}
	}

	public PIVBusRelGarbageMngtGFView getGrid() {
		return this.grid;
	}

	@Override
	public Color getBackgroundColor(int row, String attributeName, Object value) {
		/*PIVBusRelModel relacionamento = (PIVBusRelModel) this.grid.getGrid()
				.getVOListTableModel().getObjectForRow(row);
		if (isRelacionamentoSelecionado(relacionamento))
			return new Color(14, 14, 225); 
											 * ClientSettings.
											 * GRID_SELECTION_BACKGROUND;
											 
		else*/
			return ClientSettings.GRID_CELL_BACKGROUND;
	}

	public void recuperarClientes() {
		
		int row = grid.getGrid().getTable().getSelectedRow();
		PIVGarbageClientModel relSel = (PIVGarbageClientModel) grid.getGrid().getVOListTableModel().getObjectForRow( row );
		pdao.delete(relSel);
		
		int idvendedor = relSel.getVendedor().getId();
		int idcliente = relSel.getCliente().getId();
		
		String sql = "from RelCom in class org.playiv.com.mvc.model.PIVBusRelModel where RelCom.ativo = false and RelCom.vendedor.id = "
				+ idvendedor + " and RelCom.cliente.id = " + idcliente;
		
		Session s = pdao.getSession();
		PIVBusRelModel relacionamento = (PIVBusRelModel) s.createQuery(sql).uniqueResult();
		s.close();
		
		relacionamento.setAtivo(true);
		pdao.update(relacionamento);
		
		JOptionPane.showMessageDialog(MDIFrame.getInstance(),
				"Cliente(s) restaurado(s) com sucesso", "",
				JOptionPane.INFORMATION_MESSAGE);
		
		if(relComPrincipalController!=null)
			relComPrincipalController.carregar();
		
		/*if (this.relacionamentoSelecionados.size() == 0) {
			JOptionPane
					.showMessageDialog(
							null,
							"Nenhum cliente foi selecionado, selecione um ou mais relacionamentos para completar esta a��o.",
							"Aten��o", JOptionPane.ERROR_MESSAGE);
			return;
		} else {
			for (PIVBusRelModel relacionamento : relacionamentoSelecionados) {
				
				relacionamento.setAtivo(true);
				pdao.update(relacionamento);
				
				String sql = "from x in class org.playiv.com.mvc.model.PIVGarbageClientModel where x.idvendedor = " + relacionamento.getVendedor().getId() + 
						"and x.idcliente = " + relacionamento.getCliente().getId();
				
				Session s = pdao.getSession();
				
				PIVGarbageClientModel relacionamentoLixo = (PIVGarbageClientModel) s.createQuery(sql).uniqueResult();
				
				s.close();
				
				pdao.delete(relacionamentoLixo);
				
			}
			JOptionPane.showMessageDialog(null,
					"Cliente(s) restaurado(s) com sucesso", "",
					JOptionPane.INFORMATION_MESSAGE);
			
			if(relComPrincipalController!=null)
				relComPrincipalController.carregar();
		}*/
	}

}