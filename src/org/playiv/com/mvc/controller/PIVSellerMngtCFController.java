package org.playiv.com.mvc.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVBusRelCommandSubjectFunc;
import org.playiv.com.library.function.PIVSellerInfoFunc;
import org.playiv.com.library.general.PIVSellerList;
import org.playiv.com.mvc.model.PIVSellerModel;
import org.playiv.com.swing.general.PIVSalesElementComponent;
import org.playiv.com.swing.general.PIVSellerPopUpMenu;

public class PIVSellerMngtCFController {

	private PIVDao pdao = PIVDao.getInstance();

	private JPanel painel;

	private ArrayList<PIVSellerList> lista_elementos = new ArrayList<PIVSellerList>();

	private PIVBusRelCommandSubjectFunc commandsSubj;

	private PIVSalesElementComponent elementoSelecionado;

	private JTabbedPane tab;

	private PIVSellerPopUpMenu popMenu;
	
	private PIVSellerInfoFunc gerInfoVendedor;

	// var de controle para adicionar o listener de mouse nos elementos
	// cadastrados
	private boolean addListenerElementos = false;

	public PIVSellerMngtCFController( JPanel painel, PIVBusRelCommandSubjectFunc commandsSubj ) {
		this.painel = painel;
		this.commandsSubj = commandsSubj;
	}

	public void add( PIVSellerModel vendedor ) {
		if (vendedor != null)
			adicionaElementoPainel( vendedor );
	}

	private void adicionaElementoPainel( PIVSellerModel vendedor ) {

		PIVSalesElementComponent elemento = new PIVSalesElementComponent( );
		elemento.setVendedor( vendedor );

		elemento.addMouseListener(new MouseAdapter() {

			private PIVSalesElementComponent elemento;
			@Override
			public void mouseExited(MouseEvent e) {
				PIVSellerMngtCFController.this.gerInfoVendedor.resetaPainel();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				elemento = (PIVSalesElementComponent) e.getComponent();
				PIVSellerModel vendedor = elemento.getVendedor();
				PIVSellerMngtCFController.this.gerInfoVendedor.atualizaPainel(vendedor);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				
				elemento = (PIVSalesElementComponent) e.getComponent();
				
				PIVSellerModel vendedor = elemento.getVendedor();
				System.out.println(elemento + " clicado");
				
				PIVSellerMngtCFController.this.setElementoSelecionado(elemento);
				PIVSellerMngtCFController.this.gerInfoVendedor
						.setLblClienteSelecionadoValor(vendedor.getId()
								.toString() + " - " + vendedor.getNome());
				
				/*if(e.getClickCount()>=2)
					new PIVSalesMngtGFController( PIVBusRelMainCFController.this , elemento.getCliente() );*/
				
			}
		});
	
		PIVSellerList lista_elemento = new PIVSellerList(vendedor,
				elemento);

		this.painel.add(elemento);
		this.lista_elementos.add(lista_elemento);

		setElementoSelecionado(elemento);
		
		/*if (this.popMenu != null)
			elemento.addMouseListener(new PIVBusRelClickPopUpMenuListener(
					popMenu));*/

		this.painel.repaint();

	}

	public void setElementoSelecionado(
			PIVSalesElementComponent elementoSelecionado) {
		
		this.elementoSelecionado = elementoSelecionado;
		String vendedorTitulo = null;
		
		if (elementoSelecionado != null) {
			PIVSellerModel vendedor = this.elementoSelecionado.getVendedor();
			vendedorTitulo = vendedor.getId().toString() + " - "
					+ vendedor.getNome();
		}
		
		this.commandsSubj.notifyObservers(elementoSelecionado);
		
	}

	private int getIndexListaElementoByCliente(PIVSellerModel vendedor) {
		Integer id = vendedor.getId();
		for (PIVSellerList lista_elemento : this.lista_elementos) {
			PIVSellerModel _vendedor = lista_elemento.getVendedor();
			if (_vendedor.getId() == id)
				return this.lista_elementos.indexOf(lista_elemento);
		}
		return -1;
	}

	public void setgerInfoVendedor(PIVSellerInfoFunc gerInfoVendedor) {
		this.gerInfoVendedor = gerInfoVendedor;
	}

/*	public void setPopMenu(PIVBusRelPopUpMenu popMenu) {
		this.popMenu = popMenu;
		if (!addListenerElementos) {
			for (PIVBusRelClientList lista_elemento : this.lista_elementos) {
				PIVBusRelElementComponent elemento = lista_elemento
						.getElemento();
				elemento.addMouseListener(new PIVBusRelClickPopUpMenuListener(
						popMenu));
			}
			addListenerElementos = true;
		}
	}*/

	public void carregar() {

		setElementoSelecionado(null);

		String baseSQL = "from org.playiv.com.mvc.model.PIVSellerModel";

		Session session = pdao.getSession(); // obtain a JDBC connection and
		ArrayList<PIVSellerModel> vendlist = (ArrayList<PIVSellerModel>) session
				.createQuery(baseSQL).list();
		session.close();
		
		if(this.lista_elementos.size()>=1) { // existem elementos pr�vios
			// apaga os existentes
			ArrayList<PIVSellerList> lista_elementos_clone = (ArrayList<PIVSellerList>) this.lista_elementos.clone();
			for(PIVSellerList elemento_lista : lista_elementos_clone) {
				PIVSalesElementComponent elemento = (PIVSalesElementComponent) elemento_lista.getElemento();
				removeElementoPainel(elemento);
			}
			this.lista_elementos = new ArrayList<PIVSellerList>();
		}
		
		if (vendlist != null && vendlist.size() > 0) {
			for (PIVSellerModel vendedor : vendlist) {
				adicionaElementoPainel( vendedor );
			}
		}

	}
	
	private void removeElementoPainel(PIVSalesElementComponent elemento) {
		
		this.painel.remove(elemento);
		
		PIVSellerList listaASerRemovida = null;
		for(PIVSellerList lista_elemento : this.lista_elementos) {
			if(lista_elemento.getElemento()==elemento) { listaASerRemovida = lista_elemento; break ; }
		}
		
		if(listaASerRemovida!=null)
			this.lista_elementos.remove(listaASerRemovida);
		
		this.painel.repaint();
		
	}

}