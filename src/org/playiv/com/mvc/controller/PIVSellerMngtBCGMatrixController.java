package org.playiv.com.mvc.controller;

import java.util.ArrayList;
import javax.swing.JPanel;
import org.hibernate.Session;
import org.playiv.com.dao.PIVDao;
import org.playiv.com.library.function.PIVBusRelCommandSubjectFunc;
import org.playiv.com.library.general.PIVBusRelClientList;
import org.playiv.com.mvc.model.PIVClientModel;
import org.playiv.com.mvc.model.PIVSalesTargetModel;
import org.playiv.com.swing.general.PIVBusRelElementComponent;

public class PIVSellerMngtBCGMatrixController {

	public enum nivel {
		ESTRELA , QUESTIONAMENTO , VACA_LEITEIRA , ABACAXI
	}

	private PIVDao pdao = PIVDao.getInstance();
	
	private JPanel jpClientesEstrela;
	private JPanel jpClientesQuest;
	private JPanel jpClientesVacaLeit;
	private JPanel jpClientesAbacaxi;

	private ArrayList<PIVBusRelClientList> listaClientesEstrela = new ArrayList<PIVBusRelClientList>();
	private ArrayList<PIVBusRelClientList> listaClientesQuest = new ArrayList<PIVBusRelClientList>();
	private ArrayList<PIVBusRelClientList> listaClientesVacaLeit = new ArrayList<PIVBusRelClientList>();
	private ArrayList<PIVBusRelClientList> listaClientesAbaxaxi = new ArrayList<PIVBusRelClientList>();

	public PIVSellerMngtBCGMatrixController(
			PIVBusRelCommandSubjectFunc commandsSubj ,
			JPanel jpClientesEstrela ,
			JPanel jpClientesQuest ,
			JPanel jpClientesVacaLeit ,
			JPanel jpClientesAbacaxi ) {
		this.jpClientesEstrela = jpClientesEstrela;
		this.jpClientesQuest = jpClientesQuest;
		this.jpClientesVacaLeit = jpClientesVacaLeit;
		this.jpClientesAbacaxi = jpClientesAbacaxi;
	}

	public void add( PIVClientModel cliente , nivel nivel ) {
		adicionaElementoPainel( cliente , nivel );
	}

	private void adicionaElementoPainel( PIVClientModel cliente , nivel nivel ) {

		PIVBusRelElementComponent elemento = new PIVBusRelElementComponent( null );
		elemento.setCliente(cliente);

		PIVBusRelClientList lista_elemento = new PIVBusRelClientList(cliente,
				elemento);

		if( nivel == nivel.ABACAXI ) {
			this.jpClientesAbacaxi.add(elemento);
			this.listaClientesAbaxaxi.add(lista_elemento);
			this.jpClientesAbacaxi.repaint();
		} else if ( nivel == nivel.ESTRELA ) {
			this.jpClientesEstrela.add(elemento);
			this.listaClientesEstrela.add(lista_elemento);
			this.jpClientesEstrela.repaint();
		}  else if ( nivel == nivel.QUESTIONAMENTO ) {
			this.jpClientesQuest.add(elemento);
			this.listaClientesQuest.add(lista_elemento);
			this.jpClientesQuest.repaint();
		} else if ( nivel == nivel.VACA_LEITEIRA ) {
			this.jpClientesVacaLeit.add(elemento);
			this.listaClientesVacaLeit.add(lista_elemento);
			this.jpClientesVacaLeit.repaint();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void carregar() {
		
		// Carrega os dados dos clientes ABACAXI
		
		Double vlrMetaAbacaxi	= 0d;
		
		Session s = pdao.getSession();
		String baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta = 4";
		PIVSalesTargetModel meta_abacaxi = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
		if(meta_abacaxi!=null) vlrMetaAbacaxi = meta_abacaxi.getValormeta();
		s.close();
				
		s = pdao.getSession();
		baseSQL = "" +
				"select Cliente " +
				"from org.playiv.com.mvc.model.PIVClientModel as Cliente " +
				"where Cliente.id in (" +
				
				"select VendaPai.cliente.id " +
				"from org.playiv.com.mvc.model.PIVSalesMainModel as VendaPai " +
				"group by VendaPai.cliente.id " +
				"having SUM(VendaPai.valortotal) <= " + vlrMetaAbacaxi + "" +
				
				")";
		ArrayList<PIVClientModel> clientes_abacaxi = (ArrayList<PIVClientModel>) s.createQuery(baseSQL).list();
		s.close();
		
		this.jpClientesAbacaxi.removeAll();
		
		this.listaClientesAbaxaxi = new ArrayList<PIVBusRelClientList>();
		
		for(PIVClientModel cliente : clientes_abacaxi)
			adicionaElementoPainel( cliente , nivel.ABACAXI );
		
		// Carrega os dados dos clientes VACA LEITEIRA
		
		Double vlrMetaVacaLeit	= 0d;
		
		s = pdao.getSession();
		baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta = 3";
		PIVSalesTargetModel meta_vacaleitera = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
		if(meta_vacaleitera!=null) vlrMetaVacaLeit = meta_vacaleitera.getValormeta();
		s.close();
		
		s = pdao.getSession();
		baseSQL = "" +
				"select Cliente " +
				"from org.playiv.com.mvc.model.PIVClientModel as Cliente " +
				"where Cliente.id in (" +
				
				"select VendaPai.cliente.id " +
				"from org.playiv.com.mvc.model.PIVSalesMainModel as VendaPai " +
				"group by VendaPai.cliente.id " +
				"having SUM(VendaPai.valortotal) <= " + vlrMetaVacaLeit + " and SUM(VendaPai.valortotal) >= " + vlrMetaAbacaxi +
				
				")";
		ArrayList<PIVClientModel> clientes_vacaleit = (ArrayList<PIVClientModel>) s.createQuery(baseSQL).list();
		s.close();
		
		this.jpClientesVacaLeit.removeAll();
		this.listaClientesVacaLeit = new ArrayList<PIVBusRelClientList>();
		
		for(PIVClientModel cliente : clientes_vacaleit)
			adicionaElementoPainel( cliente , nivel.VACA_LEITEIRA );

		// Carrega os dados dos clientes QUESTIONAMENTO
		
		Double vlrMetaQuest		= 0d;
		
		s = pdao.getSession();
		baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta = 2";
		PIVSalesTargetModel meta_questionamento = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
		if(meta_questionamento!=null) vlrMetaQuest = meta_questionamento.getValormeta();
		s.close();
		
		s = pdao.getSession();
		baseSQL = "" +
				"select Cliente " +
				"from org.playiv.com.mvc.model.PIVClientModel as Cliente " +
				"where Cliente.id in (" +
				
				"select VendaPai.cliente.id " +
				"from org.playiv.com.mvc.model.PIVSalesMainModel as VendaPai " +
				"group by VendaPai.cliente.id " +
				"having SUM(VendaPai.valortotal) <= " + vlrMetaQuest + " and SUM(VendaPai.valortotal) >= " + vlrMetaVacaLeit +
				
				")";
		ArrayList<PIVClientModel> clientes_quest = (ArrayList<PIVClientModel>) s.createQuery(baseSQL).list();
		s.close();
		
		this.jpClientesQuest.removeAll();
		this.listaClientesQuest = new ArrayList<PIVBusRelClientList>();

		for(PIVClientModel cliente : clientes_quest)
			adicionaElementoPainel( cliente , nivel.QUESTIONAMENTO );

		// Carrega os dados dos clientes ESTRELA
		
		/*Double vlrMetaEstrela = 0d;
		
		s = pdao.getSession();
		baseSQL = "from org.playiv.com.mvc.model.PIVSalesTargetModel as MetaComercial where idmeta = 1";
		PIVSalesTargetModel meta_estrela = (PIVSalesTargetModel) s.createQuery(baseSQL).uniqueResult();
		if(meta_estrela!=null) vlrMetaEstrela = meta_estrela.getValormeta();
		s.close();*/
		
		s = pdao.getSession();
		baseSQL = "" +
				"select Cliente " +
				"from org.playiv.com.mvc.model.PIVClientModel as Cliente " +
				"where Cliente.id in (" +
				
				"select VendaPai.cliente.id " +
				"from org.playiv.com.mvc.model.PIVSalesMainModel as VendaPai " +
				"group by VendaPai.cliente.id " +
				"having SUM(VendaPai.valortotal) > " + vlrMetaQuest + "" +
				
				")";
		ArrayList<PIVClientModel> clientes_estrela = (ArrayList<PIVClientModel>) s.createQuery(baseSQL).list();
		s.close();
		
		this.jpClientesEstrela.removeAll();
		this.listaClientesEstrela = new ArrayList<PIVBusRelClientList>();
		
		for(PIVClientModel cliente : clientes_estrela)
			adicionaElementoPainel( cliente , nivel.ESTRELA );
		
		/*
		
		Integer idvendedor = vendedorlogado.getId();
		String baseSQL = "from org.playiv.com.mvc.model.PIVBusRelModel as Relacionamento where Relacionamento.vendedor.id = '"
				+ idvendedor
				+ "' and Relacionamento.idnivelcomercial = '"
				+ getNivelCod(this.nivel_matriz)
				+ "' and Relacionamento.completado = false "
				+ " and Relacionamento.ativo = true ";

		Session session = pdao.getSession(); // obtain a JDBC connection and
		ArrayList<PIVBusRelModel> relcomercial = (ArrayList<PIVBusRelModel>) session
				.createQuery(baseSQL).list();
		session.close();

		if(this.lista_elementos.size()>=1) { // existem elementos pr�vios
			// apaga os existentes
			ArrayList<PIVBusRelClientList> lista_elementos_clone = (ArrayList<PIVBusRelClientList>) this.lista_elementos.clone();
			for(PIVBusRelClientList elemento_lista : lista_elementos_clone) {
				PIVBusRelElementComponent elemento = elemento_lista.getElemento();
				removeElementoPainel(elemento);
			}
			this.lista_elementos = new ArrayList<PIVBusRelClientList>();
		}
		
		if (relcomercial != null && relcomercial.size() > 0) {
			for (PIVBusRelModel relacionamento : relcomercial) {
				PIVClientModel cliente = relacionamento.getCliente();
				Integer nivelClassificao = relacionamento.getNivel_classificao();
				adicionaElementoPainel(cliente, false, nivelClassificao);
			}
		}*/

	}

	public Integer getNivelcomercial( nivel nivel ) {
		switch( nivel ) {
			case ESTRELA :
				return 1;
			case QUESTIONAMENTO :
				return 2;
			case VACA_LEITEIRA :
				return 3;
			case ABACAXI :
				return 4;
			default : return 0;
		}
	}
		
}